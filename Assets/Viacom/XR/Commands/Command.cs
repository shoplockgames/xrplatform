﻿// <copyright file="Command.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Provides a mechanism to encapsulate all information needed to perform an action or trigger an event.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Commands
{
    /// <summary>
    /// Represents the method that will handle the <c>Completed</c> event of a <c>Command</c> class.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    public delegate void CommandCompleteHandler(Command sender);

    /// <summary>
    /// Represents the method that will handle the <c>Canceled</c> event of a <c>Command</c> class.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    public delegate void CommandCanceledHandler(Command sender);

    /// <summary>
    /// Represents an unit of work/task to execute. 
    /// </summary>
    public abstract class Command
    {
        /// <summary>
        /// Occurs when a <c>Command</c> is completed.
        /// </summary>
        public event CommandCompleteHandler Completed;
        
        /// <summary>
        /// Occurs when a <c>Command</c> is canceled.
        /// </summary>
        public event CommandCompleteHandler Canceled;

        /// <summary>
        /// Initializes a new instance of the <c>Command</c> class.
        /// </summary>
        public Command()
        {
        }

        /// <summary>
        /// Executes the <c>Command</c>.
        /// </summary>
        public abstract void Execute();

        /// <summary>
        /// Callback method that is invoked when the <c>Command</c> completes.
        /// </summary>
        protected void OnComplete()
        {
            if (Completed != null)
            {
                Completed(this);
            }

            ClearEvents();
        }

        /// <summary>
        /// Cancels the <c>Command</c>.
        /// </summary>
        public virtual void Cancel()
        {
            if (Canceled != null)
            {
                Canceled(this);
            }

            ClearEvents();
        }

        /// <summary>
        /// Removes the delegates on the <c>Command</c>.
        /// </summary>
        private void ClearEvents()
        {
            Completed = null;
            Canceled = null;
        }
    }
}
