// <copyright file="CommandQueue.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a collection of commands that are executed in a first-in, first-out order. 
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Commands
{
    using System.Collections.Generic;
    
    /// <summary>
    /// Represents a collection of commands that are executed in a first-in, first-out order. 
    /// </summary>
    public class CommandQueue
    {
        protected Command _runningCommand = null;
        protected Queue<Command> _queue = new Queue<Command>();
        protected List<Command> _logList = new List<Command>();

        /// <summary>
        /// Gets whether or not a <c>Command</c> is currently running.
        /// </summary>
        public bool IsRunning
        {
            get { return _runningCommand != null; }
        }

        /// <summary>
        /// Cancels all commands currently in the <c>CommandQueue</c> and removes them.
        /// </summary>
        public void CancelAndClearCommands()
        {
            _queue.Clear();
            _logList.Clear();

            if (_runningCommand != null)
            {
                _runningCommand.Cancel();
                _runningCommand = null;
            }
        }

        /// <summary>
        /// Adds a <c>Command</c> to the front of the <c>CommandQueue</c>.
        /// </summary>
        /// <param name="command">The <c>Command</c> to be added to the front of the <c>CommandQueue</c>.</param>
        public void AddCommand(Command command)
        {
            command.Completed += StartOneCommand;
            command.Canceled += StartOneCommand;
            _queue.Enqueue(command);
        }

        /// <summary>
        /// Begins executing the commands in the <c>CommandQueue</c>.
        /// </summary>
        public void StartProcessingCommands()
        {
            if (_runningCommand == null)
            {
                StartOneCommand(null);
            }
        }

        /// <summary>
        /// Starts the next <c>Command</c> in the <c>CommandQueue</c> if one is available. 
        /// </summary>
        /// <param name="sender">The previous <c>Command</c> that either completed or was canceled.</param>
        protected virtual void StartOneCommand(Command sender)
        {
            if (sender != null)
            {
                sender.Completed -= StartOneCommand;
                sender.Canceled -= StartOneCommand;
            }

            if (_queue.Count != 0)
            {
                Command command = _queue.Dequeue();
                _runningCommand = command;
                _logList.Add(command);
                command.Execute();
            }
            else
            {
                _runningCommand = null;
            }
        }
    }
}
