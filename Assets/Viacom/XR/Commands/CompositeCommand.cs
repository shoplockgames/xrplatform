// <copyright file="CompositeCommand.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a command that executes sub-commands.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Commands
{
    using System.Collections.Generic;

    /// <summary>
    /// Represents a command that executes sub-commands.
    /// </summary>
    public abstract class CompositeCommand : Command
    {
        protected readonly List<Command> _subCommands = new List<Command>();
        protected int _terminatedCommandCount;

        /// <summary>
        /// Initializes a new instance of the <c>CompositeCommand</c> class with a specified list of sub-commands.
        /// </summary>
        /// <param name="subCommands">The sub-commands to execute.</param>
        public CompositeCommand(params Command[] subCommands)
        {
            _subCommands.AddRange(subCommands);
        }

        /// <summary>
        /// Adds a <c>Command</c> to the end of the list of sub-commands.
        /// </summary>
        /// <param name="command">The <c>Command</c> to the end of the list of sub-commands.</param>
        public void AddCommand(Command command)
        {
            _subCommands.Add(command);
        }

        /// <summary>
        /// Prepares the <c>CompositeCommand</c> for execution.
        /// </summary>
        /// <remarks>Override this method if you want special operations done prior to execution.</remarks>
        protected virtual void Prepare()
        {
        }

        /// <summary>
        /// Executes the sub-commands.
        /// </summary>
        protected virtual void ExecuteSubCommands()
        {
        }

        /// <inheritdoc />
        public sealed override void Execute()
        {
            Prepare();
            ExecuteSubCommands();
        }

        /// <summary>
        /// Callback method that is invoked when a sub-command finishes (either by running to completion or being canceled).
        /// </summary>
        /// <param name="sender">The <c>Command</c> that was terminated.</param>
        protected abstract void OnSubCommandTerminated(Command sender);

        /// <summary>
        /// Cancels this <c>Command</c> and all of its sub-commands.
        /// </summary>
        public override void Cancel()
        {
            if (_subCommands != null && _subCommands.Count >= 1)
            {
                for (int i = 0; i < _subCommands.Count; i++)
                {
                    Command subCommand = _subCommands[i];
                    subCommand.Completed -= OnSubCommandTerminated;
                    subCommand.Canceled -= OnSubCommandTerminated;
                    subCommand.Cancel();
                }
            }

            base.Cancel();
        }
    }
}
