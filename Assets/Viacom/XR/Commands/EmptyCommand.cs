﻿// <copyright file="EmptyCommand.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an empty command.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Commands
{
    /// <summary>
    /// Represents an empty command.
    /// </summary>
    public class EmptyCommand : Command
    {
        /// <summary>
        /// Initializes a new instance of the <c>EmptyCommand</c> class.
        /// </summary>
        public EmptyCommand()
        {
        }

        /// <inheritdoc />
        public sealed override void Execute()
        {
            OnComplete();
        }
    }
}