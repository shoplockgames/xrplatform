﻿// <copyright file="LogCommand.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a command to log a message.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Commands
{
    using Viacom.XR.Diagnostics;
    
    /// <summary>
    /// Represents a command to log a message.
    /// </summary>
    public class LogCommand : Command
    {
        protected string _message;

        /// <summary>
        /// Initializes a new instance of the <c>LogCommand</c> class with a specified log message.
        /// </summary>
        /// <param name="message">The message to log.</param>
        public LogCommand(string message)
        {
            _message = message;
        }

        /// <inheritdoc />
        public sealed override void Execute()
        {
            ApplicationLog.Log("Command", _message);
            OnComplete();
        }
    }
}
