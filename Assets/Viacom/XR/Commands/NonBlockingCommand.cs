﻿// <copyright file="SerialCommand.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a command that will not block a <c>CommandQueue</c> while it executes.
// </summary>
//
// <remarks>
// This command type is particularly useful if you want to execute some logic off of the main queue but still is tracked
// and can be and canceled properly.
// </remarks>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Commands
{
    /// <summary>
    /// Represents a <c>Command</c> that will not block a <c>CommandQueue</c> while it executes.
    /// </summary>
    public class NonBlockingCommand : ParallelCommand
    {
        protected Command _command;
        protected float _waitDuration;

        /// <summary>
        /// Initializes a new instance of the <c>NonBlockingCommand</c> class.
        /// </summary>
        public NonBlockingCommand()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <c>NonBlockingCommand</c> class with the specified <c>Command</c> to execute.
        /// </summary>
        /// <param name="command">The <c>Command</c> to execute.</param>
        /// <param name="waitDuration">An optional amount of time to wait before marking this <c>Command</c> as completed.</param>
        public NonBlockingCommand(Command command, float waitDuration = 0)
        {
            _command = command;
            _waitDuration = waitDuration;
        }

        /// <inheritdoc />
        protected override void Prepare()
        {
            _command = CommandForExecution();
            AddCommand(new SimpleCommand(() => { _command.Execute(); }));
            AddCommand(new WaitCommand(WaitDuration()));
        }

        /// <summary>
        /// Gets the amount of time to wait before marking this <c>NonBlockingCommand</c> as completed.
        /// </summary>
        /// <returns>The amount of time to wait before marking this <c>NonBlockingCommand</c> as completed.</returns>
        protected virtual float WaitDuration()
        {
            return _waitDuration;
        }

        /// <summary>
        /// Gets the <c>Command</c> that this <c>NonBlockingCommand</c> should execute.
        /// </summary>
        /// <returns>The <c>Command</c> that this <c>NonBlockingCommand</c> should execute.</returns>
        protected virtual Command CommandForExecution()
        {
            return _command;
        }

        /// <inheritdoc />
        public override void Cancel()
        {
            if (_command != null)
            {
                _command.Cancel();
            }
            base.Cancel();
        }
    }
}