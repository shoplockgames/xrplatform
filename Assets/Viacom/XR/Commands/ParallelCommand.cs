﻿// <copyright file="SerialCommand.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a command that executes sub-commands in parallel.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Commands
{
    /// <summary>
    /// Represents a command that executes sub-commands in parallel.
    /// </summary>
    public class ParallelCommand : CompositeCommand
    {
        /// <summary>
        /// Initializes a new instance of the <c>ParallelCommand</c> class with a specified list of sub-commands.
        /// </summary>
        /// <param name="subCommands">The sub-commands to execute.</param>
        public ParallelCommand(params Command[] subCommands) : base(subCommands)
        {
        }

        /// <inheritdoc />
        protected sealed override void ExecuteSubCommands()
        {
            if (_subCommands != null && _subCommands.Count >= 1)
            {
                // set the complete command count to zero
                _terminatedCommandCount = 0;

                for (int i = 0; i < _subCommands.Count; i++)
                {
                    Command command = _subCommands[i];

                    // listen for the complete event of a sub-command...
                    command.Completed += OnSubCommandTerminated;
                    command.Canceled += OnSubCommandTerminated;

                    // ...and start the sub-command
                    command.Execute();
                }
            }
            else
            {
                OnComplete();
            }
        }

        /// <inheritdoc />
        protected override void OnSubCommandTerminated(Command sender)
        {
            // stop listening for the complete event
            sender.Completed -= OnSubCommandTerminated;
            sender.Canceled -= OnSubCommandTerminated;

            // increment the complete command count
            _terminatedCommandCount++;

            // if all the commands are complete...
            if (_terminatedCommandCount == _subCommands.Count)
            {
                // ...then this parallel command is complete
                OnComplete();
            }
        }
    }
}
