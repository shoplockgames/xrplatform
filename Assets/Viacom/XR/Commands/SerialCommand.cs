﻿// <copyright file="SerialCommand.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a command that executes sub-commands one after another.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Commands
{
    /// <summary>
    /// Represents a command that executes sub-commands one after another.
    /// </summary>
    public class SerialCommand : CompositeCommand
    {
        /// <summary>
        /// Initializes a new instance of the <c>SerialCommand</c> class with a specified list of sub-commands.
        /// </summary>
        /// <param name="subCommands">The sub-commands to execute.</param>
        public SerialCommand(params Command[] subCommands) : base(subCommands)
        {
        }

        /// <inheritdoc />
        protected sealed override void ExecuteSubCommands()
        {
            //set the complete command count to zero
            _terminatedCommandCount = 0;

            if (_subCommands != null && _subCommands.Count >= 1)
            {
                Command command = _subCommands[0];

                // listen for the complete event of the sub-command...
                command.Completed += OnSubCommandTerminated;
                command.Canceled += OnSubCommandTerminated;

                //...and start the sub-command
                command.Execute();
            }
            else
            {
                OnComplete();
            }
        }

        /// <inheritdoc />
        protected override void OnSubCommandTerminated(Command sender)
        {
            //stop listening for the complete event
            sender.Completed -= OnSubCommandTerminated;
            sender.Canceled -= OnSubCommandTerminated;

            //increment the complete command count
            _terminatedCommandCount++;

            //if all the commands are complete...
            if (_terminatedCommandCount == _subCommands.Count)
            {
                //...then this serial command is complete
                OnComplete();
            }
            else
            {
                //...otherwise listen for the complete event of the next sub-command...
                Command nextCommand = _subCommands[_terminatedCommandCount];
                nextCommand.Completed += OnSubCommandTerminated;
                nextCommand.Canceled += OnSubCommandTerminated;

                //...and start the sub-command
                nextCommand.Execute();

            }
        }
    }
}
