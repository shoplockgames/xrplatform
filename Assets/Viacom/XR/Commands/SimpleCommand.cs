﻿// <copyright file="SimpleCommand.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a command that executes an action.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Commands
{
    using System;
    
    /// <summary>
    /// Represents a command that executes an action.
    /// </summary>
    public class SimpleCommand : Command
    {
        private Action _simpleMethod;

        /// <summary>
        /// Initializes a new instance of the <c>SimpleCommand</c> class with a specified <c>Action</c>.
        /// </summary>
        /// <param name="action">The <c>Action</c> to execute.</param>
        public SimpleCommand(Action action = null)
        {
            _simpleMethod = action;
        }

        /// <inheritdoc />
        public override void Execute()
        {
            if (_simpleMethod != null)
            {
                _simpleMethod();
            }

            OnComplete();
        }

        /// <inheritdoc />
        public override void Cancel()
        {
            _simpleMethod = null;
            base.Cancel();
        }
    }
}