﻿// <copyright file="WaitCommand.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Provides a mechanism to wait a specified number of seconds.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Commands
{
    using System.Collections;

    using Viacom.XR.Coroutines;

    using UnityEngine;
    
    /// <summary>
    /// Represents a <c>Command</c> that yields for a specified number of seconds.
    /// </summary>
    public class WaitCommand : Command
    {
        private readonly float _delay = 0f;
        private IEnumerator _coroutine;

        /// <summary>
        /// Initializes a new instance of the <c>WaitCommand</c> class with a specified number of seconds to wait.
        /// </summary>
        /// <param name="value">The number of seconds to wait.</param>
        public WaitCommand(float value)
        {
            _delay = value;
        }

        /// <inheritdoc />
        public override void Execute()
        {
            if (_delay <= 0f)
            {
                OnComplete();
            }
            else
            {
                _coroutine = this.WaitAndComplete();
                if (CoroutineRunner.IsInScene)
                {
                    CoroutineRunner.Instance.StartCoroutine(_coroutine);
                }
                else
                {
                    Debug.LogWarning("No CoroutineRunner in scene, WaitCommand completing immediately.");
                    OnComplete();
                }
            }
        }

        /// <summary>
        /// Waits for an amount of time and then marks this <c>Command</c> as completed.
        /// </summary>
        private IEnumerator WaitAndComplete()
        {
            yield return new WaitForSeconds(_delay);
            OnComplete();
        }

        /// <inheritdoc />
        public override void Cancel()
        {
            if (_coroutine != null && CoroutineRunner.IsInScene)
            {
                CoroutineRunner.Instance.StopCoroutine(_coroutine);
            }

            base.Cancel();
        }
    }
}
