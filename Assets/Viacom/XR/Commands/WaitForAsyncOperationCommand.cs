﻿// <copyright file="WaitForAsyncOperationCommand.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Provides a mechanism to wait for an asynchronous action to complete.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Commands
{
	using System;
	using System.Collections;
	
	using UnityEngine;
	
	using Viacom.XR.Coroutines;
	
	/// <summary>
	/// Represents a <c>Command</c> that yields while an asynchronous action completes.
	/// </summary>
	public class WaitForAsyncOperationCommand : Command
	{
		private readonly Func<AsyncOperation> _startAsyncOperationMethod;
		private AsyncOperation _asyncOperation;
		private Coroutine _waitingCoroutine;

		/// <summary>
		/// Initializes a new instance of the <c>WaitForAsyncOperationCommand</c> class with a specified function to start the asynchronous action.
		/// </summary>
		/// <param name="asyncOperation">A function to start the asynchronous action.</param>
		public WaitForAsyncOperationCommand(Func<AsyncOperation> asyncOperation)
		{
			_startAsyncOperationMethod = asyncOperation;
		}

		/// <inheritdoc />
		public override void Execute()
		{
			_waitingCoroutine = CoroutineRunner.Instance.StartCoroutine(WaitForAsyncOperation());
		}

		/// <summary>
		/// Waits for the <c>AsyncOperation</c> to complete.
		/// </summary>
		private IEnumerator WaitForAsyncOperation()
		{
			_asyncOperation = _startAsyncOperationMethod();

			while (!_asyncOperation.isDone)
			{
				yield return null;
			}

			OnComplete();
		}

		/// <inheritdoc />
		public override void Cancel()
		{
			base.Cancel();

			if (_waitingCoroutine != null && CoroutineRunner.Instance != null)
			{
				CoroutineRunner.Instance.StopCoroutine(_waitingCoroutine);
			}
		}
	}
}