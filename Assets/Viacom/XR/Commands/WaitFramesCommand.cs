﻿// <copyright file="WaitFramesCommand.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Provides a mechanism to wait a specified number of frames.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Commands
{
    using System.Collections;

    using Viacom.XR.Coroutines;
    
    /// <summary>
    /// Represents a <c>Command</c> that yields for a specified number of frames.
    /// </summary>
    public class WaitFramesCommand : Command
    {
        private readonly int _frames = 0;
        private IEnumerator _coroutine;

        /// <summary>
        /// Initializes a new instance of the <c>WaitFramesCommand</c> class with a specified number of frames to wait.
        /// </summary>
        /// <param name="frames">The number of frames to wait.</param>
        public WaitFramesCommand(int frames)
        {
            _frames = frames;
        }

        /// <inheritdoc />
        public override void Execute()
        {
            if (_frames == 0)
            {
                OnComplete();
            }
            else
            {
                _coroutine = WaitAndComplete();
                CoroutineRunner.Instance.StartCoroutine(_coroutine);
            }
        }

        /// <summary>
        /// Waits for a number of frames and then marks this <c>Command</c> as completed.
        /// </summary>
        private IEnumerator WaitAndComplete()
        {
            for (int i = 0; i < _frames; i++)
            {
                // wait one frame
                yield return null;
            }

            OnComplete();
        }

        /// <inheritdoc />
        public override void Cancel()
        {
            if (_coroutine != null && CoroutineRunner.IsInScene)
            {
                CoroutineRunner.Instance.StopCoroutine(_coroutine);
            }

            base.Cancel();
        }
    }
}
