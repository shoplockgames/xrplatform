﻿// <copyright file="WaitUntilConditionMetCommand.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Provides a mechanism to wait until a condition is fulfilled.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Commands
{
    using System;
    using System.Collections;

    using Viacom.XR.Coroutines;

    using UnityEngine;
    
    /// <summary>
    /// Represents a <c>Command</c> that yields until a condition is met.
    /// </summary>
    public class WaitUntilConditionMetCommand : Command
    {
        private readonly Func<Boolean> _satisfyingExitCondition;
        private Coroutine _currentRoutine;

        /// <summary>
        /// Initializes a new instance of the <c>WaitUntilConditionMetCommand</c> class with a specified function to test for a condition.
        /// </summary>
        /// <param name="predicate">A function to test for a condition.</param>
        public WaitUntilConditionMetCommand(Func<Boolean> predicate)
        {
            _satisfyingExitCondition = predicate;
        }

        /// <inheritdoc />
        public override void Execute()
        {
            _currentRoutine = CoroutineRunner.Instance.StartCoroutine(CheckForCondition());
        }

        /// <inheritdoc />
        public override void Cancel()
        {
            if (_currentRoutine != null)
            {
                if (CoroutineRunner.IsInScene)
                {
                    CoroutineRunner.Instance.StopCoroutine(_currentRoutine);
                }

                _currentRoutine = null;
            }

            base.Cancel();
        }

        /// <summary>
        /// Waits for the condition to become true to complete.
        /// </summary>
        private IEnumerator CheckForCondition()
        {
            while (!_satisfyingExitCondition())
            {
                yield return null;
            }

            OnComplete();
        }
    }
}