// <copyright file="Singleton.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// An implementation of the Singleton design pattern for Unity.
// </summary>
//
// <remarks>
// This should be used sparingly. DO NOT ABUSE THIS.
//
// Be aware this will not prevent a non-singleton constructor such as `T myT = new T();`
// To prevent that, add `protected T () {}` to your singleton class.
//
// This implementation is made as MonoBehaviour so it can leverage co-routines.
// </remarks>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Components
{
    using UnityEngine;

    using Viacom.XR.Diagnostics;

    /// <summary>
    /// Represents a single instance of a MonoBehaviour.
    /// </summary>
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;

        private static readonly object _lock = new object();

        /// <summary>
        /// Gets the single instance of this MonoBehaviour.
        /// </summary>
        public static T Instance
        {
            get
            {
                lock (_lock)
                {
                    if (_instance == null)
                    {

                        if (_isApplicationQuitting)
                        {
                            Debug.LogWarning("[#Singleton] Instance '" + typeof(T) +
                                             "' already destroyed on application quit." +
                                             " Won't create again - returning null.");
                            return null;
                        }

                        _instance = (T) FindObjectOfType(typeof(T));

                        if (!_instance)
                        {
                            // create an instance to avoid non-static fields getting wiped on scene change
                            Debug.Log("[#Singleton] No object of type : " + typeof(T) + " found in scene. Creating");
                            _instance = new GameObject(typeof(T).ToString()).AddComponent<T>();

                            // Give it a pretty name
                            _instance.name = "[singleton] " + typeof(T).ToString();

                            // Put it under a singleton holder
                            GameObject singletonHolder = GameObject.Find("SingletonHolder");
                            if (singletonHolder == null)
                            {
                                singletonHolder = new GameObject("[SingletonHolder]");
                                singletonHolder.name = "SingletonHolder";
                            }

                            _instance.transform.SetParent(singletonHolder.transform);
                            DontDestroyOnLoad(singletonHolder);
                        }
                        else
                        {
                            // instance already existed in scene, leave as is but mark do not destroy
                            DontDestroyOnLoad(_instance);
                        }


                        if (FindObjectsOfType(typeof(T)).Length > 1)
                        {
                            Debug.Log(typeof(T));
                            Debug.LogError("[Singleton] Something went really wrong " +
                                           " - there should never be more than 1 singleton!" +
                                           " Reopening the scene might fix it.");
                            return _instance;
                        }

                    }

                    return _instance;
                }
            }
        }

        private static bool _isApplicationQuitting = false;

        /// <remarks>
        /// When Unity quits, it destroys objects in a random order.
        /// In principle, a Singleton is only destroyed when application quits.
        /// If any script calls Instance after it have been destroyed,
        ///   it will create a buggy ghost object that will stay on the Editor scene
        ///   even after stopping playing the Application. Really bad!
        /// So, this was made to be sure we're not creating that buggy ghost object.
        /// </remarks>
        protected virtual void OnApplicationQuit()
        {
            _isApplicationQuitting = true;
        }

        protected virtual void Awake()
        {
            if (_instance && _instance != this)
            {
                ApplicationLog.Log("[SINGLETON]",
                    "Duplicate instance of " + GetType() + " found. Removing " + name + " and game object");
                Destroy(gameObject);
                return;
            }

            _instance = Instance; // force setting instance
        }

        /// <summary>
        /// Gets whether this <c>GameObject</c> is currently in the scene.
        /// </summary>
        public static bool IsInScene
        {
            get { return _instance; }
        }
    }
}