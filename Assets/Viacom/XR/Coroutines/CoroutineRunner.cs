﻿// <copyright file="CoroutineRunner.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Provides a mechanism for running a <c>Coroutine</c> from classes that do not derive from </c>MonoBehaviour<c>.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Coroutines
{
    using System;
    using System.Collections;
    
    using Viacom.XR.Components;
    
    using UnityEngine;
    
    /// <summary>
    /// Represents a mechanism for running a <c>Coroutine</c> from classes that do not derive from <c>MonoBehaviour</c>.
    /// </summary>
    public class CoroutineRunner : Singleton<CoroutineRunner>
    {
        /// <summary>
        /// Executes the given <c>Action</c> after a delay by using a <c>Coroutine</c>.
        /// </summary>
        /// <param name="action">The <c>Action</c> to execute.</param>
        /// <param name="delay">The amount of time to wait before executing <paramref name="action"/>.</param>
        /// <param name="monoBehaviour">An optional <c>MonoBehaviour</c> to run the <c>Coroutine</c> on.</param>
        public void DoAfterDelay(Action action, float delay, MonoBehaviour monoBehaviour = null)
        {
            if (monoBehaviour != null)
            {
                monoBehaviour.StartCoroutine(DoAfterDelayCoroutine(action, delay));
            }
            else
            {
                StartCoroutine(DoAfterDelayCoroutine(action, delay));
            }
        }

        /// <summary>
        /// Executes the given <c>Action</c> after a delay in realtime (ignoring Unity's scaled time) by using a <c>Coroutine</c>.
        /// </summary>
        /// <param name="action">The <c>Action</c> to execute.</param>
        /// <param name="delay">The amount of time to wait before executing <paramref name="action"/>.</param>
        /// <param name="monoBehaviour">An optional </c>MonoBehaviour<c> to run the <c>Coroutine</c> on.</param>
        public void DoAfterDelayRealtime(Action action, float delay, MonoBehaviour monoBehaviour = null)
        {
            if (monoBehaviour != null)
            {
                monoBehaviour.StartCoroutine(DoAfterDelayCoroutine(action, delay, true));
            }
            else
            {
                StartCoroutine(DoAfterDelayCoroutine(action, delay, true));
            }
        }

        /// <summary>
        /// Executes the given <c>Action</c> after the specified condition becomes true.
        /// </summary>
        /// <param name="action">The <c>Action</c> to execute.</param>
        /// <param name="predicate">A function to test for a condition.</param>
        /// <param name="timeout">An amount of time in seconds to wait before ceasing to check the condition.</param>
        /// <param name="onTimeout">An optional <c>Action</c> to execute upon time-out.</param>
        /// <param name="checkInterval">A optional frequency in seconds to check the condition.</param>
        /// <param name="monoBehaviour">An optional </c>MonoBehaviour<c> to run the <c>Coroutine</c> on.</param>
        public void DoAfterConditionBecomesTrue(Action action, Func<bool> predicate, float timeout = 0,
            Action onTimeout = null,
            float checkInterval = 0.5f, MonoBehaviour monoBehaviour = null)
        {
            if (monoBehaviour != null)
            {
                monoBehaviour.StartCoroutine(DoAfterConditionBecomesTrueCoroutine(action, predicate, timeout, onTimeout,
                    checkInterval));
            }
            else
            {
                StartCoroutine(
                    DoAfterConditionBecomesTrueCoroutine(action, predicate, timeout, onTimeout, checkInterval));
            }
        }

        /// <summary>
        /// Suspends the <c>Coroutine</c> execution for the given amount of seconds using non-scaled time.
        /// </summary>
        /// <param name="value">The amount of seconds to suspend execution.</param>
        public static IEnumerator WaitForRealTime(float value)
        {
            while (true)
            {
                float pauseEndTime = Time.realtimeSinceStartup + value;
                while (Time.realtimeSinceStartup < pauseEndTime)
                {
                    yield return 0;
                }
                break;
            }
        }

        /// <summary>
        /// Executes the given <c>Action</c> after the specified amount of seconds.
        /// </summary>
        /// <param name="action">The <c>Action</c> to execute.</param>
        /// <param name="delay">The amount of time to wait before executing <paramref name="action"/>.</param>
        /// <param name="useRealtime"><c>true</c> if time should be independent of Unity's time scale; otherwise <c>false</c>.</param>
        private IEnumerator DoAfterDelayCoroutine(Action action, float delay, bool useRealtime = false)
        {
            if (useRealtime)
            {
                // can not use WaitForSeconds when TimeScale is 0
                float pauseEndTime = Time.realtimeSinceStartup + delay;
                while (Time.realtimeSinceStartup < pauseEndTime)
                {
                    yield return 0;
                }
            }
            else
            {
                yield return new WaitForSeconds(delay);
            }

            if (action != null)
            {
                action();
            }
        }

        /// <summary>
        /// Executes the given <c>Action</c> after the specified condition becomes true.
        /// </summary>
        /// <param name="action">The <c>Action</c> to execute.</param>
        /// <param name="predicate">A function to test for a condition.</param>
        /// <param name="timeout">An amount of time in seconds to wait before ceasing to check the condition.</param>
        /// <param name="onTimeout">An optional <c>Action</c> to execute upon time-out.</param>
        /// <param name="checkInterval">A optional frequency in seconds to check the condition.</param>
        private IEnumerator DoAfterConditionBecomesTrueCoroutine(Action action, Func<bool> predicate, float timeout = 0,
            Action onTimeout = null, float checkInterval = 0.1f)
        {
            float startTime = Time.time;

            bool isConditionMet = false;
            while (!(isConditionMet = predicate()) && ((Time.time - startTime) < timeout || timeout <= 0))
            {
                yield return new WaitForSeconds(checkInterval);
            }

            if (isConditionMet)
            {
                action();
            }
            else if (onTimeout != null)
            {
                onTimeout();
            }
        }

        /// <summary>
        /// Starts a <c>Coroutine</c> that yields on a collection of <c>Coroutine</c> objects.
        /// </summary>
        /// <param name="behaviour">A <c>MonoBehaviour</c> to run the <paramref name="coroutines"/> on.</param>
        /// <param name="coroutines">A <c>Coroutine</c> that yields on the execution of <paramref name="coroutines"/>.</param>
        public static Coroutine WaitForAllToFinish(params Coroutine[] coroutines)
        {
            return Instance.StartCoroutine(_WaitForAllToFinish(coroutines));
        }

        /// <summary>
        /// Starts a <c>Coroutine</c> on the specified <c>MonoBehaviour</c> that yields on a collection of <c>Coroutine</c> objects.
        /// </summary>
        /// <param name="monoBehaviour">A <c>MonoBehaviour</c> to run the <paramref name="coroutines"/> on.</param>
        /// <param name="coroutines">A <c>Coroutine</c> that yields on the execution of <paramref name="coroutines"/>.</param>
        public static Coroutine WaitForAllToFinish(MonoBehaviour monoBehaviour, params Coroutine[] coroutines)
        {
            return monoBehaviour.StartCoroutine(_WaitForAllToFinish(coroutines));
        }
        
        /// <summary>
        /// Suspends the <c>Coroutine</c> execution until a list of <c>Coroutine</c> finishes.
        /// </summary>
        /// <param name="coroutines">An array of type <c>Coroutine</c> to execute.</param>
        private static IEnumerator _WaitForAllToFinish(params Coroutine[] coroutines)
        {
            for (int i = 0; i < coroutines.Length; i++)
            {
                if (coroutines[i] != null)
                {
                    yield return coroutines[i];
                }
            }
        }
    }
}