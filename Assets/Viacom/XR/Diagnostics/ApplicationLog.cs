﻿// <copyright file="ApplicationLog.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// A static wrapper for per-project custom loggers or a default logger.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Diagnostics
{
    using System;

    using UnityEngine;
    using UnityObject = UnityEngine.Object;

    /// <summary>
    /// Represents the application log.
    /// </summary>
    public static class ApplicationLog
    {
#if UNITY_2017_1_OR_NEWER
        private static ILogHandler _unityLogHandler = Debug.unityLogger.logHandler;
#else
        private static ILogHandler _unityLogHandler = Debug.logger.logHandler;
#endif
        /// <summary>
        /// Gets the current log handler.
        /// </summary>
        public static ILogHandler UnityLogHandler
        {
            get { return _unityLogHandler; }
        }

        static ApplicationLog()
        {
#if UNITY_2017_1_OR_NEWER
            Debug.unityLogger.logHandler = new DefaultLogger();
#else
            Debug.logger.logHandler = new DefaultLogger();
#endif
        }

        /// <summary>
        /// Initializes the application log with the specified log handler.
        /// </summary>
        /// <param name="newLogHandler">The log handler to use for the application log.</param>
        public static void Init(ILogHandler newLogHandler = null)
        {
            if (newLogHandler != null)
            {
#if UNITY_2017_1_OR_NEWER
                Debug.unityLogger.logHandler = newLogHandler;
#else
                Debug.logger.logHandler = newLogHandler;
#endif
            }
        }

        /// <summary>
        /// Logs a message to the application log.
        /// </summary>
        /// <param name="tag">The tag for this message.</param>
        /// <param name="message">The message to log</param>
        /// <param name="context">The object to which the message applies.</param>
        public static void Log(string tag, string message, UnityObject context = null)
        {
            Debug.Log(string.Format("[{0}] {1}", tag, message), context);
        }

        /// <summary>
        /// Logs a warning to the application log.
        /// </summary>
        /// <param name="tag">The tag for this message.</param>
        /// <param name="message">The message to log</param>
        /// <param name="context">The object to which the message applies.</param>
        public static void Warning(string tag, string message, UnityObject context = null)
        {
            Debug.LogWarning(string.Format("[{0}] {1}", tag, message), context);
        }

        /// <summary>
        /// Logs an error to the application log.
        /// </summary>
        /// <param name="tag">The tag for this message.</param>
        /// <param name="message">The message to log</param>
        /// <param name="context">The object to which the message applies.</param>
        public static void Error(string tag, string message, UnityObject context = null)
        {
            Debug.LogError(string.Format("[{0}] {1}", tag, message), context);
        }

        /// <summary>
        /// Represents a default logger that logs to unity console if in development mode or editor.
        /// </summary>
        public class DefaultLogger : ILogHandler
        {
            public virtual void LogFormat(LogType logType, UnityObject context, string format, params object[] args)
            {
#if DEVELOPMENT_BUILD || UNITY_EDITOR || FORCE_LOG
                ApplicationLog.UnityLogHandler.LogFormat(logType, context, format, args);
#endif
            }

            public virtual void LogException(Exception exception, UnityObject context)
            {
#if DEVELOPMENT_BUILD || UNITY_EDITOR || FORCE_LOG
                ApplicationLog.UnityLogHandler.LogException(exception, context);
#endif
            }
        }
    }
}
