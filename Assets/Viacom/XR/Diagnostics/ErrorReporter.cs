// <copyright file="ErrorReporter.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// A generic wrapper for error reporters. Use this to resgister error/crash reporters such as Crashlytics, Sentry, etc.
// By default a dummy reporter is added that will log all errors to the console.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Diagnostics
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    using UnityEngine;

    /// <summary>
    /// Represents a wrapper 
    /// </summary>
    public static class ErrorReporter
    {
        /// <summary>
        /// Various severity levels to report events with.
        /// </summary>
        public enum EventSeverityLevel
        {
            Fatal,
            Error,
            Warning,
            Info,
            Debug
        }

        /// <summary>
        /// Various severity levels to report breadcrumbs with.
        /// </summary>
        public enum BreadcrumbSeverityLevel
        {
            Critical,
            Error,
            Warning,
            Info,
            Debug
        }

        /// <summary>
        /// Various types to report breadcrumbs with that are
        /// used to denote breadcrumbs associated with particular
        /// actions taken in the application.
        /// </summary>
        public enum BreadcrumbType
        {
            Default,
            HTTP,
            Navigation,
            User
        }

        private static List<IErrorReporter> _dummyReporter = new List<IErrorReporter> {new DummyReporter()};
        private static List<IErrorReporter> _reporters = new List<IErrorReporter>();

        private static ReadOnlyCollection<IErrorReporter> _Reporters
        {
            get
            {
                if (_reporters.Count == 0)
                {
                    Debug.LogWarning("No Error Reporters set up, Using Dummy for logging.");
                    return new ReadOnlyCollection<IErrorReporter>(_dummyReporter);
                }

                return new ReadOnlyCollection<IErrorReporter>(_reporters);
            }
        }

        /// <summary>
        /// Add new error reporter.
        /// </summary>
        /// <param name="newReporter">Adds a new error reporter.</param>
        public static void AddNewReporter(IErrorReporter newReporter)
        {
#if UNITY_EDITOR
            Log("Adding reporter", newReporter.ToString());
#endif

            foreach (IErrorReporter existingReporter in _reporters)
            {
                if (existingReporter.GetType().Equals(newReporter.GetType()))
                {
                    Debug.LogWarning(string.Format("Trying to add a duplicated reporter of type {0}.",
                        newReporter.GetType()));
                    return;
                }
            }

            _reporters.Add(newReporter);
            Debug.Log(string.Format("Added new error reporter {0}, Total Number of Reporters {1}",
                newReporter.GetType(), _reporters.Count));
        }

        /// <summary>
        /// Logs the passed exception to the associated reporter.
        /// </summary>
        /// <param name="e">The exception to log.</param>
        public static void LogHandledException(Exception e)
        {
#if UNITY_EDITOR
            Log("Handling Exception", e.ToString());
#endif
            for (int i = 0; i < _Reporters.Count; i++)
            {
                _Reporters[i].LogHandledException(e);
            }
        }

        /// <summary>
        /// Logs the passed exception to the associated reporter with
        /// the specified fingerprint(s) for grouping purposes.
        /// </summary>
        /// <param name="e">The exception to log.</param>
        /// <param name="fingerprint">The fingerprint to group the exception by.</param>
        public static void LogHandledException(Exception e, params string[] fingerprint)
        {
#if UNITY_EDITOR
            Log("Handling Exception", e.ToString());
#endif
            for (int i = 0; i < _Reporters.Count; i++)
            {
                _Reporters[i].LogHandledException(e, fingerprint);
            }
        }

        /// <summary>
        /// Logs the passed exception to the associated reporter with
        /// the specified severity level.
        /// </summary>
        /// <param name="e">The exception to log.</param>
        /// <param name="eventSeverityLevel">The severity level to report the exception with.</param>
        public static void LogHandledException(Exception e, EventSeverityLevel eventSeverityLevel)
        {
#if UNITY_EDITOR
            Log("Handling Exception", e.ToString());
#endif
            for (int i = 0; i < _Reporters.Count; i++)
            {
                _Reporters[i].LogHandledException(e, eventSeverityLevel);
            }
        }

        /// <summary>
        /// Logs the passed exception to the associated reporter with
        /// the specified severity level and fingerprint(s) for grouping purposes.
        /// </summary>
        /// <param name="e">The exception to log.</param>
        /// <param name="eventSeverityLevel">The severity level to report the exception with.</param>
        /// <param name="fingerprint">The fingerprint to group the exception by.</param>
        public static void LogHandledException(Exception e, EventSeverityLevel eventSeverityLevel,
            params string[] fingerprint)
        {
#if UNITY_EDITOR
            Log("Handling Exception", e.ToString());
#endif
            for (int i = 0; i < _Reporters.Count; i++)
            {
                _Reporters[i].LogHandledException(e, eventSeverityLevel, fingerprint);
            }
        }

        /// <summary>
        /// Sets the maximum amount of breadcrumbs that the reporter
        /// will store in runtime memory at any given time.
        /// </summary>
        /// <param name="maxBreadcrumbs">The maximum amount of breadcrumbs to store simultaneously.</param>
        public static void SetMaxBreadcrumbs(int maxBreadcrumbs)
        {
#if UNITY_EDITOR
            Log("Setting Max Breadcrumbs", maxBreadcrumbs.ToString());
#endif
            for (int i = 0; i < _Reporters.Count; i++)
            {
                _Reporters[i].SetMaxBreadcrumbs(maxBreadcrumbs);
            }
        }

        /// <summary>
        /// Logs a breadcrumb with the reporter that will be 
        /// submitted alongside any events or errors.
        /// </summary>
        /// <param name="breadcrumb">The breadcrumb to log.</param>
        public static void LeaveBreadcrumb(string breadcrumb)
        {
#if UNITY_EDITOR
            Log("Leaving Breadcrumb", breadcrumb);
#endif
            for (int i = 0; i < _Reporters.Count; i++)
            {
                _Reporters[i].LeaveBreadcrumb(breadcrumb);
            }
        }

        /// <summary>
        /// Configures a breadcrumb with the supplied
        /// information and logs it with the reporter.
        /// </summary>
        /// <param name="category">The category tag for the log; the calling system
        /// or feature name is generally a good category tag.</param>
        /// <param name="breadcrumb">The breadcrumb to log.</param>
        /// <param name="severitylevel">The severity level of the situation causing
        /// the breadcrumb to be recorded.</param>
        /// <param name="type">The type of event being breadcrumbed.</param>
        /// <param name="data">Extra data to supply with the breadcrumb; all object
        /// values will be converted .ToString() before being passed to native code.</param>
        public static void LeaveBreadcrumb(string category,
            string breadcrumb,
            BreadcrumbSeverityLevel severitylevel = BreadcrumbSeverityLevel.Info,
            BreadcrumbType type = BreadcrumbType.Default,
            Dictionary<string, object> data = null)
        {
#if UNITY_EDITOR
            Log("Leaving Breadcrumb", breadcrumb);
#endif
            for (int i = 0; i < _Reporters.Count; i++)
            {
                _Reporters[i].LeaveBreadcrumb(category, breadcrumb, severitylevel, type, data);
            }
        }

        /// <summary>
        /// Sets the current end-user associated with the reporter.
        /// </summary>
        /// <param name="id">The user's ID.</param>
        /// <param name="email">The user's email.</param>
        /// <param name="username">The user's username.</param>
        /// <param name="extras">Arbitrary extra data to associate with the user.</param>
        public static void SetUser(string id, string email = null, string username = null,
            Dictionary<string, string> extras = null)
        {
#if UNITY_EDITOR
            Log("Setting User", string.Format("{0}\n{1}\n{2}", id, email, username));
#endif
            for (int i = 0; i < _Reporters.Count; i++)
            {
                _Reporters[i].SetUser(id, email, username, extras);
            }
        }

#if UNITY_EDITOR
        private static void Log(string prefix, string message, UnityEngine.Object context = null)
        {
            Debug.Log("<color=#E96856>[Error Reporter] " + prefix + "</color>\n" + message, context);
        }
#endif

        private class DummyReporter : IErrorReporter
        {
            private const string _REMINDER_MESSAGE = "Hey, you should set up an Error Reporter";

            public void LogHandledException(Exception e)
            {
                Debug.LogWarning(_REMINDER_MESSAGE);
            }

            public void LogHandledException(Exception e, params string[] fingerprint)
            {
                Debug.LogWarning(_REMINDER_MESSAGE);
            }

            public void LogHandledException(Exception e, EventSeverityLevel eventSeverityLevel)
            {
                Debug.LogWarning(_REMINDER_MESSAGE);
            }

            public void LogHandledException(Exception e, EventSeverityLevel eventSeverityLevel,
                params string[] fingerprint)
            {
                Debug.LogWarning(_REMINDER_MESSAGE);
            }

            public void SetMaxBreadcrumbs(int maxBreadcrumbs)
            {
                Debug.LogWarning(_REMINDER_MESSAGE);
            }

            public void LeaveBreadcrumb(string breadcrumb)
            {
                Debug.LogWarning(_REMINDER_MESSAGE);
                Debug.Log("[BREADCRUMB] " + breadcrumb);
            }

            public void LeaveBreadcrumb(string category,
                string breadcrumb,
                BreadcrumbSeverityLevel breadcrumbSeverityLevel,
                BreadcrumbType type,
                Dictionary<string, object> data)
            {
                Debug.LogWarning(_REMINDER_MESSAGE);
                Debug.Log("[BREADCRUMB] " + breadcrumb);
            }

            public void SetUser(string id, string email = null, string username = null,
                Dictionary<string, string> extras = null)
            {
                Debug.LogWarning(_REMINDER_MESSAGE);
                Debug.Log(string.Format("[USER] {0}, Email: {1}, Usermane: {2}, Extras: {3}", id, email, username,
                    extras.FormatForLogging()));
            }
        }
    }

    public interface IErrorReporter
    {
        /// <summary>
        /// Logs the passed exception to the associated reporter.
        /// </summary>
        /// <param name="e">The exception to log.</param>
        void LogHandledException(Exception e);

        /// <summary>
        /// Logs the passed exception to the associated reporter with
        /// the specified fingerprint(s) for grouping purposes.
        /// </summary>
        /// <param name="e">The exception to log.</param>
        /// <param name="fingerprint">The fingerprint to group the exception by.</param>
        void LogHandledException(Exception e, params string[] fingerprint);

        /// <summary>
        /// Logs the passed exception to the associated reporter with
        /// the specified severity level.
        /// </summary>
        /// <param name="e">The exception to log.</param>
        /// <param name="eventSeverityLevel">The severity level to report the exception with.</param>
        void LogHandledException(Exception e, ErrorReporter.EventSeverityLevel eventSeverityLevel);

        /// <summary>
        /// Logs the passed exception to the associated reporter with
        /// the specified severity level and fingerprint(s) for grouping purposes.
        /// </summary>
        /// <param name="e">The exception to log.</param>
        /// <param name="eventSeverityLevel">The severity level to report the exception with.</param>
        /// <param name="fingerprint">The fingerprint to group the exception by.</param>
        void LogHandledException(Exception e, ErrorReporter.EventSeverityLevel eventSeverityLevel,
            params string[] fingerprint);

        /// <summary>
        /// Sets the maximum amount of breadcrumbs that the reporter
        /// will store in runtime memory at any given time.
        /// </summary>
        /// <param name="maxBreadcrumbs">The maximum amount of breadcrumbs to store simultaneously.</param>
        void SetMaxBreadcrumbs(int maxBreadcrumbs);

        /// <summary>
        /// Logs a breadcrumb with the reporter that will be 
        /// submitted alongside any events or errors.
        /// </summary>
        /// <param name="breadcrumb">The breadcrumb to log.</param>
        void LeaveBreadcrumb(string breadcrumb);

        /// <summary>
        /// Configures a breadcrumb with the supplied
        /// information and logs it with the reporter.
        /// </summary>
        /// <param name="category">The category tag for the log; the calling system
        /// or feature name is generally a good category tag.</param>
        /// <param name="breadcrumb">The breadcrumb to log.</param>
        /// <param name="breadcrumbSeverityLevel">The severity level of the situation causing
        /// the breadcrumb to be recorded.</param>
        /// <param name="type">The type of event being breadcrumbed.</param>
        /// <param name="data">Extra data to supply with the breadcrumb; all object
        /// values will be converted .ToString() before being passed to native code.</param>
        void LeaveBreadcrumb(string category,
            string breadcrumb,
            ErrorReporter.BreadcrumbSeverityLevel breadcrumbSeverityLevel = ErrorReporter.BreadcrumbSeverityLevel.Info,
            ErrorReporter.BreadcrumbType type = ErrorReporter.BreadcrumbType.Default,
            Dictionary<string, object> data = null);

        /// <summary>
        /// Sets the current end-user associated with the reporter.
        /// </summary>
        /// <param name="id">The user's ID.</param>
        /// <param name="email">The user's email.</param>
        /// <param name="username">The user's username.</param>
        /// <param name="extras">Arbitrary extra data to associate with the user.</param>
        void SetUser(string id, string email = null, string username = null, Dictionary<string, string> extras = null);
    }
}
