// <copyright file="FpsDisplay.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
//  A simple FPS display.
//
//  <code>
/// FpsDisplay.Init();
/// </code>
//
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Diagnostics
{
    using UnityEngine;

    using Viacom.XR.Components;

    /// <summary>
    /// Represents a simple display for the FPS of the currently running application.
    /// </summary>
    public class FpsDisplay : Singleton<FpsDisplay>
    {
        public float _updateInterval = 0.5F;
        public bool fpsDisplayEnabled = true;

        // FPS accumulated over the interval
        private float _accum = 0;

        // Frames drawn over the interval
        private int _frames = 0;

        // Left time for current interval
        private float _timeleft;

        // for display
        private float _fps = 0.0F;

        // guiStyle
        private GUIStyle _guiStyle = new GUIStyle();

        private GUIStyle _additionalTextStyle;
        private Rect _guiRect;

        //Fps Text
        private string _fpsText;
        private string _additionalText = string.Empty;

        /// <summary>
        /// Get the current frames per second of the application.
        /// </summary>
        public static float Fps
        {
            get { return Instance._fps; }
        }

        /// <summary>
        /// Gets or sets any additional text to display next to the FPS.
        /// </summary>
        public static string AdditionalText
        {
            get { return Instance._additionalText; }
            set { Instance._additionalText = value; }
        }

        private void Start()
        {
            _timeleft = _updateInterval;

            int w = Screen.width, h = Screen.height;
            _guiRect = new Rect(10, 0, 80, h);
            _guiStyle.alignment = TextAnchor.UpperLeft;
            _guiStyle.fontSize = 14;
            _guiStyle.fontStyle = FontStyle.Bold;

            _additionalTextStyle = new GUIStyle(_guiStyle);
            _additionalTextStyle.normal.textColor = Color.cyan;
        }

        private void Update()
        {
            if (ShouldToggleDisplay())
            {
                fpsDisplayEnabled = !fpsDisplayEnabled;
            }

            if (fpsDisplayEnabled)
            {
                _timeleft -= Time.deltaTime;
                _accum += Time.timeScale / Time.deltaTime;
                ++_frames;

                // Interval ended - update GUI text and start new interval
                if (_timeleft <= 0.0)
                {
                    // display two fractional digits (f2 format)
                    _fps = _accum / _frames;
                    _timeleft = _updateInterval;
                    _accum = 0.0F;
                    _frames = 0;
                    _fpsText = string.Format("{0:F2} FPS", _fps);
                }
            }
        }

#if DEVELOPMENT_BUILD || UNITY_EDITOR
        private void OnGUI()
        {
            if (fpsDisplayEnabled)
            {
                if (_fps < 30)
                {
                    if (_fps < 10)
                    {
                        _guiStyle.normal.textColor = Color.red;
                    }
                    else
                    {
                        _guiStyle.normal.textColor = Color.yellow;
                    }
                }
                else
                {
                    _guiStyle.normal.textColor = Color.green;
                }

                GUI.Label(_guiRect, _fpsText, _guiStyle);
                if (!string.IsNullOrEmpty(_additionalText))
                {
                    Rect additionalTextRect = _guiRect;
                    additionalTextRect.y += additionalTextRect.y;
                    additionalTextRect.x += additionalTextRect.width;
                    GUI.Label(additionalTextRect, _additionalText, _additionalTextStyle);
                }
            }
        }
#endif

        /// <summary>
        /// Initializes the FPS display game object.
        /// </summary>
        /// <param name="additionalText">Any additional text to display next to the FPS.</param>
        public static void Init(string additionalText = "")
        {
#if DEVELOPMENT_BUILD || UNITY_EDITOR
            Instance._additionalText = additionalText;
#endif
        }

        protected virtual bool ShouldToggleDisplay()
        {
            return (Input.touchCount == 2 && Input.touches[0].phase == TouchPhase.Began) || Input.GetKeyDown(KeyCode.H);
        }
    }
}
