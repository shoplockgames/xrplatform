// <copyright file="LoggingUtilities.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Provides utility functions for formatting collections into strings.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Diagnostics
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Provides utility functions for formatting collections into strings.
    /// </summary>
    public static class LoggingUtilities
    {
        /// <summary>
        /// Converts the provided <c>IList</c> to a comma-delimited string for logging.
        /// </summary>
        /// <param name="list">The <c>IList</c> to convert.</param>
        /// <returns>A comma-delimited string representation of <paramref name="list"/>.</returns>
        public static string FormatForLogging<T>(this IList<T> list)
        {
            string joinedList = string.Join(", ", list.Select(item => item.ToString()).ToArray());
            return "[ " + joinedList + " ]";
        }

        /// <summary>
        /// Converts the provided <c>IDictionary</c> to a comma-delimited string for logging.
        /// </summary>
        /// <param name="dict">The <c>IDictionary</c> to convert.</param>
        /// <returns>A comma-delimited string representation of <paramref name="dict"/>.</returns>
        public static string FormatForLogging<TKey, TValue>(this IDictionary<TKey, TValue> dict)
        {
            string joinedDict =
                string.Join(", ", dict.Keys.Select(key => "{ " + key + ", " + dict[key] + " }").ToArray());
            return "{ " + joinedDict + " }";
        }

        /// <summary>
        /// Converts the provided <c>HashSet</c> to a comma-delimited string for logging.
        /// </summary>
        /// <param name="set">The <c>HashSet</c> to convert.</param>
        /// <returns>A comma-delimited string representation of <paramref name="set"/>.</returns>
        public static string FormatForLogging<T>(this HashSet<T> set)
        {
            string joinedSet = string.Join(", ", set.Select(item => item.ToString()).ToArray());
            return "{ " + joinedSet + " }";
        }
    }
}
