﻿// <copyright file="StopwatchUtilities.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Provides utility functions for managing stopwatches.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Diagnostics
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    using UnityEngine;

    /// <summary>
    /// Provides utility functions for managing stopwatches.
    /// </summary>
    public static class StopwatchUtilities
    {
        private static readonly string LogTag = typeof(StopwatchUtilities).Name;
        private static int _timerCount = 0;
        private static readonly Dictionary<string, Stopwatch> _timers = new Dictionary<string, Stopwatch>();

        /// <summary>
        /// Starts a <c>Stopwatch</c> with a specified identifier.
        /// </summary>
        /// <param name="stopwatchId">The identifier for the <c>Stopwatch</c>.</param>
        public static void StartStopwatch(string stopwatchId)
        {
            if (_timers.ContainsKey(stopwatchId))
            {
                ApplicationLog.Log(LogTag, "Stopwatch with id " + stopwatchId + " already exists");
                return;
            }

            Stopwatch newStopwatch = new Stopwatch();
            ApplicationLog.Log(LogTag, stopwatchId + " started at " + Time.realtimeSinceStartup);
            newStopwatch.Start();
            _timerCount++;
            _timers.Add(stopwatchId, Stopwatch.StartNew());
        }

        /// <summary>
        /// Starts a <c>Stopwatch</c>.
        /// </summary>
        /// <returns>The identifier for the new <c>Stopwatch</c>.</returns>
        public static string StartStopwatch()
        {
            string newId = _timerCount.ToString();
            StartStopwatch(newId);
            return newId;
        }

        /// <summary>
        /// Logs the elapsed time in milliseconds of a <c>Stopwatch</c>.
        /// </summary>
        /// <param name="stopwatchId">The identifier for the <c>Stopwatch</c>.</param>
        /// <param name="message">Any additional information to log.</param>
        public static void LogStopwatch(string stopwatchId, string message = "")
        {
            Stopwatch stopwatch;
            if (!_timers.TryGetValue(stopwatchId, out stopwatch))
            {
                ApplicationLog.Log(LogTag, "No stopwatch with id " + stopwatchId);
                return;
            }

            ApplicationLog.Log(LogTag,
                string.Format("{0} has run for {1}ms", stopwatchId, stopwatch.ElapsedMilliseconds) + " " + message);
        }

        /// <summary>
        /// Stops a <c>Stopwatch</c>.
        /// </summary>
        /// <param name="stopwatchId">The identifier for the <c>Stopwatch</c>.</param>
        /// <param name="elapsedTime">When this method returns, contains the amount of time elapsed if the Stopwatch exists, or 0 if the <c>Stopwatch</c> does not exits.</param>
        public static void StopStopwatch(string stopwatchId, out TimeSpan elapsedTime)
        {
            Stopwatch stopwatch;

            if (!StopStopwatch(stopwatchId, out stopwatch))
            {
                elapsedTime = TimeSpan.Zero;
                return;
            }

            elapsedTime = stopwatch.Elapsed;
        }

        /// <summary>
        /// Stops a <c>Stopwatch</c>.
        /// </summary>
        /// <param name="stopwatchId">The identifier for the Stopwatch.</param>
        /// <param name="stopwatch">When this method returns, contains the <c>Stopwatch</c> that was stopped if it exists.</param>
        /// <returns>true if the <c>Stopwatch</c> was stopped successfully; otherwise, false.</returns>
        private static bool StopStopwatch(string stopwatchId, out Stopwatch stopwatch)
        {
            if (!_timers.TryGetValue(stopwatchId, out stopwatch))
            {
                ApplicationLog.Log(LogTag, "No stopwatch with ID " + stopwatchId);
                return false;
            }

            stopwatch.Stop();
            ApplicationLog.Log(LogTag,
                string.Format("{0} stopped after {1}ms (at {2})", stopwatchId, stopwatch.ElapsedMilliseconds,
                    Time.realtimeSinceStartup));
            _timers.Remove(stopwatchId);
            return true;
        }
    }
}
