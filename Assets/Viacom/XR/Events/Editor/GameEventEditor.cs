﻿// <copyright file="GameEvent.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Provides a custom editor for <see cref="GameEvent"/>.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Events
{
    using UnityEditor;
    using UnityEngine;

    /// <summary>
    /// Provides a custom editor for <see cref="GameEvent"/>.
    /// </summary>
    [CustomEditor(typeof(GameEvent))]
    public class GameEventEditor : Editor
    {
        private const string ButtonText = "Raise";

        /// <inheritdoc cref="Editor.OnInspectorGUI"/>
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUI.enabled = Application.isPlaying;

            GameEvent e = target as GameEvent;
            if (GUILayout.Button(ButtonText))
            {
                e.Raise();
            }
        }
    }
}
