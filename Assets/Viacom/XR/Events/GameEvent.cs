﻿// <copyright file="GameEvent.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Provides a mechanism to store an event via a <see cref="ScriptableObject"/>.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Events
{
    using System.Collections.Generic;

    using UnityEngine;

    /// <summary>
    /// Represents an event that can be listened to by class that implement <see cref="IGameEventListener"/>
    /// </summary>
    [CreateAssetMenu(menuName = "VXR/Game Event")]
    public class GameEvent : ScriptableObject
    {
        private readonly List<IGameEventListener> _eventListeners = new List<IGameEventListener>();

        /// <summary>
        /// Alerts all listeners on this <see cref="GameEvent"/>.
        /// </summary>
        public void Raise()
        {
            for (int i = _eventListeners.Count - 1; i >= 0; i--)
            {
                _eventListeners[i].OnEventRaised();
            }
        }

        /// <summary>
        /// Registers a listener to the <see cref="GameEvent"/>.
        /// </summary>
        /// <param name="listener">The <see cref="IGameEventListener"/> to register.</param>
        public void RegisterListener(IGameEventListener listener)
        {
            if (!_eventListeners.Contains(listener))
            {
                _eventListeners.Add(listener);
            }
        }

        /// <summary>
        /// Unregisters a listener from the <see cref="GameEvent"/>.
        /// </summary>
        /// <param name="listener">The <see cref="IGameEventListener"/> to unregister.</param>
        public void UnregisterListener(IGameEventListener listener)
        {
            if (_eventListeners.Contains(listener))
            {
                _eventListeners.Remove(listener);
            }
        }
    }
}