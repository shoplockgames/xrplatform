﻿// <copyright file="GameEventListener.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an object that can listen for a <see cref="GameEvent"/>.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Events
{
    using UnityEngine;
    using UnityEngine.Events;
    
    /// <summary>
    /// Represents an object that can listen for a <see cref="GameEvent"/>.
    /// </summary>
    public interface IGameEventListener
    {
        /// <summary>
        /// Invoked when the <see cref="GameEvent"/> this object is listening to is raised.
        /// </summary>
        void OnEventRaised();
    }

    /// <summary>
    /// Represents a <see cref="MonoBehaviour"/> that listens for a <see cref="GameEvent"/>.
    /// </summary>
    public class GameEventListener : MonoBehaviour, IGameEventListener
    {
        [Tooltip("Event to register to.")]
        [SerializeField]
        public GameEvent gameEvent;

        [Tooltip("Response to invoke when Event is raised.")]
        [SerializeField]
        public UnityEvent response;

        /// <summary>
        /// Invoked when this <see cref="MonoBehaviour"/> is enabled.
        /// </summary>
        private void OnEnable()
        {
            gameEvent.RegisterListener(this);
        }

        /// <summary>
        /// Invoked when this <see cref="MonoBehaviour"/> is disabled.
        /// </summary>
        private void OnDisable()
        {
            gameEvent.UnregisterListener(this);
        }

        /// <summary>
        /// Invoked when the <see cref="GameEvent"/> this object is listening to is raised.
        /// </summary>
        public void OnEventRaised()
        {
            response.Invoke();
        }
    }
}