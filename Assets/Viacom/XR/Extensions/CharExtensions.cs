﻿// <copyright file="CharExtensions.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Contains extension methods for characters.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR
{
	/// <summary>
	/// Contains extension methods for characters.
	/// </summary>
	public static class CharExtensions
	{
		/// <summary>
		/// Gets a value indicating whether or not the character is a valid hexadecimal character.
		/// </summary>
		/// <param name="c">The character to evaluate.</param>
		/// <returns><c>true</c> if <paramref name="c"/> is a valid hexadecimal character; otherwise, <c>false</c>.</returns>
		public static bool IsHexadecimal(this char c)
		{
			return (c >= '0' && c <= '9') ||
			       (c >= 'a' && c <= 'f') ||
			       (c >= 'A' && c <= 'F');
		}

		/// <summary>
		/// Gets a value indicating whether or not the character is a valid ASCII letter.
		/// </summary>
		/// <param name="c">The character to evaluate.</param>
		/// <returns><c>true</c> if <paramref name="c"/> is a valid ASCII letter; otherwise, <c>false</c>.</returns>
		public static bool IsAsciiLetter(this char c)
		{
			return (c >= 'A' && c <= 'Z') ||
			       (c >= 'a' && c <= 'z');
		}

		/// <summary>
		/// Gets a value indicating whether or not the character is a valid ASCII letter or digit.
		/// </summary>
		/// <param name="c">The character to evaluate.</param>
		/// <returns><c>true</c> if <paramref name="c"/> is a valid ASCII letter or digit; otherwise, <c>false</c>.</returns>
		public static bool IsAsciiLetterOrDigit(this char c)
		{
			return IsAsciiLetter(c) || char.IsDigit(c);
		}
	}
}
