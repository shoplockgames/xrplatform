﻿// <copyright file="CollectionsExtensions.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Contains extension methods for collections.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Contains extension methods for collections.
    /// </summary>
    public static class CollectionsExtensions
    {
        /// <summary>
        /// Adds an element to the specified <see cref="ICollection{T}"/> if it is not already present and returns a
        /// value that indicates whether the element was added.
        /// </summary>
        /// <param name="collection">The <see cref="ICollection{T}"/> to add the element to.</param>
        /// <param name="item">The element to add.</param>
        /// <typeparam name="T">The type of elements in the list.</typeparam>
        /// <returns><c>true</c> if <paramref name="item"/> was added to <paramref name="collection"/>; otherwise, <c>false</c>.</returns>
        public static bool TryAddUnique<T>(this ICollection<T> collection, T item)
        {
            if (collection.Contains(item))
            {
                return false;
            }

            collection.Add(item);
            return true;
        }

        /// <summary>
        /// Adds the specified key and value to the specified <see cref="IDictionary{TKey,TValue}"/> if it is not
        /// already present and returns a value that indicates whether the key and value were added.
        /// </summary>
        /// <param name="dictionary">The <see cref="IDictionary{TKey,TValue}"/> to add the key and value to.</param>
        /// <param name="key">The key of the element to add.</param>
        /// <param name="value">The value of the element to add. The value can be <c>null</c> for reference types.</param>
        /// <typeparam name="TKey">The type of the keys in the dictionary.</typeparam>
        /// <typeparam name="TValue">The type of the values in the dictionary.</typeparam>
        /// <returns><c>true</c> if <paramref name="key"/> and <paramref name="value"/> were added to <paramref name="dictionary"/>; otherwise, <c>false</c>.</returns>
        public static bool TryAdd<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            if (dictionary.ContainsKey(key))
            {
                return false;
            }
            
            dictionary.Add(key, value);
            return true;
        }

        /// <summary>
        /// Adds the item to list under key if it exists, otherwise creates a new list.
        /// </summary>
        /// <param name="dictionary">Dictionary.</param>
        /// <param name="key">Key.</param>
        /// <param name="item">Item.</param>
        /// <typeparam name="TKey">The 1st type parameter.</typeparam>
        /// <typeparam name="TValue">The 2nd type parameter.</typeparam>
        /// <typeparam name="TValueCollection">The 3rd type parameter.</typeparam>
        public static void AddItem<TKey, TValue, TValueCollection>(this IDictionary<TKey, TValueCollection> dictionary,
            TKey key, TValue item) where TValueCollection : ICollection<TValue>, new()
        {
            TValueCollection itemCollection;
            if (!dictionary.TryGetValue(key, out itemCollection))
            {
                itemCollection = new TValueCollection();
                dictionary.Add(key, itemCollection);
            }

            itemCollection.Add(item);
        }
        
        /// <summary>
        /// Enumerates through a <see cref="ICollection{T}"/> (using foreach) removing null entries as it goes.
        /// </summary>
        /// <param name="collection">The <see cref="ICollection{T}"/> to iterate through.</param>
        /// <typeparam name="T">The type of the elements in the collection.</typeparam>
        /// <returns>The non-<c>null</c> values in <paramref name="collection"/>.</returns>
        public static IEnumerable<T> EnumerateRemoveNull<T>(this ICollection<T> collection)
        {
            HashSet<T> toRemove = new HashSet<T>();
            foreach (T item in collection)
            {
                if (item == null)
                {
                    toRemove.Add(item);
                }
                else
                {
                    yield return item;
                }
            }

            foreach (T removed in toRemove)
            {
                collection.Remove(removed);
            }
        }
        
        /// <summary>
        /// Enumerates through a <see cref="LinkedList{T}"/> (using foreach) removing null entries as it goes.
        /// </summary>
        /// <param name="dictionary">The <see cref="IDictionary{TKey,TValue}"/> to iterate through.</param>
        /// <typeparam name="TKey">The type of the keys in the dictionary.</typeparam>
        /// <typeparam name="TValue">The type of the values in the dictionary.</typeparam>
        /// <returns>The non-<c>null</c> entries in <paramref name="dictionary"/>.</returns>
        public static IEnumerable<KeyValuePair<TKey, TValue>> EnumerateRemoveNull<TKey, TValue>(
            this IDictionary<TKey, TValue> dictionary)
        {
            HashSet<TKey> toRemove = new HashSet<TKey>();
            foreach (KeyValuePair<TKey, TValue> keyValuePair in dictionary)
            {
                if (keyValuePair.Value == null)
                {
                    toRemove.Add(keyValuePair.Key);
                }
                else
                {
                    yield return keyValuePair;
                }
            }

            foreach (TKey removedKey in toRemove)
            {
                dictionary.Remove(removedKey);
            }
        }
        
        /// <summary>
        /// Enumerates through a <see cref="LinkedList{T}"/> (using foreach) removing <c>null</c> entries as it goes.
        /// </summary>
        /// <param name="linkedList">The <see cref="LinkedList{T}"/> to iterate through.</param>
        /// <typeparam name="T">The type of the elements in the linked list.</typeparam>
        /// <returns>The non-<c>null</c> values in <paramref name="linkedList"/>.</returns>
        public static IEnumerable<T> EnumerateRemoveNull<T>(this LinkedList<T> linkedList)
        {
            LinkedListNode<T> node = linkedList.First;
            while (node != null)
            {
                LinkedListNode<T> next = node.Next;
                if (node.Value == null)
                {
                    linkedList.Remove(node);
                }
                else
                {
                    yield return node.Value;
                }

                node = next;
            }
        }

        /// <summary>
        /// Represents the method that will determine the sort value of an element in a collection.
        /// </summary>
        /// <param name="source">The element to determine the sort value of.</param>
        /// <typeparam name="T">The type of elements in the collection that contains <paramref name="source"/>.</typeparam>
        public delegate float GetSortValueDelegate<in T>(T source);

        /// <summary>
        /// Gets the highest element in the specified <see cref="List{T}"/> using the specified delegate.
        /// </summary>
        /// <param name="list">The list to search.</param>
        /// <param name="getSortValueDelegate">A delegate to sort elements in descending order in <paramref name="list"/>.</param>
        /// <typeparam name="T">The type of elements in the list.</typeparam>
        /// <returns>The highest element in <paramref name="list"/> as determined by <paramref name="getSortValueDelegate"/>.</returns>
        public static int FindHighest<T>(this IList<T> list, GetSortValueDelegate<T> getSortValueDelegate)
        {
            float highestSortValue = 0;
            int highestIndex = -1;
            for (int i = 0; i < list.Count; i++)
            {
                float testValue = getSortValueDelegate(list[i]);
                if (highestIndex == -1 || testValue > highestSortValue)
                {
                    highestIndex = i;
                    highestSortValue = testValue;
                }
            }

            return highestIndex;
        }

        /// <summary>
        /// Gets the key of the highest value in the specified <see cref="IDictionary{TKey,TValue}"/> using the specified delegate.
        /// </summary>
        /// <param name="dictionary">The dictionary to search.</param>
        /// <param name="getSortValueDelegate">A delegate to sort values in descending order in <paramref name="dictionary"/>.</param>
        /// <typeparam name="TKey">The type of the keys in the dictionary.</typeparam>
        /// <typeparam name="TValue">The type of the values in the dictionary.</typeparam>
        /// <returns>The key of the highest value in <paramref name="dictionary"/> as determined by <paramref name="getSortValueDelegate"/>.</returns>
        public static TKey FindKeyWithHighestValue<TKey, TValue>(this IDictionary<TKey, TValue> dictionary,
            GetSortValueDelegate<TValue> getSortValueDelegate)
        {
            float highestSortValue = float.MinValue;
            TKey highestKey = default(TKey);
            foreach (KeyValuePair<TKey, TValue> keyValuePair in dictionary)
            {
                float testValue = getSortValueDelegate(keyValuePair.Value);
                if (testValue > highestSortValue)
                {
                    highestKey = keyValuePair.Key;
                    highestSortValue = testValue;
                }
            }

            return highestKey;
        }

        /// <summary>
        /// Maps an input array to another array of the same length using the specified mapping function.
        /// </summary>
        /// <typeparam name="T">The type of elements in the source array.</typeparam>
        /// <typeparam name="K">The type of elements in the output array.</typeparam>
        /// <param name="source">The array to map.</param>
        /// <param name="mapFunction">A function that maps an element of type <typeparamref name="T"/> to an element of type <typeparamref name="K"/>.</param>
        /// <returns>An array of mapped values from <paramref name="source"/>.</returns>
        public static K[] Map<T, K>(this IList<T> source, Func<T, K> mapFunction)
        {
            K[] output = new K[source.Count];
            for (int i = 0; i < source.Count; i++)
            {
                output[i] = mapFunction(source[i]);
            }

            return output;
        }

        /// <summary>
        /// Gets a random element from a <see cref="List{T}"/>.
        /// </summary>
        /// <param name="list">The <see cref="List{T}"/> to select from.</param>
        /// <typeparam name="T">The type of elements in the list.</typeparam>
        /// <returns>A random element from <paramref name="list"/>.</returns>
        public static T RandomElement<T>(this IList<T> list)
        {
            if (list.Count > 0)
            {
                return list[UnityEngine.Random.Range(0, list.Count)];
            }

            return default(T);
        }

        /// <summary>
        /// Gets a group of unique random elements from the specified <see cref="List{T}"/>.
        /// </summary>
        /// <param name="list">The list to chose from.</param>
        /// <param name="amount">The number of elements to select.</param>
        /// <typeparam name="T">The type of elements in the list.</typeparam>
        /// <returns>An array containing random elements from <paramref name="list"/>.</returns>
        /// <exception cref="ArgumentException"><paramref name="amount"/> is greater than the number of entries in <paramref name="list"/></exception>
        public static T[] RandomElements<T>(this IList<T> list, int amount)
        {
            if (amount > list.Count)
            {
                throw new ArgumentException("Can't select more random elements than size of collection");
            }

            int[] randomIndices = new int[list.Count];
            for (int i = 0; i < randomIndices.Length; i++)
            {
                randomIndices[i] = i;
            }

            randomIndices.Shuffle();

            T[] pickedElements = new T[amount];
            for (int i = 0; i < amount; i++)
            {
                pickedElements[i] = list[randomIndices[i]];
            }

            return pickedElements;
        }

        /// <summary>
        /// Creates a new <see cref="List{T}"/> for the distinct elements from the specified <see cref="List{T}"/>.
        /// </summary>
        /// <param name="source">The list to copy.</param>
        /// <typeparam name="T">The type of elements in the list.</typeparam>
        /// <returns>A new <see cref="List{T}"/> containing the distinct elements from <paramref name="source"/>.</returns>
        public static List<T> CopyWithoutDuplicates<T>(this List<T> source)
        {
            List<T> copy = new List<T>(source);
            return copy.Distinct().ToList();
        }

        /// <summary>
        /// Removes an element from the specified <see cref="IList{T}"/> at the specified zero-based index and returns it.
        /// </summary>
        /// <param name="list">The list to remove the element from.</param>
        /// <param name="index">The zero-based index of the element to remove.</param>
        /// <typeparam name="T">The type of elements in the list.</typeparam>
        /// <returns>The value at <paramref name="index"/>.</returns>
        public static T RemoveAndGet<T>(this IList<T> list, int index)
        {
            T value = list[index];
            list.RemoveAt(index);
            return value;
        }

        /// <summary>
        /// Shuffle the specified <see cref="IList{T}"/>.
        /// </summary>
        /// <remarks>Utilizes the Fisher-Yates shuffle algorithm: https://en.wikipedia.org/wiki/Fisher–Yates_shuffle</remarks>
        /// <param name="list">The list to shuffle.</param>
        /// <typeparam name="T">The type of elements in the list.</typeparam>
        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            Random randomNumberGenerator = new Random();

            while (n > 1)
            {
                int k = randomNumberGenerator.Next(n--);
                T temp = list[n];
                list[n] = list[k];
                list[k] = temp;
            }
        }

        /// <summary>
        /// Performs an action on each item in a collection.
        /// </summary>
        /// <param name="source">The collection to enumerate over.</param>
        /// <param name="action">The <see cref="Action"/> to perform for each item.</param>
        /// <typeparam name="T">The type of elements in the collection.</typeparam>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> is <c>null</c>.</exception>
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            foreach (T value in source)
            {
                action(value);
            }
        }
    }
}
