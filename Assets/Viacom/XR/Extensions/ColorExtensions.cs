﻿// <copyright file="ColorExtensions.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Contains extension methods for colors.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR
{
    using UnityEngine;
    
    /// <summary>
    /// Contains extension methods for <see cref="Color"/>.
    /// </summary>
    public static class ColorExtensions
    {
        /// <summary>
        /// Creates a copy of a <see cref="Color"/> with the specified alpha value.
        /// </summary>
        /// <param name="color">The <see cref="Color"/> to copy.</param>
        /// <param name="alpha">An alpha value.</param>
        /// <returns>A copy of <paramref name="color"/> with alpha set to <paramref name="alpha"/>.</returns>
        public static Color CopyWithAlpha(this Color color, float alpha)
        {
            return new Color(color.r, color.g, color.b, alpha);
        }
    }
}