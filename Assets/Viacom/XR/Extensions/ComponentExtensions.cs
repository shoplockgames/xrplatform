﻿// <copyright file="ComponentExtensions.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Contains extension methods for components.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR
{
	using UnityEngine;

	/// <summary>
	/// Contains extension methods for <see cref="Component"/>.
	/// </summary>
	public static class ComponentExtensions
	{
		/// <summary>
		/// Gets the specified <see cref="Component"/> from a <see cref="GameObject"/> or adds it if it does not already exist.
		/// </summary>
		/// <param name="component">The <see cref="Component"/> to add or fetch.</param>
		/// <typeparam name="T">The type of <paramref name="component"/>.</typeparam>
		/// <returns>The already existing <see cref="Component"/> or the newly added <see cref="Component"/>.</returns>
		public static T GetOrAddComponent<T>(this Component component) where T : Component
		{
			T result = component.GetComponent<T>();
			if (result == null)
			{
				result = component.gameObject.AddComponent<T>();
			}

			return result;
		}

		/// <summary>
		/// Gets the first <see cref="Component"/> on the <see cref="GameObject"/> attached to this <see cref="Component"/>
		/// that implement interface <typeparamref name="T"/>.
		/// </summary>
		/// <param name="component">A <see cref="Component"/> on the <see cref="GameObject"/> to search through.</param>
		/// <typeparam name="T">The desired type of the interface implemented by a <see cref="Component"/>.</typeparam>
		/// <returns>The first <see cref="Component"/> that implements type <typeparamref name="T"/> on the
		/// <see cref="GameObject"/> that <paramref name="component"/> is attached to; <c>null</c> if no matches are found.</returns>
		public static T GetComponentWithInterface<T>(this Component component) where T : class
		{
			return component.gameObject.GetComponentWithInterface<T>();
		}

		/// <summary>
		/// Gets all of <see cref="Component"/> objects on the <see cref="GameObject"/> attached to this
		/// <see cref="Component"/> that implement interface <typeparamref name="T"/>.
		/// </summary>
		/// <param name="component">>A <see cref="Component"/> on the <see cref="GameObject"/> to search through.</param>
		/// <typeparam name="T">The desired type of the interface implemented by a <see cref="Component"/>.</typeparam>
		/// <returns>An array of all <see cref="Component"/> objects that implement type <typeparamref name="T"/> on the
		/// <see cref="GameObject"/> that <paramref name="component"/> is attached to.</returns>
		public static T[] GetComponentsWithInterface<T>(this Component component) where T : class
		{
			return component.gameObject.GetComponentsWithInterface<T>();
		}

		/// <summary>
		/// Gets all of the <see cref="Component"/> objects that implement type <typeparamref name="T"/> on the
		/// <see cref="GameObject"/> attached to this <see cref="Component"/> and the game object's children.
		/// </summary>
		/// <param name="component">A <see cref="Component"/> on the <see cref="GameObject"/> to search through.</param>
		/// <param name="includeInactive"><c>true</c> if inactive children should be included; otherwise <c>false</c>.</param>
		/// <typeparam name="T">The desired type of the interface implemented by a <see cref="Component"/>.</typeparam>
		/// <returns>An array of all <see cref="Component"/> objects that implement type <typeparamref name="T"/> on the
		/// <see cref="GameObject"/> that <paramref name="component"/> is attached to and that game object's children.</returns>
		public static T[] GetComponentsInChildrenWithInterface<T>(this Component component,
			bool includeInactive = false) where T : class
		{
			return component.gameObject.GetComponentsInChildrenWithInterface<T>(includeInactive);
		}
	}
}
