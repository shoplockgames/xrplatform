﻿// <copyright file="GameObjectExtensions.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Contains extension methods for game objects.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR
{
    using System.Collections.Generic;

    using UnityEngine;

    /// <summary>
    /// Contains extension methods for <see cref="GameObject"/>.
    /// </summary>
    public static class GameObjectExtensions
    {
        /// <summary>
        /// Recursively sets the layer of the <see cref="GameObject"/> and all of its children.
        /// </summary>
        /// <param name="gameObject">The <see cref="GameObject"/> to change.</param>
        /// <param name="layer">The new layer.</param>
        public static void SetLayerRecursive(this GameObject gameObject, int layer)
        {
            gameObject.layer = layer;
            Transform transform = gameObject.transform;

            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetLayerRecursive(layer);
            }
        }
        
        /// <summary>
        ///  Gets the first <see cref="Component"/> of type <typeparamref name="T"/> in the <see cref="GameObject"/> or
        /// any of the parents of the <see cref="GameObject"/> regardless of whether or not they are active.
        /// </summary>
        /// <param name="gameObject">The <see cref="GameObject"/> to begin the search from.</param>
        /// <typeparam name="T">The type of the desired <see cref="Component"/>.</typeparam>
        /// <returns>The first <see cref="Component"/> of type <typeparamref name="T"/> in either <paramref name="gameObject"/>
        /// or one of the ancestors of <paramref name="gameObject"/>; <c>null</c> if there are no matches.</returns>
        public static T GetComponentInParentIncludeInactive<T>(this GameObject gameObject) where T : Component
        {
            Transform transform = gameObject.transform;

            while (transform != null)
            {
                T component = transform.GetComponent<T>();
                if (component != null)
                {
                    return component;
                }
                
                transform = transform.parent;
            }

            return null;
        }

        /// <summary>
        /// Gets the first <see cref="Component"/> of type <typeparamref name="T"/> in any of the parents of the <see cref="GameObject"/>.
        /// </summary>
        /// <param name="gameObject">The <see cref="GameObject"/> to begin the search from.</param>
        /// <typeparam name="T">The type of the desired <see cref="Component"/>.</typeparam>
        /// <returns>The first <see cref="Component"/> of type <typeparamref name="T"/> in one of the ancestors of
        /// <paramref name="gameObject"/>; <c>null</c> if there are no matches.</returns>
        public static T GetComponentInParentDontCheckSelf<T>(this GameObject gameObject) where T : Component
        {
            Transform transform = gameObject.transform;
            transform = transform.parent;

            while (transform != null)
            {
                T component = transform.GetComponent<T>();
                if (component != null)
                {
                    return component;
                }

                transform = transform.parent;
            }

            return null;
        }

        /// <summary>
        /// Gets the first <see cref="Component"/> that implements type <typeparamref name="T"/> on the <see cref="GameObject"/>.
        /// </summary>
        /// <param name="gameObject">The <see cref="GameObject"/> to search through.</param>
        /// <typeparam name="T">The desired type of the interface implemented by a <see cref="Component"/>.</typeparam>
        /// <returns>The first <see cref="Component"/> that implements type <typeparamref name="T"/> on
        /// <paramref name="gameObject"/>; <c>null</c> if no matches are found.</returns>
        public static T GetComponentWithInterface<T>(this GameObject gameObject) where T : class
        {
            Component[] allComponents = gameObject.GetComponents<Component>();
            for (int i = 0, allComponentsLength = allComponents.Length; i < allComponentsLength; i++)
            {
                Component component = allComponents[i];
                T typedComponent = component as T;
                if (typedComponent != null)
                {
                    return typedComponent;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets all of the <see cref="Component"/> objects that implements type <typeparamref name="T"/> on the <see cref="GameObject"/>.
        /// </summary>
        /// <param name="gameObject">The <see cref="GameObject"/> to search through.</param>
        /// <typeparam name="T">The desired type of the interface implemented by a <see cref="Component"/>.</typeparam>
        /// <returns>An array of all <see cref="Component"/> objects that implement type <typeparamref name="T"/> on <paramref name="gameObject"/>.</returns>
        public static T[] GetComponentsWithInterface<T>(this GameObject gameObject) where T : class
        {
            Component[] allComponents = gameObject.GetComponents<Component>();
            List<T> interfaceComponents = new List<T>();
            for (int i = 0, allComponentsLength = allComponents.Length; i < allComponentsLength; i++)
            {
                Component component = allComponents[i];
                T typedComponent = component as T;
                if (typedComponent != null)
                {
                    interfaceComponents.Add(typedComponent);
                }
            }

            return interfaceComponents.ToArray();
        }

        /// <summary>
        /// Gets all of the <see cref="Component"/> objects that implement type <typeparamref name="T"/> on the <see cref="GameObject"/> and its children.
        /// </summary>
        /// <param name="gameObject">The <see cref="GameObject"/> to search through.</param>
        /// <param name="includeInactive"><c>true</c> if inactive children should be included; otherwise <c>false</c>.</param>
        /// <typeparam name="T">The desired type of the interface implemented by a <see cref="Component"/>.</typeparam>
        /// <returns>An array of all <see cref="Component"/> objects that implement type <typeparamref name="T"/> on <paramref name="gameObject"/> and its children.</returns>
        public static T[] GetComponentsInChildrenWithInterface<T>(this GameObject gameObject,
            bool includeInactive = false) where T : class
        {
            Component[] allComponents = gameObject.GetComponentsInChildren<Component>(includeInactive);
            List<T> interfaceComponents = new List<T>();
            for (int i = 0, allComponentsLength = allComponents.Length; i < allComponentsLength; i++)
            {
                Component component = allComponents[i];
                T typedComponent = component as T;
                if (typedComponent != null)
                {
                    interfaceComponents.Add(typedComponent);
                }
            }

            return interfaceComponents.ToArray();
        }

        /// <summary>
        /// Toggles the active state of a <see cref="GameObject"/>.
        /// </summary>
        /// <param name="gameObject">The <see cref="GameObject"/> to toggle active.</param>
        public static void ToggleActive(this GameObject gameObject)
        {
            gameObject.SetActive(!gameObject.activeSelf);
        }
    }
}
