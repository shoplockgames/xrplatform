﻿// <copyright file="GradientExtensions.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Contains extension methods for gradients.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR
{
	using UnityEngine;

	/// <summary>
	/// Contains extension methods for <see cref="Gradient"/>.
	/// </summary>
	public static class GradientExtensions
	{
#if UNITY_5_6_OR_NEWER
		/// <summary>
		/// Gets the <see cref="Color"/> from a <see cref="Gradient"/> at the specified index.
		/// </summary>
		/// <param name="gradient">The <see cref="Gradient"/> to get the <see cref="Color"/> from.</param>
		/// <param name="index">The color key index for the desired <see cref="Color"/>.</param>
		/// <returns>The <see cref="Color"/> in <paramref name="gradient"/> at <paramref name="index"/>.</returns>
		public static Color GetColorFromGradient(this Gradient gradient, int index)
		{
			index = Mathf.Clamp(index, 0, gradient.colorKeys.Length - 1);
			Color newColor = gradient.colorKeys[index].color;
			newColor.a = gradient.alphaKeys[index].alpha;
			return newColor;
		}
#endif
	}
}