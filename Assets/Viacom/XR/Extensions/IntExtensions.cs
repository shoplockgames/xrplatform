﻿// <copyright file="IntExtensions.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Contains extension methods for integers.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR
{
    /// <summary>
    /// Contains extension methods for integers.
    /// </summary>
    public static class IntExtensions
    {
        /// <summary>
        /// Converts the specified value into an ordinal string.
        /// </summary>
        /// <param name="number">The value to convert.</param>
        /// <returns>An ordinal string representation of <paramref name="number"/></returns>
        public static string ToOrdinal(this int number)
        {
            // Start with the most common extension
            string extension = "th";

            // Examine the last 2 digits
            int last_digits = number % 100;

            // If the last digits are 11, 12, or 13, use th
            if (last_digits < 11 || last_digits > 13)
            {
                // Check the last digit
                switch (last_digits % 10)
                {
                    case 1:
                        extension = "st";
                        break;
                    case 2:
                        extension = "nd";
                        break;
                    case 3:
                        extension = "rd";
                        break;
                }
            }
            
            return number.ToString() + extension;
        }
    }
}
