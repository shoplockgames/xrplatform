﻿// <copyright file="MonoBehaviourExtensions.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Contains extension methods for MonoBehaviours.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR
{
    using System;

    using Viacom.XR.Coroutines;

    using UnityEngine;

    /// <summary>
    /// Contains extension methods for <see cref="MonoBehaviour"/>.
    /// </summary>
    public static class MonoBehaviourExtensions
    {
        /// <summary>
        /// Executes the specified <see cref="Action"/> after a delay by using a <c>Coroutine</c>.
        /// </summary>
        /// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> to run the <c>Coroutine</c> on.</param>
        /// <param name="action">The <see cref="Action"/> to execute.</param>
        /// <param name="delay">The amount of time to wait before executing <paramref name="action"/>.</param>
        public static void DoAfterDelay(this MonoBehaviour monoBehaviour, Action action, float delay)
        {
            CoroutineRunner.Instance.DoAfterDelay(action, delay, monoBehaviour);
        }

        /// <summary>
        /// Executes the specified <c>Action</c> after the specified condition becomes true.
        /// </summary>
        /// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> to run the <c>Coroutine</c> on.</param>
        /// <param name="action">The <see cref="Action"/> to execute.</param>
        /// <param name="predicate">A function to test for a condition.</param>
        /// <param name="timeout">An amount of time in seconds to wait before ceasing to check the condition.</param>
        /// <param name="onTimeout">An optional <see cref="Action"/> to execute upon time-out.</param>
        /// <param name="checkInterval">A optional frequency in seconds to check the condition.</param>
        public static void DoAfterConditionBecomesTrue(this MonoBehaviour monoBehaviour, Action action,
            Func<bool> predicate, float timeout, Action onTimeout = null, float checkInterval = 0.5f)
        {
            CoroutineRunner.Instance.DoAfterConditionBecomesTrue(action, predicate, timeout, onTimeout, checkInterval,
                monoBehaviour);
        }

        /// <summary>
        /// Gets the first <see cref="Component"/> of type <typeparamref name="T"/> in any of the parents of the
        /// <see cref="GameObject"/> that the <see cref="MonoBehaviour"/> is attached to.
        /// </summary>
        /// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> attached to the <see cref="GameObject"/> to begin the search from.</param>
        /// <typeparam name="T">The type of the desired <see cref="Component"/>.</typeparam>
        /// <returns>The first <see cref="Component"/> of type <typeparamref name="T"/> in one of the ancestors of the
        /// <see cref="GameObject"/> that <paramref name="monoBehaviour"/> is attached to; <c>null</c> if there are no matches.</returns>
        public static T GetComponentInParentDontCheckSelf<T>(this MonoBehaviour monoBehaviour) where T : Component
        {
            Transform transform = monoBehaviour.transform;
            transform = transform.parent;

            while (transform != null)
            {
                T component = transform.GetComponent<T>();
                if (component != null)
                {
                    return component;
                }

                transform = transform.parent;
            }

            return null;
        }
    }
}