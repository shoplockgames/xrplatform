﻿// <copyright file="TransformExtensions.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Contains extension methods for RectTransforms.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR
{
    using System;

    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    /// Contains extension methods for <see cref="RectTransform"/>.
    /// </summary>
    public static class RectTransformExtensions
    {
        // RectTransform world corners.
        private static readonly Vector3[] WorldCorners = new Vector3[4];

        /// <summary>
        /// Specifies how to align a <see cref="RectTransform"/> to the screen.
        /// </summary>
        public enum RectTransformScreenAnchor
        {
            Center,
            Left,
            Right,
            Top,
            Bottom
        }

        /// <summary>
        /// Specifies how to determine if a <see cref="RectTransform"/> is visible.
        /// </summary>
        public enum RectTransformVisibilityCheckType
        {
            Horizontal,
            Vertical,
            AllInclusive
        }

        /// <summary>
        /// Resizes the <see cref="RectTransform"/> to completely fill its parent. 
        /// </summary>
        /// <param name="rectTransform">The <see cref="RectTransform"/> to resize.</param>
        public static void FillParent(this RectTransform rectTransform)
        {
            rectTransform.localPosition = Vector3.zero;
            rectTransform.localRotation = Quaternion.identity;
            rectTransform.localScale = Vector3.one;

            // Stretch child to fit
            rectTransform.anchorMin = Vector2.zero;
            rectTransform.anchorMax = Vector2.one;
            rectTransform.offsetMin = Vector2.zero;
            rectTransform.offsetMax = Vector2.zero;
            rectTransform.sizeDelta = Vector2.zero;
        }

        /// <summary>
        /// Centers the anchors of the <see cref="RectTransform"/> and maintains its size.
        /// </summary>
        /// <param name="rectTransform">The <see cref="RectTransform"/> to update.</param>
        public static void CenterAnchors(this RectTransform rectTransform)
        {
            Vector2 deltaSize = rectTransform.rect.size;
            rectTransform.anchorMin = Vector2.one * 0.5f;
            rectTransform.anchorMax = Vector2.one * 0.5f;
            rectTransform.sizeDelta = deltaSize;
        }

        /// <summary>
        /// Resizes the <see cref="RectTransform"/> to completely fill its parent's edge. 
        /// </summary>
        /// <param name="rectTransform">The <see cref="RectTransform"/> to resize.</param>
        /// <param name="edge">The <see cref="RectTransform.Edge"/> to use.</param>
        public static void FillParentEdge(this RectTransform rectTransform, RectTransform.Edge edge)
        {
            RectTransform parent = rectTransform.parent as RectTransform;
            Vector2 newMin = rectTransform.anchorMin;
            Vector2 newMax = rectTransform.anchorMax;

            switch (edge)
            {
                case RectTransform.Edge.Left:
                    newMin = Vector2.zero;
                    newMax = new Vector2(0, 1);
                    break;
                case RectTransform.Edge.Right:
                    newMin = new Vector2(1, 0);
                    newMax = new Vector2(1, 1);
                    break;
            }

            rectTransform.anchorMin = newMin;
            rectTransform.anchorMax = newMax;
            rectTransform.anchoredPosition3D = Vector3.zero;
            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, parent.rect.height);
        }

        /// <summary>
        /// Gets the anchored position of a <see cref="RectTransform"/> relative to another <see cref="RectTransform"/>.
        /// </summary>
        /// <param name="rectTransform">The <see cref="RectTransform"/> to evaluate.</param>
        /// <param name="other">The other <see cref="RectTransform"/>.</param>
        /// <param name="rootCanvas">The root <see cref="Canvas"/> for this element.</param>
        /// <param name="anchor">The anchor for this <see cref="RectTransform"/>.</param>
        /// <returns></returns>
        public static Vector2 GetLocalPointInRectangle(this RectTransform rectTransform, RectTransform other,
            Canvas rootCanvas, RectTransformScreenAnchor anchor = RectTransformScreenAnchor.Center)
        {
            Vector2 pos = rectTransform.position;
            Vector2 posOffset = Vector2.zero;
            Vector2 size = other.rect.size;
            if (anchor != RectTransformScreenAnchor.Center)
            {
                int cornerIndex = 0;
                switch (anchor)
                {
                    case RectTransformScreenAnchor.Left:
                    case RectTransformScreenAnchor.Right:
                        posOffset.x = size.x / 2;
                        cornerIndex = anchor == RectTransformScreenAnchor.Left ? 0 : 2;
                        break;
                    case RectTransformScreenAnchor.Top:
                    case RectTransformScreenAnchor.Bottom:
                        posOffset.y = size.y / 2;
                        cornerIndex = anchor == RectTransformScreenAnchor.Bottom ? 0 : 2;
                        break;
                }

                if (anchor == RectTransformScreenAnchor.Top || anchor == RectTransformScreenAnchor.Left)
                {
                    posOffset *= -1;
                }

                rectTransform.GetWorldCorners(WorldCorners);
                pos = WorldCorners[cornerIndex];
            }

            Vector2 screenPos = RectTransformUtility.WorldToScreenPoint(rootCanvas.worldCamera, pos);

            //Get elements anchored position relative to content's parent.
            Vector2 anchoredPos;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(other, screenPos, rootCanvas.worldCamera,
                out anchoredPos);

            return anchoredPos + posOffset;
        }

        /// <summary>
        /// Gets a value indicating whether or not the <see cref="RectTransform"/> overlaps with the specified <see cref="RectTransform"/>.
        /// </summary>
        /// <param name="rectTransform">The <see cref="RectTransform"/> to evaluate.</param>
        /// <param name="other">The other <see cref="RectTransform"/> to evaluate.</param>
        /// <param name="rootCanvas">The root <see cref="Canvas"/> for this element.</param>
        /// <param name="checkType">The visibility check to use to determine overlap.</param>
        /// <returns><c>true</c> if the <see cref="RectTransform"/> objects overlap; otherwise <c>false</c>.</returns>
        public static bool OverlapsRect(this RectTransform rectTransform, RectTransform other, Canvas rootCanvas,
            RectTransformVisibilityCheckType checkType = RectTransformVisibilityCheckType.AllInclusive)
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(rectTransform);

            Camera camera = rootCanvas != null ? rootCanvas.worldCamera : Camera.main;
            rectTransform.GetWorldCorners(WorldCorners);
            Vector2 min = camera.WorldToScreenPoint(WorldCorners[0]);
            Vector2 max = camera.WorldToScreenPoint(WorldCorners[2]);

            other.GetWorldCorners(WorldCorners);
            float topEdge = camera.WorldToScreenPoint(WorldCorners[1]).y;
            float leftEdge = camera.WorldToScreenPoint(WorldCorners[1]).x;
            float bottomEdge = camera.WorldToScreenPoint(WorldCorners[3]).y;
            float rightEdge = camera.WorldToScreenPoint(WorldCorners[3]).x;

            Func<bool> hasX = () =>
                (leftEdge < min.x && rightEdge > max.x) || (leftEdge >= min.x && leftEdge <= max.x) ||
                (rightEdge >= min.x && rightEdge <= max.x);
            Func<bool> hasY = () =>
                (bottomEdge < min.y && topEdge > max.y) || (topEdge >= min.y && topEdge <= max.y) ||
                (bottomEdge >= min.y && bottomEdge <= max.y);

            switch (checkType)
            {
                case RectTransformVisibilityCheckType.Horizontal:
                    return hasX();
                case RectTransformVisibilityCheckType.Vertical:
                    return hasY();
                case RectTransformVisibilityCheckType.AllInclusive:
                    return hasX() && hasY();
                default:
                    throw new ArgumentOutOfRangeException("checkType", checkType, null);
            }
        }

        /// <summary>
        /// Anchors the <see cref="RectTransform"/> to its bounds.
        /// </summary>
        /// <param name="rectTransform">The <see cref="RectTransform"/> to anchor.</param>
        /// <param name="parent">The parent <see cref="RectTransform"/> to define the bounds.</param>
        /// <param name="pivot">The normalized position in <paramref name="rectTransform"/> that it rotates around.</param>
        public static void AnchorToSelf(this RectTransform rectTransform, RectTransform parent, Vector2 pivot)
        {
            Vector2 offsetMin = rectTransform.offsetMin;
            Vector2 offsetMax = rectTransform.offsetMax;
            Vector2 _anchorMin = rectTransform.anchorMin;
            Vector2 _anchorMax = rectTransform.anchorMax;

            float parent_width = parent.rect.width;
            float parent_height = parent.rect.height;

            Vector2 anchorMin = new Vector2(_anchorMin.x + (offsetMin.x / parent_width),
                _anchorMin.y + (offsetMin.y / parent_height));
            Vector2 anchorMax = new Vector2(_anchorMax.x + (offsetMax.x / parent_width),
                _anchorMax.y + (offsetMax.y / parent_height));

            rectTransform.anchorMin = anchorMin;
            rectTransform.anchorMax = anchorMax;

            rectTransform.offsetMin = Vector2.zero;
            rectTransform.offsetMax = Vector2.zero;
            rectTransform.pivot = pivot;
        }

        /// <summary>
        /// Transforms a position in screen space to the local space of the <see cref="RectTransform"/>.
        /// </summary>
        /// <param name="rect">The <see cref="RectTransform"/> to find a point inside.</param>
        /// <param name="screenPoint">A screen space position.</param>
        /// <param name="camera">The camera associated with the screen space position <paramref name="screenPoint"/>.</param>
        /// <returns>The anchored position of the <see cref="RectTransform"/> according to a point in screen space.</returns>
        public static Vector2 GetPositionInCanvas(this RectTransform rectTransform, Vector2 screenPoint, Camera camera)
        {
            Vector2 pointInCanvas;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, screenPoint, camera,
                out pointInCanvas);
            return pointInCanvas;
        }
    }
}