// <copyright file="StringExtensions.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Contains extension methods for strings.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Contains extension methods for strings.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Determines if the string is <c>null</c>, <c>""</c>, or <c>"null"</c>.
        /// </summary>
        /// <param name="value">The string to check.</param>
        public static bool IsNullOrEmptyJson(this string value)
        {
            return string.IsNullOrEmpty(value) || value == "null";
        }

        /// <summary>
        /// Removes all control characters from the string.
        /// </summary>
        /// <param name="value">The source string.</param>
        /// <returns>A copy of <paramref name="value"/> without any control characters.</returns>
        public static string RemoveControlCharacters(this string value)
        {
            Regex regex = new Regex(@"[\u0000-\u001F]", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
            return regex.Replace(value, string.Empty);
        }

        /// <summary>
        /// Removes characters used in Word Editors such as smart quotations and replaces them with their .NET equivalent.
        /// </summary>
        /// <param name="value">The source string.</param>
        /// <returns>A copy of <paramref name="value"/> with all word editor characters replaced with their .NET equivalent.</returns>
        public static string ReplaceMSWordChars(this string value)
        {
            string output = value;
            output = Regex.Replace(output, "[\u2018\u2019\u201A]", "'"); // smart single quotes and apostrophe
            output = Regex.Replace(output, "[\u201C\u201D\u201E]", "\""); // smart double quotes
            output = Regex.Replace(output, "\u2026", "..."); // ellipsis
            output = Regex.Replace(output, "[\u2013\u2014]", "-"); // dashes
            output = Regex.Replace(output, "\u02C6", "^"); // circumflex
            output = Regex.Replace(output, "\u2039", "<"); // open angle bracket
            output = Regex.Replace(output, "\u203A", ">"); // close angle bracket
            return output;
        }

        /// <summary>
        /// Removes all the white spaces.
        /// </summary>
        /// <param name="value">The source string.</param>
        /// <returns>A copy of <paramref name="value"/> with no white spaces.</returns>
        public static string RemoveWhiteSpaces(this string value)
        {
            return string.Join("", value.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries));
        }

        /// <summary>
        /// Replaces a string at a position with another string.
        /// </summary>
        /// <param name="value">The source string.</param>
        /// <param name="index">The zero-based index to begin.</param>
        /// <param name="length">The number of characters be removed before inserting in <paramref name="replace"/></param>
        /// <param name="replace">The new string to insert</param>
        /// <returns>A copy of <paramref name="value"/> with <paramref name="length"/> number of characters removed
        /// starting at <paramref name="index"/> and replaced with <paramref name="replace"/>.</returns>
        public static string ReplaceAt(this string value, int index, int length, string replace)
        {
            StringBuilder stringBuilder = new StringBuilder(value);
            stringBuilder.Remove(index, length);
            stringBuilder.Insert(index, replace);
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Trims all punctuation from start and end of the string.
        /// </summary>
        /// <param name="value">The source string.</param>
        /// <param name="ignoredPunctuation">An array of punctuation characters to ignore.</param>
        /// <returns>A copy of <paramref name="value"/> with no leading or trailing punctuation.</returns>
        public static string TrimPunctuation(this string value, params char[] ignoredPunctuation)
        {
            // Count start punctuation.
            int removeFromStart = 0;
            for (int i = 0; i < value.Length; i++)
            {
                if (char.IsPunctuation(value[i]) && !ignoredPunctuation.Contains(value[i]))
                {
                    removeFromStart++;
                }
                else
                {
                    break;
                }
            }

            // Count end punctuation.
            int removeFromEnd = 0;
            for (int i = value.Length - 1; i >= 0; i--)
            {
                if (char.IsPunctuation(value[i]) && !ignoredPunctuation.Contains(value[i]))
                {
                    removeFromEnd++;
                }
                else
                {
                    break;
                }
            }

            // No characters were punctuation.
            if (removeFromStart == 0 && removeFromEnd == 0)
            {
                return value;
            }

            // All characters were punctuation.
            if (removeFromStart == value.Length && removeFromEnd == value.Length)
            {
                return string.Empty;
            }

            // Substring.
            return value.Substring(removeFromStart, value.Length - removeFromEnd - removeFromStart);
        }

        /// <summary>
        /// Converts the string to snake case.
        /// </summary>
        /// <remarks>This iterates over bytes, so it does not work for multi-byte characters.</remarks>
        /// <param name="value">The source string.</param>
        /// <returns>A copy of <paramref name="value"/> in snake case.</returns>
        public static string ToSnakeCase(this string value)
        {
            const char wordSeparator = '_';
            const char space = ' ';

            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < value.Length; ++i)
            {
                char c = value[i];
                if (c == space)
                {
                    stringBuilder.Append(wordSeparator);
                }
                else if (char.IsUpper(c))
                {
                    if (i != 0)
                    {
                        stringBuilder.Append(wordSeparator);
                    }

                    stringBuilder.Append(char.ToLower(c));
                }
                else
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Removes all non-alphanumeric characters from the string.
        /// </summary>
        /// <param name="value">The source string.</param>
        /// <returns>A copy of <paramref name="value"/> with only alphanumeric characters.</returns>
        public static string RemoveSpecialCharacters(this string value)
        {
            Regex regex = new Regex("(?:[^a-z0-9 ]|(?<=['\"])s)",
                RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
            return regex.Replace(value, string.Empty);
        }
    }
}
