﻿// <copyright file="TransformExtensions.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Contains extension methods for transforms.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR
{
    using System.Collections.Generic;

    using UnityEngine;

    /// <summary>
    /// Contains extension methods for <see cref="Transform"/>.
    /// </summary>
    public static class TransformExtensions
    {
        /// <summary>
        /// Gets a list of all the descendants of the <see cref="Transform"/>.
        /// </summary>
        /// <param name="transform">The <see cref="Transform"/> to start from.</param>
        /// <returns>A list of all the descendants of <paramref name="transform"/>.</returns>
        public static List<Transform> FlattenHierarchy(this Transform transform)
        {
            List<Transform> entireHierarchy = new List<Transform>();
            FlattenHierarchyInternal(transform, entireHierarchy);
            return entireHierarchy;
        }

        /// <summary>
        /// Adds the specified <see cref="Transform"/> and all of its immediate children to the specified list.
        /// </summary>
        /// <param name="current">The <see cref="Transform"/> to evaluate.</param>
        /// <param name="entireHierarchy">The current list of <see cref="Transform"/> objects.</param>
        private static void FlattenHierarchyInternal(Transform current, List<Transform> entireHierarchy)
        {
            entireHierarchy.Add(current);
            List<Transform> immediateChildren = current.GetImmediateChildren();

            for (int i = 0; i < immediateChildren.Count; i++)
            {
                entireHierarchy.Add(immediateChildren[i]);
                FlattenHierarchyInternal(immediateChildren[i], entireHierarchy);
            }
        }

        /// <summary>
        /// Gets a list of the immediate children of the specified <see cref="Transform"/> indexed by sibling index.
        /// </summary>
        /// <param name="transform">The <see cref="Transform"/> to evaluate.</param>
        /// <returns>A list of children of <paramref name="transform"/> indexed by sibling index.</returns>
        public static List<Transform> GetImmediateChildren(this Transform transform)
        {
            List<Transform> children = new List<Transform>();

            for (int i = 0; i < transform.childCount; i++)
            {
                Transform child = transform.GetChild(i);
                children.Add(child);
            }

            return children;
        }

        /// <summary>
        /// Sets the X component of the position of the <see cref="Transform"/>.
        /// </summary>
        /// <param name="transform">The <see cref="Transform"/> to update.</param>
        /// <param name="value">The new value.</param>
        /// <param name="space">The coordinate space. Default is <c>Self</c>.</param>
        public static void SetXPos(this Transform transform, float value, Space space = Space.Self)
        {
            if (space == Space.Self)
            {
                Vector3 newPos = transform.localPosition;
                newPos.x = value;
                transform.localPosition = newPos;
            }
            else
            {
                Vector3 newPos = transform.position;
                newPos.x = value;
                transform.position = newPos;
            }
        }

        /// <summary>
        /// Sets the Y component of the position of the <see cref="Transform"/>.
        /// </summary>
        /// <param name="transform">The <see cref="Transform"/> to update.</param>
        /// <param name="value">The new value.</param>
        /// <param name="space">The coordinate space. Default is <c>Self</c>.</param>
        public static void SetYPos(this Transform transform, float value, Space space = Space.Self)
        {
            if (space == Space.Self)
            {
                Vector3 newPos = transform.localPosition;
                newPos.y = value;
                transform.localPosition = newPos;
            }
            else
            {
                Vector3 newPos = transform.position;
                newPos.y = value;
                transform.position = newPos;
            }
        }

        /// <summary>
        /// Sets the Z component of the position of the <see cref="Transform"/>.
        /// </summary>
        /// <param name="transform">The <see cref="Transform"/> to update.</param>
        /// <param name="value">The new value.</param>
        /// <param name="space">The coordinate space. Default is <c>Self</c>.</param>
        public static void SetZPos(this Transform transform, float value, Space space = Space.Self)
        {
            if (space == Space.Self)
            {
                Vector3 newPos = transform.localPosition;
                newPos.z = value;
                transform.localPosition = newPos;
            }
            else
            {
                Vector3 newPos = transform.position;
                newPos.z = value;
                transform.position = newPos;
            }
        }

        /// <summary>
        /// Sets the X component of the scale of the <see cref="Transform"/> relative to its parent.
        /// </summary>
        /// <param name="transform">The <see cref="Transform"/> to update.</param>
        /// <param name="value">The new value.</param>
        public static void SetXScale(this Transform transform, float value)
        {
            Vector3 newScale = transform.localScale;
            newScale.x = value;
            transform.localScale = newScale;
        }

        /// <summary>
        /// Sets the Y component of the scale of the <see cref="Transform"/> relative to its parent.
        /// </summary>
        /// <param name="transform">The <see cref="Transform"/> to update.</param>
        /// <param name="value">The new value.</param>
        public static void SetYScale(this Transform transform, float value)
        {
            Vector3 newScale = transform.localScale;
            newScale.y = value;
            transform.localScale = newScale;
        }

        /// <summary>
        /// Sets the Z component of the scale of the <see cref="Transform"/> relative to its parent.
        /// </summary>
        /// <param name="transform">The <see cref="Transform"/> to update.</param>
        /// <param name="value">The new value.</param>
        public static void SetZScale(this Transform transform, float value)
        {
            Vector3 newScale = transform.localScale;
            newScale.z = value;
            transform.localScale = newScale;
        }

        /// <summary>
        /// Gets a <see cref="Ray"/> in world space starting at the position of the <see cref="Transform"/> and going along the forward direction.
        /// </summary>
        /// <param name="transform">The <see cref="Transform"/> to use.</param>
        /// <returns>A <see cref="Ray"/> in world space starting at the position of <paramref name="transform"/> and going along the forward direction.</returns>
        public static Ray GetForwardRay(this Transform transform)
        {
            return new Ray(transform.position, transform.forward);
        }

        /// <summary>
        /// Gets a <see cref="Ray"/> in world space starting at the position of the <see cref="Transform"/> and going along the up direction.
        /// </summary>
        /// <param name="transform">The <see cref="Transform"/> to use.</param>
        /// <returns>A <see cref="Ray"/> in world space starting at the position of <paramref name="transform"/> and going along the up direction.</returns>
        public static Ray GetUpRay(this Transform transform)
        {
            return new Ray(transform.position, transform.up);
        }

        /// <summary>
        /// Gets a <see cref="Ray"/> in world space starting at the position of the <see cref="Transform"/> and going along the right direction.
        /// </summary>
        /// <param name="transform">The <see cref="Transform"/> to use.</param>
        /// <returns>A <see cref="Ray"/> in world space starting at the position of <paramref name="transform"/> and going along the right direction.</returns>
        public static Ray GetRightRay(this Transform transform)
        {
            return new Ray(transform.position, transform.right);
        }

        /// <summary>
        /// Gets a list of all the active immediate children of the <see cref="Transform"/>.
        /// </summary>
        /// <param name="transform">The <see cref="Transform"/> to use.</param>
        /// <returns>A list of all the active immediate children of <paramref name="transform"/>.</returns>
        public static List<GameObject> GetActiveChildren(this Transform transform)
        {
            List<GameObject> activeObjects = new List<GameObject>();
            for (int i = 0; i < transform.childCount; i++)
            {
                GameObject childObject = transform.GetChild(i).gameObject;
                if (childObject.activeInHierarchy)
                {
                    activeObjects.Add(childObject);
                }
            }

            return activeObjects;
        }

        /// <summary>
        /// Destroys all the children of the <see cref="Transform"/>.
        /// </summary>
        /// <param name="transform">>The <see cref="Transform"/> to use.</param>
        /// <param name="isImmediate"><c>true</c> if the child objects should be destroyed immediately; otherwise <c>false</c>. Default is <c>false</c>.</param>
        public static void DestroyChildren(this Transform transform, bool isImmediate = false)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                Transform child = transform.GetChild(i);

                if (isImmediate)
                {
                    GameObject.DestroyImmediate(child.gameObject);
                }
                else
                {
                    GameObject.Destroy(child.gameObject);
                }
            }
        }

        /// <summary>
        /// Clones an object and then makes that object a child of the <see cref="Transform"/>.
        /// </summary>
        /// <param name="transform">The <see cref="Transform"/> to parent the object to.</param>
        /// <param name="original">The existing object to clone.</param>
        /// <param name="worldPositionStays"><c>true</c> if the position and rotation should keep the same world space
        /// position and rotation after being parented to <paramref name="transform"/>; otherwise <c>false</c>.</param>
        /// <typeparam name="T">The type of the object being cloned.</typeparam>
        /// <returns>A clone of <paramref name="original"/> that is a child of <paramref name="transform"/>.</returns>
        public static T InstantiateChild<T>(this Transform transform, T original, bool worldPositionStays = false)
            where T : Component
        {
            return InstantiateAndParent(original, transform.position, transform.rotation, transform, original.name,
                worldPositionStays);
        }

        /// <summary>
        /// Clones an object and then makes that object a child of the <see cref="Transform"/>.
        /// </summary>
        /// <param name="transform">The <see cref="Transform"/> to parent the object to.</param>
        /// <param name="original">The existing object to clone.</param>
        /// <param name="position">The position of the new object.</param>
        /// <param name="rotation">The rotation of the new object.</param>
        /// <param name="name">The name of the object.</param>
        /// <param name="worldPositionStays"><c>true</c> if the position and rotation should keep the same world space
        /// position and rotation after being parented to <paramref name="transform"/>; otherwise <c>false</c>.</param>
        /// <typeparam name="T">The type of the object being cloned.</typeparam>
        /// <returns>A clone of <paramref name="original"/> that is a child of <paramref name="transform"/>.</returns>
        public static T InstantiateChild<T>(this Transform transform, T original, Vector3 position, Quaternion rotation,
            string name, bool worldPositionStays = false) where T : Component
        {
            return InstantiateAndParent(original, position, rotation, transform, name, worldPositionStays);
        }

        /// <summary>
        /// Clones an object and then makes that object a child of the specified <see cref="Transform"/>.
        /// </summary>
        /// <param name="original">The existing object to clone.</param>
        /// <param name="position">The position of the new object.</param>
        /// <param name="rotation">The rotation of the new object.</param>
        /// <param name="parent">The <see cref="Transform"/> to parent the object to.</param>
        /// <param name="name">The name of the object.</param>
        /// <param name="worldPositionStays"><c>true</c> if the position and rotation should keep the same world space
        /// position and rotation after being parented to <paramref name="parent"/>; otherwise <c>false</c>.</param>
        /// <typeparam name="T">The type of the object being cloned.</typeparam>
        /// <returns>A clone of <paramref name="original"/> that is a child of <paramref name="parent"/>.</returns>
        private static T InstantiateAndParent<T>(T original, Vector3 position, Quaternion rotation, Transform parent,
            string name, bool worldPositionStays = false) where T : Component
        {
            T newObject = Object.Instantiate(original, position, rotation) as T;
            newObject.gameObject.name = name;
            newObject.transform.SetParent(parent, worldPositionStays: worldPositionStays);
            return newObject;
        }

        /// <summary>
        /// Transforms <see cref="Bounds"/> from local space to world space.
        /// </summary>
        /// <remarks>See http://answers.unity3d.com/answers/1114000/view.html</remarks>
        /// <param name="transform">The <see cref="Transform"/> to use.</param>
        /// <param name="bounds">The <see cref="Bounds"/> to transform.</param>
        /// <returns>A copy of <paramref name="bounds"/> in world space.</returns>
        public static Bounds TransformBoundsFromLocalToWorld(this Transform transform, Bounds bounds)
        {
            Vector3 center = transform.TransformPoint(bounds.center);

            // Transform the local extents' axes
            Vector3 extents = bounds.extents;
            Vector3 axisX = transform.TransformVector(extents.x, 0, 0);
            Vector3 axisY = transform.TransformVector(0, extents.y, 0);
            Vector3 axisZ = transform.TransformVector(0, 0, extents.z);

            // Sum their absolute value to get the world extents
            extents.x = Mathf.Abs(axisX.x) + Mathf.Abs(axisY.x) + Mathf.Abs(axisZ.x);
            extents.y = Mathf.Abs(axisX.y) + Mathf.Abs(axisY.y) + Mathf.Abs(axisZ.y);
            extents.z = Mathf.Abs(axisX.z) + Mathf.Abs(axisY.z) + Mathf.Abs(axisZ.z);

            return new Bounds {center = center, extents = extents};
        }

        /// <summary>
        /// Reset the local position, rotation, and scale of the <see cref="Transform"/> to their respective zero-ed out values.
        /// </summary>
        /// <param name="transform">The <see cref="Transform"/> to reset.</param>
        public static void ResetLocalValues(this Transform transform)
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = Vector3.one;
        }

        /// <summary>
        /// Gets an approximation of the world scale of the <see cref="Transform"/>.
        /// </summary>
        /// <param name="transform">The <see cref="Transform"/> to evaluate.</param>
        /// <returns>An approximation of the world scale of the <see cref="Transform"/>.</returns>
        public static float GetLossyScale(this Transform transform)
        {
            float scale = 1f;
            while (transform != null && transform.parent != null)
            {
                transform = transform.parent;
                scale *= transform.localScale.x;
            }

            return scale;
        }
    }
}