﻿// <copyright file="FloatLimits.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a range defined by two single-precision floating-point numbers.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR
{
    using System;
    
    using UnityEngine;

    /// <summary>
    /// Represents a range defined by two single-precision floating-point numbers.
    /// </summary>
    [Serializable]
    public class FloatLimits
    {
        [SerializeField] private float minimum, maximum;

        /// <summary>
        /// Gets the minimum allowed value of the limits.
        /// </summary>
        public float Minimum
        {
            get { return minimum; }
        }
        
        /// <summary>
        /// Gets the maximum allowed value of the limits.
        /// </summary>
        public float Maximum
        {
            get { return maximum; }
        }

        /// <summary>
        /// Gets the size of the range.
        /// </summary>
        public float Size
        {
            get { return maximum - minimum; }
        }
        
        /// <summary>
        /// Gets a new instance of <see cref="FloatLimits"/> with the minimum and maximum set to zero.
        /// </summary>
        public static FloatLimits Zero
        {
            get
            {
                return new FloatLimits(0f, 0f);
            }
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="FloatLimits"/> class with the minimum and maximum values.
        /// </summary>
        /// <param name="min">The minimum allowed value.</param>
        /// <param name="max">The maximum allowed value.</param>
        public FloatLimits(float min, float max)
        {
            minimum = min;
            maximum = max;
        }

        /// <summary>
        /// Linearly interpolates between the minimum and maximum of this limit by the specified value. 
        /// </summary>
        /// <param name="t">The interpolation value between the minimum and maximum.</param>
        /// <returns>The interpolated value between the minimum and maximum of this limit.</returns>
        public float Lerp(float t)
        {
            return Mathf.Lerp(minimum, maximum, t);
        }

        /// <summary>
        /// Gets a random value that falls within the limits.
        /// </summary>
        /// <returns>A random value that falls within the limits.</returns>
        public float GetRandomValue()
        {
            return UnityEngine.Random.Range(minimum, maximum);
        }

        /// <summary>
        /// Gets a value indicating whether or not the specified value falls within the limits.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <returns><c>true</c> if the <paramref name="value"/> is within the limits; otherwise, <c>false</c>.</returns>
        public bool IsWithinLimits(float value)
        {
            return (value >= minimum && value <= maximum);
        }
        
        /// <summary>
        /// Creates a new <see cref="Vector2"/> object initialized to with the minimum and maximum values of this <see cref="FloatLimits"/>.
        /// </summary>
        public Vector2 ToVector2()
        {
            return new Vector2(minimum, maximum);
        }

        /// <summary>
        /// Creates a new <see cref="FloatLimits"/> object initialized to with the specified value as both the minimum and maximum.
        /// </summary>
        /// <param name="value">A single-precision floating-point number.</param>
        /// <returns>A <see cref="FloatLimits"/> object whose minimum and maximum are initialized with <paramref name="value"/>.</returns>
        public static implicit operator FloatLimits(float value)
        {
            FloatLimits limits = new FloatLimits(value, value);
            return limits;
        }
    }
}

#if UNITY_EDITOR

namespace Viacom.XR.Variables.Internal
{
    using UnityEngine;
#if UNITY_EDITOR
    using UnityEditor;

#endif

    /// <summary>
    /// Represents a custom drawer for the <see cref="FloatLimits"/>.
    /// </summary>
    [CustomPropertyDrawer(typeof(FloatLimits))]
    public class FloatLimitsDrawer : PropertyDrawer
    {
        private const string MinimumFieldLabel = "Min";
        private const string MaximumFieldLabel = "Max";
        private const string MinimumField = "minimum";
        private const string MaximumField = "maximum";
        private const float LabelWidth = 30f;
        private const int Sections = 3;

        /// <inheritdoc cref="PropertyDrawer.OnGUI"/>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty minimum = property.FindPropertyRelative(MinimumField);
            SerializedProperty maximum = property.FindPropertyRelative(MaximumField);

            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            float updatePositionX = position.x;
            float labelWidth = LabelWidth;
            float fieldWidth = (position.width / (float) Sections) - labelWidth;

            EditorGUI.LabelField(new Rect(updatePositionX, position.y, labelWidth, position.height), MinimumFieldLabel);
            updatePositionX += labelWidth;
            minimum.floatValue =
                EditorGUI.FloatField(new Rect(updatePositionX, position.y, fieldWidth, position.height),
                    minimum.floatValue);
            updatePositionX += fieldWidth;

            EditorGUI.LabelField(new Rect(updatePositionX, position.y, labelWidth, position.height), MaximumFieldLabel);
            updatePositionX += labelWidth;
            maximum.floatValue =
                EditorGUI.FloatField(new Rect(updatePositionX, position.y, fieldWidth, position.height),
                    maximum.floatValue);
            updatePositionX += fieldWidth;

            EditorGUI.indentLevel = indent;
        }
    }
}

#endif