﻿// <copyright file="AddMaterialHighlighter.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a component that highlights a <see cref="GameObject"/> by adding a material.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Graphics
{
	using System.Collections.Generic;

	using UnityEngine;

	public class AddMaterialHighlighter : MonoBehaviour
	{
		[SerializeField] private Material material;
		[SerializeField] private Renderer[] renderers;
		
		/// <inheritdoc cref="MonoBehaviour.OnEnable"/>        
		private void OnEnable()
		{
			if (renderers.Length > 0)
			{
				for (int i = 0; i < renderers.Length; i++)
				{
					List<Material> materials = new List<Material>(renderers[i].sharedMaterials);
					materials.Add(material);
					renderers[i].sharedMaterials = materials.ToArray();
				}
			}
		}

		/// <inheritdoc cref="MonoBehaviour.OnDisable"/>   
		private void OnDisable()
		{
			if (renderers.Length > 0)
			{
				for (int i = 0; i < renderers.Length; i++)
				{
					List<Material> materials = new List<Material>(renderers[i].sharedMaterials);
					materials.Remove(material);
					renderers[i].sharedMaterials = materials.ToArray();
				}
			}
		}
		
		/// <inheritdoc cref="MonoBehaviour.Reset"/>
		private void Reset()
		{
			renderers = GetComponents<Renderer>();
		}
	}
}