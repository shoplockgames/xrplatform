﻿// <copyright file="MaterialSwapHighlighter.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a component that highlights a <see cref="GameObject"/> by swapping its material with another material.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Graphics
{
    using System;
    using System.Collections.Generic;

    using UnityEngine;

    using Viacom.XR.Diagnostics;

    /// <summary>
    /// Represents a component that highlights a <see cref="GameObject"/> by swapping its material with another material.
    /// </summary>
    public class MaterialSwapHighlighter : MonoBehaviour
    {
        [SerializeField] private Material material;
        [SerializeField] private Renderer[] renderers;

        private readonly Dictionary<Renderer, Material[]> _originalMaterials = new Dictionary<Renderer, Material[]>();
        private Material[] _materials;

        /// <inheritdoc cref="MonoBehaviour.OnEnable"/>        
        private void OnEnable()
        {
            if (_originalMaterials.Count > 0)
            {
                ErrorReporter.LogHandledException(
                    new Exception("MaterialObjectHighlighter cached materials was not empty on enable."));
                _originalMaterials.Clear();
            }

            if (renderers.Length > 0)
            {
                SwapMaterials();
            }
        }

        /// <inheritdoc cref="MonoBehaviour.OnDisable"/>   
        private void OnDisable()
        {
            foreach (KeyValuePair<Renderer, Material[]> keyValue in _originalMaterials)
            {
                Renderer renderer = keyValue.Key;
                Material[] materials = keyValue.Value;
                renderer.materials = materials;
            }

            _originalMaterials.Clear();
        }

        /// <summary>
        /// Saves the materials on the <see cref="GameObject"/> and replaces them with the new material.
        /// </summary>
        private void SwapMaterials()
        {
            foreach (Renderer render in renderers)
            {
                _materials = render.materials;
                _originalMaterials.Add(render, _materials);
                render.materials = new Material[] {material};
            }
        }
        
        /// <inheritdoc cref="MonoBehaviour.Reset"/>
        private void Reset()
        {
            renderers = GetComponents<Renderer>();
        }
    }
}
