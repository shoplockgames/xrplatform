﻿// <copyright file="OutlineHighlighter.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a component that highlights a <see cref="GameObject"/> by rendering an outline around it.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Graphics
{
    using System.Collections.Generic;
    using System.Linq;
    
    using UnityEngine;

    /// <summary>
    /// Represents a component that highlights a <see cref="GameObject"/> by rendering an outline around it.
    /// </summary>
    public class OutlineHighlighter : MonoBehaviour
    {
        private enum OutlineMode
        {
            OutlineAll,
            OutlineVisible,
            OutlineHidden,
            OutlineAndSilhouette,
            SilhouetteOnly
        }
        
        private static readonly HashSet<Mesh> registeredMeshes = new HashSet<Mesh>();
        
        [SerializeField] private OutlineMode outlineMode;
        [SerializeField] private Color outlineColor = Color.white;
        [SerializeField, Range(0f, 10f)] private float outlineWidth = 2f;
        [SerializeField] private Material fillMaterial;
        [SerializeField] private Material maskMaterial;
        [SerializeField] private Renderer[] renderers;

        private Material _outlineMaskMaterial;
        private Material _outlineFillMaterial;

        private const string OutlineColorPropertyName = "_OutlineColor";
        private const string OutlineWidthPropertyName = "_OutlineWidth";
        private const string OutlineZTestPropertyName = "_ZTest";

        /// <inheritdoc cref="MonoBehaviour.Awake"/>
        private void Awake()
        {
            LoadSmoothNormals();
            _outlineFillMaterial = Instantiate(fillMaterial);
            _outlineMaskMaterial = Instantiate(maskMaterial);
        }
        
        /// <inheritdoc cref="MonoBehaviour.OnEnable"/>
        private void OnEnable()
        {
            UpdateShaderVariables();
            
            if (renderers.Length > 0)
            {
                foreach (Renderer renderer in renderers)
                {
                    List<Material> materials = new List<Material>(renderer.sharedMaterials);
                    materials.Add(_outlineMaskMaterial);
                    materials.Add(_outlineFillMaterial);
                    renderer.materials = materials.ToArray();
                }
            }
        }

        /// <inheritdoc cref="MonoBehaviour.OnDisable"/>
        private void OnDisable()
        {
            if (renderers.Length > 0)
            {
                for (int i = 0; i < renderers.Length; i++)
                {
                    List<Material> materials = new List<Material>(renderers[i].sharedMaterials);
                    materials.Remove(_outlineMaskMaterial);
                    materials.Remove(_outlineFillMaterial);
                    renderers[i].sharedMaterials = materials.ToArray();
                }
            }
        }
        
        /// <inheritdoc cref="MonoBehaviour.OnDestroy"/>
        private void OnDestroy()
        {
            Destroy(_outlineMaskMaterial);
            Destroy(_outlineFillMaterial);
        }
        
        /// <inheritdoc cref="MonoBehaviour.Reset"/>
        private void Reset()
        {
            renderers = GetComponents<Renderer>();
        }

#if UNITY_EDITOR
        /// <inheritdoc cref="MonoBehaviour.OnValidate"/>
        private void OnValidate()
        {
            if (Application.isPlaying && _outlineFillMaterial != null && _outlineMaskMaterial != null)
            {
                UpdateShaderVariables();
            }
        }
#endif
        
        /// <summary>
        /// Retrieve or generate the smoothed normals of all attached meshes.
        /// </summary>
        private void LoadSmoothNormals()
        {
            // Retrieve or generate smooth normals
            foreach (var renderer in renderers)
            {
                MeshFilter meshFilter = renderer.GetComponent<MeshFilter>();
                
                // Skip if smooth normals have already been adopted
                if (meshFilter == null || !registeredMeshes.Add(meshFilter.sharedMesh))
                {
                    continue;
                }

                // Retrieve or generate smooth normals
                List<Vector3> smoothNormals = SmoothNormals(meshFilter.sharedMesh);

                // Store smooth normals in UV3
                meshFilter.sharedMesh.SetUVs(3, smoothNormals);
            }

            // Clear UV3 on skinned mesh renderers
            foreach (var renderer in renderers)
            {
                SkinnedMeshRenderer skinnedMeshRenderer =  renderer as SkinnedMeshRenderer;

                if (skinnedMeshRenderer == null)
                {
                    continue;
                }
                
                if (registeredMeshes.Add(skinnedMeshRenderer.sharedMesh))
                {
                    skinnedMeshRenderer.sharedMesh.uv4 = new Vector2[skinnedMeshRenderer.sharedMesh.vertexCount];
                }
            }
        }

        /// <summary>
        /// Smooth the normals of the specified mesh.
        /// </summary>
        /// <param name="mesh">The mesh to calculate the smoothed normals from.</param>
        /// <returns>A list of the smoothed normals</returns>
        private List<Vector3> SmoothNormals(Mesh mesh)
        {
            // Group vertices by location
            var groups = mesh.vertices.Select((vertex, index) => new KeyValuePair<Vector3, int>(vertex, index))
                .GroupBy(pair => pair.Key);

            // Copy normals to a new list
            List<Vector3> smoothNormals = new List<Vector3>(mesh.normals);

            // Average normals for grouped vertices
            foreach (var group in groups)
            {
                // Skip single vertices
                if (group.Count() == 1)
                {
                    continue;
                }

                // Calculate the average normal
                Vector3 smoothNormal = Vector3.zero;

                foreach (KeyValuePair<Vector3, int> pair in group)
                {
                    smoothNormal += mesh.normals[pair.Value];
                }

                smoothNormal.Normalize();

                // Assign smooth normal to each vertex
                foreach (KeyValuePair<Vector3, int> pair in group)
                {
                    smoothNormals[pair.Value] = smoothNormal;
                }
            }

            return smoothNormals;
        }

        /// <summary>
        /// Updates the shader variables to reflect the local script values.
        /// </summary>
        private void UpdateShaderVariables()
        {
            _outlineFillMaterial.SetColor(OutlineColorPropertyName, outlineColor);

            switch (outlineMode)
            {
                case OutlineMode.OutlineAll:
                    _outlineMaskMaterial.SetFloat(OutlineZTestPropertyName, (float) UnityEngine.Rendering.CompareFunction.Always);
                    _outlineFillMaterial.SetFloat(OutlineZTestPropertyName, (float) UnityEngine.Rendering.CompareFunction.Always);
                    _outlineFillMaterial.SetFloat(OutlineWidthPropertyName, outlineWidth);
                    break;
                case OutlineMode.OutlineVisible:
                    _outlineMaskMaterial.SetFloat(OutlineZTestPropertyName, (float) UnityEngine.Rendering.CompareFunction.Always);
                    _outlineFillMaterial.SetFloat(OutlineZTestPropertyName, (float) UnityEngine.Rendering.CompareFunction.LessEqual);
                    _outlineFillMaterial.SetFloat(OutlineWidthPropertyName, outlineWidth);
                    break;
                case OutlineMode.OutlineHidden:
                    _outlineMaskMaterial.SetFloat(OutlineZTestPropertyName, (float) UnityEngine.Rendering.CompareFunction.Always);
                    _outlineFillMaterial.SetFloat(OutlineZTestPropertyName, (float) UnityEngine.Rendering.CompareFunction.Greater);
                    _outlineFillMaterial.SetFloat(OutlineWidthPropertyName, outlineWidth);
                    break;
                case OutlineMode.OutlineAndSilhouette:
                    _outlineMaskMaterial.SetFloat(OutlineZTestPropertyName, (float) UnityEngine.Rendering.CompareFunction.LessEqual);
                    _outlineFillMaterial.SetFloat(OutlineZTestPropertyName, (float) UnityEngine.Rendering.CompareFunction.Always);
                    _outlineFillMaterial.SetFloat(OutlineWidthPropertyName, outlineWidth);
                    break;
                case OutlineMode.SilhouetteOnly:
                    _outlineMaskMaterial.SetFloat(OutlineZTestPropertyName, (float) UnityEngine.Rendering.CompareFunction.LessEqual);
                    _outlineFillMaterial.SetFloat(OutlineZTestPropertyName, (float) UnityEngine.Rendering.CompareFunction.Greater);
                    _outlineFillMaterial.SetFloat(OutlineWidthPropertyName, 0f);
                    break;
            }
        }
    }
}