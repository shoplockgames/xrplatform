﻿// <copyright file="OutlineObjectHighlighter.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a component that highlights a <see cref="GameObject"/> by rendering a copy it's meshes using a different material.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Graphics
{
	using UnityEngine;

	/// <summary>
	/// Represents a component that highlights a <see cref="GameObject"/> by rendering a copy it's meshes using a different material.
	/// </summary>
	public class OutlineObjectHighlighter : MonoBehaviour
	{
		[SerializeField] private Material material;
		[SerializeField] private MeshRenderer[] meshRenderers;
		[SerializeField] private SkinnedMeshRenderer[] skinnedMeshRenderers;

		private GameObject _highlightGameObject;
		private MeshRenderer[] _highlightRenderers;
		private SkinnedMeshRenderer[] _highlightSkinnedRenderers;

		private const string HighlightNameSuffix = " Highlighter";

		/// <inheritdoc cref="MonoBehaviour.OnEnable"/>   
		private void OnEnable()
		{
			if (_highlightGameObject == null && (meshRenderers.Length > 0 || skinnedMeshRenderers.Length > 0))
			{
				CreateHighlightRenderers();
			}
		}

		/// <inheritdoc cref="MonoBehaviour.OnDisable"/>   
		private void OnDisable()
		{
			if (_highlightGameObject != null)
			{
				Destroy(_highlightGameObject);
			}
		}

		/// <inheritdoc cref="MonoBehaviour.Update"/>   
		private void Update()
		{
			UpdateHighlightRenderers();
		}

		/// <summary>
		/// Creates a new game object with copies of the attached game object's <see cref="MeshRenderer"/> and <see cref="SkinnedMeshRenderer"/> components.
		/// </summary>
		private void CreateHighlightRenderers()
		{
			// Create the copy game object
			_highlightGameObject = new GameObject(gameObject.name + HighlightNameSuffix);

			// Now copy all the skinned mesh renders
			_highlightSkinnedRenderers = new SkinnedMeshRenderer[skinnedMeshRenderers.Length];
			for (int i = 0; i < skinnedMeshRenderers.Length; i++)
			{
				SkinnedMeshRenderer skinnedMeshRenderer = skinnedMeshRenderers[i];
				if (skinnedMeshRenderer == null)
				{
					continue;
				}
				
				GameObject highlightedSkinnedMeshGameObject =
					new GameObject(skinnedMeshRenderer.gameObject.name + HighlightNameSuffix);
				highlightedSkinnedMeshGameObject.transform.parent = _highlightGameObject.transform;
				SkinnedMeshRenderer highlighterSkinnedMeshRenderer =
					highlightedSkinnedMeshGameObject.AddComponent<SkinnedMeshRenderer>();
				Material[] materials = new Material[skinnedMeshRenderer.sharedMaterials.Length];
				for (int materialIndex = 0; materialIndex < materials.Length; materialIndex++)
				{
					materials[materialIndex] = material;
				}

				highlighterSkinnedMeshRenderer.sharedMaterials = materials;
				highlighterSkinnedMeshRenderer.sharedMesh = skinnedMeshRenderer.sharedMesh;
				highlighterSkinnedMeshRenderer.rootBone = skinnedMeshRenderer.rootBone;
				highlighterSkinnedMeshRenderer.updateWhenOffscreen = skinnedMeshRenderer.updateWhenOffscreen;
				highlighterSkinnedMeshRenderer.bones = skinnedMeshRenderer.bones;
				_highlightSkinnedRenderers[i] = highlighterSkinnedMeshRenderer;
			}

			// Finally copy all the mesh renders
			_highlightRenderers = new MeshRenderer[meshRenderers.Length];
			for (int i = 0; i < meshRenderers.Length; i++)
			{
				MeshRenderer meshRenderer = meshRenderers[i];
				MeshFilter meshFilter = meshRenderer.GetComponent<MeshFilter>();
				if (meshFilter == null || meshRenderer == null)
				{
					continue;
				}

				GameObject highlightedMeshGameObject =
					new GameObject(meshRenderer.gameObject.name + HighlightNameSuffix);
				highlightedMeshGameObject.transform.parent = _highlightGameObject.transform;
				MeshFilter newFilter = highlightedMeshGameObject.AddComponent<MeshFilter>();
				newFilter.sharedMesh = meshFilter.sharedMesh;
				MeshRenderer newRenderer = highlightedMeshGameObject.AddComponent<MeshRenderer>();
				Material[] materials = new Material[meshRenderer.sharedMaterials.Length];
				for (int materialIndex = 0; materialIndex < materials.Length; materialIndex++)
				{
					materials[materialIndex] = material;
				}

				newRenderer.sharedMaterials = materials;

				_highlightRenderers[i] = newRenderer;
			}
		}

		/// <summary>
		/// Updates the position, orientation, scale and visibility of all the renderer copies.
		/// </summary>
		private void UpdateHighlightRenderers()
		{
			if (_highlightGameObject == null)
			{
				return;
			}

			for (int i = 0; i < skinnedMeshRenderers.Length; i++)
			{
				SkinnedMeshRenderer skinnedMeshRenderer = skinnedMeshRenderers[i];
				SkinnedMeshRenderer highlightSkinnedMeshRenderer = _highlightSkinnedRenderers[i];

				if (skinnedMeshRenderer != null && highlightSkinnedMeshRenderer != null)
				{
					highlightSkinnedMeshRenderer.transform.position = skinnedMeshRenderer.transform.position;
					highlightSkinnedMeshRenderer.transform.rotation = skinnedMeshRenderer.transform.rotation;
					highlightSkinnedMeshRenderer.transform.localScale = skinnedMeshRenderer.transform.lossyScale;
					highlightSkinnedMeshRenderer.localBounds = skinnedMeshRenderer.localBounds;
					highlightSkinnedMeshRenderer.enabled =
						skinnedMeshRenderer.enabled && skinnedMeshRenderer.gameObject.activeInHierarchy;

					int blendShapeCount = skinnedMeshRenderer.sharedMesh.blendShapeCount;
					for (int blendShapeIndex = 0; blendShapeIndex < blendShapeCount; blendShapeIndex++)
					{
						highlightSkinnedMeshRenderer.SetBlendShapeWeight(blendShapeIndex,
							skinnedMeshRenderer.GetBlendShapeWeight(blendShapeIndex));
					}
				}
				else if (highlightSkinnedMeshRenderer != null)
				{
					highlightSkinnedMeshRenderer.enabled = false;
				}
			}

			for (int i = 0; i < _highlightRenderers.Length; i++)
			{
				MeshRenderer meshRenderer = meshRenderers[i];
				MeshRenderer highlightMeshRenderer = _highlightRenderers[i];

				if (meshRenderer != null && highlightMeshRenderer != null)
				{
					highlightMeshRenderer.transform.position = meshRenderer.transform.position;
					highlightMeshRenderer.transform.rotation = meshRenderer.transform.rotation;
					highlightMeshRenderer.transform.localScale = meshRenderer.transform.lossyScale;
					highlightMeshRenderer.enabled = meshRenderer.enabled && meshRenderer.gameObject.activeInHierarchy;
				}
				else if (highlightMeshRenderer != null)
				{
					highlightMeshRenderer.enabled = false;
				}
			}
		}

		/// <inheritdoc cref="MonoBehaviour.Reset"/>
		private void Reset()
		{
			meshRenderers = GetComponents<MeshRenderer>();
			skinnedMeshRenderers = GetComponents<SkinnedMeshRenderer>();
		}
	}
}