﻿// <copyright file="CachedDictionary.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a collection of keys and values that are persisted to local storage.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.IO
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    using UnityEngine;

    /// <summary>
    /// Represents a collection of keys and values that can be serialized.
    /// </summary>
    /// <typeparam name="TKey">The type of the keys in the dictionary.</typeparam>
    /// <typeparam name="TValue">The type of the values in the dictionary.</typeparam>
    [Serializable]
    public abstract class JsonDictionary<TKey, TValue> : ISerializationCallbackReceiver
    {
        [SerializeField] private List<TKey> keys = new List<TKey>();
        [SerializeField] private List<TValue> values = new List<TValue>();

        public Dictionary<TKey, TValue> dictionary = new Dictionary<TKey, TValue>();

        #region ISerializationCallbackReceiver implementation

        /// <summary>
        /// Prepares the <see cref="JsonDictionary{TKey,TValue}"/> for serialization.
        /// </summary>
        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            keys.Clear();
            values.Clear();
            foreach (KeyValuePair<TKey, TValue> keyValuePair in dictionary)
            {
                keys.Add(keyValuePair.Key);
                values.Add(keyValuePair.Value);
            }
        }

        /// <summary>
        /// Prepares the <see cref="JsonDictionary{TKey,TValue}"/> after deserialization.
        /// </summary>
        /// <exception cref="InvalidOperationException">The numbers of keys does not match the number of values.</exception>
        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            dictionary.Clear();

            if (keys.Count != values.Count)
            {
                // unity can't tell the difference between an array with a single empty element or an empty array :-/
                if (keys.Count == 1 && values.Count == 0)
                {
                    values.Add(default(TValue));
                }
                else if (values.Count == 1 && keys.Count == 0)
                {
                    keys.Add(default(TKey));
                }
                else
                {
                    throw new InvalidOperationException("Key length doesn't matched saved values length");
                }
            }

            for (int i = 0; i < keys.Count; i++)
            {
                if (!dictionary.TryAdd(keys[i], values[i]))
                {
                    Debug.LogWarning("Duplicate keys saved. Ignoring second");
                }
            }
        }

        #endregion
    }

    /// <summary>
    /// Represents a collection of keys and values that are persisted to local storage using <see cref="JsonUtility"/>.
    /// </summary>
    /// <typeparam name="TKey">The type of the keys in the dictionary.</typeparam>
    /// <typeparam name="TValue">The type of the values in the dictionary.</typeparam>
    public class CachedDictionary<TKey, TValue> : CachedValue, IDictionary<TKey, TValue>
    {
        /// <summary>
        /// Represents a collection of keys and values that are persisted to local storage using <see cref="JsonUtility"/>.
        /// </summary>
        [Serializable]
        public class CachedJsonDictionary : JsonDictionary<TKey, TValue>
        {
        }

        private CachedJsonDictionary _jsonDictionary;

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedDictionary{TKey,TValue}"/> class with a specified key.
        /// </summary>
        /// <param name="cacheKey">The key for this value in local storage.</param>
        public CachedDictionary(string cacheKey) : base(cacheKey)
        {
        }

        /// <summary>
        /// Converts the specified string representation of a value to its typed equivalent and returns a value that indicates whether the conversion succeeded.
        /// </summary>
        /// <returns><c>true</c>, if <paramref name="value"/> was converted successfully; otherwise, <c>false</c>.</returns>
        /// <param name="value">A string containing the value to convert.</param>
        protected override bool TryParseValueFromCacheString(string value)
        {
            _jsonDictionary = JsonUtility.FromJson<CachedJsonDictionary>(value);
            return _jsonDictionary != null;
        }

        /// <summary>
        /// Sets the <see cref="CachedDictionary{TKey,TValue}"/> to its default empty state.
        /// </summary>
        protected override void SetValueToDefault()
        {
            _jsonDictionary = new CachedJsonDictionary();
        }

        /// <summary>
        /// Get a string that can be persisted to local storage from the in-memory value.
        /// </summary>
        /// <returns>The string representation of the in-memory value.</returns>
        protected override string GetCacheStringFromValue()
        {
            return JsonUtility.ToJson(_jsonDictionary);
        }

        #region IDictionary implementation

        /// <summary>
        /// Adds an element with the provided key and value to the <see cref="CachedDictionary{TKey,TValue}"/>.
        /// </summary>
        /// <param name="key">The key of the element to add.</param>
        /// <param name="value">The value of the element to add. The value can be null for reference types.</param>
        public void Add(TKey key, TValue value)
        {
            LoadFromDisk();
            _jsonDictionary.dictionary.Add(key, value);
            SaveToDisk();
        }

        /// <summary>
        /// Determines whether the <see cref="CachedDictionary{TKey,TValue}"/> contains the specified key.
        /// </summary>
        /// <param name="key">The key to locate in the <see cref="CachedDictionary{TKey,TValue}"/>.</param>
        /// <returns><c>true</c> if the <see cref="CachedDictionary{TKey,TValue}"/> contains an element with the specified <paramref name="key"/>; otherwise, <c>false</c>.</returns>
        public bool ContainsKey(TKey key)
        {
            LoadFromDisk();
            return _jsonDictionary.dictionary.ContainsKey(key);
        }

        /// <summary>
        /// Removes the value with the specified key from the <see cref="CachedDictionary{TKey,TValue}"/>.
        /// </summary>
        /// <param name="key">The key of the element to remove.</param>
        /// <returns><c>true</c> if the element is successfully found and removed; otherwise, <c>false</c>. 
        /// This method returns <c>false</c> if key is not found in the <see cref="CachedDictionary{TKey,TValue}"/>.</returns>
        public bool Remove(TKey key)
        {
            LoadFromDisk();
            bool didRemove = _jsonDictionary.dictionary.Remove(key);
            SaveToDisk();
            return didRemove;
        }

        /// <summary>
        /// Gets the value associated with the specified key.
        /// </summary>
        /// <param name="key">The key of the value to get.</param>
        /// <param name="value">When this method returns, contains the value associated with the specified key, if the key is found; 
        /// otherwise, the default value for the type of the value parameter. This parameter is passed uninitialized.</param>
        /// <returns><c>true</c> if the <see cref="CachedDictionary{TKey,TValue}"/> contains an element with the specified key; otherwise, <c>false</c>.</returns>
        public bool TryGetValue(TKey key, out TValue value)
        {
            LoadFromDisk();
            return _jsonDictionary.dictionary.TryGetValue(key, out value);
        }

        /// <summary>
        /// Gets or sets the value associated with the specified key.
        /// </summary>
        /// <param name="key">The key of the value to get or set.</param>
        /// <returns>The element with the specified key, or <c>null</c> if the key does not exist.</returns>
        public TValue this[TKey key]
        {
            get
            {
                LoadFromDisk();
                return _jsonDictionary.dictionary[key];
            }
            set
            {
                LoadFromDisk();
                _jsonDictionary.dictionary[key] = value;
                SaveToDisk();
            }
        }

        /// <summary>
        /// Gets a collection containing the keys in the <see cref="CachedDictionary{TKey,TValue}"/>.
        /// </summary>
        public ICollection<TKey> Keys
        {
            get
            {
                LoadFromDisk();
                return _jsonDictionary.dictionary.Keys;
            }
        }

        /// <summary>
        /// Gets a collection containing the values in the <see cref="CachedDictionary{TKey,TValue}"/>.
        /// </summary>
        public ICollection<TValue> Values
        {
            get
            {
                LoadFromDisk();
                return _jsonDictionary.dictionary.Values;
            }
        }

        #endregion

        #region ICollection implementation

        /// <summary>
        /// Adds the specified value to the <see cref="CachedDictionary{TKey,TValue}"/> with the specified key.
        /// </summary>
        /// <param name="item">The <see cref="KeyValuePair{TKey,TValue}"/> structure representing the key and value to add to the <see cref="CachedDictionary{TKey,TValue}"/>.</param>
        public void Add(KeyValuePair<TKey, TValue> item)
        {
            Add(item.Key, item.Value);
        }

        /// <summary>
        /// Removes all keys and values from the<see cref="CachedDictionary{TKey,TValue}"/>.
        /// </summary>
        public void Clear()
        {
            LoadFromDisk();
            _jsonDictionary.dictionary.Clear();
            SaveToDisk();
        }

        /// <summary>
        /// Determines whether the <see cref="CachedDictionary{TKey,TValue}"/> contains a specific key and value.
        /// </summary>
        /// <param name="item">The <see cref="KeyValuePair{TKey,TValue}"/> structure to locate in the <see cref="CachedDictionary{TKey,TValue}"/>.</param>
        /// <returns><c>true</c> if item is found in the <see cref="CachedDictionary{TKey,TValue}"/>; otherwise, <c>false</c>.</returns>
        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            TValue containedItem;
            return TryGetValue(item.Key, out containedItem) && containedItem.Equals(item);
        }

        /// <summary>
        /// Copies the elements of the <see cref="CachedDictionary{TKey,TValue}"/> to an array of type <see cref="KeyValuePair{TKey,TValue}"/>, starting at the specified array index.
        /// </summary>
        /// <param name="array">The one-dimensional array of type <see cref="KeyValuePair{TKey,TValue}"/> that is the destination of the <see cref="KeyValuePair{TKey,TValue}"/> elements copied from the <see cref="CachedDictionary{TKey,TValue}"/>.
        /// The array must have zero-based indexing.</param>
        /// <param name="index">The zero-based index in the array at which copying begins.</param>
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int index)
        {
            LoadFromDisk();
            foreach (KeyValuePair<TKey, TValue> keyValuePair in _jsonDictionary.dictionary)
            {
                array[index] = keyValuePair;
                index++;
            }
        }

        /// <summary>
        /// Removes a key and value from the <see cref="CachedDictionary{TKey,TValue}"/>.
        /// </summary>
        /// <param name="item">The<see cref="KeyValuePair{TKey,TValue}"/> structure representing the key and value to remove from the <see cref="CachedDictionary{TKey,TValue}"/>.</param>
        /// <returns><c>true</c> if the key and value represented by item is successfully found and removed; otherwise, <c>false</c>. 
        /// This method returns <c>false</c> if item is not found in the <see cref="CachedDictionary{TKey,TValue}"/>.</returns>
        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            return Contains(item) && Remove(item.Key);
        }

        /// <summary>
        /// Gets the number of key/value pairs contained in the <see cref="CachedDictionary{TKey,TValue}"/>.
        /// </summary>
        public int Count
        {
            get
            {
                LoadFromDisk();
                return _jsonDictionary.dictionary.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="CachedDictionary{TKey,TValue}"/> object is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        #endregion

        #region IEnumerable implementation

        /// <summary>
        /// Returns an enumerator that iterates through the <see cref="CachedDictionary{TKey,TValue}"/>.
        /// </summary>
        /// <returns>An enumerator that can be used to iterate through the <see cref="CachedDictionary{TKey,TValue}"/>.</returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            LoadFromDisk();
            return _jsonDictionary.dictionary.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through the <see cref="CachedDictionary{TKey,TValue}"/>.
        /// </summary>
        /// <returns>An <c>IEnumerator</c> that can be used to iterate through the <see cref="CachedDictionary{TKey,TValue}"/>.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
