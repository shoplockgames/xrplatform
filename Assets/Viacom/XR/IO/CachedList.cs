﻿// <copyright file="CachedList.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a strongly typed list of objects that are persisted to local storage using <c>JsonUtility</c>.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.IO
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    using UnityEngine;

    /// <summary>
    /// Represents a strongly typed list of objects that can be serialized.
    /// </summary>
    /// <remarks>Since Unity's serializer does not support generics, this class must be overriden.</remarks>
    /// <typeparam name="T">The type of elements in the list.</typeparam>
    [Serializable]
    public abstract class JsonList<T>
    {
        public List<T> list = new List<T>();
    }

    /// <summary>
    /// Represents a strongly typed list of objects that are persisted to local storage using <c>JsonUtility</c>.
    /// </summary>
    /// <typeparam name="T">The type of elements in the list.</typeparam>
    public class CachedList<T> : CachedValue, IList<T>
    {
        /// <summary>
        /// Represents a strongly typed list of objects that can be serialized to a string using <c>JsonUtility</c>.
        /// </summary>
        [Serializable]
        public class CachedJsonList : JsonList<T>
        {
        }

        private CachedJsonList _jsonList;

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedList{T}"/> class with a specified key.
        /// </summary>
        /// <param name="cacheKey">The key for this value in local storage.</param>
        public CachedList(string cacheKey) : base(cacheKey)
        {
        }

        /// <summary>
        /// Converts the specified string representation of a value to its typed equivalent and returns a value that indicates whether the conversion succeeded.
        /// </summary>
        /// <returns><c>true</c>, if <paramref name="cacheString"/> was converted successfully; otherwise,<c>false</c>.</returns>
        /// <param name="cacheString">A string containing the value to convert.</param>
        protected override bool TryParseValueFromCacheString(string cacheString)
        {
            _jsonList = JsonUtility.FromJson<CachedJsonList>(cacheString);
            return _jsonList != null;
        }

        /// <summary>
        /// Sets the <see cref="CachedList{T}"/> to its default empty state.
        /// </summary>
        protected override void SetValueToDefault()
        {
            _jsonList = new CachedJsonList();
        }

        /// <summary>
        /// Get a string that can be persisted to local storage from the in-memory value.
        /// </summary>
        /// <returns>The string representation of the in-memory value.</returns>
        protected override string GetCacheStringFromValue()
        {
            return JsonUtility.ToJson(_jsonList);
        }

        /// <summary>
        /// Adds the elements of the specified collection to the end of the <see cref="CachedList{T}"/>.
        /// </summary>
        /// <param name="collection">The collection whose elements should be added to the end of the <see cref="CachedList{T}"/>.</param>
        public void AddRange(IEnumerable<T> collection)
        {
            LoadFromDisk();
            _jsonList.list.AddRange(collection);
            SaveToDisk();
        }

        /// <summary>
        /// Inserts the elements of a collection into the <see cref="CachedList{T}"/> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which the new elements should be inserted.</param>
        /// <param name="collection">The collection whose elements should be inserted into the <see cref="CachedList{T}"/>.</param>
        public void InsertRange(int index, IEnumerable<T> collection)
        {
            LoadFromDisk();
            _jsonList.list.InsertRange(index, collection);
            SaveToDisk();
        }

        /// <summary>
        /// Removes a range of elements from the <see cref="CachedList{T}"/>.
        /// </summary>
        /// <param name="index">The zero-based starting index of the range of elements to remove.</param>
        /// <param name="count">The number of elements to remove.</param>
        public void RemoveRange(int index, int count)
        {
            LoadFromDisk();
            _jsonList.list.RemoveRange(index, count);
            SaveToDisk();
        }

        /// <summary>
        /// Copies the elements of the <see cref="CachedList{T}"/> to a new array.
        /// </summary>
        /// <returns>An array containing copies of the elements of the <see cref="CachedList{T}"/>.</returns>
        public T[] ToArray()
        {
            LoadFromDisk();
            return _jsonList.list.ToArray();
        }

        #region IList implementation

        /// <summary>
        /// Searches for the specified object and returns the zero-based index of the first occurrence within the entire <see cref="CachedList{T}"/>.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="CachedList{T}"/>. The value can be <c>null</c> for reference types.</param>
        /// <returns>The zero-based index of the first occurrence of <paramref name="item"/> within the entire <see cref="CachedList{T}"/>, if found; otherwise, -1.</returns>
        public int IndexOf(T item)
        {
            LoadFromDisk();
            return _jsonList.list.IndexOf(item);
        }

        /// <summary>
        /// Inserts an element into the <see cref="CachedList{T}"/> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="item"/> should be inserted.</param>
        /// <param name="item">The object to insert.</param>
        public void Insert(int index, T item)
        {
            LoadFromDisk();
            _jsonList.list.Insert(index, item);
            SaveToDisk();
        }

        /// <summary>
        /// Removes the element at the specified index of the <see cref="CachedList{T}"/>.
        /// </summary>
        /// <param name="index">The zero-based index of the element to remove.</param>
        public void RemoveAt(int index)
        {
            LoadFromDisk();
            _jsonList.list.RemoveAt(index);
            SaveToDisk();
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public T this[int index]
        {
            get
            {
                LoadFromDisk();
                return _jsonList.list[index];
            }
            set
            {
                LoadFromDisk();
                _jsonList.list[index] = value;
                SaveToDisk();
            }
        }

        #endregion

        #region ICollection implementation

        /// <summary>
        /// Adds an object to the end of the <see cref="CachedList{T}"/>.
        /// </summary>
        /// <param name="item">The object to be added to the end of the <see cref="CachedList{T}"/>.</param>
        public void Add(T item)
        {
            LoadFromDisk();
            _jsonList.list.Add(item);
            SaveToDisk();
        }

        /// <summary>
        /// Removes all elements from the <see cref="CachedList{T}"/>.
        /// </summary>
        public void Clear()
        {
            LoadFromDisk();
            _jsonList.list.Clear();
            SaveToDisk();
        }

        /// <summary>
        /// Determines whether an element is in the <see cref="CachedList{T}"/>.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="CachedList{T}"/>.</param>
        /// <returns><c>true</c> if item is found in the <see cref="CachedList{T}"/>; otherwise, <c>false</c>.</returns>
        public bool Contains(T item)
        {
            LoadFromDisk();
            return _jsonList.list.Contains(item);
        }

        /// <summary>
        /// Copies the entire <see cref="CachedList{T}"/> to a compatible one-dimensional array, starting at the specified index of the target array.
        /// </summary>
        /// <param name="array">The one-dimensional array that is the destination of the elements copied from List<T>. </param>
        /// <param name="arrayIndex">The zero-based index in <paramref name="array"/> at which copying begins.</param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            LoadFromDisk();
            _jsonList.list.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="CachedList{T}"/>.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="CachedList{T}"/>. The value can be null for reference types.</param>
        /// <returns><c>true</c> if <paramref name="item"/> is successfully removed; otherwise, <c>false</c>. This method also returns <c>false</c> if item was not found in the <see cref="CachedList{T}"/>.</returns>
        public bool Remove(T item)
        {
            LoadFromDisk();
            bool didRemove = _jsonList.list.Remove(item);
            if (didRemove)
            {
                SaveToDisk();
            }

            return didRemove;
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="CachedList{T}"/>.
        /// </summary>
        public int Count
        {
            get
            {
                LoadFromDisk();
                return _jsonList.list.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="CachedList{T}"/> object is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        #endregion

        #region IEnumerable implementation

        /// <summary>
        /// Returns an enumerator that iterates through the <see cref="CachedList{T}"/>.
        /// </summary>
        /// <typeparam name="T">The type of elements in the list.</typeparam>
        /// <returns>An enumerator that can be used to iterate through the <see cref="CachedList{T}"/>.</returns>
        public IEnumerator<T> GetEnumerator()
        {
            LoadFromDisk();
            return _jsonList.list.GetEnumerator();
        }

        #endregion

        #region IEnumerable implementation

        /// <summary>
        /// Returns an enumerator that iterates through the <see cref="CachedList{T}"/>.
        /// </summary>
        /// <returns>An <c>IEnumerator</c> that can be used to iterate through the <see cref="CachedList{T}"/>.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            LoadFromDisk();
            return _jsonList.list.GetEnumerator();
        }

        #endregion
    }
}
