﻿// <copyright file="CachedValue.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// When overridden in a derived form, provides functionality for objects to be easily persisted to local storage.
// Upon initial access the value will be read from local storage. Changes to the value are immediately persisted to local storage.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.IO
{
    using System;

    using UnityEngine;

    using Viacom.XR.Diagnostics;
    using Viacom.XR.Serialization;

    /// <summary>
    /// Represents an object that is persisted to local storage.
    /// </summary>
    public abstract class CachedValue
    {
        protected bool HaveReadFromDisk { get; private set; }
        private bool _hasSetValue;

        protected readonly string _cacheKey;

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedValue"/> class with a specified key.
        /// </summary>
        /// <param name="cacheKey">The key for this value in local storage.</param>
        public CachedValue(string cacheKey)
        {
            _cacheKey = cacheKey;
        }

        /// <summary>
        /// Gets whether or not a value has been set.
        /// </summary>
        public bool HasSetValue
        {
            get
            {
                if (!_hasSetValue && !HaveReadFromDisk)
                {
                    // if value hasn't been set yet, try read from disk
                    LoadFromDisk();
                }

                return _hasSetValue;
            }
        }

        /// <summary>
        /// Loads the value from storage.
        /// </summary>
        /// <param name="forceReload"><c>true</c> if the value should be loaded from storage; 
        /// otherwise the in-memory value is returned if the value has ever been read from disk.</param>
        public void LoadFromDisk(bool forceReload = false)
        {
            if (!forceReload && HaveReadFromDisk)
            {
                return;
            }

            string cacheString = ReadString(_cacheKey);

            bool didParse = false;
            if (!string.IsNullOrEmpty(cacheString))
            {
                // try parse json
                try
                {
                    didParse = TryParseValueFromCacheString(cacheString);
                }
                catch (Exception e)
                {
                    LogValueParseException(e, cacheString);
                    didParse = false;
                }
            }

            if (!didParse)
            {
                SetValueToDefault();
            }

            HaveReadFromDisk = true;
            _hasSetValue = didParse;
        }

        /// <summary>
        /// Logs an exception that was thrown while attempting to parse the specified value from local storage.
        /// </summary>
        /// <param name="e">The exception that was thrown.</param>
        /// <param name="value">The value that was the cause of the exception.</param>
        protected virtual void LogValueParseException(Exception e, string value)
        {
            ErrorReporter.LeaveBreadcrumb("Failed to parse cached value: \"" + value + "\"" );
            ErrorReporter.LogHandledException(e);
        }

        /// <summary>
        /// Saves the current value in-memory to local storage.
        /// </summary>
        public void SaveToDisk()
        {
            string cacheString = GetCacheStringFromValue();
            SaveString(_cacheKey, cacheString);
            HaveReadFromDisk = true;
            _hasSetValue = true;
        }

        /// <summary>
        /// Converts the specified string representation of a value to its typed equivalent and returns a value that indicates whether the conversion succeeded.
        /// </summary>
        /// <returns><c>true</c>, if <paramref name="cacheString"/> was converted successfully; otherwise,<c>false</c>.</returns>
        /// <param name="cacheString">A string containing the value to convert.</param>
        protected abstract bool TryParseValueFromCacheString(string cacheString);

        /// <summary>
        /// Sets the underlying value to its default state.
        /// </summary>
        protected abstract void SetValueToDefault();

        /// <summary>
        /// Get a string that can be persisted to local storage from the in-memory value.
        /// </summary>
        /// <returns>The string representation of the in-memory value.</returns>
        protected abstract string GetCacheStringFromValue();

        /// <summary>
        /// Reads the underlying value from local storage.
        /// </summary>
        /// <param name="key">The key of the value to read.</param>
        /// <returns>The underlying value from local storage.</returns>
        protected virtual string ReadString(string key)
        {
            return LocalStorage.ReadString(key);
        }

        /// <summary>
        /// Saves the specified value to local storage.
        /// </summary>
        /// <param name="key">The key of the value to save.</param>
        /// <param name="value">The value to save.</param>
        protected virtual void SaveString(string key, string value)
        {
            LocalStorage.SaveString(key, value);
        }
    }

    /// <summary>
    /// Represents a type that is persisted to local storage.
    /// </summary>
    /// <typeparam name="T">The underlying value type of the <see cref="CachedValue{T}"/> generic type.</typeparam>
    public class CachedValue<T> : CachedValue
    {
        private const string _EMPTY_JSON = "{}";

        private T _value;

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedValue{T}"/> class with a specified key.
        /// </summary>
        /// <param name="cacheKey">The key for this value in local storage.</param>
        public CachedValue(string cacheKey) : base(cacheKey)
        {
        }

        /// <summary>
        /// Gets or sets the underlying value of the current <see cref="CachedValue{T}"/> object.
        /// </summary>
        public T Value
        {
            get
            {
                if (!HaveReadFromDisk)
                {
                    LoadFromDisk();
                }
                return _value;
            }
            set
            {
                _value = value;
                SaveToDisk();
            }
        }

        /// <summary>
        /// Converts the specified string representation of a value to its typed equivalent and returns a value that indicates whether the conversion succeeded.
        /// </summary>
        /// <returns><c>true</c>, if <paramref name="cacheString"/> was converted successfully; otherwise,<c>false</c>.</returns>
        /// <param name="cacheString">A string containing the value to convert.</param>
        protected override bool TryParseValueFromCacheString(string cacheString)
        {
            _value = JsonUtility.FromJson<T>(cacheString);
            return true;
        }

        /// <summary>
        /// Sets the underlying value to it's default state.
        /// </summary>
        protected override void SetValueToDefault()
        {
            _value = default(T);
        }

        /// <summary>
        /// Get a cacheable string from the run-time value
        /// </summary>
        /// <returns>The cache string from value.</returns>
        protected override string GetCacheStringFromValue()
        {
            string cacheString = JsonUtility.ToJson(_value);
            if (cacheString.Equals(_EMPTY_JSON))
            {
                throw new MalformedJsonException("Failed To parse JSON for type : " + typeof(T));
            }
            return cacheString;
        }
    }
}
