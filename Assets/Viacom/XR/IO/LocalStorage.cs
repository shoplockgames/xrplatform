﻿// <copyright file="LocalStorage.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// A static wrapper around the persistent local storage for an application.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.IO
{
    using UnityEngine;

    /// <summary>
    /// Represents the application's persistent local storage.
    /// </summary>
    public static class LocalStorage
    {
        // Use PlayerPrefs by default
        private static ILocalStorageProvider _localStorageProvider = new DefaultLocalStorageProvider();

        /// <summary>
        /// Gets the current local storage provider.
        /// </summary>
        public static ILocalStorageProvider LocalStorageProvider
        {
            get { return _localStorageProvider; }
        }

        /// <summary>
        /// Initializes the local storage wrapper with the specified <see cref="ILocalStorageProvider"/>.
        /// </summary>
        /// <param name="provider">The <see cref="ILocalStorageProvider"/> to use for local storage.</param>
        public static void Init(ILocalStorageProvider provider)
        {
            _localStorageProvider = provider;
        }

        /// <summary>
        /// Reads the value with the given key from local storage.
        /// </summary>
        /// <param name="key">The key of the value to read.</param>
        /// <returns>The value with the specified key.</returns>
        public static string ReadString(string key)
        {
            return _localStorageProvider.ReadString(key);
        }

        /// <summary>
        /// Saves the specied value with the specified key to local storage.
        /// </summary>
        /// <param name="key">The key of the value to save.</param>
        /// <param name="value">The value to save.</param>
        public static void SaveString(string key, string value)
        {
            _localStorageProvider.SaveString(key, value);
        }

        /// <summary>
        /// Writes all data to disk.
        /// </summary>
        public static void Save()
        {
            _localStorageProvider.Save();
        }
    }

    /// <summary>
    /// Represents a service that can read and write to local storage.
    /// </summary>
    public interface ILocalStorageProvider
    {
        /// <summary>
        /// Reads the value with the given key from local storage.
        /// </summary>
        /// <param name="key">The key of the value to read.</param>
        /// <returns>The value with the specified key.</returns>
        string ReadString(string key);

        /// <summary>
        /// Saves the specified value with the specified key to local storage.
        /// </summary>
        /// <param name="key">The key of the value to save.</param>
        /// <param name="value">The value to save.</param>
        void SaveString(string key, string value);

        /// <summary>
        /// Writes all data to disk.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// Represents a local storage provider that utilizes <see cref="PlayerPrefs"/>.
    /// </summary>
    public class DefaultLocalStorageProvider : ILocalStorageProvider
    {
        /// <summary>
        /// Reads the value with the given key from <see cref="PlayerPrefs"/>.
        /// </summary>
        /// <param name="key">The key of the value to read.</param>
        /// <returns>The value with the specified key.</returns>
        public string ReadString(string key)
        {
            return PlayerPrefs.GetString(key);
        }

        /// <summary>
        /// Saves the specified value with the specified key to <see cref="PlayerPrefs"/>.
        /// </summary>
        /// <param name="key">The key of the value to save.</param>
        /// <param name="value">The value to save.</param>
        public void SaveString(string key, string value)
        {
            PlayerPrefs.SetString(key, value);
        }

        /// <summary>
        /// Writes all data to disk.
        /// </summary>
        public void Save()
        {
            PlayerPrefs.Save();
        }
    }
}
