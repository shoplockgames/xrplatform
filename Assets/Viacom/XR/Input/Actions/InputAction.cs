﻿// <copyright file="InputAction.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
//	Represents a set of methods that are invoked when a combination of input gestures are either started, canceled or
//  performed. Multiple input combinations can be mapped to the same input action. Once one of these input combinations 
//  is started that is the only combination that can be performed or canceled. All input from other combinations will 
//  be ignored. Only after that input combination is either performed or canceled can another input combination then be
//  started.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Actions
{
	using System;
	using System.Collections.Generic;

	using UnityEngine;

	using Viacom.XR.Input.Controls;
	using Viacom.XR.Input.Devices;
	using Viacom.XR.Input.Gestures;

	/// <summary>
	/// Represents a set of methods that are invoked when a combination of input gestures are either started, canceled
	/// or performed.
	/// </summary>
	[Serializable]
	public class InputAction : InputEventRaiser, ISerializationCallbackReceiver
	{
		[SerializeField] private string name;
		[SerializeField] [ReadOnly] private string id;
		[SerializeField] private InputMapCombination[] inputMapCombinations;

		[NonSerialized] private InputMapCombination _activeCombination = null;

		/// <summary>
		/// Gets the name of this <see cref="InputAction"/>.
		/// </summary>
		public string Name
		{
			get { return name; }
		}

		/// <summary>
		/// Gets the identifier for this <see cref="InputAction"/>.
		/// </summary>
		/// <remarks>All identifiers are GUIDs.</remarks>
		public string Id
		{
			get { return id; }
			internal set { id = value; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="InputAction"/> class.
		/// </summary>
		private InputAction()
		{
			_activeCombination = null;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="InputAction"/> class with the specified name.
		/// </summary>
		/// <param name="name">The name of the <see cref="InputAction"/>.</param>
		private InputAction(string name) : this()
		{
			this.name = name;
			id = Guid.NewGuid().ToString();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="InputAction"/> class with the specified name, <see cref="InputControl"/> binding and <see cref="Gesture"/>.
		/// </summary>
		/// <param name="name">The name of the <see cref="InputAction"/>.</param>
		/// <param name="binding">The <see cref="InputControl"/> binding.</param>
		/// <param name="gesture">The <see cref="Gesture"/> to recognize for the bound <see cref="InputControl"/>.</param>
		public InputAction(string name, string binding, Gesture gesture) : this(name)
		{
			inputMapCombinations = new[] {new InputMapCombination(new InputMap(binding, gesture))};
			RegisterListeners();
			InputSystem.AddAction(this);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="InputAction"/> class with the specified name and list of <see cref="InputMapCombination"/>s.
		/// </summary>
		/// <param name="name">The name of the <see cref="InputAction"/>.</param>
		/// <param name="inputMapCombinations">A list of <see cref="InputMapCombination"/>s that can trigger this <see cref="InputAction"/>.</param>
		public InputAction(string name, InputMapCombination[] inputMapCombinations) : this(name)
		{
			this.inputMapCombinations = inputMapCombinations;
			RegisterListeners();
			InputSystem.AddAction(this);
		}

		/// <summary>
		/// Updates the current state of this <see cref="InputAction"/>.
		/// </summary>
		public void Update()
		{
			for (int i = 0; i < inputMapCombinations.Length; i++)
			{
				inputMapCombinations[i].Update();
			}
		}

		/// <summary>
		/// Resolves all <see cref="InputControl"/> bindings using a list of provided <see cref="InputDevice"/>s.
		/// </summary>
		/// <param name="devices">The current list of available <see cref="InputDevice"/>s.</param>
		public void ResolveBindings(List<InputDevice> devices)
		{
			for (int i = 0; i < inputMapCombinations.Length; i++)
			{
				inputMapCombinations[i].ResolveBindings(devices);
			}
		}

		/// <inheritdoc cref="ISerializationCallbackReceiver.OnBeforeSerialize"/>
		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
		}

		/// <inheritdoc cref="ISerializationCallbackReceiver.OnAfterDeserialize"/>
		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			RegisterListeners();
		}

		/// <summary>
		/// Registers event listeners for all <see cref="InputMapCombination"/>s.
		/// </summary>
		private void RegisterListeners()
		{
			for (int i = 0; i < inputMapCombinations.Length; i++)
			{
				inputMapCombinations[i].Started -= OnInputMapCombinationStarted;
				inputMapCombinations[i].Started += OnInputMapCombinationStarted;
				inputMapCombinations[i].Performed -= OnInputMapCombinationPerformed;
				inputMapCombinations[i].Performed += OnInputMapCombinationPerformed;
				inputMapCombinations[i].Canceled -= OnInputMapCombinationCanceled;
				inputMapCombinations[i].Canceled += OnInputMapCombinationCanceled;
			}
		}

		/// <summary>
		/// Callback for the <see cref="InputEventRaiser.Started"/> event from an <see cref="InputMapCombination"/>.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		private void OnInputMapCombinationStarted(object sender)
		{
			if (_activeCombination != null)
			{
				return;
			}

			_activeCombination = (InputMapCombination) sender;
			OnStarted();
		}

		/// <summary>
		/// Callback for the <see cref="InputEventRaiser.Performed"/> event from an <see cref="InputMapCombination"/>.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		private void OnInputMapCombinationPerformed(object sender)
		{
			if (_activeCombination != null && _activeCombination != sender)
			{
				return;
			}

			OnPerformed();
		}

		/// <summary>
		/// Callback for the <see cref="InputEventRaiser.Canceled"/> event from an <see cref="InputMapCombination"/>.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		private void OnInputMapCombinationCanceled(object sender)
		{
			if (_activeCombination == null || _activeCombination != sender)
			{
				return;
			}

			_activeCombination = null;
			OnCanceled();
		}
	}
}