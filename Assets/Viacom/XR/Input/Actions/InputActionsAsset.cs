﻿// <copyright file="InputActionsAsset.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
//	Represents a set of input actions that are stored in a scriptable object
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Actions
{
	using System;

	using UnityEngine;

	/// <summary>
	/// Represents a set of <see cref="InputAction"/>s that are stored in a <see cref="ScriptableObject"/>
	/// </summary>
	[CreateAssetMenu(menuName = "VXR/Input Actions")]
	public class InputActionsAsset : ScriptableObject
	{
		[SerializeField] public InputAction[] inputActions;

		/// <summary>
		/// Registers the contained set of <see cref="InputAction"/>s with the <see cref="InputSystem"/>.
		/// </summary>
		public void RegisterActions()
		{
			for (int i = 0; i < inputActions.Length; i++)
			{
				InputSystem.AddAction(inputActions[i]);
			}
		}

		/// <inheritdoc cref="ScriptableObject.OnValidate"/>
		private void OnValidate()
		{
			for (int i = 0; i < inputActions.Length; i++)
			{
				if (string.IsNullOrEmpty(inputActions[i].Id) || IsIdRepeated(i, inputActions[i].Id))
				{
					inputActions[i].Id = Guid.NewGuid().ToString();
				}
			}
		}

		/// <summary>
		/// Gets a value indicating whether or not the given identifier is repeated in the <see cref="InputAction"/>s list. 
		/// </summary>
		/// <remarks>This method searches backwards through the array.</remarks>
		/// <param name="startIndex">The zero-based index to start searching from.</param>
		/// <param name="id">An identifier.</param>
		/// <returns><c>true</c> if an <see cref="InputAction"/> at index <paramref name="startIndex"/> or lower has an identifier equal to <paramref name="id"/>; otherwise, <c>false</c>.</returns>
		private bool IsIdRepeated(int startIndex, string id)
		{
			for (int i = startIndex - 1; i >= 0; i--)
			{
				if (inputActions[i].Id.Equals(id))
				{
					return true;
				}
			}

			return false;
		}
	}
}