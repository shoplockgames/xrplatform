﻿// <copyright file="InputEventRaiser.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
//	Represents an object that can raise input events (Started, Canceled and Performed).
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Actions
{
	/// <summary>
	/// Represents the method that will handle the <see cref="InputEventRaiser.Started"/> event of a <see cref="InputEventRaiser"/> class.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	public delegate void InputStartedHandler(object sender);

	/// <summary>
	/// Represents the method that will handle the <see cref="InputEventRaiser.Canceled"/> event of a <see cref="InputEventRaiser"/> class.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	public delegate void InputCanceledHandler(object sender);

	/// <summary>
	/// Represents the method that will handle the <see cref="InputEventRaiser.Performed"/> event of a <see cref="InputEventRaiser"/> class.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	public delegate void InputPerformedHandler(object sender);

	/// <summary>
	/// Represents an object that can raise input events (Started, Performed and Canceled).
	/// </summary>
	public abstract class InputEventRaiser
	{
		/// <summary>
		/// Occurs when an input event has started.
		/// </summary>
		public event InputStartedHandler Started;

		/// <summary>
		/// Occurs when an input event has been canceled.
		/// </summary>
		public event InputCanceledHandler Canceled;

		/// <summary>
		/// Occurs when an input event has been performed.
		/// </summary>
		public event InputPerformedHandler Performed;

		/// <summary>
		/// Fires the <see cref="InputEventRaiser.Started"/> event.
		/// </summary>
		protected void OnStarted()
		{
			if (Started != null)
			{
				Started(this);
			}
		}

		/// <summary>
		/// Fires the <see cref="InputEventRaiser.Canceled"/> event.
		/// </summary>
		protected void OnCanceled()
		{
			if (Canceled != null)
			{
				Canceled(this);
			}
		}

		/// <summary>
		/// Fires the <see cref="InputEventRaiser.Performed"/> event.
		/// </summary>
		protected void OnPerformed()
		{
			if (Performed != null)
			{
				Performed(this);
			}
		}
	}
}
