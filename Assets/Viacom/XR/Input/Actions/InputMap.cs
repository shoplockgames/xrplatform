﻿// <copyright file="InputMap.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a mapping of an input control to a gesture. 
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Actions
{
	using System;
	using System.Collections.Generic;
	using System.Linq;

	using UnityEngine;

	using Viacom.XR.Diagnostics;
	using Viacom.XR.Input.Controls;
	using Viacom.XR.Input.Devices;
	using Viacom.XR.Input.Gestures;

	/// <summary>
	/// Represents a mapping of an <see cref="InputControl"/> to a <see cref="Gesture"/>. 
	/// </summary>
	[Serializable]
	public class InputMap : InputEventRaiser
	{
		[Tooltip("The binding path to an input control. Expected format: [Device]/[Usage]?/[Control]. Example: Keyboard/Q")]
		[SerializeField]
		public string binding;

		[SerializeField] public GestureScriptableObject gesture;

		private Gesture _gesture;
		private readonly List<GestureContext> _gestureContexts;

		private static readonly string LogTag = typeof(InputMap).Name;
		private const char BindingSeparator = '/';
		private const int MaxBindingComponentsLength = 3;

		private GestureContext _activeGestureContext = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="InputMap"/> class.
		/// </summary>
		private InputMap()
		{
			_gestureContexts = new List<GestureContext>();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="InputMap"/> class with the specified <see cref="InputControl"/> binding path and <see cref="Gesture"/>.
		/// </summary>
		/// <param name="binding"></param>
		/// <param name="gesture"></param>
		public InputMap(string binding, Gesture gesture) : this()
		{
			this.binding = binding;
			_gesture = gesture;
		}

		/// <summary>
		/// Updates the current state of this <see cref="InputMap"/>.
		/// </summary>
		public void Update()
		{
			for (int i = 0; i < _gestureContexts.Count; i++)
			{
				_gesture.Process(_gestureContexts[i]);
			}

			if (_activeGestureContext == null)
			{
				_activeGestureContext = _gestureContexts.FirstOrDefault(x =>
					x.State == GestureState.Started || x.State == GestureState.Performed);
			}

			if (_activeGestureContext != null)
			{
				switch (_activeGestureContext.State)
				{
					case GestureState.Started:
						if (_activeGestureContext.PreviousState != GestureState.Started)
						{
							OnStarted();
						}

						break;
					case GestureState.Canceled:
						OnCanceled();
						_activeGestureContext = null;
						break;
					case GestureState.Performed:
						OnPerformed();
						break;
					case GestureState.Waiting:
						_activeGestureContext = null;
						break;
					default:
						break;
				}
			}

			_gestureContexts.ForEach(x => x.OnProcessed());

			if (_activeGestureContext != null && _activeGestureContext.State == GestureState.Waiting)
			{
				_activeGestureContext = null;
			}
		}

		/// <summary>
		/// Resolves all <see cref="InputControl"/> bindings using a list of provided <see cref="InputDevice"/>s.
		/// </summary>
		/// <param name="devices">The current list of available <see cref="InputDevice"/>s.</param>
		public void ResolveBinding(List<InputDevice> devices)
		{
			if (string.IsNullOrEmpty(binding))
			{
				ErrorReporter.LogHandledException(new NullReferenceException("Missing binding for input map."));
				return;
			}

			string[] bindingComponents = binding.Split(BindingSeparator);

			if (bindingComponents.Length < 2)
			{
				ErrorReporter.LogHandledException(
					new FormatException("Input binding is not in the correct format: " + binding));
				return;
			}

			string deviceName = bindingComponents[0];
			string deviceUsage = bindingComponents.Length == MaxBindingComponentsLength ? bindingComponents[1] : null;
			string controlName = bindingComponents.Length == MaxBindingComponentsLength
				? bindingComponents[2]
				: bindingComponents[1];

			if (string.IsNullOrEmpty(deviceName) || string.IsNullOrEmpty(controlName))
			{
				ErrorReporter.LogHandledException(
					new FormatException("Input binding is not in the correct format: " + binding));
				return;
			}

			List<InputDevice> candidateDevices = new List<InputDevice>();
			for (int i = 0; i < devices.Count; i++)
			{
				InputDevice device = devices[i];
				if (TypeMatchesName(device.GetType(), deviceName))
				{
					if (!string.IsNullOrEmpty(deviceUsage) &&
					    !device.Usage.Equals(deviceUsage, StringComparison.InvariantCultureIgnoreCase))
					{
						continue;
					}

					candidateDevices.Add(device);
				}
			}

			if (candidateDevices.Count == 0)
			{
				ApplicationLog.Warning(LogTag, "No devices could be for binding: " + binding);
				return;
			}

			if (gesture != null)
			{
				_gesture = gesture.GetGesture();
			}
			
			_gestureContexts.Clear();
			
			for (int i = 0; i < candidateDevices.Count; i++)
			{
				InputControl[] inputControls = candidateDevices[i].GetControls(controlName);
				for (int j = 0; j < inputControls.Length; j++)
				{
					_gestureContexts.Add(_gesture.CreateContext(inputControls[j]));
				}
			}
		}

		/// <summary>
		/// Gets a value indicating whether or not the given <see cref="Type"/> or it's base type matches the specified name.
		/// </summary>
		/// <param name="type">The <see cref="Type"/> to check.</param>
		/// <param name="name">The name to check against.</param>
		/// <returns><c>true</c> if <paramref name="type"/> or it's base type's name matches <paramref name="name"/>; otherwise, <c>false</c>.</returns>
		private static bool TypeMatchesName(Type type, string name)
		{
			if (type.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase))
			{
				return true;
			}

			if (type.BaseType == null)
			{
				return false;
			}

			return TypeMatchesName(type.BaseType, name);
		}
	}
}