﻿// <copyright file="InputMapCombination.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
//  Represents a grouping of input maps that have to be performed simultaneously. An input combination is considered
//  started once one of it's input maps has started. An input combination is considered performed once all input maps
//  have been performed simultaneously. An input combination is considered canceled once all input maps that had been 
//  started are canceled.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Actions
{
	using System;
	using System.Collections.Generic;

	using UnityEngine;

	using Viacom.XR.Input.Controls;
	using Viacom.XR.Input.Devices;

	/// <summary>
	/// Represents a grouping of <see cref="InputMap"/>s that have to be performed simultaneously.
	/// </summary>
	[Serializable]
	public class InputMapCombination : InputEventRaiser, ISerializationCallbackReceiver
	{
		[SerializeField] public InputMap[] inputMaps;

		private readonly List<InputMap> _startedInputMaps;
		private readonly List<InputMap> _performedInputMaps;

		/// <summary>
		/// Initializes a new instance of the <see cref="InputAction"/> class.
		/// </summary>
		private InputMapCombination()
		{
			_startedInputMaps = new List<InputMap>();
			_performedInputMaps = new List<InputMap>();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="InputAction"/> class with the specified <see cref="InputMap"/>.
		/// </summary>
		/// <param name="inputMap"></param>
		public InputMapCombination(InputMap inputMap) : this()
		{
			inputMaps = new[] {inputMap};
			RegisterListeners();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="InputAction"/> class with the specified list of <see cref="InputMap"/>s.
		/// </summary>
		/// <param name="inputMaps"></param>
		public InputMapCombination(InputMap[] inputMaps)
		{
			this.inputMaps = inputMaps;
			RegisterListeners();
		}

		/// <summary>
		/// Updates the current state of this <see cref="InputMapCombination"/>.
		/// </summary>
		public void Update()
		{
			_performedInputMaps.Clear();
			for (int i = 0; i < inputMaps.Length; i++)
			{
				inputMaps[i].Update();
			}
		}

		/// <summary>
		/// Resolves all <see cref="InputControl"/> bindings using a list of provided <see cref="InputDevice"/>s.
		/// </summary>
		/// <param name="devices">The current list of available <see cref="InputDevice"/>s.</param>
		public void ResolveBindings(List<InputDevice> devices)
		{
			for (int i = 0; i < inputMaps.Length; i++)
			{
				inputMaps[i].ResolveBinding(devices);
			}
		}

		/// <inheritdoc cref="ISerializationCallbackReceiver.OnBeforeSerialize"/>
		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
		}

		/// <inheritdoc cref="ISerializationCallbackReceiver.OnAfterDeserialize"/>
		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			RegisterListeners();
		}

		/// <summary>
		/// Registers event listeners for all <see cref="InputMap"/>s.
		/// </summary>
		private void RegisterListeners()
		{
			for (int i = 0; i < inputMaps.Length; i++)
			{
				inputMaps[i].Started -= OnInputMapStarted;
				inputMaps[i].Started += OnInputMapStarted;
				inputMaps[i].Performed -= OnInputMapPerformed;
				inputMaps[i].Performed += OnInputMapPerformed;
				inputMaps[i].Canceled -= OnInputMapCanceled;
				inputMaps[i].Canceled += OnInputMapCanceled;
			}
		}

		/// <summary>
		/// Callback for the <see cref="InputMap"/> <see cref="InputEventRaiser.Started"/> event from an <see cref="InputMap"/>.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		private void OnInputMapStarted(object sender)
		{
			InputMap inputMap = (InputMap) sender;

			if (inputMap == null)
			{
				return;
			}

			if (_startedInputMaps.Count == 0)
			{
				OnStarted();
			}

			_startedInputMaps.Add(inputMap);
		}

		/// <summary>
		/// Callback for the <see cref="InputMap"/> <see cref="InputEventRaiser.Performed"/> event from an <see cref="InputMap"/>.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		private void OnInputMapPerformed(object sender)
		{
			InputMap inputMap = (InputMap) sender;

			if (inputMap == null)
			{
				return;
			}

			if (!_performedInputMaps.Contains(inputMap))
			{
				_performedInputMaps.Add(inputMap);
			}

			if (_performedInputMaps.Count == inputMaps.Length)
			{
				OnPerformed();
			}
		}

		/// <summary>
		/// Callback for the <see cref="InputEventRaiser.Canceled"/> event from an <see cref="InputMap"/>.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		private void OnInputMapCanceled(object sender)
		{
			InputMap inputMap = (InputMap) sender;

			if (inputMap == null)
			{
				return;
			}

			if (_startedInputMaps.Contains(inputMap))
			{
				_startedInputMaps.Remove(inputMap);

				if (_startedInputMaps.Count == 0)
				{
					OnCanceled();
				}
			}
		}
	}
}