﻿// <copyright file="InputControl.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an individual control that can read input. 
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Controls
{
	using System;

	using Viacom.XR.Input.Devices;

	/// <summary>
	/// Represents an individual control that can read input.
	/// </summary>
	public abstract class InputControl
	{
		/// <summary>
		/// Gets or sets the name of the <see cref="InputControl"/>.
		/// </summary>
		public string Name { get; private set; }

		/// <summary>
		/// Gets or sets a list of aliases of the <see cref="InputControl"/>.
		/// </summary>
		public string[] Aliases { get; private set; }

		/// <summary>
		/// Gets the value type of the data read by this control.
		/// </summary>
		public abstract Type ValueType { get; }

		/// <summary>
		/// Gets the <see cref="InputDevice"/> that contains this control.
		/// </summary>
		protected InputDevice Device { get; private set; }

		/// <summary>
		/// Gets the magnitude of the value of this control.
		/// </summary>
		public virtual float Magnitude
		{
			get { return 0; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="InputControl"/> class with a specified
		/// <see cref="InputDevice"/> that contains it.
		/// </summary>
		/// <param name="device">The <see cref="InputDevice"/> that contains this control.</param>
		/// <param name="name">The name of the <see cref="InputControl"/>.</param>
		/// <param name="aliases">The list of aliases of the <see cref="InputControl"/>.</param>
		protected InputControl(InputDevice device, string name, string[] aliases = null)
		{
			Device = device;
			device.RegisterControl(this);
			Name = name;
			Aliases = aliases;
		}
	}

	/// <summary>
	/// Represents an individual control that can read input. 
	/// </summary>
	/// <typeparam name="TValue">The value type of the input data.</typeparam>
	public abstract class InputControl<TValue> : InputControl where TValue : struct
	{
		/// <inheritdoc cref="InputControl.ValueType"/>
		public override Type ValueType
		{
			get { return typeof(TValue); }
		}

		/// <summary>
		/// Gets the raw input value from this control.
		/// </summary>
		public abstract TValue Value { get; }

		/// <summary>
		/// Initializes a new instance of the <see cref="InputControl{TValue}"/> class with a specified
		/// <see cref="InputDevice"/> that contains it.
		/// </summary>
		/// <param name="device">The <see cref="InputDevice"/> that contains this control.</param>
		/// <param name="name">The name of the <see cref="InputControl{TValue}"/>.</param>
		/// <param name="aliases">The list of aliases of the <see cref="InputControl{TValue}"/>.</param>
		protected InputControl(InputDevice device, string name, string[] aliases = null) : base(device, name, aliases)
		{
		}
	}
}