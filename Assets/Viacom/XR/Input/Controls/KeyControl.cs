﻿// <copyright file="KeyControl.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an keyboard input control.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Controls
{
	using UnityEngine;

	using Viacom.XR.Input.Devices;

	/// <summary>
	/// Represents an keyboard input control.
	/// </summary>
	public class KeyControl : InputControl<bool>
	{
		private readonly KeyCode _keyCode;

		/// <inheritdoc cref="InputControl{T}.Value"/>
		public override bool Value
		{
			get { return Input.GetKey(_keyCode); }
		}

		/// <inheritdoc cref="InputControl.Magnitude"/>
		public override float Magnitude
		{
			get { return  Value ? 1.0f : 0.0f; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="KeyControl"/> class with a specified
		/// <see cref="InputDevice"/> that contains it and the key it is bound to.
		/// </summary>
		/// <param name="device">The <see cref="InputDevice"/> that contains this control.</param>
		/// <param name="name">The name of the <see cref="KeyControl"/>.</param>
		/// <param name="key">The key this control reads input from.</param>
		/// <param name="aliases">The list of aliases of the <see cref="KeyControl"/>.</param>
		public KeyControl(InputDevice device, string name, KeyCode key, string[] aliases = null) : base(device, name,
			aliases)
		{
			_keyCode = key;
		}
	}
}