﻿// <copyright file="InputDevice.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a physical input device which contains a collection of individual input controls. 
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Devices
{
	using System;
	using System.Collections.Generic;
	using System.Linq;

	using Viacom.XR.Input.Controls;

	/// <summary>
	/// Represents a physical input device which contains a collection of individual input controls.
	/// </summary>
	public class InputDevice
	{
		/// <summary>
		/// Gets or sets the usage of the input device.
		/// </summary>
		public string Usage { get; set; }

		private readonly List<InputControl> _inputControls = new List<InputControl>();

		/// <summary>
		/// Registers the given control as a member of this <see cref="InputDevice"/>.
		/// </summary>
		/// <param name="inputControl">An <see cref="InputControl"/> to register.</param>
		/// <exception cref="ArgumentNullException"><paramref name="inputControl"/> is <c>null</c>.</exception>
		public void RegisterControl(InputControl inputControl)
		{
			if (inputControl == null)
			{
				throw new ArgumentNullException("inputControl");
			}
			
			if (!_inputControls.Contains(inputControl))
			{
				_inputControls.Add(inputControl);
			}
		}

		/// <summary>
		/// Gets a list of all <see cref="InputControl"/>s that belong to this <see cref="InputDevice"/> whose
		/// <see cref="InputControl.Name"/> or one of it's <see cref="InputControl.Aliases"/> matches the specified value.
		/// </summary>
		/// <param name="value">The name or alias to match.</param>
		/// <returns></returns>
		/// <exception cref="ArgumentException"><paramref name="value"/> is <c>null</c> or empty.</exception>
		public InputControl[] GetControls(string value)
		{
			if (string.IsNullOrEmpty(value))
			{
				throw new ArgumentException("name cannot be null or empty.");
			}

			return _inputControls.Where(control =>
					control.Name.Equals(value, StringComparison.InvariantCultureIgnoreCase) ||
					(control.Aliases != null &&
					 control.Aliases.Contains(value, StringComparer.InvariantCultureIgnoreCase)))
				.ToArray();
		}
	}
}