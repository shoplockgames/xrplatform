﻿// <copyright file="Keyboard.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a keyboard.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Devices
{
	using UnityEngine;

	using Viacom.XR.Input.Controls;

	/// <summary>
	/// Represents a keyboard.
	/// </summary>
	public class Keyboard : InputDevice
	{
		/// <summary>
		/// Represents the A key. This field is read-only.
		/// </summary>
		public readonly KeyControl A;

		/// <summary>
		/// Represents the B key. This field is read-only.
		/// </summary>
		public readonly KeyControl B;

		/// <summary>
		/// Represents the C key. This field is read-only.
		/// </summary>
		public readonly KeyControl C;

		/// <summary>
		/// Represents the D key. This field is read-only.
		/// </summary>
		public readonly KeyControl D;

		/// <summary>
		/// Represents the E key. This field is read-only.
		/// </summary>
		public readonly KeyControl E;

		/// <summary>
		/// Represents the F key. This field is read-only.
		/// </summary>
		public readonly KeyControl F;

		/// <summary>
		/// Represents the G key. This field is read-only.
		/// </summary>
		public readonly KeyControl G;

		/// <summary>
		/// Represents the H key. This field is read-only.
		/// </summary>
		public readonly KeyControl H;

		/// <summary>
		/// Represents the I key. This field is read-only.
		/// </summary>
		public readonly KeyControl I;

		/// <summary>
		/// Represents the J key. This field is read-only.
		/// </summary>
		public readonly KeyControl J;

		/// <summary>
		/// Represents the K key. This field is read-only.
		/// </summary>
		public readonly KeyControl K;

		/// <summary>
		/// Represents the L key. This field is read-only.
		/// </summary>
		public readonly KeyControl L;

		/// <summary>
		/// Represents the M key. This field is read-only.
		/// </summary>
		public readonly KeyControl M;

		/// <summary>
		/// Represents the N key. This field is read-only.
		/// </summary>
		public readonly KeyControl N;

		/// <summary>
		/// Represents the O key. This field is read-only.
		/// </summary>
		public readonly KeyControl O;

		/// <summary>
		/// Represents the P key. This field is read-only.
		/// </summary>
		public readonly KeyControl P;

		/// <summary>
		/// Represents the Q key. This field is read-only.
		/// </summary>
		public readonly KeyControl Q;

		/// <summary>
		/// Represents the R key. This field is read-only.
		/// </summary>
		public readonly KeyControl R;

		/// <summary>
		/// Represents the S key. This field is read-only.
		/// </summary>
		public readonly KeyControl S;

		/// <summary>
		/// Represents the T key. This field is read-only.
		/// </summary>
		public readonly KeyControl T;

		/// <summary>
		/// Represents the U key. This field is read-only.
		/// </summary>
		public readonly KeyControl U;

		/// <summary>
		/// Represents the V key. This field is read-only.
		/// </summary>
		public readonly KeyControl V;

		/// <summary>
		/// Represents the W key. This field is read-only.
		/// </summary>
		public readonly KeyControl W;

		/// <summary>
		/// Represents the X key. This field is read-only.
		/// </summary>
		public readonly KeyControl X;

		/// <summary>
		/// Represents the Y key. This field is read-only.
		/// </summary>
		public readonly KeyControl Y;

		/// <summary>
		/// Represents the Z key. This field is read-only.
		/// </summary>
		public readonly KeyControl Z;

		/// <summary>
		/// Represents the ESC (ESCAPE) key. This field is read-only.
		/// </summary>
		public readonly KeyControl Escape;

		/// <summary>
		/// Represents the F1 key. This field is read-only.
		/// </summary>
		public readonly KeyControl F1;

		/// <summary>
		/// Represents the F2 key. This field is read-only.
		/// </summary>
		public readonly KeyControl F2;

		/// <summary>
		/// Represents the F3 key. This field is read-only.
		/// </summary>
		public readonly KeyControl F3;

		/// <summary>
		/// Represents the F4 key. This field is read-only.
		/// </summary>
		public readonly KeyControl F4;

		/// <summary>
		/// Represents the F5 key. This field is read-only.
		/// </summary>
		public readonly KeyControl F5;

		/// <summary>
		/// Represents the F6 key. This field is read-only.
		/// </summary>
		public readonly KeyControl F6;

		/// <summary>
		/// Represents the F7 key. This field is read-only.
		/// </summary>
		public readonly KeyControl F7;

		/// <summary>
		/// Represents the F8 key. This field is read-only.
		/// </summary>
		public readonly KeyControl F8;

		/// <summary>
		/// Represents the F9 key. This field is read-only.
		/// </summary>
		public readonly KeyControl F9;

		/// <summary>
		/// Represents the F10 key. This field is read-only.
		/// </summary>
		public readonly KeyControl F10;

		/// <summary>
		/// Represents the F11 key. This field is read-only.
		/// </summary>
		public readonly KeyControl F11;

		/// <summary>
		/// Represents the F12 key. This field is read-only.
		/// </summary>
		public readonly KeyControl F12;

		/// <summary>
		/// Represents the BACK QUOTE key (`). This field is read-only.
		/// </summary>
		public readonly KeyControl BackQuote;
		
		/// <summary>
		/// Represents the 1 key. This field is read-only.
		/// </summary>
		public readonly KeyControl D1;
		
		/// <summary>
		/// Represents the 2 key. This field is read-only.
		/// </summary>
		public readonly KeyControl D2;
		
		/// <summary>
		/// Represents the 3 key. This field is read-only.
		/// </summary>
		public readonly KeyControl D3;

		/// <summary>
		/// Represents the 4 key. This field is read-only.
		/// </summary>
		public readonly KeyControl D4;
		
		/// <summary>
		/// Represents the 5 key. This field is read-only.
		/// </summary>
		public readonly KeyControl D5;
		
		/// <summary>
		/// Represents the 6 key. This field is read-only.
		/// </summary>
		public readonly KeyControl D6;
		
		/// <summary>
		/// Represents the 7 key. This field is read-only.
		/// </summary>
		public readonly KeyControl D7;
		
		/// <summary>
		/// Represents the 8 key. This field is read-only.
		/// </summary>
		public readonly KeyControl D8;
		
		/// <summary>
		/// Represents the 9 key. This field is read-only.
		/// </summary>
		public readonly KeyControl D9;
		
		/// <summary>
		/// Represents the 0 key. This field is read-only.
		/// </summary>
		public readonly KeyControl D0;
		
		/// <summary>
		/// Represents the MINUS key (-). This field is read-only.
		/// </summary>
		public readonly KeyControl Minus;

		/// <summary>
		/// Represents the EQUALS key (=). This field is read-only.
		/// </summary>
		public readonly KeyControl Equal;

		/// <summary>
		/// Represents the BACKSPACE key. This field is read-only.
		/// </summary>
		public readonly KeyControl Backspace;

		/// <summary>
		/// Represents the TAB key. This field is read-only.
		/// </summary>
		public readonly KeyControl Tab;

		/// <summary>
		/// Represents the left BRACKET key ([). This field is read-only.
		/// </summary>
		public readonly KeyControl LeftBracket;

		/// <summary>
		/// Represents the right BRACKET key (]). This field is read-only.
		/// </summary>
		public readonly KeyControl RightBracket;
		
		/// <summary>
		/// Represents the BACKSLASH key (\). This field is read-only.
		/// </summary>
		public readonly KeyControl Backslash;

		/// <summary>
		/// Represents the CAPS LOCK key. This field is read-only.
		/// </summary>
		public readonly KeyControl CapsLock;
		
		/// <summary>
		/// Represents the SEMICOLON (;) key. This field is read-only.
		/// </summary>
		public readonly KeyControl Semicolon;
		
		/// <summary>
		/// Represents the QUOTE (') key. This field is read-only.
		/// </summary>
		public readonly KeyControl Quote;
		
		/// <summary>
		/// Represents the RETURN key. This field is read-only.
		/// </summary>
		public readonly KeyControl Return;

		/// <summary>
		/// Represents the left SHIFT key. This field is read-only.
		/// </summary>
		public readonly KeyControl LeftShift;

		/// <summary>
		/// Represents the right SHIFT key. This field is read-only.
		/// </summary>
		public readonly KeyControl RightShift;

		/// <summary>
		/// Represents the COMMA (,) key. This field is read-only.
		/// </summary>
		public readonly KeyControl Comma;
		
		/// <summary>
		/// Represents the PERIOD (.) key. This field is read-only.
		/// </summary>
		public readonly KeyControl Period;
		
		/// <summary>
		/// Represents the SLASH (/) key. This field is read-only.
		/// </summary>
		public readonly KeyControl Slash;

		/// <summary>
		/// Represents the left CTRL (CONTROL) key. This field is read-only.
		/// </summary>
		public readonly KeyControl LeftControl;

		/// <summary>
		/// Represents the right CTRL (CONTROL) key. This field is read-only.
		/// </summary>
		public readonly KeyControl RightControl;

		/// <summary>
		/// Represents the left ALT key. This field is read-only.
		/// </summary>
		public readonly KeyControl LeftAlt;

		/// <summary>
		/// Represents the right ALT key. This field is read-only.
		/// </summary>
		public readonly KeyControl RightAlt;

		/// <summary>
		/// Represents the SPACEBAR key. This field is read-only.
		/// </summary>
		public readonly KeyControl Space;
		
		/// <summary>
		/// Represents the LEFT ARROW key. This field is read-only.
		/// </summary>
		public readonly KeyControl LeftArrow;
		
		/// <summary>
		/// Represents the UP ARROW key. This field is read-only.
		/// </summary>
		public readonly KeyControl UpArrow;
		
		/// <summary>
		/// Represents the RIGHT ARROW key. This field is read-only.
		/// </summary>
		public readonly KeyControl RightArrow;
		
		/// <summary>
		/// Represents the DOWN ARROW key. This field is read-only.
		/// </summary>
		public readonly KeyControl DownArrow;

		/// <summary>
		/// Represents the 1 key on the numeric keypad. This field is read-only.
		/// </summary>
		public readonly KeyControl NumPad1;
		
		/// <summary>
		/// Represents the 2 key on the numeric keypad. This field is read-only.
		/// </summary>
		public readonly KeyControl NumPad2;
		
		/// <summary>
		/// Represents the 3 key on the numeric keypad. This field is read-only.
		/// </summary>
		public readonly KeyControl NumPad3;
		
		/// <summary>
		/// Represents the 4 key on the numeric keypad. This field is read-only.
		/// </summary>
		public readonly KeyControl NumPad4;
		
		/// <summary>
		/// Represents the 5 key on the numeric keypad. This field is read-only.
		/// </summary>
		public readonly KeyControl NumPad5;
		
		/// <summary>
		/// Represents the 6 key on the numeric keypad. This field is read-only.
		/// </summary>
		public readonly KeyControl NumPad6;
		
		/// <summary>
		/// Represents the 7 key on the numeric keypad. This field is read-only.
		/// </summary>
		public readonly KeyControl NumPad7;
		
		/// <summary>
		/// Represents the 8 key on the numeric keypad. This field is read-only.
		/// </summary>
		public readonly KeyControl NumPad8;
		
		/// <summary>
		/// Represents the 9 key on the numeric keypad. This field is read-only.
		/// </summary>
		public readonly KeyControl NumPad9;
		
		/// <summary>
		/// Represents the 0 key on the numeric keypad. This field is read-only.
		/// </summary>
		public readonly KeyControl NumPad0;

		/// <summary>
		/// Initializes a new instance of the <see cref="Keyboard"/> class.
		/// </summary>
		public Keyboard()
		{
			A = new KeyControl(this, "a", KeyCode.A, new[] {"any"});
			B = new KeyControl(this, "b", KeyCode.B, new[] {"any"});
			C = new KeyControl(this, "c", KeyCode.C, new[] {"any"});
			D = new KeyControl(this, "d", KeyCode.D, new[] {"any"});
			E = new KeyControl(this, "e", KeyCode.E, new[] {"any"});
			F = new KeyControl(this, "f", KeyCode.F, new[] {"any"});
			G = new KeyControl(this, "g", KeyCode.G, new[] {"any"});
			H = new KeyControl(this, "h", KeyCode.H, new[] {"any"});
			I = new KeyControl(this, "i", KeyCode.I, new[] {"any"});
			J = new KeyControl(this, "j", KeyCode.J, new[] {"any"});
			K = new KeyControl(this, "k", KeyCode.K, new[] {"any"});
			L = new KeyControl(this, "l", KeyCode.L, new[] {"any"});
			M = new KeyControl(this, "m", KeyCode.M, new[] {"any"});
			N = new KeyControl(this, "n", KeyCode.N, new[] {"any"});
			O = new KeyControl(this, "o", KeyCode.O, new[] {"any"});
			P = new KeyControl(this, "p", KeyCode.P, new[] {"any"});
			Q = new KeyControl(this, "q", KeyCode.Q, new[] {"any"});
			R = new KeyControl(this, "r", KeyCode.R, new[] {"any"});
			S = new KeyControl(this, "s", KeyCode.S, new[] {"any"});
			T = new KeyControl(this, "t", KeyCode.T, new[] {"any"});
			U = new KeyControl(this, "u", KeyCode.U, new[] {"any"});
			V = new KeyControl(this, "v", KeyCode.V, new[] {"any"});
			W = new KeyControl(this, "w", KeyCode.W, new[] {"any"});
			X = new KeyControl(this, "x", KeyCode.X, new[] {"any"});
			Y = new KeyControl(this, "y", KeyCode.Y, new[] {"any"});
			Z = new KeyControl(this, "z", KeyCode.Z, new[] {"any"});
			Escape = new KeyControl(this, "escape", KeyCode.Escape, new[] {"esc", "any"});
			F1 = new KeyControl(this, "f1", KeyCode.F1, new[] {"any"});
			F2 = new KeyControl(this, "f2", KeyCode.F2, new[] {"any"});
			F3 = new KeyControl(this, "f3", KeyCode.F3, new[] {"any"});
			F4 = new KeyControl(this, "f4", KeyCode.F4, new[] {"any"});
			F5 = new KeyControl(this, "f5", KeyCode.F5, new[] {"any"});
			F6 = new KeyControl(this, "f6", KeyCode.F6, new[] {"any"});
			F7 = new KeyControl(this, "f7", KeyCode.F7, new[] {"any"});
			F8 = new KeyControl(this, "f8", KeyCode.F8, new[] {"any"});
			F9 = new KeyControl(this, "f9", KeyCode.F9, new[] {"any"});
			F10 = new KeyControl(this, "f10", KeyCode.F10, new[] {"any"});
			F11 = new KeyControl(this, "f11", KeyCode.F11, new[] {"any"});
			F12 = new KeyControl(this, "f12", KeyCode.F12, new[] {"any"});
			BackQuote = new KeyControl(this, "backquote", KeyCode.BackQuote, new[] {"`", "any"});
			D1 = new KeyControl(this, "d1", KeyCode.Alpha1, new[] {"1", "any"});
			D2 = new KeyControl(this, "d2", KeyCode.Alpha2, new[] {"2", "any"});
			D3 = new KeyControl(this, "d3", KeyCode.Alpha3, new[] {"3", "any"});
			D4 = new KeyControl(this, "d4", KeyCode.Alpha4, new[] {"4", "any"});
			D5 = new KeyControl(this, "d5", KeyCode.Alpha5, new[] {"5", "any"});
			D6 = new KeyControl(this, "d6", KeyCode.Alpha6, new[] {"6", "any"});
			D7 = new KeyControl(this, "d7", KeyCode.Alpha7, new[] {"7", "any"});
			D8 = new KeyControl(this, "d8", KeyCode.Alpha8, new[] {"8", "any"});
			D9 = new KeyControl(this, "d9", KeyCode.Alpha9, new[] {"9", "any"});
			D0 = new KeyControl(this, "d0", KeyCode.Alpha0, new[] {"0", "any"});
			Minus = new KeyControl(this, "minus", KeyCode.Minus, new[] {"-", "any"});
			Equal = new KeyControl(this, "equals", KeyCode.Equals, new[] {"=", "any"});
			Backspace = new KeyControl(this, "backspace", KeyCode.Backspace, new[] {"any"});
			Tab = new KeyControl(this, "tab", KeyCode.Tab, new[] {"any"});
			LeftBracket = new KeyControl(this, "leftbracket", KeyCode.LeftBracket, new[] {"openbracket", "bracket", "any"});
			RightBracket = new KeyControl(this, "rightbracket", KeyCode.RightBracket, new[] {"closebracket", "bracket", "any"});
			Backslash = new KeyControl(this, "backslash", KeyCode.Backslash, new[] {"\\", "any"});
			CapsLock = new KeyControl(this, "capslock", KeyCode.CapsLock, new[] {"any"});
			Semicolon = new KeyControl(this, "semicolon", KeyCode.Semicolon, new[] {";", "any"});
			Quote = new KeyControl(this, "quote", KeyCode.Quote, new[] {"'", "any"});
			Return = new KeyControl(this, "return", KeyCode.Return, new[] {"enter", "any"});
			LeftShift = new KeyControl(this, "leftshift", KeyCode.LeftShift, new[] {"shift", "any"});
			RightShift = new KeyControl(this, "rightshift", KeyCode.RightShift, new[] {"shift", "any"});
			Comma = new KeyControl(this, "comma", KeyCode.Comma, new[] {",", "any"});
			Period = new KeyControl(this, "period", KeyCode.Period, new[] {".", "any"});
			Slash = new KeyControl(this, "slash", KeyCode.Slash, new[] {"any"});
			LeftAlt = new KeyControl(this, "leftalt", KeyCode.LeftAlt, new[] {"alt", "any"});
			RightAlt = new KeyControl(this, "rightalt", KeyCode.RightAlt, new[] {"alt", "any"});
			LeftControl = new KeyControl(this, "leftcontrol", KeyCode.LeftControl, new[] {"leftctrl", "control", "ctrl", "any"});
			RightControl = new KeyControl(this, "rightcontrol", KeyCode.RightControl, new[] {"rightctrl", "control", "ctrl", "any"});
			Space = new KeyControl(this, "space", KeyCode.Space, new[] {"any"});
			LeftArrow = new KeyControl(this, "leftarrow", KeyCode.LeftArrow, new[] {"arrow", "any"});
			UpArrow = new KeyControl(this, "uparrow", KeyCode.UpArrow, new[] {"arrow", "any"});
			RightArrow = new KeyControl(this, "rightarrow", KeyCode.RightArrow, new[] {"arrow", "any"});
			DownArrow = new KeyControl(this, "downarrow", KeyCode.DownArrow, new[] {"arrow", "any"});
			NumPad1 = new KeyControl(this, "numpad1", KeyCode.Keypad1, new[] {"1", "any"});
			NumPad2 = new KeyControl(this, "numpad2", KeyCode.Keypad2, new[] {"2", "any"});
			NumPad3 = new KeyControl(this, "numpad3", KeyCode.Keypad3, new[] {"3", "any"});
			NumPad4 = new KeyControl(this, "numpad4", KeyCode.Keypad4, new[] {"4", "any"});
			NumPad5 = new KeyControl(this, "numpad5", KeyCode.Keypad5, new[] {"5", "any"});
			NumPad6 = new KeyControl(this, "numpad6", KeyCode.Keypad6, new[] {"6", "any"});
			NumPad7 = new KeyControl(this, "numpad7", KeyCode.Keypad7, new[] {"7", "any"});
			NumPad8 = new KeyControl(this, "numpad8", KeyCode.Keypad8, new[] {"8", "any"});
			NumPad9 = new KeyControl(this, "numpad9", KeyCode.Keypad9, new[] {"9", "any"});
			NumPad0 = new KeyControl(this, "numpad0", KeyCode.Keypad0, new[] {"0", "any"});
		}
	}
}