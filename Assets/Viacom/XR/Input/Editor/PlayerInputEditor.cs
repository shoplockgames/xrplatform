﻿// <copyright file="PlayerInputEditor.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Provides a custom editor for <see cref="PlayerInput"/>.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Editor
{
	using UnityEngine;
	using UnityEditor;

	/// <summary>
	/// Provides a custom editor for <see cref="PlayerInput"/>.
	/// </summary>
	[CustomEditor(typeof(PlayerInput))]
	public class PlayerInputEditor : Editor
	{
		private const string ButtonText = "Generate Events";
		
		/// <inheritdoc cref="Editor.OnInspectorGUI"/>
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			PlayerInput playerInput = (PlayerInput) target;
			if (GUILayout.Button(ButtonText))
			{
				playerInput.GenerateActionEvents();
				EditorUtility.SetDirty(playerInput);
			}
		}
	}
}