﻿// <copyright file="Gesture.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// When overridden in a derived form, provides functionality for describing input control gestures. 
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Gestures
{
    using Viacom.XR.Input.Controls;
    
    /// <summary>
    /// Specifies the state of a <see cref="Gesture"/>.
    /// </summary>
    public enum GestureState
    {
        /// <summary>
        /// The <see cref="Gesture"/> is waiting for input.
        /// </summary>
        Waiting,
        
        /// <summary>
        /// The <see cref="Gesture"/> has been started.
        /// </summary>
        Started,
        
        /// <summary>
        /// The <see cref="Gesture"/> has been performed.
        /// </summary>
        Performed,
        
        /// <summary>
        /// The <see cref="Gesture"/> has been canceled.
        /// </summary>
        Canceled
    }

    /// <summary>
    /// Represents an input control gesture.
    /// </summary>
    public abstract class Gesture
    {
        /// <summary>
        /// Processes input for this frame using the given <see cref="GestureContext"/>.
        /// </summary>
        /// <param name="context">The current context of this <see cref="Gesture"/>.</param>
        public abstract void Process(GestureContext context);
        
        /// <summary>
        /// Creates a <see cref="GestureContext"/> instance for the given <see cref="InputControl"/>.
        /// </summary>
        /// <param name="control">An <see cref="InputControl"/> to provide data for gesture recognition.</param>
        /// <returns>A new <see cref="GestureContext"/> instance for <paramref name="control"/>.</returns>
        public abstract GestureContext CreateContext(InputControl control);
    }
}