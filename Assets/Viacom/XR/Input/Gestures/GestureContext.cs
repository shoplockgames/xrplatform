﻿// <copyright file="GestureContext.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
//   Represents the recognition context for a gesture. 
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Gestures
{
    using UnityEngine;

    using Viacom.XR.Input.Controls;

    /// <summary>
    /// Represents the recognition context for a <see cref="Gesture"/>. 
    /// </summary>
    public class GestureContext
    {
        private readonly InputControl _inputControl;
        private GestureState _previousState;
        private GestureState _state;
        private GestureState? _nextState;
        private float _startTime;

        /// <summary>
        /// Gets the previous state of the <see cref="Gesture"/>.
        /// </summary>
        public GestureState PreviousState
        {
            get { return _previousState; }
        }

        /// <summary>
        /// Gets the current state of the <see cref="Gesture"/>.
        /// </summary>
        public GestureState State
        {
            get { return _state; }
        }

        /// <summary>
        /// Gets the start time in seconds since application start of the <see cref="Gesture"/>.
        /// </summary>
        public float StartTime
        {
            get { return _startTime; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GestureContext"/> class with the specified bound <see cref="InputControl"/>.
        /// </summary>
        /// <param name="inputControl">The <see cref="InputControl"/> to read input from.</param>
        public GestureContext(InputControl inputControl)
        {
            _inputControl = inputControl;
            _state = GestureState.Waiting;
            _previousState = GestureState.Waiting;
            _nextState = null;
            _startTime = -1;
        }

        /// <summary>
        /// Gets a value indicating whether or not the currently bound <see cref="InputControl"/> is actuated given the specified threshold.
        /// </summary>
        /// <param name="threshold">The value at which an <see cref="InputControl"/> is considered actuated.</param>
        /// <returns><c>true</c> if the currently bound <see cref="InputControl"/> is actuated past <paramref name="threshold"/>; otherwise, <c>false</c>.</returns>
        public bool IsControlActuated(float threshold = 0)
        {
            return _inputControl.Magnitude > threshold;
        }

        /// <summary>
        /// Moves the <see cref="Gesture"/> into the <see cref="GestureState.Started"/> state.
        /// </summary>
        public void Started()
        {
            _startTime = Time.time;
            _state = GestureState.Started;
        }

        /// <summary>
        /// Moves the <see cref="Gesture"/> into the <see cref="GestureState.Performed"/> state for one frame and then moves it into the <see cref="GestureState.Waiting"/> state.
        /// </summary>
        public void PerformedAndGoBackToWaiting()
        {
            _state = GestureState.Performed;
            _nextState = GestureState.Waiting;
            _startTime = -1;
        }

        /// <summary>
        /// Moves the <see cref="Gesture"/> into the <see cref="GestureState.Performed"/> state for one frame and then moves it into the <see cref="GestureState.Started"/> state.
        /// </summary>
        public void PerformedAndStayStarted()
        {
            _state = GestureState.Performed;
            _nextState = GestureState.Started;
        }

        /// <summary>
        /// Moves the <see cref="Gesture"/> into the <see cref="GestureState.Performed"/> state.
        /// </summary>
        public void PerformedAndStayPerformed()
        {
            _state = GestureState.Performed;
            _nextState = GestureState.Performed;
            _startTime = -1;
        }

        /// <summary>
        /// Moves the <see cref="Gesture"/> into the <see cref="GestureState.Canceled"/> state.
        /// </summary>
        public void Canceled()
        {
            _state = GestureState.Canceled;
            _startTime = -1;
        }

        /// <summary>
        /// Moves the <see cref="Gesture"/> into the <see cref="GestureState.Waiting"/> state.
        /// </summary>
        public void Waiting()
        {
            _state = GestureState.Waiting;
            _startTime = -1;
        }

        /// <summary>
        /// Handles preparing the <see cref="Gesture"/> for the next process update.
        /// </summary>
        public void OnProcessed()
        {
            _previousState = _state;
            if (_nextState.HasValue)
            {
                _state = _nextState.Value;
                _nextState = null;
            }
        }
    }
}