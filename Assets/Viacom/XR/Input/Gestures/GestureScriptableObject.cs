﻿// <copyright file="GestureScriptableObject.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
//  Represents an input control gesture that is stored in a scriptable object.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Gestures
{
    using System;

    using UnityEngine;

    /// <summary>
    /// Represents an input control gesture that is stored in a <see cref="ScriptableObject"/>.
    /// </summary>
    [Serializable]
    public abstract class GestureScriptableObject : ScriptableObject
    {
        /// <summary>
        /// Creates a new <see cref="Gesture"/> instance.
        /// </summary>
        /// <returns>A new <see cref="Gesture"/> instance.</returns>
        public abstract Gesture GetGesture();
    }
}
