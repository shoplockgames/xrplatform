﻿// <copyright file="LongPress.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
//  Represents a long press gesture.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Gestures
{
	using UnityEngine;

	using Viacom.XR.Input.Controls;

	/// <summary>
	/// Represents a long press gesture.
	/// </summary>
	public class LongPressGesture : Gesture
	{
		private readonly float _duration;
		private readonly bool _isContinuous;
		private readonly float _threshold;

		/// <summary>
		/// Initializes a new instance of the <see cref="LongPressGesture"/> class with the specified minimum duration, a value indicating whether or not this <see cref="Gesture"/> can be performed continuously and the actuation threshold.
		/// </summary>
		/// <param name="duration">The minimum duration in seconds of a long press.</param>
		/// <param name="isContinuous"><c>true</c> if once performed this <see cref="Gesture"/> should be expected to be performed even if there is no associated input in a given frame; otherwise; <c>false</c>.</param>
		/// <param name="threshold">The value at which an <see cref="InputControl"/> is considered actuated.</param>
		public LongPressGesture(float duration, bool isContinuous = false, float threshold = 0f)
		{
			_duration = duration;
			_isContinuous = isContinuous;
			_threshold = threshold;
		}

		/// <inheritdoc cref="Gesture.Process"/>
		public override void Process(GestureContext context)
		{
			if (context.StartTime > 0 && Time.time - context.StartTime < _duration)
			{
				if (_isContinuous)
				{
					context.PerformedAndStayPerformed();
				}
				else
				{
					context.PerformedAndGoBackToWaiting();
				}

				return;
			}

			bool isActuated = context.IsControlActuated(_threshold);

			switch (context.State)
			{
				case GestureState.Waiting:
					if (isActuated)
					{
						context.Started();
					}

					break;
				case GestureState.Started:
					if (Time.time - context.StartTime >= _duration)
					{
						context.PerformedAndStayPerformed();
					}
					else if (!isActuated)
					{
						context.Canceled();
					}

					break;
				case GestureState.Performed:
					if (isActuated)
					{
						if (_isContinuous)
						{
							context.PerformedAndStayPerformed();
						}
					}
					else
					{
						context.Canceled();
					}

					break;
				default:
					break;
			}
		}

		/// <inheritdoc cref="Gesture.CreateContext"/>
		public override GestureContext CreateContext(InputControl inputControl)
		{
			return new GestureContext(inputControl);
		}
	}
}