﻿// <copyright file="LongPressGestureScriptableObject.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
//  Represents a long press gesture that is stored in a scriptable object.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Gestures
{
	using System;
    
	using UnityEngine;

	/// <summary>
	/// Represents a long press gesture that is stored in a <see cref="ScriptableObject"/>.
	/// </summary>
	[Serializable]
	[CreateAssetMenu(menuName = "VXR/Gestures/Long Press")]
	public class LongPressGestureScriptableObject : GestureScriptableObject
	{
		[SerializeField] private float duration;
		[SerializeField] private bool isContinuous;
		[SerializeField] private float threshold;
		
		/// <inheritdoc cref="GestureScriptableObject.GetGesture"/>
		public override Gesture GetGesture()
		{
			return new LongPressGesture(duration, isContinuous, threshold);
		}
	}
}