﻿// <copyright file="PressGesture.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
//	Represents a press gesture.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Gestures
{
	using Viacom.XR.Input.Controls;

	/// <summary>
	/// Specifies how to recognize a press as performed.
	/// </summary>
	public enum PressBehavior
	{
		/// <summary>
		/// Perform the action when the button is pressed.
		/// </summary>
		PressOnly = 0,

		/// <summary>
		/// Perform the action when the button is released.
		/// </summary>
		ReleaseOnly = 1,

		/// <summary>
		/// Perform the action when the button is pressed and cancel the action when the button is released.
		/// </summary>
		PressAndRelease = 2,
	}
	
	/// <summary>
	/// Represents a press gesture.
	/// </summary>
	public class PressGesture : Gesture
	{
		private readonly PressBehavior _behavior;
		private readonly bool _isContinuous;
		private readonly float _threshold;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="PressGesture"/> class with the specified press behavior, a value indicating whether or not this <see cref="Gesture"/> can be performed continuously and the actuation threshold.
		/// </summary>
		/// <param name="behavior">A value that indicates to recognize a press as performed.</param>
		/// <param name="isContinuous"><c>true</c> if once performed this <see cref="Gesture"/> should be expected to be performed even if there is no associated input in a given frame; otherwise; <c>false</c>.</param>
		/// <param name="threshold">The value at which an <see cref="InputControl"/> is considered actuated.</param>
		public PressGesture(PressBehavior behavior, bool isContinuous = false, float threshold = 0f)
		{
			_behavior = behavior;
			_isContinuous = isContinuous;
			_threshold = threshold;
		}

		/// <inheritdoc cref="Gesture.Process"/>
		public override void Process(GestureContext context)
		{
			PressGestureContext gestureContext = (PressGestureContext) context;
			if (gestureContext == null)
			{
				return;
			}
			
			bool isActuated = gestureContext.IsControlActuated(_threshold);

			switch (_behavior)
			{
				case PressBehavior.PressOnly:
					if (gestureContext.IsWaitingForRelease)
					{
						if (_isContinuous)
						{
							if (isActuated)
							{
								context.PerformedAndStayPerformed();
							}
							else
							{
								context.Waiting();
							}
						}

						gestureContext.IsWaitingForRelease = isActuated;
					}
					else if (isActuated)
					{
						if (_isContinuous)
						{
							context.PerformedAndStayPerformed();
						}
						else
						{
							context.PerformedAndGoBackToWaiting();
						}

						gestureContext.IsWaitingForRelease = true;
					}

					break;
				case PressBehavior.ReleaseOnly:
					if (gestureContext.IsWaitingForRelease && !isActuated)
					{
						gestureContext.IsWaitingForRelease = false;
						context.PerformedAndGoBackToWaiting();
					}
					else if (isActuated)
					{
						gestureContext.IsWaitingForRelease = true;
					}

					break;
				case PressBehavior.PressAndRelease:
					if (gestureContext.IsWaitingForRelease)
					{
						if (_isContinuous)
						{
							if (isActuated)
							{
								gestureContext.PerformedAndStayPerformed();
							}
							else
							{
								gestureContext.PerformedAndGoBackToWaiting();
							}
						}
						else if (!isActuated)
						{
							gestureContext.PerformedAndGoBackToWaiting();
						}

						gestureContext.IsWaitingForRelease = isActuated;
					}
					else if (isActuated)
					{
						if (_isContinuous)
						{
							gestureContext.PerformedAndStayPerformed();
						}
						else
						{
							gestureContext.PerformedAndStayStarted();
						}

						gestureContext.IsWaitingForRelease = true;
					}

					break;
			}
		}

		/// <inheritdoc cref="Gesture.CreateContext"/>
		public override GestureContext CreateContext(InputControl inputControl)
		{
			return new PressGestureContext(inputControl);
		}
	}
	
	/// <summary>
	/// Represents the recognition context for a <see cref="PressGesture"/>. 
	/// </summary>
	public class PressGestureContext : GestureContext
	{
		/// <summary>
		/// Gets or sets a value indicating whether or not the <see cref="PressGesture"/> is waiting for the bound <see cref="InputControl"/> to not be actuated.
		/// </summary>
		public bool IsWaitingForRelease { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="PressGestureContext"/> class with the specified bound <see cref="InputControl"/>.
		/// </summary>
		/// <param name="inputControl">The <see cref="InputControl"/> to read input from.</param>
		public PressGestureContext(InputControl inputControl) : base(inputControl)
		{
		}
	}
}