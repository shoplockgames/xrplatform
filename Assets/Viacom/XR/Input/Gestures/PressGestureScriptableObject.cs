﻿// <copyright file="PressGestureScriptableObject.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
//  Represents a press gesture that is stored in a scriptable object.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Gestures
{
	using System;
	
	using UnityEngine;

	/// <summary>
	/// Represents a press gesture that is stored in a <see cref="ScriptableObject"/>.
	/// </summary>
	[Serializable]
	[CreateAssetMenu(menuName = "VXR/Gestures/Press")]
	public class PressGestureScriptableObject : GestureScriptableObject
	{
		[SerializeField] private PressBehavior behavior;
		[SerializeField] private bool isContinuous;
		[SerializeField] private float threshold;
		
		/// <inheritdoc cref="GestureScriptableObject.GetGesture"/>
		public override Gesture GetGesture()
		{
			return new PressGesture(behavior, isContinuous, threshold);
		}
	}
}