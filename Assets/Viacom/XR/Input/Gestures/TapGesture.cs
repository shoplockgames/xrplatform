﻿// <copyright file="TapGesture.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
//  Represents a tap gesture.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Gestures
{
	using UnityEngine;

	using Viacom.XR.Input.Controls;

	/// <summary>
	/// Represents a tap gesture.
	/// </summary>
	public class TapGesture : Gesture
	{
		private readonly float _timeout;
		private readonly float _duration;
		private readonly float _threshold;

		/// <summary>
		/// Initializes a new instance of the <see cref="TapGesture"/> class with the specified maximum timeout, minimum duration and actuation threshold.
		/// </summary>
		/// <param name="timeout">The maximum amount of time an <see cref="InputControl"/> can be actuated before considering the gesture canceled.</param>
		/// <param name="duration">The minimum duration in seconds of a tap.</param>
		/// <param name="threshold">The value at which an <see cref="InputControl"/> is considered actuated.</param>
		public TapGesture(float timeout, float duration = 0.00001f, float threshold = 0f)
		{
			_timeout = timeout;
			_duration = duration;
			_threshold = threshold;
		}

		/// <inheritdoc cref="Gesture.Process"/>
		public override void Process(GestureContext context)
		{
			if (context.StartTime > 0 && Time.time - context.StartTime >= _timeout &&
			    context.State != GestureState.Canceled)
			{
				context.Canceled();
				return;
			}

			if (context.State == GestureState.Waiting && context.IsControlActuated(_threshold))
			{
				context.Started();
				return;
			}

			if (context.State == GestureState.Started && !context.IsControlActuated(_threshold))
			{
				if (Time.time - context.StartTime >= _duration)
				{
					context.PerformedAndGoBackToWaiting();
				}
				else
				{
					context.Canceled();
				}

				return;
			}

			if (!context.IsControlActuated(_threshold))
			{
				context.Waiting();
			}
		}

		/// <inheritdoc cref="Gesture.CreateContext"/>
		public override GestureContext CreateContext(InputControl inputControl)
		{
			return new GestureContext(inputControl);
		}
	}
}