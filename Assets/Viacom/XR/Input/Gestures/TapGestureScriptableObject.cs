﻿// <copyright file="TapGestureScriptableObject.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
//  Represents a tap gesture that is stored in a scriptable object.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Gestures
{
    using System;
    
    using UnityEngine;

    /// <summary>
    /// Represents a tap gesture that is stored in a <see cref="ScriptableObject"/>.
    /// </summary>
    [Serializable]
    [CreateAssetMenu(menuName = "VXR/Gestures/Tap")]
    public class TapGestureScriptableObject : GestureScriptableObject
    {
        [SerializeField] private float threshold;
        [SerializeField] private float timeout;
        [SerializeField] private float duration;

        /// <inheritdoc cref="GestureScriptableObject.GetGesture"/>
        public override Gesture GetGesture()
        {
            return new TapGesture(timeout, duration, threshold);
        }
    }
}