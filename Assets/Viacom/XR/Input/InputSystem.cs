﻿// <copyright file="InputSystem.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents the main controller for ViacomXR's input management system. The input system is designed to be a singleton
// and all public facing APIs are static. The system maintains a list of active input devices which can be accessed directly.
// The system also supports using abstract actions which can be mapped to input controls such as a joystick or button.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input
{
	using System;
	using System.Collections.Generic;
	
	using UnityEngine;
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WSA
	using UnityEngine.XR;
#endif
	
	using Viacom.XR.Components;
	using Viacom.XR.Diagnostics;
	using Viacom.XR.Input.Actions;
	using Viacom.XR.Input.Devices;
	
	using InputDevice = Viacom.XR.Input.Devices.InputDevice;

	/// <summary>
	/// Represents the main controller for ViacomXR's input management system.
	/// </summary>
	public class InputSystem : Singleton<InputSystem>
	{
		private static readonly string LogTag = typeof(InputSystem).Name;
		private readonly List<InputDevice> _devices = new List<InputDevice>();
		private readonly List<InputAction> _actions = new List<InputAction>();

		/// <summary>
		/// Adds an <see cref="InputAction"/> to the list of active actions.
		/// </summary>
		/// <param name="action">An <see cref="InputAction"/> to add.</param>
		public static void AddAction(InputAction action)
		{
			if (!Instance._actions.Contains(action))
			{
				Instance._actions.Add(action);
				action.ResolveBindings(Instance._devices);
			}
		}
		
		/// <summary>
		/// Adds an <see cref="InputDevice"/> to the list of tracked devices.
		/// </summary>
		/// <param name="usage">The optional usage of the <see cref="InputDevice"/>.</param>
		/// <typeparam name="T">The type of the <see cref="InputDevice"/>.</typeparam>
		/// <returns>The newly added <see cref="InputDevice"/>.</returns>
		public static T AddDevice<T>(string usage = null) where T : InputDevice, new()
		{
			T device = new T();
			device.Usage = usage;
			Instance._devices.Add(device);
			ApplicationLog.Log(LogTag, string.Format("New device [{0}] ({1}) has been added", typeof(T), usage));
			return device;
		}
		
		/// <summary>
		/// Gets the first input device of the specified type in the list of tracked devices.
		/// </summary>
		/// <param name="usage">The optional usage of the <see cref="InputDevice"/>.</param>
		/// <typeparam name="T">The type of the <see cref="InputDevice"/>.</typeparam>
		/// <returns>The first device of type <typeparam name="T">T</typeparam> in the list of tracked devices if found; otherwise, <c>null</c>.</returns>
		public static T GetDevice<T>(string usage = null) where T : InputDevice
		{
			foreach (InputDevice device in Instance._devices)
			{
				T deviceOfType = device as T;
				if (deviceOfType != null)
				{
					if (!string.IsNullOrEmpty(usage))
					{
						if (!string.IsNullOrEmpty(device.Usage) &&
						    device.Usage.Equals(usage, StringComparison.InvariantCultureIgnoreCase))
						{
							return deviceOfType;
						}
					}
					else
					{
						return deviceOfType;
					}
				}
			}
			return null;
		}
		
		/// <summary>
		/// Gets a list of the input devices of the specified type in the list of tracked devices.
		/// </summary>
		/// <typeparam name="T">The type of the <see cref="InputDevice"/>.</typeparam>
		/// <returns>A list of devices of type <typeparam name="T">T</typeparam> in the list of tracked devices.</returns>
		public static T[] GetDevices<T>() where T : InputDevice
		{
			List<T> result = new List<T>();
			foreach (InputDevice device in Instance._devices)
			{
				T deviceOfType = device as T;
				if (deviceOfType != null)
				{
					result.Add(deviceOfType);
				}
			}
			return result.ToArray();
		}

		/// <inheritdoc cref="MonoBehaviour.Awake"/>
		protected override void Awake()
		{
#if (UNITY_EDITOR || UNITY_STANDALONE || UNITY_WSA) && ENABLE_STEAM_VR
			if (XRSettings.enabled)
			{
				Plugins.VR.SteamVRSupport.Initialize();
			}
#endif
			
#if PLATFORM_LUMIN
			Plugins.MagicLeap.MagicLeapSupport.Initialize();
#endif
			AddDevice<Keyboard>();
			base.Awake();
		}

		/// <inheritdoc cref="MonoBehaviour.Update"/>
		private void Update()
		{
			for (int i = 0; i < _actions.Count; i++)
			{
				_actions[i].Update();
			}
		}
	}
}