﻿// <copyright file="PlayerInput.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a wrapper around the input system that takes care of managing input actions.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input
{
	using System;
	using System.Linq;

	using UnityEngine;
	using UnityEngine.Events;

	using Viacom.XR.Input.Actions;

	/// <summary>
	/// Represents a wrapper around the <see cref="InputSystem"/> that takes care of managing <see cref="InputAction"/>s.
	/// </summary>
	[DisallowMultipleComponent]
	public class PlayerInput : MonoBehaviour
	{
		[SerializeField] private InputActionsAsset actions;
		[SerializeField] private ActionEvent[] events;

		/// <inheritdoc cref="MonoBehaviour.Start"/>
		private void Start()
		{
			if (actions != null)
			{
				actions.RegisterActions();
				for (int i = 0; i < actions.inputActions.Length; i++)
				{
					ActionEvent actionEvent = events.FirstOrDefault(x => x.id.Equals(actions.inputActions[i].Id));
					if (actionEvent == null)
					{
						continue;
					}

					actionEvent.RegisterListeners(actions.inputActions[i]);
				}
			}
		}

		/// <summary>
		/// Generates a set of mappings between <see cref="UnityEvent"/>s and the events fired from an
		/// <see cref="InputAction"/>.
		/// </summary>
		public void GenerateActionEvents()
		{
			ActionEvent[] newEvents = new ActionEvent[actions.inputActions.Length];
			for (int i = 0; i < actions.inputActions.Length; i++)
			{
				ActionEvent actionEvent = events.FirstOrDefault(x => x.id.Equals(actions.inputActions[i].Id));
				newEvents[i] = actionEvent != null ? actionEvent : new ActionEvent(actions.inputActions[i]);
			}

			events = newEvents;
		}
	}

	/// <summary>
	/// Represents a mapping between <see cref="UnityEvent"/>s and the events fired from an <see cref="InputAction"/>.
	/// </summary>
	[Serializable]
	public class ActionEvent
	{
		[HideInInspector] public string name;
		[HideInInspector] public string id;
		public UnityEvent performed;
		public UnityEvent canceled;
		public UnityEvent started;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ActionEvent"/> class with the specified <see cref="InputAction"/>.
		/// </summary>
		/// <param name="inputAction">The <see cref="InputAction"/> to listen for events from.</param>
		public ActionEvent(InputAction inputAction)
		{
			name = inputAction.Name;
			id = inputAction.Id;
		}

		/// <summary>
		/// Callback for the <see cref="InputEventRaiser.Started"/> event from an <see cref="InputAction"/>.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		private void InputActionOnStarted(object sender)
		{
			if (started != null)
			{
				started.Invoke();
			}
		}

		/// <summary>
		/// Callback for the <see cref="InputEventRaiser.Canceled"/> event from an <see cref="InputAction"/>.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		private void InputActionOnCanceled(object sender)
		{
			if (canceled != null)
			{
				canceled.Invoke();
			}
		}

		/// <summary>
		/// Callback for the <see cref="InputEventRaiser.Performed"/> event from an <see cref="InputAction"/>.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		private void InputActionOnPerformed(object sender)
		{
			if (performed != null)
			{
				performed.Invoke();
			}
		}

		/// <summary>
		/// Registers all event listeners for the given <see cref="InputAction"/>
		/// </summary>
		/// <param name="inputAction">The <see cref="InputAction"/> to listen for events from.</param>
		public void RegisterListeners(InputAction inputAction)
		{
			inputAction.Started -= InputActionOnStarted;
			inputAction.Started += InputActionOnStarted;
			inputAction.Performed -= InputActionOnPerformed;
			inputAction.Performed += InputActionOnPerformed;
			inputAction.Canceled -= InputActionOnCanceled;
			inputAction.Canceled += InputActionOnCanceled;
		}
	}
}