﻿// <copyright file="MagicLeapSupport.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents support for the Magic Leap input.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

#if PLATFORM_LUMIN

namespace Viacom.XR.Input.Plugins.MagicLeap
{
    using System;

    using UnityEngine;
    using UnityEngine.XR.MagicLeap;

    using Viacom.XR.Diagnostics;
    using Viacom.XR.Input.Controls;
    using Viacom.XR.Input.Devices;

    /// <summary>
    /// Represents support for the Magic Leap.
    /// </summary>
    public static class MagicLeapSupport
    {
        /// <summary>
        /// Initializes support for input from Magic Leap.
        /// </summary>
        public static void Initialize()
        {
            MLResult result = MLInput.Start();

            if (!result.IsOk)
            {
                ErrorReporter.LogHandledException(new Exception("Could not initialize input on Magic Leap"));
                return;
            }

            InputSystem.AddDevice<MagicLeapController>();
        }
    }

    /// <summary>
    /// Represents a Magic Leap controller.
    /// </summary>
    public class MagicLeapController : InputDevice
    {
        private readonly MLInputController _controller;

        /// <summary>
        /// Gets the position of the device.
        /// </summary>
        public Vector3 Position
        {
            get { return _controller != null ? _controller.Position : Vector3.zero; }
        }

        /// <summary>
        /// Gets the orientation of the device.
        /// </summary>
        public Quaternion Rotation
        {
            get { return _controller != null ? _controller.Orientation : Quaternion.identity; }
        }

        /// <summary>
        /// Gets a value indicating whether or not the controller is currently connected.
        /// </summary>
        public bool IsConnected
        {
            get { return _controller != null && _controller.Connected; }
        }

        /// <summary>
        /// Represents the bumper on a Magic Leap controller. This field is read-only.
        /// </summary>
        public readonly MagicLeapBumperControl Bumper;

        /// <summary>
        /// Represents the trigger on a Magic Leap controller. This field is read-only.
        /// </summary>
        public readonly MagicLeapTriggerControl Trigger;

        /// <summary>
        /// Initializes a new instance of the <see cref="MagicLeapController"/> class.
        /// </summary>
        public MagicLeapController()
        {
            _controller = MLInput.GetController(0);
            Bumper = new MagicLeapBumperControl(this, "bumper", _controller);
            Trigger = new MagicLeapTriggerControl(this, "trigger", _controller);
        }
    }

    /// <summary>
    /// Represents the bumper on a Magic Leap controller.
    /// </summary>
    public class MagicLeapBumperControl : InputControl<bool>
    {
        private readonly MLInputController _controller;

        /// <inheritdoc cref="InputControl{T}.Value"/>
        public override bool Value
        {
            get { return _controller != null ? _controller.IsBumperDown : false; }
        }

        /// <inheritdoc cref="InputControl.Magnitude"/>
        public override float Magnitude
        {
            get { return Value ? 1.0f : 0.0f; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MagicLeapBumperControl"/> class with a specified
        /// <see cref="InputDevice"/> that contains it as well as corresponding Magic Leap controller.
        /// </summary>
        /// <param name="device">The <see cref="InputDevice"/> that contains this control.</param>
        /// <param name="name">The name of the <see cref="MagicLeapBumperControl"/>.</param>
        /// <param name="controller">A Magic Leap controller.</param>
        /// <param name="aliases">The list of aliases of the <see cref="MagicLeapBumperControl"/>.</param>
        public MagicLeapBumperControl(InputDevice device, string name, MLInputController controller,
            string[] aliases = null) : base(device, name, aliases)
        {
            _controller = controller;
        }
    }

    /// <summary>
    /// Represents the trigger on a Magic Leap controller.
    /// </summary>
    public class MagicLeapTriggerControl : InputControl<float>
    {
        private readonly MLInputController _controller;

        /// <inheritdoc cref="InputControl{T}.Value"/>
        public override float Value
        {
            get { return _controller != null ? _controller.TriggerValue : 0; }
        }

        /// <inheritdoc cref="InputControl.Magnitude"/>
        public override float Magnitude
        {
            get { return Value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MagicLeapTriggerControl"/> class with a specified
        /// <see cref="InputDevice"/> that contains it as well as corresponding Magic Leap controller.
        /// </summary>
        /// <param name="device">The <see cref="InputDevice"/> that contains this control.</param>
        /// <param name="name">The name of the <see cref="MagicLeapTriggerControl"/>.</param>
        /// <param name="controller">A Magic Leap controller.</param>
        /// <param name="aliases">The list of aliases of the <see cref="MagicLeapTriggerControl"/>.</param>
        public MagicLeapTriggerControl(InputDevice device, string name, MLInputController controller,
            string[] aliases = null) : base(device, name, aliases)
        {
            _controller = controller;
        }
    }
}

#endif