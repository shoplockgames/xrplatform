﻿// <copyright file="MissingSteamVRActionException.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// The exception that is thrown when an attempt to load a SteamVR action fails.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Plugins.VR
{
    using System;
    
    using Valve.VR;
    
    /// <summary>
    /// The exception that is thrown when an attempt to load a <see cref="SteamVR_Action"/> fails.
    /// </summary>
    public sealed class MissingSteamVRActionException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MissingSteamVRActionException"/> class.
        /// </summary>
        public MissingSteamVRActionException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MissingSteamVRActionException"/> class with a specified error message.
        /// </summary>
        /// <param name="message">A description of the error.</param>
        public MissingSteamVRActionException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MissingSteamVRActionException"/> class with a specified error message and the name of the <see cref="SteamVR_Action"/> that failed to load.
        /// </summary>
        /// <param name="message">A description of the error.</param>
        /// <param name="name">The name of the <see cref="SteamVR_Action"/> that failed to load.</param>
        public MissingSteamVRActionException(string message, string name) : base(message)
        {
            Data["action"] = name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MissingSteamVRActionException"/> class with a specified error message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">A description of the error.</param>
        /// <param name="inner">The exception that is the cause of the current exception.</param>
        public MissingSteamVRActionException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}

