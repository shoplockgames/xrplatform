﻿// <copyright file="MissingSteamVRActionSetException.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// The exception that is thrown when an attempt to load a SteamVR action set fails.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Plugins.VR
{
	using System;
	
	using Valve.VR;
	
	/// <summary>
	/// The exception that is thrown when an attempt to load a <see cref="SteamVR_ActionSet"/> fails.
	/// </summary>
	public sealed class MissingSteamVRActionSetException : Exception
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="MissingSteamVRActionSetException"/> class.
		/// </summary>
		public MissingSteamVRActionSetException()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MissingSteamVRActionSetException"/> class with a specified error message.
		/// </summary>
		/// <param name="message">A description of the error.</param>
		public MissingSteamVRActionSetException(string message) : base(message)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MissingSteamVRActionSetException"/> class with a specified error message and the name of the <see cref="SteamVR_ActionSet"/> that failed to load.
		/// </summary>
		/// <param name="message">A description of the error.</param>
		/// <param name="name">The name of the <see cref="SteamVR_ActionSet"/> that failed to load.</param>
		public MissingSteamVRActionSetException(string message, string name) : base(message)
		{
			Data["action_set"] = name;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MissingSteamVRActionSetException"/> class with a specified error message and a reference to the inner exception that is the cause of this exception.
		/// </summary>
		/// <param name="message">A description of the error.</param>
		/// <param name="inner">The exception that is the cause of the current exception.</param>
		public MissingSteamVRActionSetException(string message, Exception inner) : base(message, inner)
		{
		}
	}
}