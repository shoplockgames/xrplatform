﻿// <copyright file="SteamVRDevice.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents input devices and controls that utilize the SteamVR plugin.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Plugins.VR
{
	using System;

	using UnityEngine;

	using Valve.VR;

	using Viacom.XR.Input.Controls;
	using Viacom.XR.Input.Devices;

	/// <summary>
	/// Represents an input device that utilizes the SteamVR plugin.
	/// </summary>
	public class SteamVRInputDevice : InputDevice
	{
		public SteamVR_Input_Sources SteamVRInputSource
		{
			get { return (SteamVR_Input_Sources) Enum.Parse(typeof(SteamVR_Input_Sources), Usage, true); }
		}
	}

	/// <summary>
	/// Represents a head mounted display that utilizes the SteamVR plugin.
	/// </summary>
	public class SteamVRHmd : SteamVRInputDevice
	{
		private readonly SteamVR_Action_Boolean _headsetOnHeadAction = SteamVR_Input.GetBooleanAction("HeadsetOnHead");

		/// <summary>
		/// Gets a value indicating whether or not the headset is current on the user's head.
		/// </summary>
		public bool IsOnHead
		{
			get { return _headsetOnHeadAction.state; }
		}
	}

	/// <summary>
	/// Represents an input device that utilizes the SteamVR plugin and can be posed.
	/// </summary>
	public class SteamVRPosedDevice : SteamVRInputDevice
	{
		private readonly SteamVR_Action_Pose _poseAction = SteamVR_Input.GetAction<SteamVR_Action_Pose>("Pose");

		/// <summary>
		/// Gets the position of the device.
		/// </summary>
		public Vector3 Position
		{
			get { return _poseAction[SteamVRInputSource].localPosition; }
		}

		/// <summary>
		/// Gets the orientation of the device.
		/// </summary>
		public Quaternion Rotation
		{
			get { return _poseAction[SteamVRInputSource].localRotation; }
		}

		/// <summary>
		/// Gets the velocity of the device.
		/// </summary>
		public Vector3 Velocity
		{
			get { return _poseAction[SteamVRInputSource].velocity; }
		}

		/// <summary>
		/// Gets the angular velocity of the device.
		/// </summary>
		public Vector3 AngularVelocity
		{
			get { return _poseAction[SteamVRInputSource].angularVelocity; }
		}

		/// <summary>
		/// Gets a value indicating whether or not the current pose is valid.
		/// </summary>
		public bool IsPoseValid
		{
			get { return _poseAction[SteamVRInputSource].poseIsValid; }
		}
	}

	/// <summary>
	/// Represents a SteamVR input controller.
	/// </summary>
	public abstract class SteamVRController : SteamVRPosedDevice
	{
		/// <summary>
		/// Gets the left hand controller.
		/// </summary>
		public static SteamVRController LeftHand
		{
			get { return InputSystem.GetDevice<SteamVRController>(SteamVR_Input_Sources.LeftHand.ToString()); }
		}

		/// <summary>
		/// Gets the right hand controller.
		/// </summary>
		public static SteamVRController RightHand
		{
			get { return InputSystem.GetDevice<SteamVRController>(SteamVR_Input_Sources.RightHand.ToString()); }
		}

		public readonly SteamVRFloatControl Trigger;
		public readonly SteamVRBooleanControl TriggerButton;
		public readonly SteamVRBooleanControl GripButton;
		public readonly SteamVRBooleanControl MenuButton;

		/// <summary>
		/// Gets the SteamVR action set name for this input device.
		/// </summary>
		protected abstract string ActionSetName { get; }

		/// <summary>
		/// Initializes a new instance of the <see cref="SteamVRController"/> class.
		/// </summary>
		/// <exception cref="MissingSteamVRActionSetException">The action set for this device cannot be loaded.</exception>
		protected SteamVRController()
		{
			SteamVR_ActionSet actionSet = SteamVR_Input.GetActionSet(ActionSetName);

			if (actionSet == null)
			{
				throw new MissingSteamVRActionSetException("Could not find SteamVR action set", ActionSetName);
			}

			actionSet.Activate();
			Trigger = new SteamVRFloatControl(this, "trigger", ActionSetName, "Trigger");
			TriggerButton = new SteamVRBooleanControl(this, "triggerButton", ActionSetName, "TriggerButton");
			GripButton = new SteamVRBooleanControl(this, "gripButton", ActionSetName, "GripButton");
			MenuButton = new SteamVRBooleanControl(this, "menuButton", ActionSetName, "MenuButton");
		}
	}

	/// <summary>
	/// Represents a SteamVR input control that returns a boolean.
	/// </summary>
	public class SteamVRBooleanControl : InputControl<bool>
	{
		private readonly SteamVR_Action_Boolean _steamVrAction;

		/// <inheritdoc cref="InputControl{T}.Value"/>
		public override bool Value
		{
			get { return _steamVrAction.GetState(((SteamVRInputDevice) Device).SteamVRInputSource); }
		}
		
		/// <inheritdoc cref="InputControl.Magnitude"/>
		public override float Magnitude
		{
			get { return  Value ? 1.0f : 0.0f; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SteamVRBooleanControl"/> class with a specified
		/// <see cref="InputDevice"/> that contains it as well as the names of the <see cref="SteamVR_ActionSet"/> and
		/// <see cref="SteamVR_Action"/> used to read input.
		/// </summary>
		/// <param name="device">The <see cref="InputDevice"/> that contains this control.</param>
		/// <param name="name">The name of the <see cref="SteamVRBooleanControl"/>.</param>
		/// <param name="actionSetName">The name of <see cref="SteamVR_ActionSet"/> that contains the <see cref="SteamVR_Action"/> used to read the input.</param>
		/// <param name="actionName">The name of <see cref="SteamVR_Action"/> used to read the input.</param>
		/// <param name="aliases">The list of aliases of the <see cref="SteamVRBooleanControl"/>.</param>
		/// <exception cref="MissingSteamVRActionException"><paramref name="actionName"/> cannot be loaded.</exception>
		public SteamVRBooleanControl(InputDevice device, string name, string actionSetName, string actionName,
			string[] aliases = null) : base(device, name, aliases)
		{
			_steamVrAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>(actionSetName, actionName);

			if (_steamVrAction == null)
			{
				throw new MissingSteamVRActionException("Could not find specified SteamVR action.", actionName);
			}
		}
	}

	/// <summary>
	/// Represents a SteamVR input control that returns a float.
	/// </summary>
	public class SteamVRFloatControl : InputControl<float>
	{
		private readonly SteamVR_Action_Single _steamVrAction;

		/// <inheritdoc cref="InputControl{T}.Value"/>
		public override float Value
		{
			get { return _steamVrAction.GetAxis(((SteamVRInputDevice) Device).SteamVRInputSource); }
		}
		
		/// <inheritdoc cref="InputControl.Magnitude"/>
		public override float Magnitude
		{
			get { return Value; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SteamVRFloatControl"/> class with a specified
		/// <see cref="InputDevice"/> that contains it as well as the names of the <see cref="SteamVR_ActionSet"/> and
		/// <see cref="SteamVR_Action"/> used to read input.
		/// </summary>
		/// <param name="device">The <see cref="InputDevice"/> that contains this control.</param>
		/// <param name="name">The name of the <see cref="SteamVRFloatControl"/>.</param>
		/// <param name="actionSetName">The name of <see cref="SteamVR_ActionSet"/> that contains the <see cref="SteamVR_Action"/> used to read the input.</param>
		/// <param name="actionName">The name of <see cref="SteamVR_Action"/> used to read the input.</param>
		/// <param name="aliases">The list of aliases of the <see cref="SteamVRFloatControl"/>.</param>
		/// <exception cref="MissingSteamVRActionException"><paramref name="actionName"/> cannot be loaded.</exception>
		public SteamVRFloatControl(InputDevice device, string name, string actionSetName, string actionName,
			string[] aliases = null) : base(device, name, aliases)
		{
			_steamVrAction = SteamVR_Input.GetAction<SteamVR_Action_Single>(actionSetName, actionName);

			if (_steamVrAction == null)
			{
				throw new MissingSteamVRActionException("Could not find specified SteamVR action.", actionName);
			}
		}
	}

	/// <summary>
	/// Represents a SteamVR input control that returns a two-dimensional vector.
	/// </summary>
	public class SteamVRVector2Control : InputControl<Vector2>
	{
		private readonly SteamVR_Action_Vector2 _steamVrAction;

		/// <inheritdoc cref="InputControl{T}.Value"/>
		public override Vector2 Value
		{
			get { return _steamVrAction.GetAxis(((SteamVRInputDevice) Device).SteamVRInputSource); }
		}
		
		/// <inheritdoc cref="InputControl.Magnitude"/>
		public override float Magnitude
		{
			get { return Value.magnitude; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SteamVRVector2Control"/> class with a specified
		/// <see cref="InputDevice"/> that contains it as well as the names of the <see cref="SteamVR_ActionSet"/> and
		/// <see cref="SteamVR_Action"/> used to read input.
		/// </summary>
		/// <param name="device">The <see cref="InputDevice"/> that contains this control.</param>
		/// <param name="name">The name of the <see cref="SteamVRVector2Control"/>.</param>
		/// <param name="actionSetName">The name of <see cref="SteamVR_ActionSet"/> that contains the <see cref="SteamVR_Action"/> used to read the input.</param>
		/// <param name="actionName">The name of <see cref="SteamVR_Action"/> used to read the input.</param>
		/// <param name="aliases">The list of aliases of the <see cref="SteamVRVector2Control"/>.</param>
		/// <exception cref="MissingSteamVRActionException"><paramref name="actionName"/> cannot be loaded.</exception>
		public SteamVRVector2Control(InputDevice device, string name, string actionSetName, string actionName,
			string[] aliases = null) : base(device, name, aliases)
		{
			_steamVrAction = SteamVR_Input.GetAction<SteamVR_Action_Vector2>(actionSetName, actionName);

			if (_steamVrAction == null)
			{
				throw new MissingSteamVRActionException("Could not find specified SteamVR action.", actionName);
			}
		}
	}
}