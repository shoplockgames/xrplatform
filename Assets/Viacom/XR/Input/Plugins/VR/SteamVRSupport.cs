﻿// <copyright file="SteamVRSupport.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents support for the SteamVR input plugin.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Plugins.VR
{
	using System;

	using Valve.VR;

	using Viacom.XR.Diagnostics;

	/// <summary>
	/// Represents support for the SteamVR input plugin.
	/// </summary>
	public static class SteamVRSupport
	{
		/// <summary>
		/// Initializes support for input from SteamVR.
		/// </summary>
		public static void Initialize()
		{
			SteamVR.Initialize();

			switch (SteamVR.instance.hmd_TrackingSystemName)
			{
				case "holographic":
					WindowsMixedRealitySupport.Initialize();
					break;
				default:
					ErrorReporter.LogHandledException(
						new Exception("Unsupported HMD detected: " + SteamVR.instance.hmd_TrackingSystemName));
					return;
			}
		}
	}
}