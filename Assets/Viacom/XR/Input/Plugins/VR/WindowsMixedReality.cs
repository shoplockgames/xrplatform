﻿// <copyright file="WindowsMixedReality.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents support for Windows Mixed Reality devices.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Input.Plugins.VR
{
    using Valve.VR;
    
    /// <summary>
    /// Represents support for Windows Mixed Reality devices.
    /// </summary>
    public static class WindowsMixedRealitySupport
    {
        /// <summary>
        /// Initializes support for Windows Mixed Reality devices.
        /// </summary>
        public static void Initialize()
        {
            InputSystem.AddDevice<WindowsMixedRealityHmd>();
            InputSystem.AddDevice<WindowsMixedRealityMotionController>(SteamVR_Input_Sources.LeftHand.ToString());
            InputSystem.AddDevice<WindowsMixedRealityMotionController>(SteamVR_Input_Sources.RightHand.ToString());
        }   
    }

    /// <summary>
    /// Represents a Windows Mixed Reality head mounted display.
    /// </summary>
    public class WindowsMixedRealityHmd : SteamVRHmd
    {
    }
    
    /// <summary>
    /// Represents a Windows Mixed Reality motion controller.
    /// </summary>
    public class WindowsMixedRealityMotionController : SteamVRController
    {
        /// <inheritdoc cref="SteamVRController.ActionSetName"/>
        protected override string ActionSetName
        {
            get { return "windowsmixedreality"; }
        }
        
        public readonly SteamVRVector2Control Trackpad;
        public readonly SteamVRVector2Control Joystick;

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowsMixedRealityMotionController"/> class.
        /// </summary>
        public WindowsMixedRealityMotionController()
        {
            Trackpad = new SteamVRVector2Control(this, "trackpad", ActionSetName,"Trackpad");
            Joystick = new SteamVRVector2Control(this, "joystick", ActionSetName,"Joystick");
        }
    }
}