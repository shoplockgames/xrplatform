﻿// <copyright file="ConditionalFieldAttribute.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Provides a mechanism to specify that a field is conditional on another field.
// </summary>
//
// <remarks>
// Due to how arrays are shown in the inspector this does not work for array types.
// </remarks>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR
{
	using System;
	using System.Collections.Generic;
	
#if UNITY_EDITOR
	using UnityEditor;
#endif
	using UnityEngine;
	
	/// <summary>
	/// Represents an property attribute that marks a variable as visible in the inspector conditional on another variable.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field)]
	public class ConditionalFieldAttribute : PropertyAttribute
	{
		private readonly string _propertyToCheck;
		private readonly object _compareValue;
		private readonly HashSet<object> _warningsPool = new HashSet<object>();

		private const string NullValue = "NULL";
		private const string FalseValue = "FALSE";
		private const string CharType = "char";
		private const string LeftBracket = "[";
		private const string RightBracket = "]";
		private const string ArrayProperty = ".Array.data[";
		private const char PathSeparator = '.';
		private const string ArrayPropertyWarning = "Array fields is not supported by [ConditionalFieldAttribute]";
		private const string PropertyWarningFormat =
			"Property <color=brown>{0}</color> in object <color=brown>{1}</color> caused: ";
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ConditionalFieldAttribute"/> class with the specified property name and value to compare.
		/// </summary>
		/// <param name="propertyToCheck">The name of the property to check against.</param>
		/// <param name="compareValue">The value to compare the value of <paramref name="propertyToCheck"/> to.</param>
		public ConditionalFieldAttribute(string propertyToCheck, object compareValue = null)
		{
			_propertyToCheck = propertyToCheck;
			_compareValue = compareValue;
		}

#if UNITY_EDITOR
		/// <summary>
		/// Gets a value indicating whether or not the specified property on the specified <see cref="MonoBehaviour"/>
		/// should be visible in the inspector.
		/// </summary>
		/// <param name="behaviour">The <see cref="MonoBehaviour"/> that contains the property.</param>
		/// <param name="propertyName">The name of the property.</param>
		/// <returns><c>true</c> if the property named <paramref name="propertyName"/> should be visible; otherwise, <c>false</c>.</returns>
		public bool CheckBehaviourPropertyVisible(MonoBehaviour behaviour, string propertyName)
		{
			if (string.IsNullOrEmpty(_propertyToCheck))
			{
				return true;
			}

			SerializedObject serializedObject = new SerializedObject(behaviour);
			SerializedProperty property = serializedObject.FindProperty(propertyName);
			return CheckPropertyVisible(property);
		}

		/// <summary>
		/// Gets a value indicating whether or not the specified property should be visible in the inspector.
		/// </summary>
		/// <param name="property">The <see cref="SerializedProperty"/> to check.</param>
		/// <returns><c>true</c> if the <paramref name="property"/> should be visible; otherwise, <c>false</c>.</returns>
		public bool CheckPropertyVisible(SerializedProperty property)
		{
			SerializedProperty conditionProperty = FindRelativeProperty(property, _propertyToCheck);
			if (conditionProperty == null)
			{
				return true;
			}
			bool isBoolMatch = conditionProperty.propertyType == SerializedPropertyType.Boolean &&
			                   conditionProperty.boolValue;
			string compareStringValue = _compareValue != null ? _compareValue.ToString().ToUpper() : NullValue;
			if (isBoolMatch && compareStringValue == FalseValue)
			{
				isBoolMatch = false;
			}

			string conditionPropertyStringValue = AsStringValue(conditionProperty).ToUpper();
			bool objectMatch = compareStringValue == conditionPropertyStringValue;
			bool notVisible = !isBoolMatch && !objectMatch;
			return !notVisible;
		}

		/// <summary>
		/// Finds the <see cref="SerializedProperty"/> with specified path in the same <see cref="SerializedObject"/>
		/// hierarchy as the specified <see cref="SerializedProperty"/>.
		/// </summary>
		/// <param name="property">The relative <see cref="SerializedProperty"/> to search from.</param>
		/// <param name="propertyPath">The property path to find.</param>
		/// <returns>A <see cref="SerializedProperty"/> with the path <paramref name="propertyPath"/>.</returns>
		private SerializedProperty FindRelativeProperty(SerializedProperty property, string propertyPath)
		{
			if (property.depth == 0)
			{
				return property.serializedObject.FindProperty(propertyPath);
			}

			string path = property.propertyPath.Replace(ArrayProperty, LeftBracket);
			string[] elements = path.Split(PathSeparator);

			SerializedProperty nestedProperty = NestedPropertyOrigin(property, elements);

			// if nested property is null = we hit an array property
			if (nestedProperty == null)
			{
				string cleanPath = path.Substring(0, path.IndexOf(LeftBracket));
				SerializedProperty arrayProp = property.serializedObject.FindProperty(cleanPath);
				if (_warningsPool.Contains(arrayProp.exposedReferenceValue))
				{
					return null;
				}
				UnityEngine.Object target = arrayProp.serializedObject.targetObject;
				string who = string.Format(PropertyWarningFormat, arrayProp.name, target.name);
				Debug.LogWarning(who + ArrayPropertyWarning, target);
				_warningsPool.Add(arrayProp.exposedReferenceValue);
				return null;
			}

			return nestedProperty.FindPropertyRelative(propertyPath);
		}

		/// <summary>
		/// Gets the parent <see cref="SerializedProperty"/> of the specified elements in the same <see cref="SerializedObject"/>
		/// hierarchy as the specified <see cref="SerializedProperty"/>.
		/// </summary>
		/// <param name="property">The relative <see cref="SerializedProperty"/> to search from.</param>
		/// <param name="elements">The child elements.</param>
		/// <returns>The parent <see cref="SerializedProperty"/> of <paramref name="elements"/> that is in the same hierarchy as <paramref name="property"/>.</returns>
		private SerializedProperty NestedPropertyOrigin(SerializedProperty property, string[] elements)
		{
			SerializedProperty parent = null;

			for (int i = 0; i < elements.Length - 1; i++)
			{
				string element = elements[i];
				int index = -1;
				if (element.Contains(LeftBracket))
				{
					index = Convert.ToInt32(element.Substring(element.IndexOf(LeftBracket, StringComparison.Ordinal))
						.Replace(LeftBracket, string.Empty).Replace(RightBracket, string.Empty));
					element = element.Substring(0, element.IndexOf(LeftBracket, StringComparison.Ordinal));
				}

				parent = i == 0
					? property.serializedObject.FindProperty(element)
					: parent.FindPropertyRelative(element);

				if (index >= 0)
				{
					parent = parent.GetArrayElementAtIndex(index);
				}
			}

			return parent;
		}

		/// <summary>
		/// Converts the value of the specified <see cref="SerializedProperty"/> to a <c>string</c>.
		/// </summary>
		/// <param name="property">The <see cref="SerializedProperty"/> to convert.</param>
		/// <returns>A <c>string</c> representation of <paramref name="property"/>.</returns>
		private static string AsStringValue(SerializedProperty property)
		{
			switch (property.propertyType)
			{
				case SerializedPropertyType.String:
					return property.stringValue;
				case SerializedPropertyType.Character:
				case SerializedPropertyType.Integer:
					if (property.type == CharType)
					{
						return Convert.ToChar(property.intValue).ToString();
					}
					return property.intValue.ToString();
				case SerializedPropertyType.ObjectReference:
					return property.objectReferenceValue != null ? property.objectReferenceValue.ToString() : NullValue.ToLower();
				case SerializedPropertyType.Boolean:
					return property.boolValue.ToString();
				case SerializedPropertyType.Enum:
					return property.enumNames[property.enumValueIndex];
				default:
					return string.Empty;
			}
		}
#endif
	}
}

#if UNITY_EDITOR

namespace Viacom.XR.Internal
{
	using UnityEngine;
#if UNITY_EDITOR
	using UnityEditor;
#endif
	
	/// <summary>
	/// Represents a custom drawer for the <see cref="ConditionalFieldAttribute"/>.
	/// </summary>
	[CustomPropertyDrawer(typeof(ConditionalFieldAttribute))]
	public class ConditionalFieldAttributeDrawer : PropertyDrawer
	{
		private ConditionalFieldAttribute _attribute;
		private bool _shouldShow = true;
		
		/// <summary>
		/// Gets the <see cref="PropertyAttribute"/> for the property being drawn.
		/// </summary>
		private ConditionalFieldAttribute Attribute
		{
			get { return _attribute ?? (_attribute = attribute as ConditionalFieldAttribute); }
		}

		/// <inheritdoc cref="PropertyDrawer.GetPropertyHeight"/>
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			_shouldShow = Attribute.CheckPropertyVisible(property);
			return _shouldShow ? EditorGUI.GetPropertyHeight(property) : 0;
		}

		/// <inheritdoc cref="PropertyDrawer.OnGUI"/>
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (_shouldShow)
			{
				EditorGUI.PropertyField(position, property, label, true);
			}
		}
	}
}

#endif