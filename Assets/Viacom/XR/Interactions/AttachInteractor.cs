﻿// <copyright file="AttachInteractor.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a game object that can attach another game object.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Interactions
{
	using System.Collections;
	using System.Collections.Generic;

	using UnityEngine;

	using Viacom.XR.Commands;
	using Viacom.XR.Coroutines;
	using Viacom.XR.Math;

	/// <summary>
	/// Represents a game object that can attach another game object.
	/// </summary>
	public class AttachInteractor : Interactable
	{
		[SerializeField] private Transform attachmentPoint;

		private readonly Dictionary<Attachable, Command> _attachCommands = new Dictionary<Attachable, Command>();

		/// <summary>
		/// Attaches any <see cref="Attachable"/> objects that are currently being hovered over. 
		/// </summary>
		public void AttachAll()
		{
			HoverInteractor hoverInteractor = GetComponent<HoverInteractor>();
			
			if (hoverInteractor == null)
			{
				return;
			}
			
			Interaction[] hoverInteractions = hoverInteractor.Interactions;
			if (hoverInteractions.Length < 0 || (!supportsMultipleInteractions && Interactions.Length != 0))
			{
				return;
			}

			for (int i = 0; i < hoverInteractions.Length; i++)
			{
				Attachable attachable = hoverInteractions[i].Interactable.GetComponent<Attachable>();

				if (attachable == null)
				{
					attachable =  hoverInteractions[i].Interactable.GetComponentInChildren<Attachable>();
					
					if (attachable == null)
					{
						attachable =  hoverInteractions[i].Interactable.GetComponentInParent<Attachable>();
						
						if (attachable == null)
						{
							continue;
						}
					}
				}

				if (Attach(attachable) && !supportsMultipleInteractions)
				{
					break;
				}
			}
		}

		/// <summary>
		/// Attaches the specified <see cref="Attachable"/> and returns a value indicating whether or not the operation was successful. 
		/// </summary>
		/// <param name="attachable">The <see cref="Attachable"/> to attach.</param>
		/// <param name="attachmentOffset">An optional offset.</param>
		/// <returns><c>true</c> if the <paramref name="attachable"/> was attached successfully; otherwise, <c>false</c>.</returns>
		public bool Attach(Attachable attachable, Transform attachmentOffset = null)
		{ 
			if (!TryStartInteraction(this, attachable))
			{
				return false;
			}
			
			if (attachable.ParentToInteractor)
			{
				attachable.transform.SetParent(transform, attachable.WorldPositionStaysOnAttach);
			}

			if (attachable.WorldPositionStaysOnAttach)
			{
				return true;
			}
			
			Transform attachmentPointTransform = attachmentPoint == null ? transform : attachmentPoint;
			Vector3 initialPosition = attachable.transform.position;
			Quaternion initialRotation = attachable.transform.rotation;

			if (attachable.SnapOnAttach)
			{
				if (attachmentOffset != null)
				{
					// offset the object from the interactor by the positional and rotational difference between the
					// offset transform and the attached object
					Quaternion rotDiff = Quaternion.Inverse(attachmentOffset.transform.rotation) *
					                     attachable.transform.rotation;
					attachable.transform.rotation = attachmentPointTransform.rotation * rotDiff;

					Vector3 posDiff = attachable.transform.position - attachmentOffset.transform.position;
					attachable.transform.position = attachmentPointTransform.position + posDiff;
				}
				else
				{
					// Snap the object to the center of the attach point
					attachable.transform.rotation = attachmentPointTransform.rotation;
					attachable.transform.position = attachmentPointTransform.position;
				}
			}
			else
			{
				Vector3 initialPositionalOffset;
				Quaternion initialRotationalOffset;
				if (attachmentOffset != null)
				{
					// Get the initial positional and rotational offsets between this and the offset transform
					Quaternion rotDiff = Quaternion.Inverse(attachmentOffset.transform.rotation) *
					                     attachable.transform.rotation;
					Quaternion targetRotation = attachmentPointTransform.rotation * rotDiff;
					Quaternion rotationPositionBy = targetRotation * Quaternion.Inverse(attachable.transform.rotation);

					Vector3 posDiff = (rotationPositionBy * attachable.transform.position) -
					                  (rotationPositionBy * attachmentOffset.transform.position);

					initialPositionalOffset =
						attachmentPointTransform.InverseTransformPoint(attachmentPointTransform.position + posDiff);
					initialRotationalOffset = Quaternion.Inverse(attachmentPointTransform.rotation) *
					                          (attachmentPointTransform.rotation * rotDiff);
				}
				else
				{
					initialPositionalOffset =
						attachmentPointTransform.InverseTransformPoint(attachmentPointTransform.position);
					initialRotationalOffset = Quaternion.Inverse(attachmentPointTransform.rotation) *
					                          attachable.transform.rotation;
				}

				AttachObjectCommand attachCommand = new AttachObjectCommand(attachable, initialPosition,
					initialRotation, attachmentPointTransform, initialPositionalOffset, initialRotationalOffset);
				attachCommand.Completed += OnAttachCommandCompleted;
				_attachCommands.Add(attachable, attachCommand);
				attachCommand.Execute();
			}

			return true;
		}

		/// <summary>
		/// Callback that is invoked when an <see cref="AttachObjectCommand"/> completes.
		/// </summary>
		/// <param name="sender">The <see cref="AttachObjectCommand"/> that completed.</param>
		private void OnAttachCommandCompleted(Command sender)
		{
			AttachObjectCommand command = (AttachObjectCommand) sender;

			if (command == null)
			{
				return;
			}

			_attachCommands.Remove(command.Attachable);
		}

		/// <summary>
		/// Detaches the specified <see cref="Attachable"/> and returns a value indicating whether or not the operation was successful. 
		/// </summary>
		/// <param name="attachable">The <see cref="Attachable"/> to detach.</param>
		/// <returns><c>true</c> if the <paramref name="attachable"/> was detached successfully; otherwise, <c>false</c>.</returns>
		public bool Detach(Attachable attachable)
		{
			if (!TryFinishInteraction(this, attachable))
			{
				return false;
			}
			
			if (_attachCommands.ContainsKey(attachable))
			{
				_attachCommands[attachable].Cancel();
				_attachCommands.Remove(attachable);
			}

			return true;
		}

		/// <summary>
		/// Detaches all currently attached objects.
		/// </summary>
		public void DetachAll()
		{
			for (int i = 0; i < ActiveInteractions.Count; i++)
			{
				Detach((Attachable) ActiveInteractions[i].Interactable);
			}
		}

		/// <summary>
		/// Represents a command to move an <see cref="Attachable"/> object to a specified <see cref="Transform"/>.
		/// </summary>
		private class AttachObjectCommand : Command
		{
			private Coroutine _coroutine;
			private float _startTime;
			private readonly Vector3 _startPosition;
			private readonly Quaternion _startRotation;
			private readonly Transform _transform;
			private readonly Vector3 _initialPositionalOffset;
			private readonly Quaternion _initialRotationalOffset;
			
			/// <summary>
			/// Gets the <see cref="Attachable"/> that is being animated.
			/// </summary>
			public Attachable Attachable { get; private set; }

			/// <summary>
			/// Initializes a new instance of the <see cref="AttachObjectCommand"/> class.
			/// </summary>
			/// <param name="attachable">The <see cref="Attachable"/> to animate.</param>
			/// <param name="startPosition">The starting position of <paramref name="attachable"/>.</param>
			/// <param name="startRotation">The starting orientation of <paramref name="attachable"/>.</param>
			/// <param name="transform">The transform for <paramref name="attachable"/>.</param>
			/// <param name="initialPositionalOffset">The initial positional offset from the target position.</param>
			/// <param name="initialRotationalOffset">The initial rotational offset from the target orientation.</param>
			public AttachObjectCommand(Attachable attachable, Vector3 startPosition, Quaternion startRotation,
				Transform transform, Vector3 initialPositionalOffset, Quaternion initialRotationalOffset)
			{
				Attachable = attachable;
				_startPosition = startPosition;
				_startRotation = startRotation;
				_transform = transform;
				_initialPositionalOffset = initialPositionalOffset;
				_initialRotationalOffset = initialRotationalOffset;
			}

			/// <inheritdoc cref="Command.Execute"/>
			public override void Execute()
			{
				_coroutine = CoroutineRunner.Instance.StartCoroutine(AttachObject());
			}

			/// <inheritdoc cref="Command.Cancel"/>
			public override void Cancel()
			{
				if (_coroutine != null)
				{
					if (CoroutineRunner.IsInScene)
					{
						CoroutineRunner.Instance.StopCoroutine(_coroutine);
					}

					_coroutine = null;
				}

				base.Cancel();
			}

			/// <summary>
			/// Animates an <see cref="Attachable"/> object to a position over time.
			/// </summary>
			private IEnumerator AttachObject()
			{
				_startTime = Time.time;
				float t = 0;
				while (t < 1.0f)
				{
					t = MathUtilities.RemapNumberClamped(Time.time, _startTime, _startTime + Attachable.EaseInTime,
						0.0f, 1.0f);
					
					if (t < 1.0f)
					{
						t = Attachable.EaseInAnimationCurve.Evaluate(t);
						Attachable.transform.position =
							Vector3.Lerp(_startPosition, _transform.TransformPoint(_initialPositionalOffset), t);
						Attachable.transform.rotation =
							Quaternion.Lerp(_startRotation, _transform.rotation * _initialRotationalOffset, t);
					}

					yield return null;
				}

				Attachable.transform.position = _transform.TransformPoint(_initialPositionalOffset);
				Attachable.transform.rotation = _transform.rotation * _initialRotationalOffset;
				OnComplete();
			}
		}
	}
}