﻿// <copyright file="Attachable.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an object that supports attaching interactions.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Interactions
{
	using System;
	
	using UnityEngine;
	
	using Viacom.XR.Diagnostics;
	
	public class Attachable : Interactable
	{
		/// <summary>
		/// Represents the settings for translating an <see cref="Attachable"/> when attached.
		/// </summary>
		[Serializable]
		protected class TranslationSettings
		{
			[Tooltip("Should snap to the position of the specified attachment point on the interactor")]
			[SerializeField]
			public bool snapOnAttach = true;

			[ConditionalField("snapOnAttach", false)]
			[SerializeField]
			public AnimationCurve easeInAnimationCurve;
		
			[ConditionalField("snapOnAttach", false)] 
			[SerializeField]
			public float easeInTime = 0.15f;
		}
		
		[SerializeField]
		protected bool worldPositionStaysOnAttach;
		
		[ConditionalField("worldPositionStaysOnAttach", false)] 
		[SerializeField]
		protected TranslationSettings translationOnAttach;
		
		[SerializeField] protected bool turnOffGravity;
		[SerializeField] protected bool turnOnKinematic;
		[SerializeField] protected bool parentToInteractor;

		private Transform _originalParent;
		private bool _wasKinematicEnabled;
		private bool _wasGravityEnabled;
		private Rigidbody _rigidbody;
		
		private static readonly string LogTag = typeof(Attachable).Name;
		
		/// <summary>
		/// Gets a value that indicates whether or not this object keeps the same world space position, rotation and scale when attached.
		/// </summary>
		public bool WorldPositionStaysOnAttach
		{
			get { return worldPositionStaysOnAttach; }
		}

		/// <summary>
		/// Gets a value that indicates whether or not this object should snap to the desired target position when attached.
		/// </summary>
		public bool SnapOnAttach
		{
			get { return translationOnAttach.snapOnAttach; }
		}

		/// <summary>
		/// Gets the animation curve that should be used when animating this object to its desired position when attached.
		/// </summary>
		public AnimationCurve EaseInAnimationCurve
		{
			get { return translationOnAttach.easeInAnimationCurve; }
		}

		/// <summary>
		/// Gets the amount of time in seconds it should take to animate this object to its desired position when attached.
		/// </summary>
		public float EaseInTime
		{
			get { return translationOnAttach.easeInTime; }
		}

		/// <summary>
		/// Gets a value that indicates whether or not this object should be parented to the object it is interacting with when it is attached.
		/// </summary>
		public bool ParentToInteractor
		{
			get { return parentToInteractor; }
		}

		/// <inheritdoc cref="MonoBehaviour.OnEnable"/>
		protected virtual void OnEnable()
		{
			_rigidbody = gameObject.GetComponent<Rigidbody>();

			if (!worldPositionStaysOnAttach && !SnapOnAttach && EaseInAnimationCurve == null)
			{
				ApplicationLog.Warning(LogTag, "Easing animation curve is undefined.");
			}
		}
		
		/// <inheritdoc cref="Interactable.StartInteraction"/>
		protected override void StartInteraction(Interaction interaction)
		{
			if (parentToInteractor)
			{
				_originalParent = transform.parent;
			}

			if (turnOffGravity && _rigidbody)
			{
				_wasGravityEnabled = _rigidbody.useGravity;
				_rigidbody.useGravity = false;
			}

			if (turnOnKinematic && _rigidbody)
			{
				_wasKinematicEnabled = _rigidbody.isKinematic;
				_rigidbody.isKinematic = true;
			}
			
			base.StartInteraction(interaction);
		}

		/// <inheritdoc cref="Interactable.FinishInteraction"/>
		protected override void FinishInteraction(Interactable interactable)
		{
			if (parentToInteractor)
			{
				transform.parent = _originalParent;
			}
			
			if (turnOffGravity && _rigidbody)
			{
				_rigidbody.useGravity = _wasGravityEnabled;
			}

			if (turnOnKinematic && _rigidbody)
			{
				_rigidbody.isKinematic = _wasKinematicEnabled;
			}
			
			base.FinishInteraction(interactable);
		}
	}
}