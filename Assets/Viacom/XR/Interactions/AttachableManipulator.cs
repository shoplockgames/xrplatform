﻿// <copyright file="AttachableManipulator.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an object that manipulates something when attached.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Interactions
{
    using UnityEngine;

    /// <summary>
    /// Specifies the operating axis of manipulation for a <see cref="AttachableManipulator"/>.
    /// </summary>
    public enum OperatingAxis
    {
        /// <summary>
        /// The local X Axis of the transform.
        /// </summary>
        X,

        /// <summary>
        /// The local Y Axis of the transform.
        /// </summary>
        Y,

        /// <summary>
        /// The local Z Axis of the transform.
        /// </summary>
        Z
    }

    /// <summary>
    /// Represents an object that manipulates something when attached.
    /// </summary>
    public abstract class AttachableManipulator : Attachable
    {
        [Tooltip("The local axis in which the manipulation will operate through.")] [SerializeField]
        protected OperatingAxis operatingAxis = OperatingAxis.Y;

        protected Interaction CurrentInteraction;

        protected Transform CurrentInteractionTransform
        {
            get { return CurrentInteraction != null ? CurrentInteraction.GameObject.transform : null; }
        }
        
        /// <summary>
        /// Gets a value that represents the current value of the manipulator.
        /// </summary>
        public abstract float Value { get; }
        
        /// <summary>
        /// Gets a value that represents the current value of the manipulator normalized between 0 and 1.
        /// </summary>
        public abstract float NormalizedValue { get; }
        
        /// <inheritdoc cref="Interactable.StartInteraction"/>
        protected override void StartInteraction(Interaction interaction)
        {
            base.StartInteraction(interaction);
            CurrentInteraction = interaction;
        }

        /// <inheritdoc cref="Interactable.FinishInteraction"/>
        protected override void FinishInteraction(Interactable interactable)
        {
            base.FinishInteraction(interactable);
            CurrentInteraction = null;
        }

        /// <summary>
        /// Gets the relevant direction vector for this <see cref="AttachableManipulator"/>'s operating axis.
        /// </summary>
        /// <param name="overrideTransform">An optional transform to use it's direction vectors instead of the standard unit vectors.</param>
        /// <returns>The direction vector for this <see cref="AttachableManipulator"/>'s operating axis.</returns>
        protected Vector3 AxisDirection(Transform overrideTransform = null)
        {
            Vector3[] worldDirections = (overrideTransform != null
                ? new Vector3[] {overrideTransform.right, overrideTransform.up, overrideTransform.forward}
                : new Vector3[] {Vector3.right, Vector3.up, Vector3.forward});
            return worldDirections[(int) Mathf.Clamp((int) operatingAxis, 0f, worldDirections.Length)];
        }
    }
}