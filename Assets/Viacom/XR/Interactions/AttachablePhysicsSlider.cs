﻿// <copyright file="AttachablePhysicsSlider.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an object that moves along an axis when attached using Unity's physics system.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Interactions
{
    using UnityEngine;

    /// <summary>
    /// Represents an object that moves along an axis when attached.
    /// </summary>
    public class AttachablePhysicsSlider : AttachableManipulator
    {
        [SerializeField] Transform attachPoint;
        [SerializeField] private float maximumLength = 0.1f;
        [SerializeField] private float snapForce = 10f;
        [SerializeField] private float releaseFriction = 10f;
        [SerializeField] private float velocityLimit = float.PositiveInfinity;
        [SerializeField] private float angularVelocityLimit = float.PositiveInfinity;
        [SerializeField] private float maxDistanceDelta = 10f;

        private Vector3 _originalLocalPosition;
        private Rigidbody _controlRigidbody;
        private bool _createdRigidbody;
        private ConfigurableJoint _controlJoint;
        private bool _createdControlJoint;
        private float _storedValue;
        private float _positionTarget;
        private float _previousPositionTarget;
        
        private const float PositionSpring = 1000f;
        private const float PositionDamper = 100f;
        
        /// <summary>
        /// Gets a value that represents the current value of the slider.
        /// </summary>
        public override float Value 
        {
            get
            {
                return transform.localPosition[(int)operatingAxis];
            }
        }

        /// <summary>
        /// Gets a value that represents the current value of the slider normalized between 0 and 1.
        /// </summary>
        public override float NormalizedValue
        {
            get
            {
                return (transform.localPosition[(int) operatingAxis] - _originalLocalPosition[(int) operatingAxis]) /
                       (MaximumLength()[(int) operatingAxis] - _originalLocalPosition[(int) operatingAxis]);
            }
        }
        
        /// <inheritdoc cref="MonoBehaviour.Awake"/>
        private void Awake()
        {
            _originalLocalPosition = transform.localPosition;
            _storedValue = Value;
        }

        /// <inheritdoc cref="MonoBehaviour.OnEnable"/>
        protected override void OnEnable()
        {
            base.OnEnable();
            SetupRigidbody();
            SetupJoint();
            SetValue(_storedValue);
        }

        /// <inheritdoc cref="MonoBehaviour.OnDisable"/>
        protected override void OnDisable()
        {
            base.OnDisable();

            _storedValue = Value;
            
            if (_createdControlJoint)
            {
                Destroy(_controlJoint);
            }
            
            if (_createdRigidbody)
            {
                Destroy(_controlRigidbody);
            }
        }

        /// <summary>
        /// Sets the position of the slider based on the specified value.
        /// </summary>
        /// <param name="value">The desired value.</param>
        private void SetValue(float value)
        {
            Vector3 tempPos = new Vector3();
            tempPos = transform.localPosition;
            tempPos[(int)operatingAxis] = value;
            transform.localPosition = tempPos;
            _positionTarget = (value - _originalLocalPosition[(int) operatingAxis]) /
                (MaximumLength()[(int) operatingAxis] - _originalLocalPosition[(int) operatingAxis]);
            SetPositionWithNormalizedValue(_positionTarget);
        }
        
        /// <summary>
        /// Sets the position of the slider to the specified position.
        /// </summary>
        /// <param name="targetPosition">The desired position.</param>
        private void SetPositionWithNormalizedValue(float targetPosition)
        {
            float positionOnAxis = Mathf.Lerp(_controlJoint.linearLimit.limit, -_controlJoint.linearLimit.limit, Mathf.Clamp01(targetPosition));
            SnapToPosition(positionOnAxis);
        }

        /// <summary>
        /// Snaps the physics joint to the specified position on the operating axis of this slider.
        /// </summary>
        /// <param name="positionOnAxis">The desired position.</param>
        private void SnapToPosition(float positionOnAxis)
        {
            if (_controlJoint != null)
            {
                _controlJoint.targetPosition = (AxisDirection() * Mathf.Sign(maximumLength)) * positionOnAxis;
            }
        }

        /// <summary>
        /// Configures or creates the <see cref="UnityEngine.Rigidbody"/> for the slider.
        /// </summary>
        private void SetupRigidbody()
        {
            _controlRigidbody = GetComponent<Rigidbody>();
            _createdRigidbody = false;
            if (_controlRigidbody == null)
            {
                _controlRigidbody = gameObject.AddComponent<Rigidbody>();
                _createdRigidbody = true;
                _controlRigidbody.useGravity = false;
#if UNITY_2018_3_OR_NEWER
                _controlRigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
#else
                controlRigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
#endif
                _controlRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            }
            
            _controlRigidbody.isKinematic = false;
        }
        
        /// <summary>
        /// Configures or creates the <see cref="UnityEngine.ConfigurableJoint"/> for the slider.
        /// </summary>
        private void SetupJoint()
        {
            //move transform towards activation distance
            transform.localPosition = _originalLocalPosition + (AxisDirection() * (maximumLength * 0.5f));

            _controlJoint = GetComponent<ConfigurableJoint>();
            _createdControlJoint = false;
            if (_controlJoint == null)
            {
                _controlJoint = gameObject.AddComponent<ConfigurableJoint>();
                _createdControlJoint = true;

                _controlJoint.angularXMotion = ConfigurableJointMotion.Locked;
                _controlJoint.angularYMotion = ConfigurableJointMotion.Locked;
                _controlJoint.angularZMotion = ConfigurableJointMotion.Locked;

                _controlJoint.xMotion = (operatingAxis == OperatingAxis.X ? ConfigurableJointMotion.Limited : ConfigurableJointMotion.Locked);
                _controlJoint.yMotion = (operatingAxis == OperatingAxis.Y ? ConfigurableJointMotion.Limited : ConfigurableJointMotion.Locked);
                _controlJoint.zMotion = (operatingAxis == OperatingAxis.Z ? ConfigurableJointMotion.Limited : ConfigurableJointMotion.Locked);

                SoftJointLimit linearLimit = new SoftJointLimit();
                linearLimit.limit = Mathf.Abs(maximumLength * 0.5f * transform.lossyScale[(int)operatingAxis]);
                _controlJoint.linearLimit = linearLimit;

                EnableJointDriver();
            }
        }
        
        /// <summary>
        /// Enables the <see cref="JointDrive"/> on the <see cref="ConfigurableJoint"/>.
        /// </summary>
        private void EnableJointDriver()
        {
            SetJointDrive(snapForce);
        }

        /// <summary>
        /// Disables the <see cref="JointDrive"/> on the <see cref="ConfigurableJoint"/>.
        /// </summary>
        private void DisableJointDriver()
        {
            SetJointDrive(0f);
        }
        
        /// <summary>
        /// Sets the specified amount of force to apply to the <see cref="JointDrive"/> on the <see cref="ConfigurableJoint"/>.
        /// </summary>
        /// <param name="driverForce">The amount of force to apply.</param>
        private void SetJointDrive(float driverForce)
        {
            JointDrive snapDriver = new JointDrive();
            snapDriver.positionSpring = PositionSpring;
            snapDriver.positionDamper = PositionDamper;
            snapDriver.maximumForce = driverForce;

            _controlJoint.xDrive = snapDriver;
            _controlJoint.yDrive = snapDriver;
            _controlJoint.zDrive = snapDriver;
        }
        
        /// <inheritdoc cref="MonoBehaviour.FixedUpdate"/>
        private void FixedUpdate()
        {
            if (CurrentInteraction == null)
            {
                return;
            }
            
            Vector3 positionDelta = CurrentInteractionTransform.position - attachPoint.position;
            Quaternion rotationDelta = CurrentInteractionTransform.rotation * Quaternion.Inverse(attachPoint.rotation);

            float angle;
            Vector3 axis;
            rotationDelta.ToAngleAxis(out angle, out axis);

            angle = ((angle > 180) ? angle -= 360 : angle);

            if (angle != 0)
            {
                Vector3 angularTarget = angle * axis;
                Vector3 calculatedAngularVelocity = Vector3.MoveTowards(_controlRigidbody.angularVelocity, angularTarget, maxDistanceDelta);
                if (angularVelocityLimit == float.PositiveInfinity || calculatedAngularVelocity.sqrMagnitude < angularVelocityLimit)
                {
                    _controlRigidbody.angularVelocity = calculatedAngularVelocity;
                }
            }

            Vector3 velocityTarget = positionDelta / Time.fixedDeltaTime;
            Vector3 calculatedVelocity = Vector3.MoveTowards(_controlRigidbody.velocity, velocityTarget, maxDistanceDelta);

            if (velocityLimit == float.PositiveInfinity || calculatedVelocity.sqrMagnitude < velocityLimit)
            {
                _controlRigidbody.velocity = calculatedVelocity;
            }
        }

        /// <inheritdoc cref="Interactable.StartInteraction"/>
        protected override void StartInteraction(Interaction interaction)
        {
            base.StartInteraction(interaction);
            _controlRigidbody.drag = 0.0f;
            DisableJointDriver();
        }
        
        /// <inheritdoc cref="Interactable.FinishInteraction"/>
        protected override void FinishInteraction(Interactable interactable)
        {
            base.FinishInteraction(interactable);
            _controlRigidbody.drag = releaseFriction;
        }

        /// <summary>
        /// Gets the maximum position of this <see cref="GameObject"/>.
        /// </summary>
        /// <returns>The maximum position of this <see cref="GameObject"/>.</returns>
        private Vector3 MaximumLength()
        {
            return _originalLocalPosition + (AxisDirection() * maximumLength);
        }
    }
}