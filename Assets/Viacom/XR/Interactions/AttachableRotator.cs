﻿// <copyright file="AttachableRotator.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an object that rotates about an axis when attached.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Interactions
{
    using UnityEngine;

    /// <summary>
    /// Represents an object that rotates about an axis when attached.
    /// </summary>
    public class AttachableRotator : AttachableManipulator
    {
        [Tooltip("The amount of friction to apply when rotating, simulates a tougher rotation.")] [Range(1f, 32f)]
        private float rotationFriction = 1f;

        [Tooltip("A Transform that denotes the position where the rotator will rotate around.")] [SerializeField]
        private Transform hingePoint;

        [SerializeField] private FloatLimits angleLimits;

        private Vector3 _previousAttachInteractorPosition;
        private Vector3 _currentRotation;
        private GameObject _rotatorContainer;
        private Transform _savedParent;

        private const string NameSuffix = "RotatorContainer";

        /// <summary>
        /// Gets a value that represents the current value of the rotator.
        /// </summary>
        public override float Value 
        {
            get
            {
                return _currentRotation[(int)operatingAxis];
            }
        }

        /// <summary>
        /// Gets a value that represents the current value of the rotator normalized between 0 and 1.
        /// </summary>
        public override float NormalizedValue
        {
            get
            {
                return (Value - angleLimits.Minimum) /
                       (angleLimits.Maximum - angleLimits.Minimum);
            }
        }

        /// <inheritdoc cref="MonoBehaviour.OnEnable"/>
        protected override void OnEnable()
        {
            ResetParentContainer();
            base.OnEnable();
            worldPositionStaysOnAttach = true;
            parentToInteractor = false;
            _rotatorContainer = gameObject;
        }

        /// <inheritdoc cref="MonoBehaviour.Update"/>
        private void Update()
        {
            if (CurrentInteraction == null)
            {
                return;
            }

            Vector3 currentAttachPosition = CurrentInteractionTransform.position;
            Vector3 newRotation = CalculateAngle(_rotatorContainer.transform.position,
                _previousAttachInteractorPosition,
                currentAttachPosition);
            _previousAttachInteractorPosition = currentAttachPosition;
            UpdateRotation(newRotation, true, true);
        }

        /// <inheritdoc cref="Interactable.StartInteraction"/>
        protected override void StartInteraction(Interaction interaction)
        {
            base.StartInteraction(interaction);
            _previousAttachInteractorPosition = CurrentInteractionTransform.position;
            if (hingePoint != null)
            {
                hingePoint.transform.SetParent(transform.parent);
                Vector3 storedScale = transform.localScale;
                _rotatorContainer = new GameObject(name + NameSuffix);
                _rotatorContainer.transform.SetParent(transform.parent);
                _rotatorContainer.transform.localPosition = transform.localPosition;
                _rotatorContainer.transform.localRotation = transform.localRotation;
                _rotatorContainer.transform.localScale = Vector3.one;
                transform.SetParent(_rotatorContainer.transform);
                _rotatorContainer.transform.localPosition = hingePoint.localPosition;
                transform.localPosition = -hingePoint.localPosition;
                transform.localScale = storedScale;
                hingePoint.transform.SetParent(transform);
            }
        }

        /// <inheritdoc cref="Interactable.FinishInteraction"/>
        protected override void FinishInteraction(Interactable interactable)
        {
            base.FinishInteraction(interactable);
            RemoveParentContainer();
            ResetParentContainer();
        }

        /// <summary>
        /// Records the rotation container's parent.
        /// </summary>
        protected virtual void RemoveParentContainer()
        {
            if (_rotatorContainer != null)
            {
                _savedParent = _rotatorContainer.transform.parent;
            }
        }

        /// <summary>
        /// Resets the <see cref="GameObject"/> to have it's original parent.
        /// </summary>
        private void ResetParentContainer()
        {
            if (_savedParent != null)
            {
                transform.SetParent(_savedParent);
                Destroy(_rotatorContainer);
            }
        }

        /// <summary>
        /// Calculates the angle between the original attachment point and the current attachment point.
        /// </summary>
        /// <param name="originPoint">The origin position.</param>
        /// <param name="originalAttachPoint">The original attachment position.</param>
        /// <param name="currentAttachPoint">The current attachment position.</param>
        /// <returns>The angle between <paramref name="originalAttachPoint"/> and <paramref name="currentAttachPoint"/>.</returns>
        private Vector3 CalculateAngle(Vector3 originPoint, Vector3 originalAttachPoint, Vector3 currentAttachPoint)
        {
            float xRotated = (operatingAxis == OperatingAxis.X
                ? CalculateAngle(originPoint, originalAttachPoint, currentAttachPoint,
                    _rotatorContainer.transform.right)
                : 0f);
            float yRotated = (operatingAxis == OperatingAxis.Y
                ? CalculateAngle(originPoint, originalAttachPoint, currentAttachPoint, _rotatorContainer.transform.up)
                : 0f);
            float zRotated = (operatingAxis == OperatingAxis.Z
                ? CalculateAngle(originPoint, originalAttachPoint, currentAttachPoint,
                    _rotatorContainer.transform.forward)
                : 0f);

            float frictionMultiplier = DividerToMultiplier(rotationFriction);
            return new Vector3(xRotated * frictionMultiplier, yRotated * frictionMultiplier,
                zRotated * frictionMultiplier);
        }

        /// <summary>
        /// Calculates the angle between two points relative to an origin point and a given direction.
        /// </summary>
        /// <param name="originPoint">The origin position.</param>
        /// <param name="previousPoint">The previous position.</param>
        /// <param name="currentPoint">The current position.</param>
        /// <param name="direction">The vector around which the other vectors are rotated.</param>
        /// <returns>The angle between <paramref name="previousPoint"/> and <paramref name="currentPoint"/>.</returns>
        private float CalculateAngle(Vector3 originPoint, Vector3 previousPoint, Vector3 currentPoint,
            Vector3 direction)
        {
            Vector3 sideA = previousPoint - originPoint;
            Vector3 sideB = VectorDirection(originPoint, currentPoint);
            return AngleSigned(sideA, sideB, direction);
        }

        /// <summary>
        /// Updates the local rotation of this <see cref="GameObject"/> with the specified value.
        /// </summary>
        /// <param name="rotation">The new rotation.</param>
        /// <param name="isAdditive"><c>true</c> if the new rotation should be added to the local rotation; otherwise, <c>false</c>.</param>
        /// <param name="updateCurrentRotation"><c>true</c> if the cached current rotation should be updated; otherwise, <c>false</c></param>
        private void UpdateRotation(Vector3 rotation, bool isAdditive, bool updateCurrentRotation)
        {
            if (WithinRotationLimit(_currentRotation + rotation))
            {
                if (updateCurrentRotation)
                {
                    _currentRotation += rotation;
                }

                _rotatorContainer.transform.localRotation = (isAdditive
                    ? _rotatorContainer.transform.localRotation * Quaternion.Euler(rotation)
                    : Quaternion.Euler(rotation));
            }
        }

        /// <summary>
        /// Gets a value indicating whether the given angles are within limits.
        /// </summary>
        /// <param name="angles">The current angles.</param>
        /// <returns><c>true</c> if the angles are within limits; otherwise, <c>false</c>.</returns>
        private bool WithinRotationLimit(Vector3 angles)
        {
            switch (operatingAxis)
            {
                case OperatingAxis.X:
                    return angleLimits.IsWithinLimits(angles.x);
                case OperatingAxis.Y:
                    return angleLimits.IsWithinLimits(angles.y);
                case OperatingAxis.Z:
                    return angleLimits.IsWithinLimits(angles.z);
            }

            return false;
        }

        /// <summary>
        /// Calculates the direction the a target position is in relation to a origin position.
        /// </summary>
        /// <param name="originPosition">The point to use as the originating position for the direction calculation.</param>
        /// <param name="targetPosition">The point to use as the target position for the direction calculation.</param>
        /// <returns>The direction of <paramref name="targetPosition"/> in relation to <paramref name="originPosition"/>.</returns>
        private static Vector3 VectorDirection(Vector3 originPosition, Vector3 targetPosition)
        {
            Vector3 heading = targetPosition - originPosition;
            return heading * DividerToMultiplier(heading.magnitude);
        }

        /// <summary>
        /// Converts a number to be used in a division into a number to be used for multiplication.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <returns>The calculated number that can replace the divider number in a multiplication sum.</returns>
        private static float DividerToMultiplier(float value)
        {
            return (value != 0f ? 1f / value : 1f);
        }

        /// <summary>
        /// Gets the signed angle in degrees between two vectors.
        /// </summary>
        /// <param name="v1">The vector from which the angular difference is measured.</param>
        /// <param name="v2">The vector to which the angular difference is measured.</param>
        /// <param name="n">A vector around which the other vectors are rotated.</param>
        /// <returns>The signed angle in degrees between <paramref name="v1"/> and <paramref name="v2"/>.</returns>
        private static float AngleSigned(Vector3 v1, Vector3 v2, Vector3 n)
        {
            return Mathf.Atan2(Vector3.Dot(n, Vector3.Cross(v1, v2)), Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
        }
    }
}