﻿// <copyright file="AttachableSlider.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an object that moves along an axis when attached.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Interactions
{
    using UnityEngine;

    /// <summary>
    /// Represents an object that moves along an axis when attached.
    /// </summary>
    public class AttachableSlider : AttachableManipulator
    {
        [SerializeField] Transform attachPoint;
        [SerializeField] private float maximumLength = 0.1f;
        [SerializeField] private float minMaxThreshold = 0.01f;
        [SerializeField] private float trackingSpeed = 25f;

        private Vector3 _originalLocalPosition;
        private FloatLimits _xAxisLimits = FloatLimits.Zero;
        private FloatLimits _yAxisLimits = FloatLimits.Zero;
        private FloatLimits _zAxisLimits = FloatLimits.Zero;
        
        /// <summary>
        /// Gets a value that represents the current value of the slider.
        /// </summary>
        public override float Value 
        {
            get
            {
                return transform.localPosition[(int)operatingAxis];
            }
        }

        /// <summary>
        /// Gets a value that represents the current value of the slider normalized between 0 and 1.
        /// </summary>
        public override float NormalizedValue
        {
            get
            {
                return (transform.localPosition[(int) operatingAxis] - _originalLocalPosition[(int) operatingAxis]) /
                       (MaximumLength()[(int) operatingAxis] - _originalLocalPosition[(int) operatingAxis]);
            }
        }

        /// <inheritdoc cref="MonoBehaviour.Awake"/>
        private void Awake()
        {
            _originalLocalPosition = transform.localPosition;
        }

        /// <inheritdoc cref="MonoBehaviour.OnEnable"/>
        protected override void OnEnable()
        {
            base.OnEnable();
            worldPositionStaysOnAttach = true;
            parentToInteractor = false;
        }

        /// <inheritdoc cref="MonoBehaviour.Update"/>
        private void Update()
        {
            if (CurrentInteraction == null)
            {
                return;
            }

            Vector3 currentPosition = transform.localPosition;
            Vector3 moveTowards = currentPosition +
                                  Vector3.Scale(
                                      (transform.InverseTransformPoint(CurrentInteractionTransform.position) -
                                       transform.InverseTransformPoint(attachPoint.position)),
                                      transform.localScale);
            Vector3 targetPosition = Vector3.Lerp(currentPosition, moveTowards, trackingSpeed * Time.deltaTime);
            UpdatePosition(targetPosition, false);
        }

        /// <inheritdoc cref="Interactable.StartInteraction"/>
        protected override void StartInteraction(Interaction interaction)
        {
            base.StartInteraction(interaction);
            FloatLimits axisLimits = new FloatLimits(_originalLocalPosition[(int) operatingAxis],
                MaximumLength()[(int) operatingAxis]);
            switch (operatingAxis)
            {
                case OperatingAxis.X:
                    _xAxisLimits = axisLimits;
                    break;
                case OperatingAxis.Y:
                    _yAxisLimits = axisLimits;
                    break;
                case OperatingAxis.Z:
                    _zAxisLimits = axisLimits;
                    break;
            }
        }

        /// <summary>
        /// Updates the local position of this <see cref="GameObject"/> with the specified value.
        /// </summary>
        /// <param name="position">The new position.</param>
        /// <param name="isAdditive"><c>true</c> if the new position should be added to the local position; otherwise, <c>false</c>.</param>
        /// <param name="forceClamp"><c>true</c> if the resulting updated position should be clamped; otherwise, <c>false</c>.</param>
        protected virtual void UpdatePosition(Vector3 position, bool isAdditive, bool forceClamp = true)
        {
            transform.localPosition = (isAdditive ? transform.localPosition + position : position);
            if (forceClamp)
            {
                transform.localPosition = new Vector3(
                    ClampAxis(_xAxisLimits, transform.localPosition.x),
                    ClampAxis(_yAxisLimits, transform.localPosition.y),
                    ClampAxis(_zAxisLimits, transform.localPosition.z)
                );
            }
        }

        /// <summary>
        /// Clamps the positional value of an axis given a set of <see cref="FloatLimits"/>.
        /// </summary>
        /// <param name="limits">The <see cref="FloatLimits"/> that define the acceptable range of values.</param>
        /// <param name="axisValue">The desired value.</param>
        /// <returns>The given <paramref name="axisValue"/> if it is within <paramref name="limits"/>; otherwise, either the minimum or maximum allowed value.</returns>
        private float ClampAxis(FloatLimits limits, float axisValue)
        {
            axisValue = (axisValue < limits.Minimum + minMaxThreshold ? limits.Minimum : axisValue);
            axisValue = (axisValue > limits.Maximum - minMaxThreshold ? limits.Maximum : axisValue);
            return Mathf.Clamp(axisValue, limits.Minimum, limits.Maximum);
        }

        /// <summary>
        /// Gets the maximum position of this <see cref="GameObject"/>.
        /// </summary>
        /// <returns>The maximum position of this <see cref="GameObject"/>.</returns>
        private Vector3 MaximumLength()
        {
            return _originalLocalPosition + (AxisDirection() * maximumLength);
        }
    }
}