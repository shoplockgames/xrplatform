﻿// <copyright file="GazeInteractor.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a game object that can gaze at another game object.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Interactions
{
	using System.Collections.Generic;
	using System.Linq;

	using UnityEngine;
	
	public class GazeInteractor : Interactable
	{
		[SerializeField] private float updateInterval = 0.1f;
		[SerializeField] private Camera sourceCamera;
		[SerializeField] private LayerMask layerMask;
		[SerializeField] private bool ignoreOcclusion = true;

		private Plane[] _planes;
		private Collider[] _colliders;
		private RaycastHit[] _raycastHits;

		private const float MaxStartTime = 0.5f;
		private const string UpdateMethodName = "UpdateState";
		
		/// <inheritdoc cref="MonoBehaviour.OnEnable"/>
		private void OnEnable()
		{
			// To try to have these interactors update on different intervals we start at a random time
			float startTime = Random.Range(0, MaxStartTime);
			InvokeRepeating(UpdateMethodName, startTime, updateInterval);
		}

		/// <inheritdoc cref="MonoBehaviour.OnDisable"/>
		protected override void OnDisable()
		{
			CancelInvoke();
			base.OnDisable();
		}
		
		/// <summary>
        /// Updates the current gaze state.
        /// </summary>
        private void UpdateState()
        {
            float closestDistance = float.MaxValue;
            Gazeable closestGazeable = null;
            List<Interactable> gazeables = new List<Interactable>();
            _planes = GeometryUtility.CalculateFrustumPlanes(sourceCamera);
            CheckGazingForTransform(transform.position, ref closestDistance, ref closestGazeable, ref gazeables);

            if (!supportsMultipleInteractions)
            {
                // Exit out if we aren't gazing at anything
                if (Interactions.Length > 0 && Interactions[0].Interactable == closestGazeable)
                {
                    return;
                }

                // If we were previously gazing at something then finish the interaction
                if (Interactions.Length > 0)
                {
                    TryFinishInteraction(Interactions[0].Interactable, this);
                }

                // If we are now gazing at something then start the interaction
                if (closestGazeable != null)
                {
                    TryStartInteraction(closestGazeable, this);
                }
            }
            else
            {
                // Finish any interactions with interactables that are no longer being gazed at
                Interaction[] interactions = Interactions.Where(x => !gazeables.Any(y => y.Equals(x.Interactable))).ToArray();
                int i;
                for (i = 0; i < interactions.Length; i++)
                {
                    TryFinishInteraction(interactions[i].Interactable, this);
                }
                
                // Start interacting with any interactables that we just started gazing at
                Interactable[] interactables = gazeables.Where(x => !Interactions.Any(y => x.Equals(y.Interactable))).ToArray();
                for (i = 0; i < interactables.Length; i++)
                {
                    TryStartInteraction(interactables[i], this);
                }
            }
        }
		
		/// <summary>
		/// Checks if the currently list of gazeables are visible to the camera and determines which one is the closest.
		/// </summary>
		/// <param name="position">The gazing position.</param>
		/// <param name="closestDistance">The current closest distance to a <see cref="Gazeable"/> that is being gazed at.</param>
		/// <param name="closestGazeable">The current closest <see cref="Gazeable"/>.</param>
		/// <param name="gazeables">The current list of items that are being gazed at.</param>
		private void CheckGazingForTransform(Vector3 position, ref float closestDistance,
			ref Gazeable closestGazeable, ref List<Interactable> gazeables)
		{
			Gazeable[] candidates = FindObjectsOfType<Gazeable>();
			
			for (int candidateIndex = 0; candidateIndex < candidates.Length; candidateIndex++)
			{
				Gazeable candidate = candidates[candidateIndex];

				// Make sure the object is within view
				if (!candidate.enabled || !candidate.IsWithinBounds(_planes))
				{
					continue;
				}
				
				// Now make sure you can see it by raycasting to it.
				Gazeable gazeable = candidate;
				if (!ignoreOcclusion)
				{
					RaycastHit hit;
					Vector3 direction = candidate.transform.position - sourceCamera.transform.position;
					direction.Normalize();
					if (!Physics.Raycast(sourceCamera.transform.position, direction, out hit, sourceCamera.farClipPlane, layerMask))
					{
						continue;
					}

					Collider collider = hit.collider;
					gazeable = collider.GetComponentInParent<Gazeable>();
					if (gazeable == null || !gazeable.IsValidCollider(collider))
					{
						continue;
					}
				}

				float distance = Vector3.Distance(gazeable.transform.position, position);
				if (distance < closestDistance)
				{
					closestDistance = distance;
					closestGazeable = gazeable;
				}

				gazeables.TryAddUnique(gazeable);
			}
		}

		/// <inheritdoc cref="MonoBehaviour.Reset"/>
		private void Reset()
		{
			sourceCamera = GetComponent<Camera>();
		}
	}
}