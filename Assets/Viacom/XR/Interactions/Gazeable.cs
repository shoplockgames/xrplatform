﻿// <copyright file="Gazeable.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an object that supports gaze interactions.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Interactions
{
    using System.Linq;

    using UnityEngine;

    /// <summary>
    /// Represents an object that supports gaze interactions.
    /// </summary>
    public class Gazeable : Interactable
    {
        [SerializeField] private Collider[] colliders;

        /// <summary>
        /// Gets a value indicating whether not the specified <see cref="Collider"/> is a valid collider for interacting.
        /// </summary>
        /// <param name="collider">The <see cref="Collider"/> to verify.</param>
        /// <returns><c>true</c> if <paramref name="collider"/> is a valid collider for interacting; otherwise, <c>false</c>.</returns>
        public bool IsValidCollider(Collider collider)
        {
            return colliders.Contains(collider);
        }

        /// <summary>
        ///  Gets a value indicating whether not the bounds of any colliders are within an array of planes.
        /// </summary>
        /// <param name="planes">An array of <see cref="Plane"/> objects.</param>
        /// <returns><c>true</c> if the bounds of any colliders are inside <paramref name="planes"/>; otherwise, <c>false</c>.</returns>
        public bool IsWithinBounds(Plane[] planes)
        {
            for (int i = 0; i < colliders.Length; i++)
            {
                if (GeometryUtility.TestPlanesAABB(planes, colliders[i].bounds))
                {
                    return true;
                }
            }

            return false;
        }

        /// <inheritdoc cref="MonoBehaviour.Reset"/>
        private void Reset()
        {
            colliders = GetComponents<Collider>();
        }
    }
}