﻿// <copyright file="HoverInteractor.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a game object that can hover over another game object. Only supports primitive colliders.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

using System;

namespace Viacom.XR.Interactions
{
    using System.Collections.Generic;
    using System.Linq;

    using UnityEngine;

    using Viacom.XR.Diagnostics;

    /// <summary>
    /// Represents a game object that can hover over another game object.
    /// </summary>
    public class HoverInteractor : Interactable
    {
        [SerializeField] private float updateInterval = 0.1f;
        [SerializeField] private LayerMask layerMask;
        [SerializeField] private int collisionBufferSize = 16;
        [SerializeField] private Collider[] colliders;

        private Collider[] _overlappingColliders;

        private const float MaxStartTime = 0.5f;
        private const string UpdateMethodName = "UpdateState";
        private static readonly string LogTag = typeof(HoverInteractor).Name;

        /// <inheritdoc cref="MonoBehaviour.OnEnable"/>
        private void OnEnable()
        {
            for (int i = 0; i < colliders.Length; i++)
            {
                Collider collider = colliders[i];

                if (!(collider is BoxCollider || collider is CapsuleCollider || collider is SphereCollider))
                {
                    ApplicationLog.Warning(LogTag, "Unsupported collider of type " + collider.GetType() + " detected.");
                }
            }

            _overlappingColliders = new Collider[collisionBufferSize];

            // To try to have these interactors update on different intervals we start at a random time
            float startTime = Random.Range(0, MaxStartTime);
            InvokeRepeating(UpdateMethodName, startTime, updateInterval);
        }

        /// <inheritdoc cref="MonoBehaviour.OnDisable"/>
        protected override void OnDisable()
        {
            CancelInvoke();
            base.OnDisable();
        }

        /// <summary>
        /// Updates the current hover state.
        /// </summary>
        private void UpdateState()
        {
            float closestDistance = float.MaxValue;
            Hoverable closestHoverable = null;
            List<Interactable> hoverables = new List<Interactable>();

            for (int i = 0; i < colliders.Length; i++)
            {
                Collider collider = colliders[i];
                Vector3 position;

                if (collider is BoxCollider)
                {
                    BoxCollider boxCollider = (BoxCollider) collider;
                    Vector3 halfExtents = boxCollider.size * Mathf.Abs(boxCollider.transform.GetLossyScale() * 0.5f);
                    position = boxCollider.transform.position + boxCollider.center;
                    EmptyCollisionBuffer();
                    Physics.OverlapBoxNonAlloc(position, halfExtents, _overlappingColliders,
                        boxCollider.transform.rotation, layerMask.value);
                }
                else if (collider is CapsuleCollider)
                {
                    CapsuleCollider capsuleCollider = (CapsuleCollider) collider;
                    float scaledRadius = capsuleCollider.radius * Mathf.Abs(capsuleCollider.transform.GetLossyScale());
                    position = capsuleCollider.transform.position + capsuleCollider.center;
                    Vector3 point0 = position;
                    Vector3 point1 = position;
                    float halfHeight = capsuleCollider.height / 2.0f;
                    switch (capsuleCollider.direction)
                    {
                        case 0:
                            point0.x += halfHeight;
                            point1.x -= halfHeight;
                            break;
                        case 1:
                            point0.y += halfHeight;
                            point1.y -= halfHeight;
                            break;
                        case 2:
                            point0.z += halfHeight;
                            point1.z -= halfHeight;
                            break;
                        default:
                            ErrorReporter.LogHandledException(
                                new Exception("Unrecognized capsule collider direction: " + capsuleCollider.direction));
                            continue;
                    }

                    EmptyCollisionBuffer();
                    Physics.OverlapCapsuleNonAlloc(point0, point1, scaledRadius, _overlappingColliders,
                        layerMask.value);
                }
                else if (collider is SphereCollider)
                {
                    SphereCollider sphereCollider = (SphereCollider) collider;
                    float scaledRadius = sphereCollider.radius * Mathf.Abs(sphereCollider.transform.GetLossyScale());
                    position = sphereCollider.transform.position + sphereCollider.center;
                    EmptyCollisionBuffer();
                    Physics.OverlapSphereNonAlloc(position, scaledRadius, _overlappingColliders, layerMask.value);
                }
                else
                {
                    continue;
                }

                CheckHoveringForTransform(position, ref closestDistance, ref closestHoverable, ref hoverables);
            }

            if (!supportsMultipleInteractions)
            {
                // Exit out if we aren't hovering over anything
                if (Interactions.Length > 0 && Interactions[0].Interactable == closestHoverable)
                {
                    return;
                }

                // If we were previously hovering over something then finish the interaction
                if (Interactions.Length > 0)
                {
                    TryFinishInteraction(Interactions[0].Interactable, this);
                }

                // If we are now hovering over something then start the interaction
                if (closestHoverable != null)
                {
                    TryStartInteraction(closestHoverable, this);
                }
            }
            else
            {
                // Finish any interactions with interactables that are no longer being hovered over
                Interaction[] interactions =
                    Interactions.Where(x => !hoverables.Any(y => y.Equals(x.Interactable))).ToArray();
                int i;
                for (i = 0; i < interactions.Length; i++)
                {
                    TryFinishInteraction(interactions[i].Interactable, this);
                }

                // Start interacting with any interactables that we just started hovering over
                Interactable[] interactables =
                    hoverables.Where(x => !Interactions.Any(y => x.Equals(y.Interactable))).ToArray();
                for (i = 0; i < interactables.Length; i++)
                {
                    TryStartInteraction(interactables[i], this);
                }
            }
        }

        /// <summary>
        /// Empties the collision buffer.
        /// </summary>
        private void EmptyCollisionBuffer()
        {
            for (int i = 0; i < _overlappingColliders.Length; i++)
            {
                _overlappingColliders[i] = null;
            }
        }

        /// <summary>
        /// Checks if the currently overlapping colliders are attached to objects that can be hovered over and determines which one is the closest.
        /// </summary>
        /// <param name="position">The hovering position.</param>
        /// <param name="closestDistance">The current closest distance to a <see cref="Hoverable"/> that is being hovered over.</param>
        /// <param name="closestHoverable">The current closest <see cref="Hoverable"/>.</param>
        /// <param name="hoverables">The current list of items that are being hovered over.</param>
        private void CheckHoveringForTransform(Vector3 position, ref float closestDistance,
            ref Hoverable closestHoverable, ref List<Interactable> hoverables)
        {
            for (int colliderIndex = 0; colliderIndex < _overlappingColliders.Length; colliderIndex++)
            {
                Collider collider = _overlappingColliders[colliderIndex];

                if (collider == null)
                {
                    continue;
                }

                Hoverable hoverable = collider.GetComponentInParent<Hoverable>();

                if (hoverable == null || !hoverable.enabled || !hoverable.IsValidCollider(collider))
                {
                    continue;
                }

                float distance = Vector3.Distance(hoverable.transform.position, position);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestHoverable = hoverable;
                }

                hoverables.TryAddUnique(hoverable);
            }
        }

        /// <inheritdoc cref="MonoBehaviour.Reset"/>
        private void Reset()
        {
            Collider[] possibleColliders = GetComponents<Collider>();
            List<Collider> result = new List<Collider>();

            for (int i = 0; i < possibleColliders.Length; i++)
            {
                Collider possibleCollider = possibleColliders[i];
                if (possibleCollider is BoxCollider || possibleCollider is CapsuleCollider ||
                    possibleCollider is SphereCollider)
                {
                    result.Add(possibleCollider);
                }
            }

            colliders = result.ToArray();
        }
    }
}