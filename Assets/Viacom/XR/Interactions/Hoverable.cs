﻿// <copyright file="Hoverable.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an object that supports hover interactions.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Interactions
{
	using System.Linq;

	using UnityEngine;

	/// <summary>
	/// Represents an object that supports hover interactions.
	/// </summary>
	public class Hoverable : Interactable
	{
		[SerializeField] private Collider[] colliders;

		/// <summary>
		/// Gets a value indicating whether not the specified <see cref="Collider"/> is a valid collider for interacting.
		/// </summary>
		/// <param name="collider">The <see cref="Collider"/> to verify.</param>
		/// <returns><c>true</c> if <paramref name="collider"/> is a valid collider for interacting; otherwise, <c>false</c>.</returns>
		public bool IsValidCollider(Collider collider)
		{
			return colliders.Contains(collider);
		}

		/// <inheritdoc cref="MonoBehaviour.Reset"/>
		private void Reset()
		{
			colliders = GetComponents<Collider>();
		}
	}
}