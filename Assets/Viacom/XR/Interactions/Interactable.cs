﻿// <copyright file="Interactable.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an object that can be interacted with.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Interactions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using UnityEngine;
    using UnityEngine.Events;
    
    using Viacom.XR.Diagnostics;

    /// <summary>
    /// Provides data for <see cref="Interaction"/> events.
    /// </summary>
    public class InteractionEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the <see cref="Interaction"/> that triggered the event.
        /// </summary>
        public Interaction Interaction { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="InteractionEventArgs"/> class with the <see cref="Interaction"/> that has triggered the event.
        /// </summary>
        /// <param name="Interaction">The <see cref="Interaction"/> the interaction that has triggered the event.</param>
        public InteractionEventArgs(Interaction interaction)
        {
            Interaction = interaction;
        }
    }

    /// <summary>
    /// Represents the method that will handle interactions events regarding <see cref="Interaction"/>.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e"><see cref="InteractionEventArgs"/></param>
    public delegate void InteractionEventHandler(object sender, InteractionEventArgs e);

    /// <summary>
    /// Represents an object that can be interacted with.
    /// </summary>
    public abstract class Interactable : MonoBehaviour
    {
        [SerializeField] protected bool supportsMultipleInteractions;
        [SerializeField] protected UnityEvent interactionStarted;
        [SerializeField] protected UnityEvent interactionFinished;
        [SerializeField] protected UnityEvent allInteractionsFinished;

        protected readonly List<Interaction> ActiveInteractions = new List<Interaction>();

        /// <summary>
        /// Gets a value indicating whether or not this <see cref="Interactable"/> is currently interacting with anything.
        /// </summary>
        public bool IsInteracting
        {
            get { return ActiveInteractions.Count > 0; }
        }

        /// <summary>
        /// Gets a value indicating whether or not this <see cref="Interactable"/> is supports more than one simultaneous interaction.
        /// </summary>
        public bool SupportsMultipleInteractions
        {
            get { return supportsMultipleInteractions; }
        }

        /// <summary>
        /// Gets an array of all objects that are currently interacting with this <see cref="Interactable"/>.
        /// </summary>
        public Interaction[] Interactions
        {
            get { return ActiveInteractions.ToArray(); }
        }

        /// <summary>
        /// Occurs when an <see cref="Interactable"/> starts interacting with this object.
        /// </summary>
        public event InteractionEventHandler InteractionStarted;

        /// <summary>
        /// Occurs when an <see cref="Interactable"/> finishes interacting with this object.
        /// </summary>
        public event InteractionEventHandler InteractionFinished;

        /// <summary>
        /// Occurs when there are no longer any interactions with this object.
        /// </summary>
        public event InteractionEventHandler AllInteractionsFinished;

        /// <inheritdoc cref="MonoBehaviour.OnDisable"/>
        protected virtual void OnDisable()
        {
            // Finish all interactions when this interactable is disabled
            for (int i = 0; i < ActiveInteractions.Count; i++)
            {
                TryFinishInteraction(ActiveInteractions[i].Interactable, this);
            }
        }

        /// <summary>
        /// Starts an interaction.
        /// </summary>
        /// <param name="interaction">The <see cref="Interaction"/>.</param>
        protected virtual void StartInteraction(Interaction interaction)
        {
            ActiveInteractions.Add(interaction);

            if (InteractionStarted != null)
            {
                InteractionStarted(this, new InteractionEventArgs(interaction));
            }

            if (interactionStarted != null)
            {
                interactionStarted.Invoke();
            }
        }

        /// <summary>
        /// Finishes an interaction with the <see cref="Interactable"/>.
        /// </summary>
        /// <param name="interactable">The other <see cref="Interactable"/>.</param>
        protected virtual void FinishInteraction(Interactable interactable)
        {
            Interaction endingInteraction =
                ActiveInteractions.FirstOrDefault(interaction => interaction.Interactable.Equals(interactable));
            ActiveInteractions.Remove(endingInteraction);

            if (InteractionFinished != null)
            {
                InteractionFinished(this, new InteractionEventArgs(endingInteraction));
            }

            if (interactionFinished != null)
            {
                interactionFinished.Invoke();
            }

            if (ActiveInteractions.Count == 0)
            {
                if (AllInteractionsFinished != null)
                {
                    AllInteractionsFinished(this, new InteractionEventArgs(endingInteraction));
                }

                if (allInteractionsFinished != null)
                {
                    allInteractionsFinished.Invoke();
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Interactable"/> can start an interaction with another <see cref="Interactable"/>.
        /// </summary>
        /// <param name="interactable">The other <see cref="Interactable"/>.</param>
        /// <returns><c>true</c> if <paramref name="interactable"/> can start an interaction; otherwise, <c>false</c>.</returns>
        protected virtual bool CanStartInteraction(Interactable interactable)
        {
            if (!enabled)
            {
                return false;
            }

            if (interactable == null)
            {
                ErrorReporter.LogHandledException(new ArgumentNullException("interactable"));
                return false;
            }
            
            if (!enabled)
            {
                return false;
            }

            if (ActiveInteractions.Any(interaction => interaction.Interactable == interactable))
            {
                ErrorReporter.LogHandledException(new InteractionException(
                    "Attempting to start an interaction that is already happening.", gameObject.name,
                    interactable.gameObject.name));
                return false;
            }
            
            if (ActiveInteractions.Count > 0 && !supportsMultipleInteractions)
            {
                return false;
            }

            return true;
        }
        
        /// <summary>
        /// Gets a value indicating whether this <see cref="Interactable"/> can finish an interaction with another <see cref="Interactable"/>.
        /// </summary>
        /// <param name="interactable">The other <see cref="Interactable"/>.</param>
        /// <returns><c>true</c> if <paramref name="interactable"/> can finish an interaction; otherwise, <c>false</c>.</returns>
        protected virtual bool CanFinishInteraction(Interactable interactable)
        {
            if (interactable == null)
            {
                ErrorReporter.LogHandledException(new ArgumentNullException("interactable"));
                return false;
            }

            if (!ActiveInteractions.Any(interaction => interaction.Interactable == interactable))
            {
                ErrorReporter.LogHandledException(new InteractionException(
                    "Attempting to finish an interaction that was not started.", gameObject.name,
                    interactable.gameObject.name));
                return false;
            }

            return true;
        }
        
        /// <summary>
        /// Sets the current 3D location of an interaction.
        /// </summary>
        /// <param name="interaction">The <see cref="Interaction"/> to update.</param>
        /// <param name="point">The location of the interaction in 3D space.</param>
        protected void SetInteractionPoint(Interaction interaction, Vector3 point)
        {
            interaction.SetInteractionPoint(point);
            interaction.Interactable.Interactions.Where(x => x.Interactable == this).ForEach(y => y.SetInteractionPoint(point));
        }
        
        /// <summary>
        /// Sets a list of locations in 3D space of an interaction.
        /// </summary>
        /// <param name="interaction">The <see cref="Interaction"/> to update.</param>
        /// <param name="points">The locations of the interaction in 3D space.</param>
        protected void SetInteractionPoints(Interaction interaction, Vector3[] points)
        {
            interaction.SetInteractionPoints(points);
            interaction.Interactable.Interactions.Where(x => x.Interactable == this).ForEach(y => y.SetInteractionPoints(points));
        }

        /// <summary>
        /// Starts an interaction between a pair of <see cref="Interactable"/> objects returns a
        /// value that indicates whether the interaction was started successfully.
        /// </summary>
        /// <param name="first">The first <see cref="Interactable"/>.</param>
        /// <param name="second">The second <see cref="Interactable"/>.</param>
        /// <returns><c>true</c> if the interaction was started; otherwise, <c>false</c>.</returns>
        protected static bool TryStartInteraction(Interactable first, Interactable second)
        {
            if (first.CanStartInteraction(second) && second.CanStartInteraction(first))
            {
                first.StartInteraction(new Interaction(second));
                second.StartInteraction(new Interaction(first));
                return true;
            }

            return false;
        }
        
        /// <summary>
        /// Starts an interaction between a pair of <see cref="Interactable"/> objects at a specified location in 3D space
        /// and returns a value that indicates whether the interaction was started successfully.
        /// </summary>
        /// <param name="first">The first <see cref="Interactable"/>.</param>
        /// <param name="second">The second <see cref="Interactable"/>.</param>
        /// <param name="interactionPoint">The location of the interaction in 3D space.</param>
        /// <returns><c>true</c> if the interaction was started; otherwise, <c>false</c>.</returns>
        protected static bool TryStartInteraction(Interactable first, Interactable second, Vector3 interactionPoint)
        {
            if (first.CanStartInteraction(second) && second.CanStartInteraction(first))
            {
                first.StartInteraction(new Interaction(second, interactionPoint));
                second.StartInteraction(new Interaction(first, interactionPoint));
                return true;
            }

            return false;
        }
        
        /// <summary>
        /// Starts an interaction between a pair of <see cref="Interactable"/> objects at a specified locations in 3D space
        /// and returns a value that indicates whether the interaction was started successfully.
        /// </summary>
        /// <param name="first">The first <see cref="Interactable"/>.</param>
        /// <param name="second">The second <see cref="Interactable"/>.</param>
        /// <param name="interactionPoints">The locations of the interaction in 3D space.</param>
        /// <returns><c>true</c> if the interaction was started; otherwise, <c>false</c>.</returns>
        protected static bool TryStartInteraction(Interactable first, Interactable second, Vector3[] interactionPoints)
        {
            if (first.CanStartInteraction(second) && second.CanStartInteraction(first))
            {
                first.StartInteraction(new Interaction(second, interactionPoints));
                second.StartInteraction(new Interaction(first, interactionPoints));
                return true;
            }

            return false;
        }

        /// <summary>
        /// Finishes an interaction between a pair of <see cref="Interactable"/> objects returns a
        /// value that indicates whether the interaction was finished successfully.
        /// </summary>
        /// <param name="first">The first <see cref="Interactable"/>.</param>
        /// <param name="second">The second <see cref="Interactable"/>.</param>
        /// <returns><c>true</c> if the interaction was finished; otherwise, <c>false</c>.</returns>
        protected static bool TryFinishInteraction(Interactable first, Interactable second)
        {
            if (first.CanFinishInteraction(second) && second.CanFinishInteraction(first))
            {
                first.FinishInteraction(second);
                second.FinishInteraction(first);
                return true;
            }

            return false;
        }
    }
}