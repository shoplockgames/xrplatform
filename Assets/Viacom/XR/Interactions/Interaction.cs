﻿// <copyright file="Interaction.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an going interaction between two interactable objects.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Interactions
{
	using System.Collections.Generic;

	using UnityEngine;

	/// <summary>
	/// Represents an going interaction between two <see cref="Interactable"/> objects.
	/// </summary>
	public class Interaction
	{
		private List<Vector3> _interactionPoints = new List<Vector3>();
		
		/// <summary>
		/// Gets the <see cref="Interactable"/> component that is being interacted with.
		/// </summary>
		public Interactable Interactable { get; private set; }

		/// <summary>
		/// Gets the <see cref="GameObject"/> that is being interacted with.
		/// </summary>
		public GameObject GameObject
		{
			get { return Interactable.gameObject; }
		}

		/// <summary>
		/// Gets a list of points where the interaction is occuring.
		/// </summary>
		public Vector3[] InteractionPoints
		{
			get { return _interactionPoints.ToArray(); }
		}

		/// <summary>
		/// Gets the time since the beginning of the game at which this interaction started.
		/// </summary>
		public float StartTime { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="Interaction"/> class with a specified <see cref="Interactable"/> and points of interaction.
		/// </summary>
		/// <param name="interactable">The <see cref="Interactable"/> that is being interacted with.</param>
		/// <param name="interactionPoints">The points in 3D space at which the interaction is occuring.</param>
		public Interaction(Interactable interactable, Vector3[] interactionPoints) : this(interactable)
		{
			SetInteractionPoints(interactionPoints);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Interaction"/> class with a specified <see cref="Interactable"/> and point of interaction.
		/// </summary>
		/// <param name="interactable">The <see cref="Interactable"/> that is being interacted with.</param>
		/// <param name="interactionPoint">The point in 3D space at which the interaction is occuring.</param>
		public Interaction(Interactable interactable, Vector3 interactionPoint) : this(interactable)
		{
			SetInteractionPoint(interactionPoint);
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="Interaction"/> class with a specified <see cref="Interactable"/>.
		/// </summary>
		/// <param name="interactable">The <see cref="Interactable"/> that is being interacted with.</param>
		public Interaction(Interactable interactable)
		{
			Interactable = interactable;
			StartTime = Time.time;
		}
		
		/// <summary>
		/// Sets the point in 3D space at which this interaction is occuring.
		/// </summary>
		/// <param name="point">The point in 3D space at which this interaction is occuring.</param>
		public void SetInteractionPoint(Vector3 point)
		{
			_interactionPoints.Clear();
			_interactionPoints.Add(point);
		}
		
		/// <summary>
		/// Sets the points in 3D space at which this interaction is occuring.
		/// </summary>
		/// <param name="points">The points in 3D space at which this interaction is occuring.</param>
		public void SetInteractionPoints(Vector3[] points)
		{
			_interactionPoints.Clear();
			_interactionPoints.AddRange(points);
		}
	}
}