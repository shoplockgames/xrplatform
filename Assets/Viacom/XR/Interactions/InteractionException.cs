﻿// <copyright file="InteractionException.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// The exception that is thrown when attempting to interact with an Interactable fails.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Interactions
{
    using System;
    
    using UnityEngine;
    
    /// <summary>
    /// The exception that is thrown when attempting to interact with an <see cref="Interactable"/> fails.
    /// </summary>
    public sealed class InteractionException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InteractionException"/> class.
        /// </summary>
        public InteractionException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InteractionException"/> class with a specified error message.
        /// </summary>
        /// <param name="message">A description of the error.</param>
        public InteractionException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InteractionException"/> class with a specified error message and the names of the objects that were interacting.
        /// </summary>
        /// <param name="message">A description of the error.</param>
        /// <param name="firstName">The name of the first <see cref="GameObject"/>.</param>
        /// <param name="secondName">The name of the second <see cref="GameObject"/>.</param>
        public InteractionException(string message, string firstName, string secondName) : base(message)
        {
            Data["first"] = firstName;
            Data["second"] = secondName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InteractionException"/> class with a specified error message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">A description of the error.</param>
        /// <param name="inner">The exception that is the cause of the current exception.</param>
        public InteractionException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
