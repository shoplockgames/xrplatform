﻿// <copyright file="PointInteractor.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a game object that can point at another game object. Uses a sphere to raycast.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Interactions
{
	using System;
	using System.Collections.Generic;
	using System.Linq;

	using UnityEngine;

	/// <summary>
	/// Represents a game object that can point at another game object.
	/// </summary>
	public class PointInteractor : Interactable
	{
		[SerializeField] private float updateInterval = 0.1f;
		[SerializeField] private LayerMask layerMask;
		[SerializeField] private float radius;
		[SerializeField] private Vector3 direction = Vector3.forward;
		[SerializeField] private int raycastHitBufferSize = 16;
		[SerializeField] private int maxDistance = 20;
		[SerializeField] private Color debugColor = Color.cyan;

		private Collider[] _colliders;
		private RaycastHit[] _raycastHits;
		private bool _hasStarted = false;

		private const float MaxStartTime = 0.5f;
		private const string UpdateMethodName = "UpdateState";

		/// <inheritdoc cref="MonoBehaviour.OnEnable"/>
		private void OnEnable()
		{
			_raycastHits = new RaycastHit[raycastHitBufferSize];

			// To try to have these interactors update on different intervals we start at a random time
			float startTime = UnityEngine.Random.Range(0, MaxStartTime);
			InvokeRepeating(UpdateMethodName, startTime, updateInterval);
		}

		/// <inheritdoc cref="MonoBehaviour.OnDisable"/>
		protected override void OnDisable()
		{
			CancelInvoke();
			base.OnDisable();
		}

		/// <inheritdoc cref="MonoBehaviour.Start"/>
		private void Start()
		{
			_hasStarted = true;
		}

		/// <summary>
		/// Updates the current pointing state.
		/// </summary>
		private void UpdateState()
		{
			float closestDistance = float.MaxValue;
			Pointable closestPointable = null;
			List<Interactable> pointables = new List<Interactable>();
			EmptyRaycastHitBuffer();
			Vector3 transformedDirection = transform.TransformDirection(direction);
			float scaledRadius = radius * Mathf.Abs(transform.GetLossyScale());
			Physics.SphereCastNonAlloc(transform.position, scaledRadius, transformedDirection, _raycastHits,
				maxDistance, layerMask.value);
			CheckGazingForTransform(transform.position, ref closestDistance, ref closestPointable, ref pointables);

			if (!supportsMultipleInteractions)
			{
				// Exit out if we aren't pointing at anything
				if (Interactions.Length > 0 && Interactions[0].Interactable == closestPointable)
				{
					return;
				}

				// If we were previously pointing at something then finish the interaction
				if (Interactions.Length > 0)
				{
					TryFinishInteraction(Interactions[0].Interactable, this);
				}

				// If we are now pointing at something then start the interaction
				if (closestPointable != null)
				{
					TryStartInteraction(closestPointable, this);
				}
			}
			else
			{
				// Finish any interactions with interactables that are no longer being pointing at
				Interaction[] interactions =
					Interactions.Where(x => !pointables.Any(y => y.Equals(x.Interactable))).ToArray();
				int i;
				for (i = 0; i < interactions.Length; i++)
				{
					TryFinishInteraction(interactions[i].Interactable, this);
				}

				// Start interacting with any interactables that we just started pointing at
				Interactable[] interactables =
					pointables.Where(x => !Interactions.Any(y => x.Equals(y.Interactable))).ToArray();
				for (i = 0; i < interactables.Length; i++)
				{
					TryStartInteraction(interactables[i], this);
				}
			}
		}

		/// <summary>
		/// Checks if the currently overlapping colliders are attached to objects that can be pointed at and determines which one is the closest.
		/// </summary>
		/// <param name="position">The pointing source position.</param>
		/// <param name="closestDistance">The current closest distance to a <see cref="Pointable"/> that is being pointed at.</param>
		/// <param name="closestPointable">The current closest <see cref="Pointable"/>.</param>
		/// <param name="pointables">The current list of items that are being pointed at.</param>
		private void CheckGazingForTransform(Vector3 position, ref float closestDistance,
			ref Pointable closestPointable, ref List<Interactable> pointables)
		{
			for (int raycastHitIndex = 0; raycastHitIndex < _raycastHits.Length; raycastHitIndex++)
			{
				RaycastHit hit = _raycastHits[raycastHitIndex];

				if (hit.collider == null)
				{
					continue;
				}

				Collider collider = hit.collider;
				Pointable pointable = collider.GetComponentInParent<Pointable>();

				if (pointable == null || !pointable.enabled || !pointable.IsValidCollider(collider))
				{
					continue;
				}

				float distance = Vector3.Distance(pointable.transform.position, position);
				if (distance < closestDistance)
				{
					closestDistance = distance;
					closestPointable = pointable;
				}

				pointables.TryAddUnique(pointable);
			}
		}

		/// <summary>
		/// Empties the raycast hit buffer.
		/// </summary>
		private void EmptyRaycastHitBuffer()
		{
			Array.Clear(_raycastHits, 0, _raycastHits.Length);
		}

		/// <inheritdoc cref="MonoBehaviour.OnDrawGizmos"/>
		private void OnDrawGizmos()
		{
			Gizmos.color = debugColor;

			if (_hasStarted)
			{
				float scaledRadius = radius * Mathf.Abs(transform.GetLossyScale());
				Gizmos.DrawWireSphere(transform.position, scaledRadius);
				Vector3 transformedDirection = transform.TransformDirection(direction);
				Gizmos.DrawRay(transform.position, transformedDirection);
			}
		}
	}
}