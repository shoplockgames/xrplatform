﻿// <copyright file="Pointable.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an object that supports pointing interactions.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Interactions
{
    using System.Linq;

    using UnityEngine;

    /// <summary>
    /// Represents an object that supports pointing interactions.
    /// </summary>
    public class Pointable : Interactable
    {
        [SerializeField] protected Collider[] colliders;

        /// <summary>
        /// Gets a value indicating whether not the specified <see cref="Collider"/> is a valid collider for interacting.
        /// </summary>
        /// <param name="collider">The <see cref="Collider"/> to verify.</param>
        /// <returns><c>true</c> if <paramref name="collider"/> is a valid collider for interacting; otherwise, <c>false</c>.</returns>
        public bool IsValidCollider(Collider collider)
        {
            return colliders.Contains(collider);
        }

        /// <inheritdoc cref="MonoBehaviour.Reset"/>
        protected void Reset()
        {
            colliders = GetComponents<Collider>();
        }
    }
}