﻿// <copyright file="Throwable.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an object that can be thrown.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Interactions
{
	using UnityEngine;

	/// <summary>
	/// Represents a <see cref="GameObject"/> that can be thrown.
	/// </summary>
	[RequireComponent(typeof(Rigidbody))]
	public class Throwable : MonoBehaviour
	{
		[SerializeField] private float velocityScaler = 1.0f;
		[SerializeField] private VelocityEstimator velocityEstimator;
		
		private Rigidbody _rigidbody;
		private RigidbodyInterpolation _hadInterpolation = RigidbodyInterpolation.None;
		
		/// <inheritdoc cref="MonoBehaviour.Awake"/>
		private void Awake()
		{
			_rigidbody = GetComponent<Rigidbody>();
		}

		/// <summary>
		/// Begins tracking the velocity <see cref="GameObject"/>.
		/// </summary>
		public void BeginTrackingVelocity()
		{
			_hadInterpolation = _rigidbody.interpolation;
			_rigidbody.interpolation = RigidbodyInterpolation.None;

			if (velocityEstimator != null)
			{
				velocityEstimator.Begin();
			}
		}
		
		/// <summary>
		/// Finishes tracking the velocity of the <see cref="GameObject"/> and sets the velocity of the attached <see cref="Rigidbody"/>.
		/// </summary>
		public void Throw()
		{
			_rigidbody.interpolation = _hadInterpolation;
			if (velocityEstimator != null)
			{
				velocityEstimator.Finish();
				_rigidbody.velocity = velocityEstimator.Velocity * velocityScaler;
				_rigidbody.angularVelocity = velocityEstimator.AngularVelocity;
			}
			else
			{
				_rigidbody.velocity =_rigidbody.velocity * velocityScaler;
			}
		}
	}
}