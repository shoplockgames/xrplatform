﻿// <copyright file="VelocityEstimator.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Provides a mechanism to estimate the velocity and angular velocity of a <see cref="GameObject"/> over time.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Interactions
{
	using System.Collections;

	using UnityEngine;

	/// <summary>
	/// Represents an estimator of the velocity and angular velocity of a <see cref="GameObject"/> over time.
	/// </summary>
	public class VelocityEstimator : MonoBehaviour
	{
		[Tooltip("How many frames to average over for computing velocity")]
		[SerializeField]
		private int velocityAverageFrames = 5;

		[Tooltip("How many frames to average over for computing angular velocity")]
		[SerializeField]
		private int angularVelocityAverageFrames = 11;

		[Tooltip("Start estimating velocity on awake")]
		[SerializeField]
		public bool beginOnAwake = false;

		private Coroutine _coroutine;
		private int _sampleCount;
		private Vector3[] _velocitySamples;
		private Vector3[] _angularVelocitySamples;

		/// <summary>
		/// Gets the current estimated velocity of the <see cref="GameObject"/>.
		/// </summary>
		public Vector3 Velocity
		{
			get
			{
				// Compute average velocity
				Vector3 velocity = Vector3.zero;
				int velocitySampleCount = Mathf.Min(_sampleCount, _velocitySamples.Length);
				if (velocitySampleCount != 0)
				{
					for (int i = 0; i < velocitySampleCount; i++)
					{
						velocity += _velocitySamples[i];
					}

					velocity *= (1.0f / velocitySampleCount);
				}

				return velocity;
			}
		}

		/// <summary>
		/// Gets the current estimated angular velocity of the <see cref="GameObject"/>.
		/// </summary>
		public Vector3 AngularVelocity
		{
			get
			{
				// Compute average angular velocity
				Vector3 angularVelocity = Vector3.zero;
				int angularVelocitySampleCount = Mathf.Min(_sampleCount, _angularVelocitySamples.Length);
				if (angularVelocitySampleCount != 0)
				{
					for (int i = 0; i < angularVelocitySampleCount; i++)
					{
						angularVelocity += _angularVelocitySamples[i];
					}

					angularVelocity *= (1.0f / angularVelocitySampleCount);
				}

				return angularVelocity;
			}
		}

		/// <summary>
		/// Gets the current estimated accleration of the <see cref="GameObject"/>.
		/// </summary>
		public Vector3 Acceleration
		{
			get
			{
				Vector3 average = Vector3.zero;
				for (int i = 2 + _sampleCount - _velocitySamples.Length; i < _sampleCount; i++)
				{
					if (i < 2)
						continue;

					int first = i - 2;
					int second = i - 1;

					Vector3 v1 = _velocitySamples[first % _velocitySamples.Length];
					Vector3 v2 = _velocitySamples[second % _velocitySamples.Length];
					average += v2 - v1;
				}

				average *= (1.0f / Time.deltaTime);
				return average;
			}
		}

		/// <inheritdoc cref="MonoBehaviour.Awake"/>
		private void Awake()
		{
			_velocitySamples = new Vector3[velocityAverageFrames];
			_angularVelocitySamples = new Vector3[angularVelocityAverageFrames];

			if (beginOnAwake)
			{
				Begin();
			}
		}

		/// <summary>
		/// Calculates velocity and angular velocity since the last frame.
		/// </summary>
		private IEnumerator EstimateVelocityCoroutine()
		{
			_sampleCount = 0;
			Vector3 previousPosition = transform.position;
			Quaternion previousRotation = transform.rotation;

			while (true)
			{
				yield return new WaitForEndOfFrame();

				float velocityFactor = 1.0f / Time.deltaTime;

				int v = _sampleCount % _velocitySamples.Length;
				int w = _sampleCount % _angularVelocitySamples.Length;
				_sampleCount++;

				// Estimate linear velocity
				_velocitySamples[v] = velocityFactor * (transform.position - previousPosition);

				// Estimate angular velocity
				Quaternion deltaRotation = transform.rotation * Quaternion.Inverse(previousRotation);

				float theta = 2.0f * Mathf.Acos(Mathf.Clamp(deltaRotation.w, -1.0f, 1.0f));
				if (theta > Mathf.PI)
				{
					theta -= 2.0f * Mathf.PI;
				}

				Vector3 angularVelocity = new Vector3(deltaRotation.x, deltaRotation.y, deltaRotation.z);
				if (angularVelocity.sqrMagnitude > 0.0f)
				{
					angularVelocity = theta * velocityFactor * angularVelocity.normalized;
				}

				_angularVelocitySamples[w] = angularVelocity;

				previousPosition = transform.position;
				previousRotation = transform.rotation;
			}
		}

		/// <summary>
		/// Begins collecting samples of velocity and angular velocity.
		/// </summary>
		public void Begin()
		{
			Finish();
			_coroutine = StartCoroutine(EstimateVelocityCoroutine());
		}

		/// <summary>
		/// Finishes collecting samples of velocity and angular velocity.
		/// </summary>
		public void Finish()
		{
			if (_coroutine != null)
			{
				StopCoroutine(_coroutine);
				_coroutine = null;
			}
		}
	}
}
