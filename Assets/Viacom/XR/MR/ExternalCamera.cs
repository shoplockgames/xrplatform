﻿// <copyright file="ExternalCamera.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an external camera in a virtual reality scene. This will render a quad across the screen with the top-left
// section rendering the near plane, the top-right rendering the alpha of the near plane, the bottom-right rendering any
// other cameras that are active and the bottom-left rendering the far plane.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.MR
{
	using System;
	using System.IO;
	using System.Collections;
	using System.Collections.Generic;
	using System.Reflection;

	using UnityEngine;
	using UnityEngine.Rendering;
	using UnityEngine.XR;

	using Viacom.XR.Diagnostics;

	/// <summary>
	/// Represents an external camera in a virtual reality scene.
	/// </summary>
	public class ExternalCamera : MonoBehaviour
	{
		[Serializable]
		private struct Config
		{
			public float x, y, z;
			public float rx, ry, rz;
			public float fov;
			public float near, far;
			public float eyeTexResolutionScale;
			public float frameSkip;
			public float nearOffset, farOffset;
			public float hmdOffset;
			public float r, g, b, a; //Chroma key override
			public bool disableStandardAssets;
		}

		[SerializeField] private Camera attachedCamera;
		[SerializeField] private string configFilePath;
		[SerializeField] private Transform target;
		[SerializeField] private Material colorMaterial;
		[SerializeField] private Material alphaMaterial;
		[SerializeField] private Material clipMaterial;

		private Camera _renderCamera;
		private Config _config;
		private FileSystemWatcher _fileSystemWatcher;
		private readonly Dictionary<Camera, Rect> _cameraRectPairs = new Dictionary<Camera, Rect>();
		private GameObject _clipQuad;
		private int _frameSkip = 0;
		private readonly Rect _gameCameraRect = new Rect(0.5f, 0.0f, 0.5f, 0.5f);
		private readonly Rect _fixCameraRect = new Rect(0, 0, 1, 1);
		private readonly Vector3 _clipQuadScale = new Vector3(1000.0f, 1000.0f, 1.0f);
		private Rect _nearRenderColorRect;
		private Rect _nearRenderAlphaRect;
		private Rect _farRenderRect;
		private int _halfScreenWidth;
		private int _halfScreenHeight;
		private float _lastScreenWidth = -1;
		private float _lastScreenHeight = -1;
		private float _originalEyeTextureResolutionScale;
		private bool _isConfigDirty = false;

		private static readonly string LogTag = typeof(ExternalCamera).Name;
		private const char ConfigKeyValSeparator = '=';
		private const string ConfigMatrixKey = "m";
		private const string ConfigAssetsKey = "disableStandardAssets";
		private const string PostProcessorTypeName = "PostProcessingBehavior";
		private const float Epsilon = .01f;
		private const string CameraGameObjectName = "Camera";
		private const string ClipQuadGameObjectName = "ClipQuad";

		/// <inheritdoc cref="MonoBehaviour.Awake"/>
		private void Awake()
		{
			if (attachedCamera == null)
			{
				attachedCamera = gameObject.GetComponent<Camera>();

				if (attachedCamera == null)
				{
					ApplicationLog.Warning(LogTag, "Could not find camera. Falling back to main camera.");
				}

				attachedCamera = Camera.main;

				if (attachedCamera == null)
				{
					ErrorReporter.LogHandledException(
						new NullReferenceException("External camera could not find a valid camera in the scene."));
					enabled = false;
					return;
				}
			}

			ReadConfig();
			InitializeRenderCamera();
		}

		/// <inheritdoc cref="MonoBehaviour.OnEnable"/>
		private void OnEnable()
		{
			StartCoroutine(WaitForConfigUpdate());
			StartCoroutine(RenderLoop());

			// Find all game view cameras and only render them to the lower-right quadrant
			Camera[] cameras = FindObjectsOfType<Camera>();
			if (cameras != null)
			{
				foreach (Camera cam in cameras)
				{
					// Only change cameras that aren't ours or who aren't rendering to a texture
					if (cam != _renderCamera && cam.targetTexture == null &&
					    cam.stereoTargetEye == StereoTargetEyeMask.None)
					{
						_cameraRectPairs.Add(cam, cam.rect);
						cam.rect = _gameCameraRect;
					}
				}
			}

			if (_config.eyeTexResolutionScale > 0f)
			{
				_originalEyeTextureResolutionScale = XRSettings.eyeTextureResolutionScale;
				XRSettings.eyeTextureResolutionScale = _config.eyeTexResolutionScale;
			}
		}

		/// <inheritdoc cref="MonoBehaviour.OnDisable"/>
		private void OnDisable()
		{
			StopAllCoroutines();

			// Restore the view back to the game-view cameras
			foreach (KeyValuePair<Camera, Rect> pair in _cameraRectPairs)
			{
				if (pair.Key == null)
				{
					continue;
				}

				pair.Key.rect = pair.Value;
			}

			if (_config.eyeTexResolutionScale > 0f)
			{
				XRSettings.eyeTextureResolutionScale = _originalEyeTextureResolutionScale;
			}

			_cameraRectPairs.Clear();
		}

		/// <inheritdoc cref="MonoBehaviour.OnDestroy"/>
		private void OnDestroy()
		{
			_fileSystemWatcher = null;
		}

		/// <inheritdoc cref="MonoBehaviour.OnGUI"/>
		private void OnGUI()
		{
			// Necessary for Graphics.DrawTexture to work even though we don't do anything here.
		}

		/// <inheritdoc cref="MonoBehaviour.Reset"/>
		private void Reset()
		{
			if (attachedCamera == null)
			{
				attachedCamera = gameObject.GetComponent<Camera>();
			}
		}

		/// <summary>
		/// Reads the config file from disk and stores the info in a Config object
		/// </summary>
		private void ReadConfig()
		{
			try
			{
				// Box the config object
				object tempConfig = _config;

				string[] lines = File.ReadAllLines(configFilePath);
				foreach (string line in lines)
				{
					string[] split = line.Split(ConfigKeyValSeparator);
					if (split.Length == 2)
					{
						string key = split[0];
						if (key.Equals(ConfigMatrixKey, StringComparison.InvariantCultureIgnoreCase))
						{
							// TODO: In SteamVR they support taking a general OpenVR coord for transform and rotation
							// from the config and converting it to Unity. We are skipping this for now.
						}
						else if (key.Equals(ConfigAssetsKey, StringComparison.InvariantCultureIgnoreCase))
						{
							FieldInfo field = typeof(Config).GetField(key);
							if (field != null)
							{
								field.SetValue(tempConfig, bool.Parse(split[1]));
							}
						}
						else
						{
							FieldInfo field = typeof(Config).GetField(key);
							if (field != null)
							{
								field.SetValue(tempConfig, float.Parse(split[1]));
							}
						}
					}
				}

				// Now unbox the config object
				_config = (Config) tempConfig;				
			}
			catch (Exception e)
			{
				ErrorReporter.LogHandledException(
					new Exception("Could not read configuration file for external renderCamera.", e));
			}

			// Listen to config file updates
			if (_fileSystemWatcher == null && File.Exists(configFilePath))
			{
				FileInfo fileInfo = new FileInfo(configFilePath);
				_fileSystemWatcher = new FileSystemWatcher(fileInfo.DirectoryName, fileInfo.Name);
				_fileSystemWatcher.NotifyFilter = NotifyFilters.LastWrite;
				_fileSystemWatcher.Changed += OnConfigFileChanged;
				_fileSystemWatcher.EnableRaisingEvents = true;
			}
		}

		/// <summary>
		/// Callback method that is invoked when the configuration file changes.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e"><see cref="FileSystemEventArgs"/></param>
		private void OnConfigFileChanged(object sender, FileSystemEventArgs e)
		{
			ReadConfig();
			_isConfigDirty = true;
		}

		/// <summary>
		/// Checks to see if there are any unapplied updates to the render <see cref="Camera"/>.
		/// </summary>
		private IEnumerator WaitForConfigUpdate() 
		{
			while (!_isConfigDirty) 
			{
				yield return null;
			}

			UpdateRenderCamera();
		}

		/// <summary>
		/// Updates the render <see cref="Camera"/> to the latest configuration values.
		/// </summary>
		private void UpdateRenderCamera()
		{
			// Update anything that depends on the config
			clipMaterial.color = new Color(_config.r, _config.g, _config.b, _config.a);
			if (_clipQuad != null) 
			{
				_clipQuad.GetComponent<MeshRenderer>().material = clipMaterial;
			}

			if (_renderCamera != null) 
			{
				_renderCamera.transform.localPosition = new Vector3(_config.x, _config.y, _config.z);
				_renderCamera.transform.localRotation = Quaternion.Euler(_config.rx, _config.ry, _config.rz);
				_renderCamera.fieldOfView = _config.fov;
			}

			_isConfigDirty = false;
			StartCoroutine(WaitForConfigUpdate());
		}

		/// <summary>
		/// Executes the main render loop.
		/// </summary>
		private IEnumerator RenderLoop()
		{
			while (Application.isPlaying)
			{
				yield return new WaitForEndOfFrame();
				Render();
			}
		}

		/// <summary>
		/// Renders the current scene.
		/// </summary>
		private void Render()
		{
			//Check if we can render this frame or if we skip
			if (Time.frameCount % (_frameSkip + 1) == 0)
			{
				//If the screen resized adjust the size of the renderCamera Rects we draw onscreen
				if (Screen.width != _lastScreenWidth || Screen.height != _lastScreenHeight)
				{
					_halfScreenWidth = Screen.width / 2;
					_halfScreenHeight = Screen.height / 2;
					_nearRenderColorRect = new Rect(0, 0, _halfScreenWidth, _halfScreenHeight);
					_nearRenderAlphaRect = new Rect(_halfScreenWidth, 0, _halfScreenWidth, _halfScreenHeight);
					_farRenderRect = new Rect(0, _halfScreenHeight, _halfScreenWidth, _halfScreenHeight);

					_lastScreenWidth = Screen.width;
					_lastScreenHeight = Screen.height;

					// Make a new renderTexture at the proper size (Unity won't let you edit an existing one)
					_renderCamera.targetTexture = new RenderTexture(_halfScreenWidth, _halfScreenHeight, 24,
						RenderTextureFormat.ARGB32);
				}

				RenderNear();
				RenderFar();
			}
		}

		/// <summary>
		/// Renders the near plane.
		/// </summary>
		private void RenderNear()
		{
			SetClippingPlanes();

			//Store then change the clear flags and background color of our renderCamera
			CameraClearFlags clearFlags = _renderCamera.clearFlags;
			Color backgroundColor = _renderCamera.backgroundColor;
			_renderCamera.clearFlags = CameraClearFlags.Color;
			_renderCamera.backgroundColor = Color.clear;

			float distance = Mathf.Clamp(GetTargetDistance() + _config.nearOffset, _config.near, _config.far);
			Transform parent = _clipQuad.transform.parent;
			_clipQuad.transform.position = parent.position + parent.forward * distance;

			_clipQuad.SetActive(true);
			_renderCamera.Render();
			Graphics.DrawTexture(new Rect(_nearRenderColorRect), _renderCamera.targetTexture, colorMaterial);

			// Render scene with post-processing fx disabled since they override alpha
			// Gotta do it this weird way so there's no references to other Post-proc plugins
			MonoBehaviour postProcessorBehaviour = _renderCamera.GetComponent(PostProcessorTypeName) as MonoBehaviour;
			if (postProcessorBehaviour != null && postProcessorBehaviour.enabled)
			{
				postProcessorBehaviour.enabled = false;
				_renderCamera.Render();
				postProcessorBehaviour.enabled = true;
			}

			Graphics.DrawTexture(new Rect(_nearRenderAlphaRect), _renderCamera.targetTexture, alphaMaterial);

			_clipQuad.SetActive(false);

			// Reset the clear flags and background color
			_renderCamera.clearFlags = clearFlags;
			_renderCamera.backgroundColor = backgroundColor;
		}

		/// <summary>
		/// Renders the far plane.
		/// </summary>
		private void RenderFar()
		{
			SetClippingPlanes();
			_renderCamera.Render();
			Graphics.DrawTexture(new Rect(_farRenderRect), _renderCamera.targetTexture, colorMaterial);
		}

		/// <summary>
		/// Sets the near and far clipping plane distances for the <see cref="Camera"/>.
		/// </summary>
		private void SetClippingPlanes()
		{
			_renderCamera.nearClipPlane = _config.near;
			_renderCamera.farClipPlane = _config.far;
		}

		/// <summary>
		/// Calculates the distance between the rendering <see cref="Camera"/> and target.
		/// </summary>
		/// <returns>The distance between the rendering <see cref="Camera"/> and target.</returns>
		private float GetTargetDistance()
		{
			if (target == null)
			{
				return _config.near + Epsilon;
			}

			Transform offset = _renderCamera.transform;
			Vector3 forward = new Vector3(offset.forward.x, 0f, offset.forward.z).normalized;
			Vector3 targetPosition = target.position +
			                         new Vector3(target.forward.x, 0f, target.forward.z).normalized *
			                         _config.hmdOffset;
			float distance = -(new Plane(forward, targetPosition)).GetDistanceToPoint(offset.position);
			return Mathf.Clamp(distance, _config.near + Epsilon, _config.far - Epsilon);
		}

		/// <summary>
		/// Initialize the <see cref="Camera"/> that will be used to render the scene.
		/// </summary>
		private void InitializeRenderCamera()
		{
			// If we had a camera before, destroy the game object
			if (_renderCamera != null)
			{
				Destroy(_renderCamera.gameObject);
			}

			// Make sure this camera and the attached camera have the same parent
			transform.parent = attachedCamera.transform.parent;
			
			// Clone the attached camera and make it a child of this game object
			attachedCamera.enabled = false;
			GameObject renderCameraGameObject = Instantiate(attachedCamera.gameObject);
			attachedCamera.enabled = true;
			renderCameraGameObject.name = CameraGameObjectName;

			_renderCamera = renderCameraGameObject.GetComponent<Camera>();
			PrepareRenderCamera(_renderCamera);
			
			Transform cameraOffset = renderCameraGameObject.transform;
			cameraOffset.parent = transform;
			cameraOffset.localPosition = new Vector3(_config.x, _config.y, _config.z);
			cameraOffset.localRotation = Quaternion.Euler(_config.rx, _config.ry, _config.rz);
			cameraOffset.localScale = Vector3.one;

			// Strip children of cloned object (AudioListener in particular).
			cameraOffset.DestroyChildren(true);

			// Setup clipping quad (using camera clip causes problems with shadows).
			_clipQuad = GameObject.CreatePrimitive(PrimitiveType.Quad);
			_clipQuad.name = ClipQuadGameObjectName;
			DestroyImmediate(_clipQuad.GetComponent<MeshCollider>());
			MeshRenderer clipRenderer = _clipQuad.GetComponent<MeshRenderer>();
			clipMaterial.color = new Color(_config.r, _config.g, _config.b, _config.a);
			clipRenderer.material = clipMaterial;
			clipRenderer.shadowCastingMode = ShadowCastingMode.Off;
			clipRenderer.receiveShadows = false;
			clipRenderer.lightProbeUsage = LightProbeUsage.Off;
			clipRenderer.reflectionProbeUsage = ReflectionProbeUsage.Off;

			Transform clipTransform = _clipQuad.transform;
			clipTransform.parent = cameraOffset;
			clipTransform.localScale = _clipQuadScale;
			clipTransform.localRotation = Quaternion.identity;

			_clipQuad.SetActive(false);
		}

		/// <summary>
		/// Performs any modifications that need to be done on the newly created render <see cref="Camera"/>.
		/// </summary>
		/// <param name="newCamera">The newly created <see cref="Camera"/>.</param>
		protected virtual void PrepareRenderCamera(Camera newCamera)
		{
			newCamera.stereoTargetEye = StereoTargetEyeMask.None;
			newCamera.fieldOfView = _config.fov;
			newCamera.useOcclusionCulling = false;
			newCamera.enabled = false; // manually rendered
			newCamera.rect = _fixCameraRect;
		}
	}
}