﻿// <copyright file="MathUtilities.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Contains useful utility mathematics utility methods.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Math
{
    using System;

    using UnityEngine;

    /// <summary>
    /// Provides helper mathematics methods
    /// </summary>
    public static class MathUtilities
    {
        /// <summary>
        /// Format a floating point number to given number of significant digits.
        /// </summary>
        /// <param name="value">The value to format.</param>
        /// <param name="digits">The number of significant digits</param>
        /// <returns><paramref name="value"/> formatted to <paramref name="digits"/> number of significant digits.</returns>
        public static string FormatToSignificantDigits(float value, int digits)
        {
            if (Mathf.Abs(value - 0) < Mathf.Epsilon)
            {
                return value.ToString();
            }

            // 'normalize' to put all digits after decimal place, round, then format
            // based on http://stackoverflow.com/questions/374316/round-a-double-to-x-significant-figures/374470#374470
            int leftSideNumbers = Mathf.FloorToInt(Mathf.Log10(Mathf.Abs(value))) + 1;
            decimal scale = (decimal) Mathf.Pow(10, leftSideNumbers);
            return ((double) (scale * Math.Round((decimal) value / scale, digits, MidpointRounding.AwayFromZero)))
                .ToString();
        }

        /// <summary>
        /// Ping pong a value between 0 and <paramref name="max"/> (0 inclusive, <paramref name="max"/> exclusive).
        /// </summary>
        /// <param name="value">The current value.</param>
        /// <param name="max">The maximum value.</param>
        /// <returns>The updated value.</returns>
        public static int PingPongInt(int value, int max)
        {
            if (max == 1)
            {
                return 0;
            }

            // ping pong
            int exclusiveLength = max - 1;
            int ponged = value % exclusiveLength;
            if ((value / exclusiveLength) % 2 == 1) // if bouncing back, count down from length
            {
                ponged = exclusiveLength - ponged;
            }

            return ponged;
        }

        /// <summary>
        /// Computes the square distance of two points.
        /// </summary>
        /// <param name="x1">The first X component.</param>
        /// <param name="y1">The first Y component.</param>
        /// <param name="x2">The second X component.</param>
        /// <param name="y2">The second Y component.</param>
        /// <returns>The square distance between the two points.</returns>
        public static float SquareDistance(float x1, float y1, float x2, float y2)
        {
            return Mathf.Pow(x1 - x2, 2) + Mathf.Pow(y1 - y2, 2);
        }

        /// <summary>
        /// Re-maps a number from an existing range to another range. This value can be outside the range.
        /// </summary>
        /// <param name="value">The value to re-map.</param>
        /// <param name="low1">The low end value of the original range.</param>
        /// <param name="high1">The high end value of the original range.</param>
        /// <param name="low2">The low end value of the new range.</param>
        /// <param name="high2">The high end value of the new range.</param>
        /// <returns><paramref name="value"/> re-mapped to the new range.</returns>
        public static float RemapNumber(float value, float low1, float high1, float low2, float high2)
        {
            return low2 + (value - low1) * (high2 - low2) / (high1 - low1);
        }

        /// <summary>
        /// Re-maps a number from an existing range to another range and then clamps it to be inside the new range.
        /// </summary>
        /// <param name="value">The value to re-map.</param>
        /// <param name="low1">The low end value of the original range.</param>
        /// <param name="high1">The high end value of the original range.</param>
        /// <param name="low2">The low end value of the new range.</param>
        /// <param name="high2">The high end value of the new range.</param>
        /// <returns><paramref name="value"/> re-mapped to the new range and clamped inside the new range.</returns>
        public static float RemapNumberClamped(float value, float low1, float high1, float low2, float high2)
        {
            return Mathf.Clamp(RemapNumber(value, low1, high1, low2, high2), Mathf.Min(low2, high2),
                Mathf.Max(low2, high2));
        }
    }
}
