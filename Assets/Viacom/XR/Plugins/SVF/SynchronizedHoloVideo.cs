// <copyright file="SynchronizedHoloVideo.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents HoloCap video that is synchronized to a specified value.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Plugins.SVF
{
    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    /// Represents the method that will handle the <see cref="SynchronizedHoloVideo.Finished"/> event of the <see cref="SynchronizedHoloVideo"/> class.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    public delegate void SynchronizedHoloVideoFinishedHandler(SynchronizedHoloVideo sender);

    /// <summary>
    /// Represents HoloCap video that is synchronized to a specified value.
    /// </summary>
    [DisallowMultipleComponent]
    public class SynchronizedHoloVideo : HoloVideoObject
    {
        [SerializeField] private float targetFrameRate = 30f;
        [SerializeField] private UnityEvent onFinished;
        
        private Material _material;
        private float _materialAlpha = 1f;
        private readonly PidController _pidController = new PidController(GainProportional, GainIntegral,
            GainDerivative, MaxFrameDelta, -MaxFrameDelta);
        
        //Index of the material we are going to fade
        private const int MaterialIndex = 0;
        //Key of the property we are changing on the material (We want to change the color's alpha value)
        private const string MaterialPropertyKey = "_Color";
        private const float GainProportional = 0.05f;
        private const float GainIntegral = 2.0f;
        private const float GainDerivative = 0.01f;
        private const float MaxFrameDelta = 9.0f;
        
        /// <summary>
        /// Occurs when the <see cref="SynchronizedHoloVideo"/> is finished playing.
        /// </summary>
        public event SynchronizedHoloVideoFinishedHandler Finished;
        
        /// <summary>
        /// Gets or sets the value that the <see cref="SynchronizedHoloVideo"/> will synchronize to. Should be clamped between 0 and 1.
        /// </summary>
        public float SynchronizationValue
        {
            get;
            set;
        }
        
        /// <summary>
        /// Gets or sets the alpha value for the first <see cref="Material"/> on this <see cref="SynchronizedHoloVideo"/>.
        /// </summary>
        public float MaterialAlpha
        {
            get
            {
                return _materialAlpha;
            }
            set
            {
                _materialAlpha = value;
                SetMaterialAlpha(_materialAlpha);
            }
        }

        /// <summary>
        /// Sets the alpha value for the first <see cref="Material"/> on this <see cref="SynchronizedHoloVideo"/> to the specified value.
        /// </summary>
        /// <param name="alpha">The desired alpha value.</param>
        private void SetMaterialAlpha(float alpha)
        {
            if (Application.isPlaying)
            {
                _material = GetComponent<MeshRenderer>().materials[MaterialIndex];
                Color newColor = _material.color;
                newColor.a = alpha;
                _material.SetColor(MaterialPropertyKey, newColor);
            }
            
        }

        /// <inheritdoc cref="MonoBehaviour.LateUpdate"/>
        protected void LateUpdate()
        {
            if (isInitialized && GetCurrentState() == SVFPlaybackState.Playing)
            {
                if (lastFrameInfo.frameId == fileInfo.frameCount && !Settings.AutoLooping &&
                    (int) _pidController.ProcessVariable != lastFrameInfo.frameId)
                {
                    if (Finished != null)
                    {
                        Finished(this);
                    }

                    if (onFinished != null)
                    {
                        onFinished.Invoke();
                    }
                }

                _pidController.SetPoint = SynchronizationValue * targetFrameRate;
                _pidController.ProcessVariable = (int) lastFrameInfo.frameId;

                if (_pidController.SetPoint - _pidController.ProcessVariable != 0)
                {
                    float delta = _pidController.ControlVariable(Time.deltaTime);
                    ClockScale = (_pidController.SetPoint + 1 + delta) / (_pidController.SetPoint + 1);
                }
            }
        }
        
        /// <inheritdoc cref="MonoBehaviour.OnApplicationPause"/>
        protected override void OnApplicationPause(bool pause)
        {
            if (pause)
            {
                Pause();
            }
            else
            {
                if (IsPlaying)
                {
                    // Play automatically enables the meshRenderer so we save it and set it back to what it was
                    MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
                    bool enabled = meshRenderer.enabled;
                    Play();
                    if (enabled == false || Time.timeScale == 0)
                    {
                        Pause();
                    }
                    meshRenderer.enabled = enabled;
                }
            }
        }

        /// <summary>
        /// Opens the <see cref="HoloVideoObject"/> if not already open and plays it.
        /// </summary>
        public void OpenAndPlay()
        {
            if (!isInitialized)
            {
                //Force open
                Open(Url);
            }

            Play();
        }

        /// <summary>
        /// Represents a (P)roportional, (I)ntegral, (D)erivative controller.
        /// </summary>
        /// <see cref="https://en.wikipedia.org/wiki/PID_controller"/>
        private sealed class PidController
        {
            private float _processVariable = 0;

            /// <summary>
            /// Gets or sets the derivative term which is proportional to the rate of change of the error
            /// </summary>
            public float GainDerivative { get; set; }

            /// <summary>
            /// Gets or sets the integral term which is proportional to both the magnitude of the error and the
            /// duration of the error
            /// </summary>
            public float GainIntegral { get; set; }

            /// <summary>
            /// Gets or sets the proportional term which produces an output value that is proportional to the current error value.
            /// </summary>
            /// <remarks>
            /// Tuning theory and industrial practice indicate that the
            /// proportional term should contribute the bulk of the output change.
            /// </remarks>
            public float GainProportional { get; set; }

            /// <summary>
            /// The maximum output value the control device can accept.
            /// </summary>
            public float OutputMax { get; private set; }

            /// <summary>
            /// The minimum output value the control device can accept.
            /// </summary>
            public float OutputMin { get; private set; }

            /// <summary>
            /// Gets the current value.
            /// </summary>
            public float ProcessVariable
            {
                get { return _processVariable; }
                set
                {
                    ProcessVariableLast = _processVariable;
                    _processVariable = value;
                }
            }

            /// <summary>
            /// Gets the last reported value. 
            /// </summary>
            /// <remarks>
            /// This is used to calculate the rate of change.
            /// </remarks>
            public float ProcessVariableLast { get; private set; }

            /// <summary>
            /// Gets or sets the desired value.
            /// </summary>
            public float SetPoint { get; set; }

            /// <summary>
            /// Gets the adjustment made by considering the accumulated error over time.
            /// </summary>
            /// <remarks>
            /// An alternative formulation of the integral action, is the
            /// proportional-summation-difference used in discrete-time systems.
            /// </remarks>
            public float IntegralTerm { get; private set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="PidController"/> class with the specified values.
            /// </summary>
            /// <param name="gainProportional">The proportional term.</param>
            /// <param name="gainIntegral">The integral term.</param>
            /// <param name="gainDerivative">The derivative term.</param>
            /// <param name="outputMax">The maximum output value the control device can accept.</param>
            /// <param name="outputMin">The minimum output value the control device can accept.</param>
            public PidController(float gainProportional, float gainIntegral, float gainDerivative, float outputMax,
                float outputMin)
            {
                ProcessVariableLast = 0;
                GainDerivative = gainDerivative;
                GainIntegral = gainIntegral;
                GainProportional = gainProportional;
                OutputMax = outputMax;
                OutputMin = outputMin;
            }

            /// <summary>
            /// Gets the controller output.
            /// </summary>
            /// <param name="secondsSinceLastUpdate">The number of elapsed seconds since the previous time that
            /// <see cref="PidController.ControlVariable"/> was called.</param>
            /// <returns>The value of the variable that needs to be controlled.</returns>
            public float ControlVariable(float secondsSinceLastUpdate)
            {
                float error = SetPoint - ProcessVariable;

                // integral term calculation
                IntegralTerm += GainIntegral * error * secondsSinceLastUpdate;
                IntegralTerm = Mathf.Clamp(IntegralTerm, OutputMin, OutputMax);

                // derivative term calculation
                float dInput = _processVariable - ProcessVariableLast;
                float derivativeTerm = GainDerivative * (dInput / secondsSinceLastUpdate);

                // proportional term calculation
                float proportionalTerm = GainProportional * error;

                float output = proportionalTerm + IntegralTerm - derivativeTerm;

                output = Mathf.Clamp(output, OutputMin, OutputMax);

                return output;
            }
        }
    }
}

