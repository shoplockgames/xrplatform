// <copyright file="HoloVideoBehaviour.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a PlayableBehaviour that controls a SynchronizedHoloVideo.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Plugins.SVF.Timeline
{
    using System;

    using UnityEngine;
    using UnityEngine.Playables;

    using Viacom.XR.Diagnostics;
    using Viacom.XR.Plugins.SVF;

    /// <summary>
    /// Represents a <see cref="PlayableBehaviour"/> that controls a <see cref="SynchronizedHoloVideo"/>.
    /// </summary>
    [Serializable]
    public class HoloVideoBehaviour : PlayableBehaviour
    {
        [SerializeField] private bool loop;

        private SynchronizedHoloVideo _trackBinding;

        /// <inheritdoc cref="PlayableBehaviour.ProcessFrame"/>
        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            if (Application.isPlaying)
            {
                SynchronizedHoloVideo currentBinding = playerData as SynchronizedHoloVideo;
                if (_trackBinding == null)
                {
                    _trackBinding = currentBinding;
                    _trackBinding.Finished -= OnHoloVideoFinished;
                    _trackBinding.Finished += OnHoloVideoFinished;
                    _trackBinding.Stop();
                    _trackBinding.OpenAndPlay();
                }
                else
                {
                    if (_trackBinding != currentBinding)
                    {
                        _trackBinding.Stop();
                        ErrorReporter.LogHandledException(new Exception("ViacomHoloVideoObject binding changed during play, stopping previous hvo"));
                        return;
                    }

                    //Set the sync value to be equal to our current spot in the track
                    _trackBinding.SynchronizationValue = (float)playable.GetTime();
                }
            }
        }

        /// <summary>
        /// Function listening to when the <see cref="SynchronizedHoloVideo"/> finishes playing and restarts it if loop is enabled.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        private void OnHoloVideoFinished(object sender)
        {
            if (loop && Application.isPlaying)
            {
                _trackBinding.Rewind();
                _trackBinding.Play();
            }
        }

        /// <inheritdoc cref="PlayableBehaviour.OnBehaviourPlay"/>
        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
            if (_trackBinding != null)
            {
                _trackBinding.Finished -= OnHoloVideoFinished;
            }
            //Signal that we need to play this again
            _trackBinding = null;
        }

        /// <inheritdoc cref="PlayableBehaviour.OnBehaviourPause"/>
        public override void OnBehaviourPause(Playable playable, FrameData info)
        {
            if (_trackBinding != null && Application.isPlaying)
            {
                _trackBinding.Pause();
            }
        }
    }
}

#if UNITY_EDITOR

//Property Drawer
namespace Viacom.XR.Plugins.SVF.Timeline.Internal
{
    using UnityEngine;   
#if UNITY_EDITOR
    using UnityEditor;
#endif

    using Viacom.XR.Plugins.SVF.Timeline;

    /// <summary>
    /// Represents a custom drawer for the <see cref="HoloVideoBehaviour"/>.
    /// </summary>
    [CustomPropertyDrawer(typeof(HoloVideoBehaviour))]
    public class HoloVideoDrawer : PropertyDrawer
    {
        private const string LoopPropString = "loop";

        /// <inheritdoc cref="PropertyDrawer.GetPropertyHeight"/>
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            int fieldCount = 1;
            return fieldCount * EditorGUIUtility.singleLineHeight;
        }

        /// <inheritdoc cref="PropertyDrawer.OnGUI"/>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty loopProp = property.FindPropertyRelative(LoopPropString);

            Rect singleFieldRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
            EditorGUI.PropertyField(singleFieldRect, loopProp);
        }
    }
}

#endif