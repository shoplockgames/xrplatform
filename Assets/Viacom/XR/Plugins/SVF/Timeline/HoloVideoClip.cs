// <copyright file="HoloVideoClip.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a timeline clip that is bound to a HoloVideoTrack that has the HoloVideoBehaviour.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Plugins.SVF.Timeline
{
    using System;

    using UnityEngine;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    /// <summary>
    /// Represents a timeline clip that is bound to a <see cref="HoloVideoTrack"/> that has the <see cref="HoloVideoBehaviour"/>.
    /// </summary>
    [Serializable]
    public class HoloVideoClip : PlayableAsset, ITimelineClipAsset
    {
        [SerializeField] private HoloVideoBehaviour template = new HoloVideoBehaviour();

        /// <inheritdoc cref="ITimelineClipAsset.clipCaps"/>
        public ClipCaps clipCaps
        {
            get { return ClipCaps.Blending; }
        }

        /// <inheritdoc cref="PlayableAsset.CreatePlayable"/>
        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            return ScriptPlayable<HoloVideoBehaviour>.Create(graph, template);
        }
    }
}
