// <copyright file="HoloVideoMixerBehaviour.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a PlayableBehaviour that manages the mixing of clips/weights.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Plugins.SVF.Timeline
{
    using UnityEngine;
    using UnityEngine.Playables;

    using Viacom.XR.Plugins.SVF;

    /// <summary>
    /// Represents a <see cref="PlayableBehaviour"/> that manages the opacity of a <see cref="SynchronizedHoloVideo"/> via clip weights.
    /// </summary>
    public class HoloVideoMixerBehaviour : PlayableBehaviour
    {
        private SynchronizedHoloVideo _trackBinding;

        /// <inheritdoc cref="PlayableBehaviour.ProcessFrame"/>
        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            _trackBinding = playerData as SynchronizedHoloVideo;

            if (_trackBinding == null)
            {
                return;
            }
                
            int inputCount = playable.GetInputCount();
            float highestWeight = 0f;

            for (int i = 0; i < inputCount; i++)
            {
                float inputWeight = playable.GetInputWeight(i);

                //There can be multiple clips on the same track so this sets the alpha to whatever the highest
                // input weight is currently on the play head.
                if (inputWeight > highestWeight)
                {
                    highestWeight = inputWeight;
                }
            }
            
            //Only play if playing
            if (Application.isPlaying && _trackBinding != null)
            {
                _trackBinding.MaterialAlpha = highestWeight;
            }
        }
    }
}
