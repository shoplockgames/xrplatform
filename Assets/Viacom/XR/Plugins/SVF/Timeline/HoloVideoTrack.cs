// <copyright file="HoloVideoTrack.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a TrackAsset that interacts with a SynchronizedHoloVideo.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Plugins.SVF.Timeline
{
    using UnityEngine;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    using Viacom.XR.Plugins.SVF;

    /// <summary>
    /// Represents a <see cref="TrackAsset"/> that interacts with an <see cref="SynchronizedHoloVideo"/>.
    /// </summary>
    [TrackColor(0.1019608f, 0.7176471f, 0.9176471f)]
    [TrackClipType(typeof(HoloVideoClip))]
    [TrackBindingType(typeof(SynchronizedHoloVideo))]
    public class HoloVideoTrack : TrackAsset
    {
        /// <inheritdoc cref="TrackAsset.CreateTrackMixer"/>
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            return ScriptPlayable<HoloVideoMixerBehaviour>.Create(graph, inputCount);
        }
    }
}
