﻿// <copyright file="IJsonifiable.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an object that can be represented as a JSON string.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Serialization
{
    using UnityEngine;

    /// <summary>
    /// Represents an object that can be represented as a JSON string.
    /// </summary>
    public interface IJsonifiable
    {
        bool IsValid { get; }
    }

    /// <summary>
    /// Contains extension methods that enable converting Jsonifiable objects to JSON strings.
    /// </summary>
    public static class JsonifiableExtensions
    {
        /// <summary>
        /// Generates a JSON representation of the serializable fields of an object using <see cref="JsonUtility"/>.
        /// </summary>
        /// <param name="instance">The object to convert to JSON form.</param>
        /// <param name="prettyPrint">If true, formats the output for readability. If false, formats the output for minimum size. Default is false.</param>
        /// <returns>The object's data in JSON format.</returns>
        public static string ToJson(this IJsonifiable instance, bool prettyPrint = false)
        {
            return JsonUtility.ToJson(instance, prettyPrint);
        }
    }
}
