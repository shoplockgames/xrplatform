﻿// <copyright file="JsonObject.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// When overridden in a derived form, provides functionality for objects to be serialized from json.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Serialization
{
    using UnityEngine;

    /// <summary>
    /// Represents an object that can be represented as a JSON string.
    /// </summary>
    public abstract class JsonObject : IJsonifiable
    {
        /// <summary>
        /// Deserializes a JsonObject from a JSON string.
        /// </summary>
        /// <typeparam name="T">The type of the object to be returned, constrained to <see cref="IJsonifiable"/>.</typeparam>
        /// <param name="jsonString">The JSON string to deserialize.</param>
        /// <returns>The deserialized object.</returns>
        public static T FromJson<T>(string jsonString) where T : IJsonifiable
        {
            T newObject = JsonUtility.FromJson<T>(jsonString);

            if (!newObject.IsValid)
            {
                throw new MalformedJsonException(string.Format("Json Object has failed validation: {0}", newObject),
                    jsonString);
            }

            return newObject;
        }

        /// <summary>
        /// Gets whether or not the object is considered to be in a valid state.
        /// </summary>
        public abstract bool IsValid { get; }
    }
}