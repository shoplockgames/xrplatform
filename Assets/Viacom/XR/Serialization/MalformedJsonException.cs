﻿// <copyright file="MalformedJsonException.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// The exception that is thrown when an attempt serialize an object using <see cref="JsonUtility"/> fails.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Serialization
{
    using System;

    using UnityEngine;
    
    /// <summary>
    /// The exception that is thrown when an attempt serialize an object using <see cref="JsonUtility"/> fails.
    /// </summary>
    public sealed class MalformedJsonException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MalformedJsonException"/> class.
        /// </summary>
        public MalformedJsonException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MalformedJsonException"/> class with a specified error message.
        /// </summary>
        /// <param name="message">A description of the error.</param>
        public MalformedJsonException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MalformedJsonException"/> class with a specified error message and the JSON that could not be parsed.
        /// </summary>
        /// <param name="message">A description of the error.</param>
        /// <param name="json">The JSON that could not be parsed.</param>
        public MalformedJsonException(string message, string json) : base(message)
        {
            Data["json"] = json;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MalformedJsonException"/> class with a specified error message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">A description of the error.</param>
        /// <param name="inner">The exception that is the cause of the current exception.</param>
        public MalformedJsonException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
