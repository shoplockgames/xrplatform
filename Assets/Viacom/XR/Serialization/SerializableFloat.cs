﻿// <copyright file="SerializableFloat.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a single-precision floating-point number that can be serialized to a string by <see cref="JsonUtility"/>.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Serialization
{
    using System;

    using UnityEngine;

    /// <summary>
    /// Represents a single-precision floating-point number that can be serialized to a string by <see cref="JsonUtility"/>.
    /// </summary>
    [Serializable]
    public struct SerializableFloat
    {
        [SerializeField]
        private float value;

        /// <summary>
        /// Gets the value of the current <see cref="SerializableFloat"/> object.
        /// </summary>
        public float Value
        {
            get { return value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializableFloat"/> structure to the specified value.
        /// </summary>
        /// <param name="value">An integer value.</param>
        public SerializableFloat(float value)
        {
            this.value = value;
        }

        /// <summary>
        /// Converts the specified <see cref="SerializableFloat"/> to a single-precision floating-point number.
        /// </summary>
        /// <param name="s">The <see cref="SerializableFloat"/> to convert.</param>
        public static implicit operator float(SerializableFloat s)
        {
            return s.Value;
        }

        /// <summary>
        /// Creates a new <see cref="SerializableFloat"/> object initialized to a specified value.
        /// </summary>
        /// <param name="value">A single-precision floating-point number.</param>
        public static implicit operator SerializableFloat(float value)
        {
            return new SerializableFloat(value);
        }
    }
}
