﻿// <copyright file="SerializableInt.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a 32-bit signed integer that can be serialized to a string by <see cref="JsonUtility"/>.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Serialization
{
    using System;

    using UnityEngine;

    /// <summary>
    /// Represents a 32-bit signed integer that can be serialized to a string by <see cref="JsonUtility"/>.
    /// </summary>
    [Serializable]
    public struct SerializableInt
    {
        [SerializeField]
        private int value;

        /// <summary>
        /// Gets the value of the current <see cref="SerializableInt"/> object.
        /// </summary>
        public int Value
        {
            get { return value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializableInt"/> structure to the specified value.
        /// </summary>
        /// <param name="value">An integer value.</param>
        public SerializableInt(int value)
        {
            this.value = value;
        }

        /// <summary>
        /// Converts the specified <see cref="SerializableInt"/> to a 32-bit signed integer.
        /// </summary>
        /// <param name="s">The <see cref="SerializableInt"/> to convert.</param>
        public static implicit operator int(SerializableInt s)
        {
            return s.Value;
        }

        /// <summary>
        /// Creates a new <see cref="SerializableInt"/> object initialized to a specified value.
        /// </summary>
        /// <param name="value">An integer value.</param>
        public static implicit operator SerializableInt(int value)
        {
            return new SerializableInt(value);
        }
    }
}
