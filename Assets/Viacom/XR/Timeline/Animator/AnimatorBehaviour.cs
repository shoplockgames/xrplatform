﻿// <copyright file="AnimatorBehaviour.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a playable script that gets used on an Animator Track.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Timeline.Animator
{
    using UnityEngine.Playables;

    /// <summary>
    /// Represents a playable script that is bound to an <see cref="AnimatorTrack"/>.
    /// </summary>
    public class AnimatorBehaviour : PlayableBehaviour
    {
    }
}
