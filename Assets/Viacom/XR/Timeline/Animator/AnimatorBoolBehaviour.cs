﻿// <copyright file="AnimatorBoolBehaviour.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an AnimatorBehaviour that interacts with an Animator's bool parameter.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Timeline.Animator
{
    using System;

    using UnityEngine;
    using UnityEngine.Playables;

    using Viacom.XR.Diagnostics;

    /// <summary>
    /// Represents an <see cref="AnimatorBehaviour"/> that interacts with an <see cref="Animator"/>'s bool parameter.
    /// </summary>
    [Serializable]
    public class AnimatorBoolBehaviour : AnimatorBehaviour
    {
        [SerializeField] private string name;
        [Tooltip("The value this clip sets the bool to when it is active")]
        [SerializeField] private bool value = true;
        [SerializeField] private ResetType postPlayback;

        private enum ResetType { Revert, Hold, ForceTrue, ForceFalse };

        private Animator _trackBinding;
        private bool _previousBool;

        /// <inheritdoc cref="PlayableBehaviour.ProcessFrame"/>
        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            Animator currentBinding = playerData as Animator;
            if (_trackBinding == null)
            {
                if (currentBinding == null)
                {
                    ErrorReporter.LogHandledException(new NullReferenceException("Current track binding is null."));
                    return;
                }

                _trackBinding = currentBinding;
                _previousBool = _trackBinding.GetBool(name);
                _trackBinding.SetBool(name, value);
            }
            else
            {
                if (_trackBinding != currentBinding)
                {
                    ErrorReporter.LogHandledException(new Exception("Animator binding changed during play."));
                }
            }
        }

        /// <inheritdoc cref="PlayableBehaviour.OnBehaviourPlay"/>
        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
            _trackBinding = null;
        }

        /// <inheritdoc cref="PlayableBehaviour.OnBehaviourPause"/>
        public override void OnBehaviourPause(Playable playable, FrameData info)
        {
            if (_trackBinding != null)
            {
                switch (postPlayback)
                {
                    case ResetType.Revert:
                        _trackBinding.SetBool(name, _previousBool);
                        break;
                    case ResetType.ForceTrue:
                        _trackBinding.SetBool(name, true);
                        break;
                    case ResetType.ForceFalse:
                        _trackBinding.SetBool(name, false);
                        break;
                    case ResetType.Hold:
                        //Do nothing
                        break;
                    default:
                        break;
                }
            }
        }
    }
}

#if UNITY_EDITOR

//Property Drawer
namespace Viacom.XR.Timeline.Animator.Internal
{
    using UnityEngine;
#if UNITY_EDITOR
    using UnityEditor;
#endif

    /// <summary>
    /// Represents a custom drawer for the <see cref="AnimatorBoolBehaviour"/>.
    /// </summary>
    [CustomPropertyDrawer(typeof(AnimatorBoolBehaviour))]
    public class AnimatorBoolBehaviourDrawer : PropertyDrawer
    {
        private const string NamePropString = "name";
        private const string ValuePropString = "value";
        private const string PostPlaybackPropString = "postPlayback";

        /// <inheritdoc cref="PropertyDrawer.GetPropertyHeight"/>
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            int fieldCount = 3;
            return fieldCount * EditorGUIUtility.singleLineHeight;
        }

        /// <inheritdoc cref="PropertyDrawer.OnGUI"/>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty nameProp = property.FindPropertyRelative(NamePropString);
            SerializedProperty setValueProp = property.FindPropertyRelative(ValuePropString);
            SerializedProperty postPlayback = property.FindPropertyRelative(PostPlaybackPropString);

            Rect singleFieldRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
            EditorGUI.PropertyField(singleFieldRect, nameProp);
            Rect singleFieldRect1 = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight, position.width, EditorGUIUtility.singleLineHeight);
            EditorGUI.PropertyField(singleFieldRect1, setValueProp);
            Rect singleFieldRect2 = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 2, position.width, EditorGUIUtility.singleLineHeight);
            EditorGUI.PropertyField(singleFieldRect2, postPlayback);
        }
    }
}

#endif