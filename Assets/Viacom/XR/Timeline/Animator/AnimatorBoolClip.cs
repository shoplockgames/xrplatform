﻿// <copyright file="AnimatorBoolClip.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a timeline clip that is bound to an AnimatorTrack that has the AnimatorBoolBehaviour.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Timeline.Animator
{
    using System;

    using UnityEngine;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    /// <summary>
    /// Represents a timeline clip that is bound to an <see cref="AnimatorTrack"/> that has the <see cref="AnimatorBoolBehaviour"/>.
    /// </summary>
    [Serializable]
    public class AnimatorBoolClip : PlayableAsset, ITimelineClipAsset
    {
        [SerializeField] private AnimatorBoolBehaviour template = new AnimatorBoolBehaviour();

        /// <inheritdoc cref="ITimelineClipAsset.clipCaps"/>
        public ClipCaps clipCaps
        {
            get { return ClipCaps.ClipIn | ClipCaps.SpeedMultiplier; }
        }

        /// <inheritdoc cref="PlayableAsset.CreatePlayable"/>
        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            return ScriptPlayable<AnimatorBoolBehaviour>.Create(graph, template);
        }
    }
}
