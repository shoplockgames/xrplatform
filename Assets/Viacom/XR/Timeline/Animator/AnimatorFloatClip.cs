﻿// <copyright file="AnimatorFloatClip.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a timeline clip that is bound to an AnimatorTrack that has the AnimatorFloatBehaviour.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Timeline.Animator
{
    using System;

    using UnityEngine;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    /// <summary>
    /// Represents a timeline clip that is bound to an <see cref="AnimatorTrack"/> that has the <see cref="AnimatorFloatBehaviour"/>.
    /// </summary>
    [Serializable]
    public class AnimatorFloatClip : PlayableAsset, ITimelineClipAsset
    {
        [SerializeField] private AnimatorFloatBehaviour template = new AnimatorFloatBehaviour();

        /// <inheritdoc cref="ITimelineClipAsset.clipCaps"/>
        public ClipCaps clipCaps
        {
            get { return ClipCaps.ClipIn | ClipCaps.SpeedMultiplier; }
        }

        /// <inheritdoc cref="PlayableAsset.CreatePlayable"/>
        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            return ScriptPlayable<AnimatorFloatBehaviour>.Create(graph, template);
        }
    }
}
