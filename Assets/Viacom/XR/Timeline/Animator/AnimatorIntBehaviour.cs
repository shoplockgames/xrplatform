﻿// <copyright file="AnimatorIntBehaviour.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an AnimatorBehaviour that interacts with an Animator's integer parameter.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Timeline.Animator
{
    using System;

    using UnityEngine;
    using UnityEngine.Playables;

    using Viacom.XR.Diagnostics;

    /// <summary>
    /// Represents an <see cref="AnimatorBehaviour"/> that interacts with an <see cref="Animator"/>'s integer parameter.
    /// </summary>
    [Serializable]
    public class AnimatorIntBehaviour : AnimatorBehaviour
    {
        [SerializeField] private string name;
        [SerializeField] private int value;
        [SerializeField] private bool useCurve = false;
        [SerializeField] private AnimationCurve valueCurve;
        [SerializeField] private ResetType postPlayback;

        private enum ResetType { Revert, Hold };

        private Animator _trackBinding;
        private int _previousInt;

        /// <inheritdoc cref="PlayableBehaviour.ProcessFrame"/>
        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            Animator currentBinding = playerData as Animator;
            if (_trackBinding == null)
            {
                if (currentBinding == null)
                {
                    ErrorReporter.LogHandledException(new NullReferenceException("Current track binding is null."));
                    return;
                }

                _trackBinding = currentBinding;
                _previousInt = _trackBinding.GetInteger(name);
                if (!useCurve)
                {
                    _trackBinding.SetInteger(name, value);
                }
            }
            else
            {
                if (useCurve)
                {
                    float evaluationPoint = (float)(playable.GetTime() / playable.GetDuration());
                    _trackBinding.SetInteger(name, Mathf.RoundToInt(valueCurve.Evaluate(evaluationPoint)));
                }

                if (_trackBinding != currentBinding)
                {
                    ErrorReporter.LogHandledException(new Exception("Animator binding changed during play."));
                }
            }
        }

        /// <inheritdoc cref="PlayableBehaviour.OnBehaviourPlay"/>
        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
            _trackBinding = null;
        }

        /// <inheritdoc cref="PlayableBehaviour.OnBehaviourPause"/>
        public override void OnBehaviourPause(Playable playable, FrameData info)
        {
            if (_trackBinding != null)
            {
                switch (postPlayback)
                {
                    case ResetType.Revert:
                        _trackBinding.SetInteger(name, _previousInt);
                        break;
                    case ResetType.Hold:
                        //Do nothing
                        break;
                    default:
                        break;
                }
            }
        }
    }
}

#if UNITY_EDITOR

//Property Drawer
namespace Viacom.XR.Timeline.Animator.Internal
{
    using UnityEngine;
#if UNITY_EDITOR
    using UnityEditor;
#endif

    /// <summary>
    /// Represents a custom drawer for the <see cref="AnimatorIntBehaviour"/>
    /// </summary>
    [CustomPropertyDrawer(typeof(AnimatorIntBehaviour))]
    public class AnimatorIntBehaviourDrawer : PropertyDrawer
    {
        private const string NamePropString = "name";
        private const string ValuePropString = "value";
        private const string UseCurvePropString = "useCurve";
        private const string ValueCurvePropString = "valueCurve";
        private const string PostPlaybackPropString = "postPlayback";

        /// <inheritdoc cref="PropertyDrawer.GetPropertyHeight"/>
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            int fieldCount = 5;
            return fieldCount * EditorGUIUtility.singleLineHeight;
        }

        /// <inheritdoc cref="PropertyDrawer.OnGUI"/>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty name = property.FindPropertyRelative(NamePropString);
            SerializedProperty value = property.FindPropertyRelative(ValuePropString);
            SerializedProperty useCurve = property.FindPropertyRelative(UseCurvePropString);
            SerializedProperty valueCurve = property.FindPropertyRelative(ValueCurvePropString);
            SerializedProperty postPlayback = property.FindPropertyRelative(PostPlaybackPropString);

            Rect singleFieldRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
            EditorGUI.PropertyField(singleFieldRect, name);
            Rect singleFieldRect1 = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight, position.width, EditorGUIUtility.singleLineHeight);
            EditorGUI.PropertyField(singleFieldRect1, value);
            Rect singleFieldRect2 = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 2, position.width, EditorGUIUtility.singleLineHeight);
            EditorGUI.PropertyField(singleFieldRect2, useCurve);
            Rect singleFieldRect3 = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 3, position.width, EditorGUIUtility.singleLineHeight);
            EditorGUI.PropertyField(singleFieldRect3, valueCurve);
            Rect singleFieldRect4 = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * 4, position.width, EditorGUIUtility.singleLineHeight);
            EditorGUI.PropertyField(singleFieldRect4, postPlayback);
        }
    }
}

#endif