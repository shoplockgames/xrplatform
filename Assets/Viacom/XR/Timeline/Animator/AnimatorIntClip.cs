﻿// <copyright file="AnimatorIntClip.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a timeline clip that is bound to an AnimatorTrack that has the AnimatorIntBehaviour.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Timeline.Animator
{
    using System;

    using UnityEngine;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    /// <summary>
    /// Represents a timeline clip that is bound to an <see cref="AnimatorTrack"/> that has the <see cref="AnimatorIntBehaviour"/>.
    /// </summary>
    [Serializable]
    public class AnimatorIntClip : PlayableAsset, ITimelineClipAsset
    {
        [SerializeField] private AnimatorIntBehaviour template = new AnimatorIntBehaviour();

        /// <inheritdoc cref="ITimelineClipAsset.clipCaps"/>
        public ClipCaps clipCaps
        {
            get { return ClipCaps.ClipIn | ClipCaps.SpeedMultiplier; }
        }

        /// <inheritdoc cref="PlayableAsset.CreatePlayable"/>
        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            return ScriptPlayable<AnimatorIntBehaviour>.Create(graph, template);
        }
    }
}
