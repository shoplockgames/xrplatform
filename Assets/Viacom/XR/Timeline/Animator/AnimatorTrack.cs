// <copyright file="AnimatorTrack.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a TrackAsset that interacts with an Animator.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Timeline.Animator
{
    using UnityEngine;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    /// <summary>
    /// Represents a <see cref="TrackAsset"/> that interacts with an <see cref="Animator"/>.
    /// </summary>
    [TrackColor(0.1019608f, 0.7176471f, 0.9176471f)]
    [TrackClipType(typeof(AnimatorTriggerClip))]
    [TrackClipType(typeof(AnimatorBoolClip))]
    [TrackClipType(typeof(AnimatorFloatClip))]
    [TrackClipType(typeof(AnimatorIntClip))]
    [TrackBindingType(typeof(Animator))]
    public class AnimatorTrack : TrackAsset
    {
        /// <inheritdoc cref="TrackAsset.CreateTrackMixer"/>
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            return ScriptPlayable<AnimatorMixerBehaviour>.Create(graph, inputCount);
        }
    }
}
