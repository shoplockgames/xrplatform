// <copyright file="AnimatorTriggerBehaviour.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents an AnimatorBehaviour that interacts with an Animator's trigger parameter.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Timeline.Animator
{
    using System;

    using UnityEngine;
    using UnityEngine.Playables;

    using Viacom.XR.Diagnostics;

    /// <summary>
    /// Represents an <see cref="AnimatorBehaviour"/> that interacts with an <see cref="Animator"/>'s trigger parameter.
    /// </summary>
    [Serializable]
    public class AnimatorTriggerBehaviour : AnimatorBehaviour
    {
        [SerializeField] private string name;
        [SerializeField] private bool reset;

        private Animator _trackBinding;

        /// <inheritdoc cref="PlayableBehaviour.ProcessFrame"/>
        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            Animator currentBinding = playerData as Animator;
            if (_trackBinding == null)
            {
                if (currentBinding == null)
                {
                    ErrorReporter.LogHandledException(new NullReferenceException("Current track binding is null."));
                    return;
                }

                _trackBinding = currentBinding;
                _trackBinding.SetTrigger(name);
            }
            else
            {
                if (_trackBinding != currentBinding)
                {
                    ErrorReporter.LogHandledException(new Exception("Animator binding changed during play."));
                }
            }
        }

        /// <inheritdoc cref="PlayableBehaviour.OnBehaviourPlay"/>
        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
            //Signal to processFrame that we need to set the trigger;
            _trackBinding = null;
        }

        /// <inheritdoc cref="PlayableBehaviour.OnBehaviourPause"/>
        public override void OnBehaviourPause(Playable playable, FrameData info)
        {
            if (_trackBinding != null && reset)
            {
                _trackBinding.ResetTrigger(name);
            }
        }
    }
}

#if UNITY_EDITOR

//Property Drawer
namespace Viacom.XR.Timeline.Animator.Internal
{
    using UnityEngine;
#if UNITY_EDITOR
    using UnityEditor;
#endif

    /// <summary>
    /// Represents a custom drawer for the <see cref="AnimatorTriggerBehaviour"/>.
    /// </summary>
    [CustomPropertyDrawer(typeof(AnimatorTriggerBehaviour))]
    public class AnimatorTriggerBehaviourDrawer : PropertyDrawer
    {
        private const string NamePropString = "name";
        private const string ResetPropString = "reset";

        /// <inheritdoc cref="PropertyDrawer.GetPropertyHeight"/>
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            const int fieldCount = 2;
            return fieldCount * EditorGUIUtility.singleLineHeight;
        }

        /// <inheritdoc cref="PropertyDrawer.OnGUI"/>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty nameProp = property.FindPropertyRelative(NamePropString);
            SerializedProperty resetProp = property.FindPropertyRelative(ResetPropString);

            Rect singleFieldRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
            EditorGUI.PropertyField(singleFieldRect, nameProp);
            Rect singleFieldRect1 = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight, position.width, EditorGUIUtility.singleLineHeight);
            EditorGUI.PropertyField(singleFieldRect1, resetProp);
        }
    }
}

#endif