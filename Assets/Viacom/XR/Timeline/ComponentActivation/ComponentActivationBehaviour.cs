// <copyright file="ComponentActivationBehaviour.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a PlayableBehaviour that interacts with a GameObjects's components.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Timeline.ComponentActivation
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    using UnityEngine;
    using UnityEngine.Playables;

    using Viacom.XR.Diagnostics;

    /// <summary>
    /// Represents the enabled setting for a particular component.
    /// </summary>
    [Serializable]
    public struct ComponentSetting
    {
        public string componentType;
        public bool enabled;

        public ComponentSetting(string componentType, bool enabled)
        {
            this.componentType = componentType;
            this.enabled = enabled;
        }
    }

    /// <summary>
    /// Represents a <see cref="PlayableBehaviour"/> that interacts with a <see cref="GameObject"/>'s components.
    /// </summary>
    [Serializable]
    public class ComponentActivationBehaviour : PlayableBehaviour
    {
        [SerializeField] private ResetType postPlayback;
        [SerializeField] private ComponentSetting[] componentSettings = new ComponentSetting[] { };

        private enum ResetType { Revert, Hold, ForceTrue, ForceFalse };
        private GameObject _trackBinding;
        private readonly List<ComponentSetting> _previousSettings = new List<ComponentSetting>();
        private const string EnabledPropertyName = "enabled";

        /// <inheritdoc cref="PlayableBehaviour.ProcessFrame"/>
        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            GameObject currentBinding = playerData as GameObject;
            if (_trackBinding == null)
            {
                if (currentBinding == null)
                {
                    ErrorReporter.LogHandledException(new NullReferenceException("Current track binding is null."));
                    return;
                }

                _trackBinding = currentBinding;
                foreach (ComponentSetting setting in componentSettings)
                {
                    SetComponentEnabled(setting.componentType, setting.enabled, true);
                }
            }
            else
            {
                if (_trackBinding != currentBinding)
                {
                    ErrorReporter.LogHandledException(new Exception("Gameobject binding changed during play."));
                }
            }
        }

        /// <inheritdoc cref="PlayableBehaviour.OnBehaviourPlay"/>
        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
            //Signal to process frame to go through components
            _trackBinding = null;
            _previousSettings.Clear();
        }

        /// <inheritdoc cref="PlayableBehaviour.OnBehaviourPause"/>
        public override void OnBehaviourPause(Playable playable, FrameData info)
        {
            if (_trackBinding != null)
            {
                switch (postPlayback)
                {
                    case ResetType.Revert:
                        RevertComponentSettings();
                        break;
                    case ResetType.Hold:
                        //Do nothing
                        break;
                    case ResetType.ForceTrue:
                        ForceComponentSettings(true);
                        break;
                    case ResetType.ForceFalse:
                        ForceComponentSettings(false);
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// This function resets all of the components specified in _previousSettings, back to enabled/disabled state before this clip.
        /// </summary>
        private void RevertComponentSettings()
        {
            foreach (ComponentSetting setting in _previousSettings)
            {
                SetComponentEnabled(setting.componentType, setting.enabled);
            }
        }

        /// <summary>
        /// This function sets all the components specified on this clip and enables/disables them.
        /// </summary>
        /// <param name="enabled">Whether the components should be enabled or disabled.</param>
        private void ForceComponentSettings(bool enabled)
        {
            foreach (ComponentSetting setting in componentSettings)
            {
                SetComponentEnabled(setting.componentType, enabled);
            }
        }

        /// <summary>
        /// This function attempts to enable/disable the component specified by componentType.
        /// </summary>
        /// <param name="componentType">The <see cref="Type"/> of component that you want to control.</param>
        /// <param name="enabled">Whether this component should be enabled or disabled.</param>
        /// <param name="saveValue">Whether this component's enabled property should be saved for reverting later.</param>
        private void SetComponentEnabled(string componentType, bool enabled, bool saveValue = false)
        {
            Component component = _trackBinding.GetComponent(componentType);
            if (component != null)
            {
                PropertyInfo propInfo = component.GetType().GetProperty(EnabledPropertyName, BindingFlags.Public | BindingFlags.Instance);
                if (propInfo != null && propInfo.CanWrite && propInfo.CanRead)
                {
                    //This is a valid component with an enabled property so save it.
                    if (saveValue && postPlayback == ResetType.Revert)
                    {
                        _previousSettings.Add(new ComponentSetting(componentType, (bool)propInfo.GetValue(component, null)));
                    }

                    propInfo.SetValue(component, enabled, null);
                }
                else
                {
                    ErrorReporter.LogHandledException(new Exception("Could not find property: " + EnabledPropertyName + " or the property is not set for read and write."));
                }
            }
            else
            {
                ErrorReporter.LogHandledException(new Exception("Could not find component of type " + componentType + " on " + _trackBinding.name));
            }
        }
    }
}