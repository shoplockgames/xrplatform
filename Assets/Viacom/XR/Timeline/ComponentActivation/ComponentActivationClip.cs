// <copyright file="ComponentActivationClip.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a timeline clip that is bound to a ComponentActivationTrack that has the ComponentActivationBehaviour.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Timeline.ComponentActivation
{
    using System;

    using UnityEngine;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    /// <summary>
    /// Represents a timeline clip that is bound to a <see cref="ComponentActivationTrack"/> that has the <see cref="ComponentActivationBehaviour"/>.
    /// </summary>
    [Serializable]
    public class ComponentActivationClip : PlayableAsset, ITimelineClipAsset
    {
        [SerializeField] private ComponentActivationBehaviour template = new ComponentActivationBehaviour();

        /// <inheritdoc cref="ITimelineClipAsset.clipCaps"/>
        public ClipCaps clipCaps
        {
            get { return ClipCaps.ClipIn; }
        }

        /// <inheritdoc cref="PlayableAsset.CreatePlayable"/>
        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            return ScriptPlayable<ComponentActivationBehaviour>.Create(graph, template);
        }
    }
}