// <copyright file="ComponentActivationMixerBehaviour.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a PlayableBehaviour that manages the mixing of clips/weights. 
// </summary>
//
// <remarks>
// Although this script doesn't do anything it's required by Unity.
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Timeline.ComponentActivation
{
    using UnityEngine.Playables;

    /// <summary>
    /// Represents a <see cref="PlayableBehaviour"/> that manages the mixing of clips/weights on a <see cref="ComponentActivationTrack"/>.
    /// </summary>
    public class ComponentActivationMixerBehaviour : PlayableBehaviour
    {
    }
}
