// <copyright file="ComponentActivationTrack.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a TrackAsset that interacts with an GameObject and controls it's components.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Timeline.ComponentActivation
{
    using UnityEngine;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    /// <summary>
    /// Represents a <see cref="TrackAsset"/> that interacts with an <see cref="GameObject"/> and controls it's components.
    /// </summary>
    [TrackColor(0.1019608f, 0.7176471f, 0.9176471f)]
    [TrackClipType(typeof(ComponentActivationClip))]
    [TrackBindingType(typeof(GameObject))]
    public class ComponentActivationTrack : TrackAsset
    {
        /// <inheritdoc cref="TrackAsset.CreateTrackMixer"/>
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            return ScriptPlayable<ComponentActivationMixerBehaviour>.Create(graph, inputCount);
        }
    }
}
