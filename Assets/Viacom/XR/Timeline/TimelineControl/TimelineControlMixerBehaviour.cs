// <copyright file="TimelineControlMixerBehaviour.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a PlayableBehaviour that manages the mixing of clips/weights on a TimelineControlTrack. 
// </summary>
//
// <remarks>
// Although this script doesn't do anything it's required by Unity.
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Timeline.TimelineControl
{
    using UnityEngine.Playables;

    /// <summary>
    /// Represents a <see cref="PlayableBehaviour"/> that manages the mixing of clips/weights on a <see cref="TimelineControlTrack"/>.
    /// </summary>
    public class TimelineControlMixerBehaviour : PlayableBehaviour
    {
    }
}