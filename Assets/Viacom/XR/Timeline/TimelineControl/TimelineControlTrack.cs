// <copyright file="TimelineControlTrack.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a TrackAsset that interacts with a PlayableDirector.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------


namespace Viacom.XR.Timeline.TimelineControl
{
    using UnityEngine;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    /// <summary>
    /// Represents a <see cref="TrackAsset"/> that interacts with a <see cref="PlayableDirector"/>.
    /// </summary>
    [TrackColor(0.855f, 0.8623f, 0.87f)]
    [TrackClipType(typeof(TimelinePauseClip))]
    public class TimelineControlTrack : TrackAsset
    {
        /// <inheritdoc cref="TrackAsset.CreateTrackMixer"/>
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            return ScriptPlayable<TimelineControlMixerBehaviour>.Create(graph, inputCount);
        }
    }

}