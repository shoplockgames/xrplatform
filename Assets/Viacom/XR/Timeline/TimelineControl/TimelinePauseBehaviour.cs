// <copyright file="TimelinePauseBehaviour.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a PlayableBehaviour that pauses a PlayableDirector.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Timeline.TimelineControl
{
    using System;

    using UnityEngine.Playables;

    /// <summary>
    /// Represents a <see cref="PlayableBehaviour"/> that pauses a <see cref="PlayableDirector"/>.
    /// </summary>
    [Serializable]
    public class TimelinePauseBehaviour : PlayableBehaviour
    {
        private bool _hasRun = false;

        /// <inheritdoc cref="PlayableBehaviour.OnBehaviourPlay"/>
        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
            if (_hasRun == false)
            {
                PlayableDirector playableDirector = playable.GetGraph().GetResolver() as PlayableDirector;
                playableDirector.Pause();
                _hasRun = true;
            }
        }
    }
}