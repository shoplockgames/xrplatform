// <copyright file="TimelinePauseClip.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a timeline clip that is bound to a TimelineControlTrack that has the TimelinePauseBehaviour.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Timeline.TimelineControl
{
    using System;

    using UnityEngine;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    /// <summary>
    /// Represents a timeline clip that is bound to a <see cref="TimelineControlTrack"/> that has the <see cref="TimelinePauseBehaviour"/>.
    /// </summary>
    [Serializable]
    public class TimelinePauseClip : PlayableAsset, ITimelineClipAsset
    {
        [SerializeField] private TimelinePauseBehaviour template = new TimelinePauseBehaviour();

        /// <inheritdoc cref="ITimelineClipAsset.clipCaps"/>
        public ClipCaps clipCaps
        {
            get { return ClipCaps.ClipIn; }
        }

        /// <inheritdoc cref="PlayableAsset.CreatePlayable"/>
        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            return ScriptPlayable<TimelinePauseBehaviour>.Create(graph, template);
        }
    }
}