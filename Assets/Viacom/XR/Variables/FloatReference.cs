﻿// <copyright file="FloatReference.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a single-precision floating-point number that is can be either stored in a <see cref="ScriptableObject"/>
// or use a constant value.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Variables
{
    using System;

    using UnityEngine;

    /// <summary>
    /// Represents a single-precision floating-point number that is can be either stored in a <see cref="ScriptableObject"/>
    /// or use a constant value.
    /// </summary>
    [Serializable]
    public class FloatReference
    {
        [SerializeField] private bool useConstant = true;
        [SerializeField] private float constantValue;
        [SerializeField] private FloatVariable variable;

        /// <summary>
        /// Initializes a new instance of the <see cref="FloatReference"/> class.
        /// </summary>
        public FloatReference()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FloatReference"/> class with the specified constant value.
        /// </summary>
        /// <param name="value">A single-precision floating-point number.</param>
        public FloatReference(float value)
        {
            useConstant = true;
            constantValue = value;
        }

        /// <summary>
        /// Gets the current value.
        /// </summary>
        public float Value
        {
            get { return useConstant ? constantValue : variable.Value; }
        }

        /// <summary>
        /// Converts a <see cref="FloatReference"/> to an equivalent single-precision floating-point number.
        /// </summary>
        /// <param name="reference">The <see cref="FloatReference"/> to convert.</param>
        /// <returns>A single-precision floating-point number equivalent of <paramref name="reference"/>.</returns>
        public static implicit operator float(FloatReference reference)
        {
            return reference.Value;
        }

        /// <summary>
        /// Creates a new <see cref="FloatReference"/> object initialized to a specified value.
        /// </summary>
        /// <param name="value">A single-precision floating-point number.</param>
        /// <returns>A <see cref="FloatReference"/> object whose <see cref="FloatReference.Value"/> property is initialized with <paramref name="value"/>.</returns>
        public static implicit operator FloatReference(float value)
        {
            return new FloatReference(value);
        }
    }
}

#if UNITY_EDITOR

namespace Viacom.XR.Variables.Internal
{
    using UnityEngine;
#if UNITY_EDITOR
    using UnityEditor;

#endif

    /// <summary>
    /// Represents a custom drawer for the <see cref="FloatReference"/>.
    /// </summary>
    [CustomPropertyDrawer(typeof(FloatReference))]
    public class FloatReferenceDrawer : PropertyDrawer
    {
        private GUIStyle _popupStyle;

        private readonly string[] _popupOptions = {"Use Constant", "Use Variable"};

        private const string GuiStyle = "PaneOptions";
        private const string UseConstantField = "useConstant";
        private const string ConstantValueField = "constantValue";
        private const string VariableField = "variable";

        /// <inheritdoc cref="PropertyDrawer.OnGUI"/>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (_popupStyle == null)
            {
                _popupStyle = new GUIStyle(GUI.skin.GetStyle(GuiStyle));
                _popupStyle.imagePosition = ImagePosition.ImageOnly;
            }

            label = EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, label);

            EditorGUI.BeginChangeCheck();

            // Get properties
            SerializedProperty useConstant = property.FindPropertyRelative(UseConstantField);
            SerializedProperty constantValue = property.FindPropertyRelative(ConstantValueField);
            SerializedProperty variable = property.FindPropertyRelative(VariableField);

            // Calculate rect for configuration button
            Rect buttonRect = new Rect(position);
            buttonRect.yMin += _popupStyle.margin.top;
            buttonRect.width = _popupStyle.fixedWidth + _popupStyle.margin.right;
            position.xMin = buttonRect.xMax;

            // Store old indent level and set it to 0, the PrefixLabel takes care of it
            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            int result = EditorGUI.Popup(buttonRect, useConstant.boolValue ? 0 : 1, _popupOptions, _popupStyle);

            useConstant.boolValue = result == 0;

            EditorGUI.PropertyField(position, useConstant.boolValue ? constantValue : variable, GUIContent.none);

            if (EditorGUI.EndChangeCheck())
            {
                property.serializedObject.ApplyModifiedProperties();
            }

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }
    }
}

#endif