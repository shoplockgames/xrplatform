﻿// <copyright file="FloatVariable.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a single-precision floating-point number that is stored in a <see cref="ScriptableObject"/>.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Variables
{
    using UnityEngine;

    /// <summary>
    /// Represents a single-precision floating-point number that is stored in a <see cref="ScriptableObject"/>.
    /// </summary>
    [CreateAssetMenu(menuName = "VXR/Variables/Float")]
    public class FloatVariable : ScriptableObject
    {
#if UNITY_EDITOR
#pragma warning disable 0414
        [Multiline, SerializeField] private string description = string.Empty;
#pragma warning restore 0414
#endif
        [SerializeField] private float value;

        /// <summary>
        /// Gets the underlying value.
        /// </summary>
        public float Value
        {
            get { return value; }
            set { this.value = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FloatVariable"/> class with a specified value.
        /// </summary>
        /// <param name="value">A single-precision floating-point number.</param>
        public FloatVariable(float value)
        {
            this.value = value;
        }

        /// <summary>
        /// Converts a <see cref="FloatVariable"/> to an equivalent single-precision floating-point number.
        /// </summary>
        /// <param name="variable">The <see cref="FloatVariable"/> to convert.</param>
        /// <returns>A single-precision floating-point number equivalent of <paramref name="variable"/>.</returns>
        public static implicit operator float(FloatVariable variable)
        {
            return variable.Value;
        }

        /// <summary>
        /// Creates a new <see cref="FloatVariable"/> object initialized to a specified value.
        /// </summary>
        /// <param name="value">A single-precision floating-point number.</param>
        /// <returns>A <see cref="FloatVariable"/> object whose <see cref="FloatVariable.Value"/> property is initialized with <paramref name="value"/>.</returns>
        public static implicit operator FloatVariable(float value)
        {
            FloatVariable variable = CreateInstance<FloatVariable>();
            variable.value = value;
            return variable;
        }
    }
}