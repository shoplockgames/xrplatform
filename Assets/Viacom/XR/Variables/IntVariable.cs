﻿// <copyright file="IntVariable.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a 32-bit signed integer that is stored in a <see cref="ScriptableObject"/>.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Variables
{
    using UnityEngine;

    /// <summary>
    /// Represents a 32-bit signed integer that is stored in a <see cref="ScriptableObject"/>.
    /// </summary>
    [CreateAssetMenu(menuName = "VXR/Variables/Int")]
    public class IntVariable : ScriptableObject
    {
#if UNITY_EDITOR
#pragma warning disable 0414
        [Multiline, SerializeField] private string description = string.Empty;
#pragma warning restore 0414
#endif
        [SerializeField] private int value;

        /// <summary>
        /// Gets the underlying value.
        /// </summary>
        public int Value
        {
            get { return value; }
            set { this.value = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IntVariable"/> class with a specified value.
        /// </summary>
        /// <param name="value">A 32-bit signed integer.</param>
        public IntVariable(int value)
        {
            this.value = value;
        }

        /// <summary>
        /// Converts a <see cref="IntVariable"/> to an equivalent 32-bit signed integer.
        /// </summary>
        /// <param name="variable">The <see cref="IntVariable"/> to convert.</param>
        /// <returns>A 32-bit signed integer equivalent of <paramref name="variable"/>.</returns>
        public static implicit operator int(IntVariable variable)
        {
            return variable.Value;
        }

        /// <summary>
        /// Creates a new <see cref="IntVariable"/> object initialized to a specified value.
        /// </summary>
        /// <param name="value">A 32-bit signed integer.</param>
        /// <returns>An <see cref="IntVariable"/> object whose <see cref="IntVariable.Value"/> property is initialized with <paramref name="value"/>.</returns>
        public static implicit operator IntVariable(int value)
        {
            IntVariable variable = CreateInstance<IntVariable>();
            variable.value = value;
            return variable;
        }
    }
}