﻿// <copyright file="StringReference.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a sequence of UTF-16 code units that is can be either stored in a <see cref="ScriptableObject"/> or use a constant value.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Variables
{
    using System;

    using UnityEngine;

    [Serializable]
    public class StringReference
    {
        [SerializeField] private bool useConstant = true;
        [SerializeField] private string constantValue;
        [SerializeField] private StringVariable variable;

        /// <summary>
        /// Initializes a new instance of the <see cref="StringReference"/> class.
        /// </summary>
        public StringReference()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StringReference"/> class with the specified constant value.
        /// </summary>
        /// <param name="value">A sequence of UTF-16 code units.</param>
        public StringReference(string value)
        {
            useConstant = true;
            constantValue = value;
        }

        /// <summary>
        /// Gets the current value.
        /// </summary>
        public string Value
        {
            get { return useConstant ? constantValue : variable.Value; }
        }

        /// <summary>
        /// Converts a <see cref="StringReference"/> to an equivalent sequence of UTF-16 code units.
        /// </summary>
        /// <param name="reference">The <see cref="StringReference"/> to convert.</param>
        /// <returns>A sequence of UTF-16 code units equivalent of <paramref name="reference"/>.</returns>
        public static implicit operator string(StringReference reference)
        {
            return reference.Value;
        }

        /// <summary>
        /// Creates a new <see cref="StringReference"/> object initialized to a specified value.
        /// </summary>
        /// <param name="value">A sequence of UTF-16 code units.</param>
        /// <returns>An <see cref="StringReference"/> object whose <see cref="StringReference.Value"/> property is initialized with <paramref name="value"/>.</returns>
        public static implicit operator StringReference(string value)
        {
            return new StringReference(value);
        }
    }
}

#if UNITY_EDITOR

namespace Viacom.XR.Variables.Internal
{
    using UnityEngine;
#if UNITY_EDITOR
    using UnityEditor;

#endif

    /// <summary>
    /// Represents a custom drawer for the <see cref="StringReference"/>.
    /// </summary>
    [CustomPropertyDrawer(typeof(StringReference))]
    public class StringReferenceDrawer : PropertyDrawer
    {
        private GUIStyle _popupStyle;

        private readonly string[] _popupOptions = {"Use Constant", "Use Variable"};

        private const string GuiStyle = "PaneOptions";
        private const string UseConstantField = "useConstant";
        private const string ConstantValueField = "constantValue";
        private const string VariableField = "variable";

        /// <inheritdoc cref="PropertyDrawer.OnGUI"/>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (_popupStyle == null)
            {
                _popupStyle = new GUIStyle(GUI.skin.GetStyle(GuiStyle));
                _popupStyle.imagePosition = ImagePosition.ImageOnly;
            }

            label = EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, label);

            EditorGUI.BeginChangeCheck();

            // Get properties
            SerializedProperty useConstant = property.FindPropertyRelative(UseConstantField);
            SerializedProperty constantValue = property.FindPropertyRelative(ConstantValueField);
            SerializedProperty variable = property.FindPropertyRelative(VariableField);

            // Calculate rect for configuration button
            Rect buttonRect = new Rect(position);
            buttonRect.yMin += _popupStyle.margin.top;
            buttonRect.width = _popupStyle.fixedWidth + _popupStyle.margin.right;
            position.xMin = buttonRect.xMax;

            // Store old indent level and set it to 0, the PrefixLabel takes care of it
            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            int result = EditorGUI.Popup(buttonRect, useConstant.boolValue ? 0 : 1, _popupOptions, _popupStyle);

            useConstant.boolValue = result == 0;

            EditorGUI.PropertyField(position, useConstant.boolValue ? constantValue : variable, GUIContent.none);

            if (EditorGUI.EndChangeCheck())
            {
                property.serializedObject.ApplyModifiedProperties();
            }

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }
    }
}

#endif