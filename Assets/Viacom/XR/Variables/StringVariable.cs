﻿// <copyright file="StringVariable.cs" company="Viacom">
//     Copyright (c) Viacom Inc. All rights reserved.
// </copyright>
//
// <summary>
// Represents a sequence of UTF-16 code units that is stored in a <see cref="ScriptableObject"/>.
// </summary>
//
// <remarks/>
//
// <disclaimer/>
//
//-------------------------------------------------------------------

namespace Viacom.XR.Variables
{
    using UnityEngine;

    /// <summary>
    /// Represents a 32-bit signed integer that is stored in a <see cref="ScriptableObject"/>.
    /// </summary>
    [CreateAssetMenu(menuName = "VXR/Variables/String")]
    public class StringVariable : ScriptableObject
    {
#if UNITY_EDITOR
#pragma warning disable 0414
        [Multiline, SerializeField] private string description = string.Empty;
#pragma warning restore 0414
#endif
        [SerializeField] private string value;

        /// <summary>
        /// Gets the underlying value.
        /// </summary>
        public string Value
        {
            get { return value; }
            set { this.value = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StringVariable"/> class with a specified value.
        /// </summary>
        /// <param name="value">A sequence of UTF-16 code units.</param>
        public StringVariable(string value)
        {
            this.value = value;
        }

        /// <summary>
        /// Converts a <see cref="StringVariable"/> to an equivalent sequence of UTF-16 code units.
        /// </summary>
        /// <param name="variable">The <see cref="StringVariable"/> to convert.</param>
        /// <returns>A sequence of UTF-16 code units equivalent of <paramref name="variable"/>.</returns>
        public static implicit operator string(StringVariable variable)
        {
            return variable.Value;
        }

        /// <summary>
        /// Creates a new <see cref="StringVariable"/> object initialized to a specified value.
        /// </summary>
        /// <param name="value">A sequence of UTF-16 code units.</param>
        /// <returns>An <see cref="StringVariable"/> object whose <see cref="StringVariable.Value"/> property is initialized with <paramref name="value"/>.</returns>
        public static implicit operator StringVariable(string value)
        {
            StringVariable variable = CreateInstance<StringVariable>();
            variable.value = value;
            return variable;
        }
    }
}